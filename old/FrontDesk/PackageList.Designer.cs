﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
namespace DigitalFrontDesk
{
    partial class PackageList : Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.package_list = new System.Windows.Forms.ListView();
            this.i = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.id = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.studentID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.packageID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.packageName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.packageType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.building = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.packageDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.new_package = new System.Windows.Forms.PictureBox();
            this.signForPackage_btn = new System.Windows.Forms.PictureBox();
            this.newPkgTip = new System.Windows.Forms.ToolTip(this.components);
            this.signForPkgTip = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.new_package)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.signForPackage_btn)).BeginInit();
            this.SuspendLayout();
            // 
            // package_list
            // 
            this.package_list.AllowColumnReorder = true;
            this.package_list.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.i,
            this.id,
            this.studentID,
            this.packageID,
            this.packageName,
            this.packageType,
            this.building,
            this.packageDate});
            this.package_list.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.package_list.FullRowSelect = true;
            this.package_list.Location = new System.Drawing.Point(16, 60);
            this.package_list.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.package_list.MultiSelect = false;
            this.package_list.Name = "package_list";
            this.package_list.Size = new System.Drawing.Size(1212, 636);
            this.package_list.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.package_list.TabIndex = 0;
            this.package_list.UseCompatibleStateImageBehavior = false;
            this.package_list.View = System.Windows.Forms.View.Details;
            this.package_list.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.package_list_ColumnClick);
            this.package_list.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.package_list_ItemSelectionChanged);
            // 
            // i
            // 
            this.i.Width = 0;
            // 
            // id
            // 
            this.id.Text = "id";
            this.id.Width = 0;
            // 
            // studentID
            // 
            this.studentID.Text = "";
            this.studentID.Width = 0;
            // 
            // packageID
            // 
            this.packageID.Text = "ID";
            // 
            // packageName
            // 
            this.packageName.Text = "Name";
            this.packageName.Width = 261;
            // 
            // packageType
            // 
            this.packageType.Text = "Type";
            this.packageType.Width = 192;
            // 
            // building
            // 
            this.building.Text = "Building";
            this.building.Width = 211;
            // 
            // packageDate
            // 
            this.packageDate.Text = "Date";
            this.packageDate.Width = 260;
            // 
            // new_package
            // 
            this.new_package.Cursor = System.Windows.Forms.Cursors.Hand;
            this.new_package.Image = global::DigitalFrontDesk.Properties.Resources._new;
            this.new_package.InitialImage = global::DigitalFrontDesk.Properties.Resources._new;
            this.new_package.Location = new System.Drawing.Point(16, 14);
            this.new_package.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.new_package.Name = "new_package";
            this.new_package.Size = new System.Drawing.Size(39, 36);
            this.new_package.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.new_package.TabIndex = 2;
            this.new_package.TabStop = false;
            this.newPkgTip.SetToolTip(this.new_package, "Log new packages");
            this.new_package.Click += new System.EventHandler(this.new_package_Click);
            // 
            // signForPackage_btn
            // 
            this.signForPackage_btn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.signForPackage_btn.Enabled = false;
            this.signForPackage_btn.Image = global::DigitalFrontDesk.Properties.Resources.sign_inactive;
            this.signForPackage_btn.InitialImage = global::DigitalFrontDesk.Properties.Resources.sign;
            this.signForPackage_btn.Location = new System.Drawing.Point(84, 14);
            this.signForPackage_btn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.signForPackage_btn.Name = "signForPackage_btn";
            this.signForPackage_btn.Size = new System.Drawing.Size(48, 36);
            this.signForPackage_btn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.signForPackage_btn.TabIndex = 1;
            this.signForPackage_btn.TabStop = false;
            this.signForPkgTip.SetToolTip(this.signForPackage_btn, "Sign a package to a student");
            this.signForPackage_btn.Click += new System.EventHandler(this.signForPackage_btn_Click);
            // 
            // newPkgTip
            // 
            this.newPkgTip.ShowAlways = true;
            // 
            // signForPkgTip
            // 
            this.signForPkgTip.ShowAlways = true;
            // 
            // PackageList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1236, 710);
            this.Controls.Add(this.new_package);
            this.Controls.Add(this.signForPackage_btn);
            this.Controls.Add(this.package_list);
            this.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MinimumSize = new System.Drawing.Size(992, 567);
            this.Name = "PackageList";
            this.ShowIcon = false;
            this.Text = "Package Log";
            this.SizeChanged += new System.EventHandler(this.PackageList_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.new_package)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.signForPackage_btn)).EndInit();
            this.ResumeLayout(false);

        }

       


        #endregion

        private ListView package_list;
        public ColumnHeader packageID;
        private ColumnHeader packageName;
        private ColumnHeader packageDate;
        private PictureBox signForPackage_btn;
        private PictureBox new_package;
        private ColumnHeader id;
        private ColumnHeader studentID;
        private ColumnHeader packageType;
        private ToolTip newPkgTip;
        private ToolTip signForPkgTip;
        private ColumnHeader building;
        private ColumnHeader i;
    }
}