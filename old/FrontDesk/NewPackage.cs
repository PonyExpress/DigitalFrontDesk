﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace DigitalFrontDesk
{
    public partial class NewPackage : Form
    {
        private int package_id;

        private ListViewColumnSorter sorter;
        private ListViewColumnSorter sorter2;

        private Resident pickedResident = null;

        public NewPackage()
        {
            InitializeComponent();

            typeBox.SelectedIndex = 0;

            sorter = new ListViewColumnSorter();
            sorter2 = new ListViewColumnSorter();
            resident_list.ListViewItemSorter = this.sorter;
            filtered_residents.ListViewItemSorter = this.sorter2;
            
            // Get the package id to use
            package_id = API.NextPackageID();
            packageDay_ID.Text = package_id.ToString();

            // Clear selected resident labels
            selectedRes_Name.Text = "";
            selectedRes_Addr.Text = "";

            // Fill in list of residents
            FillResidents();
        }

        // Fill the list view with residents
        private void FillResidents()
        {
            int id;
            resident_list.Items.Clear();
            // Fill in the box of residents
            for (int i = 0; i < API.Resident_IDs.Count(); i++)
            {
                id = API.Resident_IDs[i];
                ListViewItem item = new ListViewItem(new string[] { API.Residents[id].Id.ToString(), API.Residents[id].LastName, API.Residents[id].FirstName, API.Residents[id].Building.ShortName, API.Residents[id].RoomNumber.ToString() });
                resident_list.Items.Add(item);

            }
            // Sort the columns
            resident_list.Sort();
        }

        // Filter Residents according to filter string
        private void FilterResidents(String filter)
        {
            // Show the waiting cursor incase filtering takes a few seconds
            Cursor.Current = Cursors.WaitCursor;

            if (!String.IsNullOrWhiteSpace(filter) && !String.IsNullOrEmpty(filter))
            {
                // Clear the previously filtered results
                filtered_residents.Items.Clear();

                int id;
                for (int i = 0; i < API.Resident_IDs.Count(); i++)
                {
                    id = API.Resident_IDs[i];

                    if (API.Residents[id].LastName.ToLower().StartsWith(filter) || API.Residents[id].FirstName.ToLower().StartsWith(filter) || API.Residents[id].RoomNumber.ToString().StartsWith(filter))
                    {
                        filtered_residents.Items.Add(new ListViewItem(new string[] { API.Residents[id].Id.ToString(), API.Residents[id].LastName, API.Residents[id].FirstName, API.Residents[id].Building.ShortName, API.Residents[id].RoomNumber.ToString() }));
                    }
                  
                }
                resident_list.Sort();
            }

            // Reset Cursor
            Cursor.Current = Cursors.Default;

            return;
        }

        // Close the form without saving the new package
        private void cancel_btn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        // Submit the form
        private void submit_btn_Click(object sender, EventArgs e)
        { 
            if(pickedResident != null)
            {
                Package newPackage;
                
                newPackage = new Package(package_id, pickedResident.Id, (typeBox.SelectedIndex)+1, API.TimeStamp);

                // Add the package to the database
                API.AddPackage(newPackage);

                if(nextPackage.Checked != true)
                {
                    // No more packages
                    this.Close();
                }
                else
                {
                    // Enter more packages
                    searchBox.Text = "";
                    selectedRes_Name.Text = "";
                    selectedRes_Addr.Text = "";
                    pickedResident = null;
                    filtered_residents.Visible = false;
                    submit_btn.Enabled = false;
                    searchBox.Focus();

                    // Change to the next valid package ID
                    package_id = API.NextPackageID();
                    packageDay_ID.Text = package_id.ToString();
                }

            }

            return;
        }



        // Function for when a row has been highlighed
        private void ChangeResidents(ref ListView obj)
        {
            if (obj.SelectedItems.Count == 0) { return; } // Do nothing if no rows are selected

            // Enable the submit button
            submit_btn.Enabled = true;

            // Get the selected index
            int index = obj.SelectedItems[0].Index;

            // Generate the new resident 
            pickedResident = new Resident(int.Parse(obj.Items[index].SubItems[0].Text));

            // Change the labels to show new selected resident info
            selectedRes_Name.Text = pickedResident.Name;
            selectedRes_Addr.Text = pickedResident.RoomNumber + " " + pickedResident.Building.ShortName;

            return;
        }
        private void resident_list_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Pick a resident from the resident list
            ChangeResidents(ref resident_list);
        }
        private void filtered_residents_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Pick a resident from the filtered list
            ChangeResidents(ref filtered_residents);
        }
        
        

       

        // Perform LastName search
        private void searchBox_TextChanged(object sender, EventArgs e)
        {
            // Exit the function if the last character typed was a space
            if (searchBox.Text != "" && searchBox.Text.Last() == ' ') { return; }

            String search = searchBox.Text.ToString().ToLower() ;
            
            // Remove spaces from the filter
            if (search != "")
            {
                // Filter the results
                FilterResidents(search);
                
                // Make results visible
                filtered_residents.Visible = true;
                filtered_residents.BringToFront();
            }
            else
            {
                // Disable the filtered residents view
                filtered_residents.BringToFront();
                filtered_residents.Visible = false;
            }
        }




        // Sorting when a column header is clicked
        private void resident_list_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            // Change sort order
            sorter.ChangeSort(e.Column);

            // Perform the sort with these new sort options.
            this.resident_list.Sort();
        }
        private void filtered_residents_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            // Change sort
            sorter2.ChangeSort(e.Column);

            // Perform the sort with these new sort options.
            this.filtered_residents.Sort();
        }
    }
}
