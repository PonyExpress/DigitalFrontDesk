﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace DigitalFrontDesk
{
    // Class that allows the program to access the Front Desk API Bitch
    public static class API
    {
        public const int BUILDING = 0; // Building number this program is fors
        private const int MAX_PACKAGE_ID = 550;

        private static MySqlConnection _connection;

        // Database connection information
        private static string _connectionString = DigitalFrontDesk.Properties.Settings.Default.connection;
       

        // Dictionary of residents, reduces number of times needed to query the database
        // API.Residents[id] will return a Resident object
        public static Dictionary<int, Resident> Residents;
        public static List<int> Resident_IDs;

        // Dictionary of Buildings, reduces number of times needed to query the database
        // API.Buildings[id] will return a Building object
        public static Dictionary<int, Building> Buildings;

      

        // Public function to connect to the API
        public static void Connect()
        {
            _connection = FormConnection();

            Resident_IDs = new List<int>();
            
            // Load the Preloaded Dictionaries
            Buildings = LoadBuildings();
            Residents = LoadResidents();
        }

        private static MySqlConnection FormConnection()
        {
            return new MySqlConnection(_connectionString);
        }

        // Function to open the connection
        private static bool OpenConnection(MySqlConnection theConnection = null)
        {
            if (theConnection == null){theConnection = _connection;}

            try
            {
                theConnection.Open();
                return true;
            }
            catch (MySqlException ex)
            {
                //When handling errors, you can your application's response based 
                //on the error number.
                //The two most common error numbers when connecting are as follows:
                //0: Cannot connect to server.
                //1045: Invalid user name and/or password.
                switch (ex.Number)
                {
                    case 0:
                        MessageBox.Show("Cannot connect to server.  Contact administrator");
                        break;

                    case 1045:
                        MessageBox.Show("Invalid username/password, please try again");
                        break;
                }
                return false;
            }
        }

        // Function to close the connection 
        private static bool CloseConnection(MySqlConnection theConnection = null)
        {
            if (theConnection == null)
            {
                theConnection = _connection;
            }

            try
            {
                theConnection.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }



        // Load residents into Dictionary for quick accessing later 
        public static Dictionary<int, Resident> LoadResidents()
        {
            Dictionary<int, Resident> list = null;

            if (OpenConnection() == true)
            {
                list = new Dictionary<int, Resident>();
                Resident_IDs.Clear();
                // Perform query that will retrieve students
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM `residents` WHERE `building`='0'", _connection);
                MySqlDataReader reader = cmd.ExecuteReader() ;

                int id;
                while (reader.Read())
                {
                    id = int.Parse(reader["id"].ToString());
                    list.Add(id, new Resident(reader));
                    Resident_IDs.Add(id);
                }

                reader.Close();

                CloseConnection();
            }

            if (list == null) { throw new Exception("Connection to Database failed! :( \nUnable to get list of residents"); }

            return list;
        }

        // Load buildings
        private static Dictionary<int, Building> LoadBuildings()
        {
            Dictionary<int, Building> list = null;

            if (OpenConnection() == true)
            {
                list = new Dictionary<int, Building>();

                // Perform query
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM `buildings`", _connection);
                MySqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    list.Add(int.Parse(reader["id"].ToString()), new Building(reader));
                }

                reader.Close();

                CloseConnection();
            }

            if (list == null) { throw new Exception("Connection to Database failed! :(\nUnable to get list of Buildings."); }

            return list;
        }

        // Returns a List of Packages 
        public static List<Package> GetPackages()
        {
            List<Package> packages = null;

            if (OpenConnection() == true)
            {
                if (packages != null) { packages.Clear(); }
                packages = new List<Package>();

                MySqlCommand cmd = new MySqlCommand("SELECT * FROM `packages` WHERE `picked_up`='0' AND `time_out`='0000-00-00 00:00:00'", _connection);
                MySqlDataReader reader = cmd.ExecuteReader();

                while(reader.Read())
                {
                    packages.Add(new Package(reader));
                }

                reader.Close();

                CloseConnection();
            }

            if (packages == null) { throw new Exception("Connection to database failed! :(\nUnable to get list of packages"); }

            return packages;
        }
        
        // Log a package into the system 
        public static void AddPackage(Package newPackage)
        {
            MySqlConnection conn = FormConnection();

            if (OpenConnection(conn))
            {
                MySqlCommand cmd = new MySqlCommand("INSERT INTO `packages` (`resident`,`package_id`,`type`,`time_in`,`time_out`) VALUES" +
                                                    "('" + newPackage.ResidentId + "', '" + newPackage.Id + "', '" + newPackage.TypeId + "', '" +
                                                    newPackage.TimeIn + "', '" + newPackage.TimeOut + "')", conn);
                cmd.ExecuteNonQuery();

                CloseConnection(conn);


            }
            else {
                throw new Exception("Connection to database failed! :(\nUnable to add new package");
            }
            return;
        }

        // Signs for a package
        public static void SignForPackage(int uID)
        {
            MySqlConnection conn = FormConnection();

            if (OpenConnection(conn) == true)
            {
                MySqlCommand cmd = new MySqlCommand("UPDATE `packages` SET `picked_up`='1', `time_out`='" + TimeStamp + "' WHERE `uID`='" +
                                                    uID + "'", conn);
                cmd.ExecuteNonQuery();

                CloseConnection(conn);
            }
            else
            {
                throw new Exception("Connection to database Failed! :(\nUnable to update package as signed for...");
            }

            return;
        }

        // Gets the next uID of a package
        public static int NextPackageID()
        {
            int nextID = -1;
            MySqlConnection newConnection = FormConnection();

            if (OpenConnection(newConnection) == true)
            {
                MySqlCommand cmd = new MySqlCommand("SELECT MAX(`uID`) FROM `packages`", newConnection);
                MySqlDataReader reader = cmd.ExecuteReader();

                reader.Read();
                nextID = int.Parse(reader[0].ToString());
                
                reader.Close();

                CloseConnection(newConnection);
            }
            
            if (nextID == -1) {throw new Exception("Connection to Database failed! :(\nUnable to get next avaialable package id.");}

            return (nextID % MAX_PACKAGE_ID) + 1;
        }

        // Return formatted Date
        public static string TimeStamp
        {
            get
            {
                return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            }
        }
    }
}
