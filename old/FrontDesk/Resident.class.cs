﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace DigitalFrontDesk
{
    public class Resident
    {
        private int _Id; // Resident ID
        private string _FirstName; // Resident's first name 
        private string _LastName; // Resident's last name 
        private int _BuildingNumber; // Building Number
        public Building _BuildingObj; // Building object 
        private int _RoomNumber; // Resident's room number 
        private string _Email; // Email for the resident


        // Constructor 
        public Resident(MySqlDataReader reader)
        {
            this._Id = int.Parse(reader["id"].ToString());
            this._FirstName = reader["first_name"].ToString();
            this._LastName = reader["last_name"].ToString();
            this._BuildingNumber = int.Parse(reader["building"].ToString());
            this._RoomNumber = int.Parse(reader["room_number"].ToString());
            this._Email = reader["email"].ToString();

            this._BuildingObj = API.Buildings[this._BuildingNumber];
        }
        // Constructor using just a resident ID
        public Resident(int id)
        {
            this._Id = API.Residents[id].Id;
            this._FirstName = API.Residents[id].FirstName;
            this._LastName = API.Residents[id].LastName;
            this._BuildingNumber = API.Residents[id].BuildingNumber;
            this._RoomNumber = API.Residents[id].RoomNumber;
            this._Email = API.Residents[id].Email;

            this._BuildingObj = API.Residents[id].Building;
        }


        // Get the ID
        public int Id
        {
            get
            {
                return this._Id;
            }
        }


        // Get first name 
        public string FirstName
        {
            get
            {
                return this._FirstName;
            }
        }

        // Get last name
        public string LastName
        {
            get
            {
                return this._LastName;
            }
        }

        // Get full name
        public string Name
        {
            get
            {
                return this._LastName + ", " + this._FirstName;
            }
        }

        // Get Building number
        public int BuildingNumber
        {
            get
            {
                return this._BuildingNumber;
            }
        }
        
        // Get Room Number
        public int RoomNumber
        {
            get
            {
                return this._RoomNumber;
            }
        }

        // Get Email
        public string Email
        {
            get
            {
                return this._Email;
            }
        }

        // Get the Building Object
        public Building Building
        {
            get
            {
                return this._BuildingObj;
            }
        }

    }
}
