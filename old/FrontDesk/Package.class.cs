﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace DigitalFrontDesk
{
    public class Package
    {
        private int _UId;               // Unique ID for each packages
        private int _Id;                // Package ID (resets at 250)
        private int _ResidentId;        // ID of the resident the package belongs to 
        private Resident _ResidentObj;  // Resident object
        private int _Type;              // Type of package (1, 2, 3)
        private string _TimeIn;            // Time package was logged at the front desk 
        private string _TimeOut;           // Time the package was picked up by the resident
        private int _PickedUp;          // 0 if package has not been picked up yet, 1 if it has

        // Package Constructor
        public Package(MySqlDataReader reader)// int resident_id)
        {
            this._UId = int.Parse(reader["uID"].ToString());
            this._Id = int.Parse(reader["package_id"].ToString());
            this._ResidentId = int.Parse(reader["resident"].ToString());
            this._ResidentObj = API.Residents[this._ResidentId];// Creates resident object
            this._Type = int.Parse(reader["type"].ToString());
            this._TimeIn = reader["time_in"].ToString();
            this._TimeOut = reader["time_out"].ToString();
            this._PickedUp = 0;

            if (this._TimeOut == "") { this._TimeOut = "0000-00-00 00:00:00"; }
        }

        // Second Package Constructor
        public Package(int id, int resident, int type, string time_in, string time_out = "0000-00-00 00:00:00", int picked_up = 0)
        {
            this._UId = -1;
            this._Id = id;
            this._ResidentId = resident;
            this._ResidentObj = API.Residents[this._ResidentId]; // Creates resident object
            this._Type = type;
            this._TimeIn = time_in;
            this._TimeOut = time_out;
            
            this._PickedUp = picked_up;
        }


        // Get the Package Unique ID
        public int UId
        {
            get
            {
            return this._UId;
            }
        }

        // Get the Package ID
        public int Id
        {
            get
            {
                return this._Id;
            }
        }

        // Get the Package Resident ID 
        public int ResidentId
        {
            get
            {
                return this._ResidentId;
            }
        }

        // Returns the package resident object
        public Resident Resident
        {
            get
            {
                return this._ResidentObj;
            }
        }

        // Get the package type 
        public string Type
        {
            get
            {
                return (this._Type == 1) ? "Box" : (this._Type == 2) ? "Envelope" : "Other";
            }
        }

        // Get the package type ID
        public int TypeId
        {
            get
            {
                return this._Type;
            }
        }
       
        // Get the Package Checked in time
        public  string TimeIn
        {
            get
            {
                return this._TimeIn;
            }
        }

        // Get the Package Checked Out time
        public string TimeOut
        {
            get
            {
                return this._TimeOut;
            }
        }
        
        // Get if the package has been picked up yet 
        public bool PickedUp
        {
            get
            {
                return (this._PickedUp == 1) ? true : false;
            }
        }

        
    }
}
