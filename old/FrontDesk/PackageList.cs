﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DigitalFrontDesk
{
    public partial class PackageList : Form
    {

        // List of packages
        private List<Package> packages = null;

        private int selectedPackage;

        private ListViewColumnSorter sorter;

        // Constructor
        public PackageList()
        {            
            InitializeComponent();

            sorter = new ListViewColumnSorter();
            package_list.ListViewItemSorter = this.sorter;

            populate();
        }
         
        // Populate the list with open packages
        public void populate()
        {
            package_list.Items.Clear();

            // Add to view
            packages = API.GetPackages();
            
            for (int i = 0; i < packages.Count(); i++)
            {
                // Create the ListView Item
                ListViewItem item = new ListViewItem(new string[] {i.ToString(), packages[i].UId.ToString(), packages[i].ResidentId.ToString(), packages[i].Id.ToString(), packages[i].Resident.Name, packages[i].Type, packages[i].Resident.Building.ShortName, packages[i].TimeIn });

                // Add to package list
                package_list.Items.Add(item);
            }

        }


        // New Package Dialog Box
        private void new_package_Click(object sender, EventArgs e)
        {
            
            NewPackage new_pkg = new NewPackage();
            new_pkg.ShowDialog();
            this.populate();
        }

        private void signForPackage_btn_Click(object sender, EventArgs e)
        {
            Package pkg = packages[selectedPackage];
            DialogResult question = MessageBox.Show("Waiting for resident to sign...\nPackage ID: " + pkg.Id + "\nResident: " + pkg.Resident.Name, "Waiting", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (question == DialogResult.No) { return; }

            API.SignForPackage(pkg.UId);
            this.populate();
        }

        // What happens when you select a row
        private void package_list_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            signForPackage_btn.Enabled = true;
            signForPackage_btn.Image = global::DigitalFrontDesk.Properties.Resources.sign; // Change to active sign icon
            
            selectedPackage = int.Parse(e.Item.SubItems[0].Text);
           
  
        }

        // Sort the column
        private void package_list_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            this.sorter.ChangeSort(e.Column);

            this.package_list.Sort();
        }

        // Resize the list view
        private void PackageList_SizeChanged(object sender, EventArgs e)
        {
            package_list.Width = this.Size.Width - 42;
            package_list.Height = this.Size.Height - 100;

            // Resize the columns in the listview
            package_list.Columns[4].Width = (int)((double)package_list.Size.Width * 0.215346);
            package_list.Columns[5].Width = (int)((double)package_list.Size.Width * 0.15841);
            package_list.Columns[6].Width = (int)((double)package_list.Size.Width * 0.174092);
            package_list.Columns[7].Width = (int)((double)package_list.Size.Width * 0.2145);

        }
    }
}
