﻿namespace DigitalFrontDesk
{
    partial class NewPackage
    {

     
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
       
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.searchLbl = new System.Windows.Forms.Label();
            this.searchBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cancel_btn = new System.Windows.Forms.Button();
            this.submit_btn = new System.Windows.Forms.Button();
            this.nextPackage = new System.Windows.Forms.CheckBox();
            this.resident_list = new System.Windows.Forms.ListView();
            this.id = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.last_name = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.first_name = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.building = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.room = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.filtered_residents = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.typeBox = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.packageDay_ID = new System.Windows.Forms.Label();
            this.selectedRes_Name = new System.Windows.Forms.Label();
            this.selectedRes_Addr = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // searchLbl
            // 
            this.searchLbl.AutoSize = true;
            this.searchLbl.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchLbl.Location = new System.Drawing.Point(355, 40);
            this.searchLbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.searchLbl.Name = "searchLbl";
            this.searchLbl.Size = new System.Drawing.Size(70, 28);
            this.searchLbl.TabIndex = 99;
            this.searchLbl.Text = "Search";
            // 
            // searchBox
            // 
            this.searchBox.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchBox.Location = new System.Drawing.Point(433, 40);
            this.searchBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.searchBox.Name = "searchBox";
            this.searchBox.Size = new System.Drawing.Size(419, 34);
            this.searchBox.TabIndex = 0;
            this.searchBox.TextChanged += new System.EventHandler(this.searchBox_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(17, 40);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 28);
            this.label2.TabIndex = 99;
            this.label2.Text = "Package ID";
            // 
            // cancel_btn
            // 
            this.cancel_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancel_btn.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancel_btn.Location = new System.Drawing.Point(22, 572);
            this.cancel_btn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cancel_btn.Name = "cancel_btn";
            this.cancel_btn.Size = new System.Drawing.Size(166, 67);
            this.cancel_btn.TabIndex = 5;
            this.cancel_btn.Text = "Cancel";
            this.cancel_btn.UseVisualStyleBackColor = true;
            this.cancel_btn.Click += new System.EventHandler(this.cancel_btn_Click);
            // 
            // submit_btn
            // 
            this.submit_btn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.submit_btn.Enabled = false;
            this.submit_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.submit_btn.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.submit_btn.Location = new System.Drawing.Point(692, 571);
            this.submit_btn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.submit_btn.Name = "submit_btn";
            this.submit_btn.Size = new System.Drawing.Size(166, 67);
            this.submit_btn.TabIndex = 4;
            this.submit_btn.Text = "Submit";
            this.submit_btn.UseVisualStyleBackColor = true;
            this.submit_btn.Click += new System.EventHandler(this.submit_btn_Click);
            // 
            // nextPackage
            // 
            this.nextPackage.AutoSize = true;
            this.nextPackage.Checked = true;
            this.nextPackage.CheckState = System.Windows.Forms.CheckState.Checked;
            this.nextPackage.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nextPackage.Location = new System.Drawing.Point(131, 451);
            this.nextPackage.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.nextPackage.Name = "nextPackage";
            this.nextPackage.Size = new System.Drawing.Size(181, 32);
            this.nextPackage.TabIndex = 3;
            this.nextPackage.Text = "Another Package";
            this.nextPackage.UseVisualStyleBackColor = true;
            // 
            // resident_list
            // 
            this.resident_list.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.resident_list.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.id,
            this.last_name,
            this.first_name,
            this.building,
            this.room});
            this.resident_list.FullRowSelect = true;
            this.resident_list.Location = new System.Drawing.Point(131, 89);
            this.resident_list.MultiSelect = false;
            this.resident_list.Name = "resident_list";
            this.resident_list.Size = new System.Drawing.Size(728, 312);
            this.resident_list.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.resident_list.TabIndex = 100;
            this.resident_list.UseCompatibleStateImageBehavior = false;
            this.resident_list.View = System.Windows.Forms.View.Details;
            this.resident_list.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.resident_list_ColumnClick);
            this.resident_list.SelectedIndexChanged += new System.EventHandler(this.resident_list_SelectedIndexChanged);
            // 
            // id
            // 
            this.id.Width = 0;
            // 
            // last_name
            // 
            this.last_name.Text = "Last Name";
            this.last_name.Width = 233;
            // 
            // first_name
            // 
            this.first_name.Text = "First Name";
            this.first_name.Width = 240;
            // 
            // building
            // 
            this.building.Text = "Building";
            this.building.Width = 121;
            // 
            // room
            // 
            this.room.Text = "Room";
            this.room.Width = 101;
            // 
            // filtered_residents
            // 
            this.filtered_residents.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.filtered_residents.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
            this.filtered_residents.FullRowSelect = true;
            this.filtered_residents.Location = new System.Drawing.Point(131, 89);
            this.filtered_residents.MultiSelect = false;
            this.filtered_residents.Name = "filtered_residents";
            this.filtered_residents.Size = new System.Drawing.Size(728, 312);
            this.filtered_residents.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.filtered_residents.TabIndex = 104;
            this.filtered_residents.UseCompatibleStateImageBehavior = false;
            this.filtered_residents.View = System.Windows.Forms.View.Details;
            this.filtered_residents.Visible = false;
            this.filtered_residents.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.filtered_residents_ColumnClick);
            this.filtered_residents.SelectedIndexChanged += new System.EventHandler(this.filtered_residents_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Width = 0;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Last Name";
            this.columnHeader2.Width = 233;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "First Name";
            this.columnHeader3.Width = 240;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Building";
            this.columnHeader4.Width = 121;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Room";
            this.columnHeader5.Width = 101;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(429, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(348, 20);
            this.label3.TabIndex = 105;
            this.label3.Text = "Search will match first/last name and room number\r\n";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(161, 479);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(226, 60);
            this.label4.TabIndex = 106;
            this.label4.Text = "If checked another window for a \r\nnew package will open when \r\nyou submit\r\n";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(30, 89);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 28);
            this.label5.TabIndex = 107;
            this.label5.Text = "Residents";
            // 
            // typeBox
            // 
            this.typeBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.typeBox.FormattingEnabled = true;
            this.typeBox.Items.AddRange(new object[] {
            "Box",
            "Envelope",
            "Other"});
            this.typeBox.Location = new System.Drawing.Point(131, 407);
            this.typeBox.Name = "typeBox";
            this.typeBox.Size = new System.Drawing.Size(181, 36);
            this.typeBox.TabIndex = 108;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(71, 410);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 28);
            this.label6.TabIndex = 109;
            this.label6.Text = "Type";
            // 
            // packageDay_ID
            // 
            this.packageDay_ID.AutoSize = true;
            this.packageDay_ID.Font = new System.Drawing.Font("Segoe UI", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.packageDay_ID.ForeColor = System.Drawing.Color.Red;
            this.packageDay_ID.Location = new System.Drawing.Point(117, 5);
            this.packageDay_ID.Name = "packageDay_ID";
            this.packageDay_ID.Size = new System.Drawing.Size(94, 81);
            this.packageDay_ID.TabIndex = 110;
            this.packageDay_ID.Text = "-1";
            // 
            // selectedRes_Name
            // 
            this.selectedRes_Name.CausesValidation = false;
            this.selectedRes_Name.Location = new System.Drawing.Point(25, 30);
            this.selectedRes_Name.Name = "selectedRes_Name";
            this.selectedRes_Name.Size = new System.Drawing.Size(394, 33);
            this.selectedRes_Name.TabIndex = 111;
            this.selectedRes_Name.Text = "[Name]";
            // 
            // selectedRes_Addr
            // 
            this.selectedRes_Addr.CausesValidation = false;
            this.selectedRes_Addr.Location = new System.Drawing.Point(25, 65);
            this.selectedRes_Addr.Name = "selectedRes_Addr";
            this.selectedRes_Addr.Size = new System.Drawing.Size(394, 33);
            this.selectedRes_Addr.TabIndex = 112;
            this.selectedRes_Addr.Text = "[Address]";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.selectedRes_Name);
            this.groupBox1.Controls.Add(this.selectedRes_Addr);
            this.groupBox1.Location = new System.Drawing.Point(433, 407);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(425, 156);
            this.groupBox1.TabIndex = 113;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Selected Resident";
            // 
            // NewPackage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(871, 653);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.packageDay_ID);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.typeBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.resident_list);
            this.Controls.Add(this.nextPackage);
            this.Controls.Add(this.submit_btn);
            this.Controls.Add(this.cancel_btn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.searchBox);
            this.Controls.Add(this.searchLbl);
            this.Controls.Add(this.filtered_residents);
            this.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "NewPackage";
            this.Text = "New Package";
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label searchLbl;
        private System.Windows.Forms.TextBox searchBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button cancel_btn;
        private System.Windows.Forms.Button submit_btn;
        private System.Windows.Forms.CheckBox nextPackage;
        private System.Windows.Forms.ListView resident_list;
        private System.Windows.Forms.ColumnHeader last_name;
        private System.Windows.Forms.ColumnHeader first_name;
        private System.Windows.Forms.ColumnHeader room;
        private System.Windows.Forms.ColumnHeader id;
        private System.Windows.Forms.ColumnHeader building;
        private System.Windows.Forms.ListView filtered_residents;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox typeBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label packageDay_ID;
        private System.Windows.Forms.Label selectedRes_Name;
        private System.Windows.Forms.Label selectedRes_Addr;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}