﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace DigitalFrontDesk
{
    public class Building
    {
        private int _Id;
        private string _Name;
        private string _Address;

        // Constructor
        public Building(MySqlDataReader reader)
        {
            this._Id = int.Parse(reader["id"].ToString());
            this._Name = reader["name"].ToString();
            this._Address = reader["address"].ToString();
        }

        // Get id 
        public int Id
        {
            get
            {
                return this._Id;
            }
        }

        // Get Building Name
        public string Name
        {
            get
            {
                return this._Name;
            }
        }

        // Get Buildings Abbreviated Name
        public string ShortName
        {
            get
            {
                return (this._Id == 0) ? "RC 1" : (this._Id == 1) ? "RC 2" : "TJ";
            }
        }

        // Get Building Address
        public string Address
        {
            get
            {
                return this._Address;
            }
        }
    }
}
