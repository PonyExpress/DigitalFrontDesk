﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace DigitalFrontDesk
{
    public partial class MainGUI : Form
    {

        public MainGUI()
        {
            //this.FormBorderStyle = FormBorderStyle.None;
            //this.WindowState = FormWindowState.Maximized;
            InitializeComponent();
        }

        // Packages
        private void packages_btn_Click(object sender, EventArgs e)
        {
           // Open new package GUI
            PackageList package_gui = new PackageList();
            package_gui.Show();
        }
        private void packages_lbl_Click(object sender, EventArgs e)
        {
            packages_btn_Click(sender, e);
        }


        // Keys
        private void key_log_btn_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Key Log");
        }

        private void key_lbl_Click(object sender, EventArgs e)
        {
            key_log_btn_Click(sender, e);
        }

        // Test log
        private void test_btn_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Test log");
        }

        private void test_lbl_Click(object sender, EventArgs e)
        {
            test_btn_Click(sender, e);
        }

    }
}
