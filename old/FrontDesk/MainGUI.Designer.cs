﻿namespace DigitalFrontDesk
{
    partial class MainGUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainGUI));
            this.packages_lbl = new System.Windows.Forms.Label();
            this.key_lbl = new System.Windows.Forms.Label();
            this.test_lbl = new System.Windows.Forms.Label();
            this.test_btn = new System.Windows.Forms.PictureBox();
            this.key_log_btn = new System.Windows.Forms.PictureBox();
            this.packages_btn = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.test_btn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.key_log_btn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.packages_btn)).BeginInit();
            this.SuspendLayout();
            // 
            // packages_lbl
            // 
            this.packages_lbl.Cursor = System.Windows.Forms.Cursors.Hand;
            this.packages_lbl.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.packages_lbl.Location = new System.Drawing.Point(19, 216);
            this.packages_lbl.Name = "packages_lbl";
            this.packages_lbl.Size = new System.Drawing.Size(190, 38);
            this.packages_lbl.TabIndex = 3;
            this.packages_lbl.Text = "Package Log";
            this.packages_lbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.packages_lbl.Click += new System.EventHandler(this.packages_lbl_Click);
            // 
            // key_lbl
            // 
            this.key_lbl.Cursor = System.Windows.Forms.Cursors.Hand;
            this.key_lbl.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.key_lbl.Location = new System.Drawing.Point(329, 216);
            this.key_lbl.Name = "key_lbl";
            this.key_lbl.Size = new System.Drawing.Size(190, 38);
            this.key_lbl.TabIndex = 4;
            this.key_lbl.Text = "Dolly Log";
            this.key_lbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.key_lbl.Click += new System.EventHandler(this.key_lbl_Click);
            // 
            // test_lbl
            // 
            this.test_lbl.Cursor = System.Windows.Forms.Cursors.Hand;
            this.test_lbl.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.test_lbl.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.test_lbl.Location = new System.Drawing.Point(642, 216);
            this.test_lbl.Name = "test_lbl";
            this.test_lbl.Size = new System.Drawing.Size(190, 38);
            this.test_lbl.TabIndex = 5;
            this.test_lbl.Text = "Test Log";
            this.test_lbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.test_lbl.Click += new System.EventHandler(this.test_lbl_Click);
            // 
            // test_btn
            // 
            this.test_btn.AccessibleName = "Packages";
            this.test_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.test_btn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.test_btn.Image = global::DigitalFrontDesk.Properties.Resources.test;
            this.test_btn.InitialImage = ((System.Drawing.Image)(resources.GetObject("test_btn.InitialImage")));
            this.test_btn.Location = new System.Drawing.Point(642, 19);
            this.test_btn.Margin = new System.Windows.Forms.Padding(10);
            this.test_btn.Name = "test_btn";
            this.test_btn.Padding = new System.Windows.Forms.Padding(10);
            this.test_btn.Size = new System.Drawing.Size(190, 197);
            this.test_btn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.test_btn.TabIndex = 2;
            this.test_btn.TabStop = false;
            this.test_btn.Tag = "";
            this.test_btn.Click += new System.EventHandler(this.test_btn_Click);
            // 
            // key_log_btn
            // 
            this.key_log_btn.AccessibleName = "Packages";
            this.key_log_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.key_log_btn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.key_log_btn.Image = global::DigitalFrontDesk.Properties.Resources.dollies;
            this.key_log_btn.InitialImage = ((System.Drawing.Image)(resources.GetObject("key_log_btn.InitialImage")));
            this.key_log_btn.Location = new System.Drawing.Point(329, 19);
            this.key_log_btn.Margin = new System.Windows.Forms.Padding(10);
            this.key_log_btn.Name = "key_log_btn";
            this.key_log_btn.Padding = new System.Windows.Forms.Padding(10);
            this.key_log_btn.Size = new System.Drawing.Size(190, 197);
            this.key_log_btn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.key_log_btn.TabIndex = 1;
            this.key_log_btn.TabStop = false;
            this.key_log_btn.Tag = "";
            this.key_log_btn.Click += new System.EventHandler(this.key_log_btn_Click);
            // 
            // packages_btn
            // 
            this.packages_btn.AccessibleName = "Packages";
            this.packages_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.packages_btn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.packages_btn.Image = global::DigitalFrontDesk.Properties.Resources.package2;
            this.packages_btn.InitialImage = ((System.Drawing.Image)(resources.GetObject("packages_btn.InitialImage")));
            this.packages_btn.Location = new System.Drawing.Point(19, 19);
            this.packages_btn.Margin = new System.Windows.Forms.Padding(10);
            this.packages_btn.Name = "packages_btn";
            this.packages_btn.Padding = new System.Windows.Forms.Padding(10);
            this.packages_btn.Size = new System.Drawing.Size(190, 197);
            this.packages_btn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.packages_btn.TabIndex = 0;
            this.packages_btn.TabStop = false;
            this.packages_btn.Click += new System.EventHandler(this.packages_btn_Click);
            // 
            // MainGUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(851, 656);
            this.Controls.Add(this.test_lbl);
            this.Controls.Add(this.key_lbl);
            this.Controls.Add(this.packages_lbl);
            this.Controls.Add(this.test_btn);
            this.Controls.Add(this.key_log_btn);
            this.Controls.Add(this.packages_btn);
            this.Name = "MainGUI";
            this.Text = "Front Desk";
            ((System.ComponentModel.ISupportInitialize)(this.test_btn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.key_log_btn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.packages_btn)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox packages_btn;
        private System.Windows.Forms.PictureBox key_log_btn;
        private System.Windows.Forms.PictureBox test_btn;
        private System.Windows.Forms.Label packages_lbl;
        private System.Windows.Forms.Label key_lbl;
        private System.Windows.Forms.Label test_lbl;
    }
}

