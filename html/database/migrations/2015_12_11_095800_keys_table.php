<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class KeysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('keys', function (Blueprint $table) {
          $table->engine = 'InnoDB';
           $table->increments('id')->unsigned()->index();
           $table->string('label')->index();
           $table->integer('room')->unsigned();
           $table->integer('building_id')->unsigned();
           $table->foreign('building_id')->references('id')->on('buildings');

       });
     }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('keys_table');
    }
}
