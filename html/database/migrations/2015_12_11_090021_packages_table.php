<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('package_log', function(Blueprint $table)
        {
          $table->engine = 'InnoDB';
          $table->increments('id')->unsigned()->index();
          $table->bigInteger('resident_id')->unsigned()->index();
          $table->foreign('resident_id')->references('id')->on('residents');
          $table->integer('package_number')->unsigned();
          $table->integer('type')->unsigned();
          $table->datetime('time_in')->nullable()->default(null)->index();
          $table->datetime('time_out')->nullable()->default(null);
          $table->integer('worker_in')->unsigned()->nullable()->default(null)->index();
          $table->foreign('worker_in')->references('id')->on('desk_workers');
          $table->integer('worker_out')->unsigned()->nullable()->default(null)->index();
          $table->foreign('worker_out')->references('id')->on('desk_workers');
          $table->longText('signature')->nullable()->default(null);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('package_log');
    }
}
