<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::create('email_log', function (Blueprint $table)
         {
            $table->engine = 'InnoDB';
             $table->increments('id')->unsigned()->index();

             $table->bigInteger('resident_id')->unsigned()->index();
             $table->foreign('resident_id')->references('id')->on('residents');

             $table->text('subject');
             $table->longtext('body');
             $table->datetime('time_sent');

             $table->integer('worker_id')->unsinged()->nullable()->default(null)->index();

         });
     }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('email_log');
    }
}
