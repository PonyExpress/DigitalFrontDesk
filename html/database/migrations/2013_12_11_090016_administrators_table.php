<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdministratorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
        Schema::create('administrators', function (Blueprint $table)
        {
          $table->engine = 'InnoDB';
          $table->increments('id')->unsigned()->index();
          $table->integer('building_id')->unsigned()->index();

        });
     }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('administrators');
    }
}
