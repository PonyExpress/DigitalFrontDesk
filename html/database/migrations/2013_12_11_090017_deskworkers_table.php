<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeskworkersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('desk_workers', function(Blueprint $table)
      {
        $table->engine = 'InnoDB';
        $table->increments('id')->unsigned()->index();
        $table->integer('administrator_id')->unsigned()->nullable()->default(null);


      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('desk_workers');
    }
}
