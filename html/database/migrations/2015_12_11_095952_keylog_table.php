<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class KeylogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::create('key_log', function (Blueprint $table)
         {
            $table->engine = 'InnoDB';
             $table->increments('id')->unsigned()->index();

             $table->integer('key_id')->unsigned()->index();
             $table->foreign('key_id')->references('id')->on('keys');

             $table->bigInteger('resident_id')->unsigned()->index();
             $table->foreign('resident_id')->references('id')->on('residents');

             $table->datetime('time_in')->nullable()->default(null)->index();
             $table->datetime('time_out')->nullable()->default(null);
             $table->longText('signature_in')->nullable()->default(null);
             $table->longText('signature_out')->nullable()->default(null);

             $table->integer('worker_in')->unsigned()->nullable()->index();
             $table->foreign('worker_in')->references('id')->on('desk_workers');

             $table->integer('worker_out')->unsigned()->nullable()->index();
             $table->foreign('worker_out')->references('id')->on('desk_workers');
         });
     }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('key_log');
    }
}
