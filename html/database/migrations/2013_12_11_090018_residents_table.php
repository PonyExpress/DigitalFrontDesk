<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ResidentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('residents', function(Blueprint $table)
      {
        $table->engine = 'InnoDB';

        $table->bigInteger('id')->unsigned()->primary()->index();
        $table->text('first_name');
        $table->text('middle_name')->nullable()->default(null);
        $table->text('last_name');
        $table->integer('room_number')->unsigned()->index();
        $table->integer('building_id')->unsigned()->index();
        $table->integer('worker_id')->unsigned()->nullable()->default(null);
        $table->text('email')->nullable()->default(null);

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('residents');
    }
}
