<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BuildingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('buildings', function(Blueprint $table)
      {
        $table->engine = 'InnoDB';
        $table->increments('uID')->unsigned()->index();
        $table->text('group_id');
        $table->text('name');
        $table->text('address');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('buildings');
    }
}
