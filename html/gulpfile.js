var elixir = require('laravel-elixir');
elixir.config.sourcemaps = false;
elixir.config.css.cssnano.pluginOptions.safe =true;
elixir.config.production = true;
require('es6-promise').polyfill();


/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    // App CSS
    mix.styles([
      'css/lato.css',
      'css/app.css',
      'css/table.css'
    ], 'public/css/app.css', 'resources/assets/global/');

    // App JavaScript
    mix.scripts([
      'js/misc.js',
      'js/list.js',
    ], 'public/js/app.js', 'resources/assets/global');

    // Card Scanner
    mix.scripts([
      'js/card_scanner.js'
    ], 'public/js/card_scanner.js', 'resources/assets/global');

    // Shim Shit
    mix.scripts([
      'js/html5shiv.js',
      'js/respond.js'
    ], 'public/js/shim.js', 'resources/assets/global');



    // Desk CSS
    mix.styles([
      'css/date-picker.css',
      'css/digitalfrontdesk.css'
    ], 'public/css/digitalfrontdesk.css', 'resources/assets/desk');

    // Desk Date JavaScript
    mix.scripts([
      'js/date-date.js',,
      'js/date-legacy.js',
      'js/date-picker.js',
      'js/date-time.js'
    ], 'public/js/date.js', 'resources/assets/desk');

    // Desk Signature Javascript
    mix.scripts([
      'js/signature.js',
      'js/signature_control.js'
    ], 'public/js/signature.js', 'resources/assets/desk');

    // Desk JavaScript
    mix.scripts([
      'js/digitalfrontdesk.js'
    ], 'public/js/digitalfrontdesk.js', 'resources/assets/desk');



    // Student CSS
    mix.styles([
      'css/studentportal.css'
    ], 'public/css/studentportal.css', 'resources/assets/student');

    // Student JavaScript
    mix.scripts([
      'js/studentportal.js',
      'js/settings.js'
    ], 'public/js/studentportal.js', 'resources/assets/student');



    // Admin CSS
    mix.styles([
      'css/tiles.css'
    ], 'public/css/adminportal.css', 'resources/assets/admin');




    // Bootstrap CSS
    mix.styles([
      'css/bootstrap-switch.css',
      'css/bootstrap.css'
    ], 'public/css/bootstrap.css', 'resources/assets/global');

    // Bootstrap Javascript
    mix.scripts([
      'js/jquery.js',
      'js/bootstrap.js',
      'js/bootstrap-switch.js',
      'js/bootbox.js'
    ], 'public/js/bootstrap.js', 'resources/assets/global');





    // Searching Javascript
    mix.scripts([
      'js/search.js'
    ], 'public/js/search.js', 'resources/assets/global');

    // Ajax Javascript
    mix.scripts([
      'js/load_residents.js',
      'js/search.js'
    ], 'public/js/load_residents.js', 'resources/assets/global');

    // Residents click
    mix.scripts([
      'js/load_residents_click.js',
      'js/search.js'
    ], 'public/js/load_residents_click.js', 'resources/assets/global');


});


var gulp = require('gulp');
var nano = require('gulp-cssnano');


gulp.task('watch', function(){
  gulp.watch('resources/assets/*/js/*.js', ['default']);
  gulp.watch('resources/assets/*/css/*.css', ['default']);
});
