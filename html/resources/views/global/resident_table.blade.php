<div id="resident_list" class="form-group">
  <input type="text" id="resident_search" class="form-control search" placeholder="Search by name, room number, or student ID" autofocus>
  {!! Form::input('hidden', 'resident_id', null, ['id' => 'selected_resident', 'style' => 'height:0px;width:0px;display:hidden', 'required' => '']) !!}
  <br/>

  <table class="scroll zebra responsiveTable residentTable">
    <thead>
      <tr>
        <th class="hidden">ID</th>
        <th class="name sort" data-sort="resident_search_name">Name</th>
        <th class="room sort" data-sort="resident_search_room">Room</th>
        <th class="building sort" data-sort="resident_search_building">Building</th>
      </tr>
    </thead>
    <tbody class="clickableRows clickableResidentRow hoverRows list" id="resident_table_body">

    </tbody>
  </table>
  <div id="resident_loading">
    <img src="{{ URL::to('assets/desk/images/loading.gif') }}" />
  </div>
</div>
