<?php
  $root = Request::root();
  $domain = explode('://', $root)[1];
  $domain = explode('.', $domain)[0];
  if ($domain == 'dev'){$domain = ' DEV';}else{$domain='';}
 ?>
<topnav>
<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
       <span class="sr-only">Toggle navigation</span>
       MENU
     </button>
     <a class="navbar-brand" href="{{ URL::to('/') }}" style="font-weight:900;font-size:40px;color:#085706" title="home">
       DFD{{ $domain }}
     </a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
     <ul class="nav navbar-nav">
       @yield('navbar')
     </ul>

     @if (!is_null(Auth::user()))
     <ul class="nav navbar-nav navbar-right">
       <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }}<span class="caret"></span></a>
        <ul class="dropdown-menu">
          @if (Auth::user()->isAdmin())
            <li><a href="{{ URL::to('admin') }}" title="View the Admin Portal">Administrator Portal</a></li>
          @endif
          @if (!is_null(Auth::user()->worker_id))
              <li><a href="{{ URL::to('desk') }}" title="View Desk Worker Portal">Desk Worker Portal</a></li>
          @endif
              <li><a href="{{ URL::to('student') }}" title="View Student Portal">Student Portal</a></li>
              <li><a href="{{ URL::to('student/settings') }}" title="Student Settings">Settings</a></li>
          <li role="separator" class="divider"></li>
          <li><a name="emailButton" href="https://gmail.com" target="_blank">Mail</a></li>
          <li><a name="logOutButton" href="{{ URL::to('logout') }}">Sign Out</a></li>
        </ul>
      </li>
     </ul>
     @else
      <ul class="nav navbar-nav navbar-right">
        <li style="cursor:pointer"><a href="{{ URL::to('login') }}">Login</a></li>
      </ul>
     @endif
   </div><!-- /.navbar-collapse -->
  </div>
</nav>
</topnav>
