<script type="text/javascript">var BASE_URL = '{{ URL::to('/') }}';</script>
<script src="{{ URL::to('js/bootstrap.js') }}"></script>
<script src="{{ URL::to('js/app.js') }}"></script>
@if (!is_null(Auth::user()))
<script type="text/javascript">var APP_PATH='{{ \Session::get('app_path', 'desk') }}';$(function(){setInterval(function(){window.location = "/logout/inactivity";}, ({{intval(config('settings')->time_out) }}));});</script>
  @if (!Request::is('student/*') && !Request::is('student'))
  <script src="{{ URL::to('js/card_scanner.js') }}"></script>
  @endif
@endif
