<errors id ="showErrors">
  @if (Session::has('error'))
    <div class="alert alert-danger alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {!! Session::get('error') !!}
    </div>

    {{ Session::forget('error') }}
  @endif

  @if (Session::has('error_dismisses'))
    <div class="alert alert-danger dismisses" role="alert">{!! Session::get('error_dismisses') !!}</div>

    {{ Session::forget('error_dismisses') }}
  @endif


  @if (Session::has('success'))
    <div class="alert alert-success dismisses" role="alert">{!! Session::get('success') !!}</div>

    {{ Session::forget('success') }}
  @endif

  @if (Session::has('info'))
    <div class="alert alert-info alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{!! Session::get('info') !!}</div>

    {{ Session::forget('info') }}
  @endif

  @if (Session::has('info_dismisses'))
    <div class="alert alert-info dismisses" role="alert">{!! Session::get('info_dismisses') !!}</div>

    {{ Session::forget('info_dismisses') }}
  @endif

  <noscript>
    <div class="alert alert-warning" role="alert">This website might not function properly with JavaScript disabled.</div>
  </noscript>
</errors>
