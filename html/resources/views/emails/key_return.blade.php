Hello, {{ $firstname }}<br/>
<br/>
You have returned key {{ $info['key_label'] }} back to the front desk.<br/>
<br/>
Here is a copy of the signature used to return the key:<br/>
<img src="{{ URL::to('signature/key/'.$info['key_id'].'/in.png') }}" alt="{{ URL::to('signature/key/'.$info['key_id'].'/in.png') }}">
<br/>
If the image does not show up, use <a href="{{ URL::to('signature/key/'.$info['key_id'].'/in.png') }}">this link</a> to view the image.<br/>
<br/>
To view more details about your key return, please visit your <a href="{{ URL::to('student/keys/'.$info['key_id']) }}">student portal</a>.
