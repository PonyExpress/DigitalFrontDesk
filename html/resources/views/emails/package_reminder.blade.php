Hello, {{ $firstname }},<br/>
<br/>
Please remember that you have a package waiting for you to be picked up.
<br/><br/>
Please bring your Student ID with you, the desk worker will need it to verify the package belongs to you.
<br/><br/>
- Front Desk
