Hello, {{ $firstname }}<br/>
<br/>
You have returned the item {{ $info['item_name'] }}.<br/>
<br/>
Here is a copy of the signature used to return the item :<br/>
<img src="{{ URL::to('signature/item/'.$info['item_id'].'/in.png') }}" alt="{{ URL::to('signature/item/'.$info['item_id'].'/in.png') }}">
<br/>
If the image does not show up, use <a href="{{ URL::to('signature/item/'.$info['item_id'].'/in.png') }}">this link</a> to view the image.<br/>
<br/>
To view more details about your item loan and return, please visit your <a href="{{ URL::to('student/items/'.$info['item_id']) }}">student portal</a>.
