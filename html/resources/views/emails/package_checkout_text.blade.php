This is a notification letting you know that a package under your name has been signed out.

You can view information about this package by going to your student portal at: {{ URL::to('student/packages/'.$info['package_id']) }}
