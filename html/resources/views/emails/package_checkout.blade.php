Hello, {{ $firstname }}<br/>
<br/>
A package under your name has been checked out.<br/>
<br/>
Here is a copy of the signature used to signout the package:<br/>
<img src="{{ URL::to('signature/package/'.$info['package_id'].'/out.png') }}" alt="Image not loaded, can be viewed at: {{ URL::to('signature/package/'.$info['package_id'].'/out.png') }}}">
<br/>
If the image does not show up, use <a href="{{ URL::to('signature/package/'.$info['package_id'].'/out.png') }}">this link</a> to view the image.<br/>
<br/>
To view more details about your package checkout, please visit your <a href="{{ URL::to('student/packages/'.$info['package_id']) }}">student portal</a>.
