Hello, {{ $firstname }},<br/>
<br/>
This is an automated email message letting you know that you have received a package and it is waiting for you at the front desk of your dorm building.
<br/><br/>
Please bring your Student ID with you, the desk worker will need it to verify the package belongs to you.
<br/><br/>
- Front Desk
