Hello, {{ $firstname }}<br/>
<br/>
You have been loaned key {{ $info['key_label'] }}.<br/>
<br/>
Here is a copy of the signature used to loan out the key:<br/>
<a href="{{ URL::to('signature/key/'.$info['key_id'].'/out.png') }}"><img src="{{ URL::to('signature/key/'.$info['key_id'].'/out.png') }}" alt="{{ URL::to('signature/key/'.$info['key_id'].'/out.png') }}"></a>
<br/><br/>
If the image does not show up, use <a href="{{ URL::to('signature/key/'.$info['key_id'].'/out.png') }}">this link</a> to view the image.<br/>
<br/>
To view more details about your key loan, please visit your <a href="{{ URL::to('student/keys/'.$info['key_id']) }}">student portal</a>.
