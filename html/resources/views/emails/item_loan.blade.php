Hello, {{ $firstname }},<br/>
<br/>
This is an automated email message letting you know that {{ $info['item_name'] }} has been checked out under your name.
<br/><br/>
- The Front Desk
