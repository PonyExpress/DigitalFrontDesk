@extends('student.master')


@section('title')
Package {{ $package->number }}
@stop


@section('navbar')
  <li><a href="{{ URL::to('student') }}">Student</a></li>
  <li class="sepratron"><a>/</a></li>
  <li><a href="{{ URL::to('student') }}">Package Log</a></li>
  <li class="sepratron"><a>/</a></li>
  <li class="active"><a href="{{ URL::to('student/packages/' . $package->id) }}">{{ $package->id }}</a><span class="sr-only">(current)</span></li>
@stop

@section('content')
  <div class="title">Package #{{ $package->number }} @if ($package->isPickedUp()) History @endif</div>

  <table class="responsiveTable zebra">
    <tr>
      <td>Resident</td>
      <td>{{ $package->resident->informalName }}</td>
   </tr>
    <tr>
      <td>Address</td>
      <td>{{ $package->resident->room_number }}&nbsp;{{ $package->resident->building->name }}</td>
    </tr>
   <tr>
      <td>Type</td>
      <td>{{ $package->packageType }}</td>
    </tr>
    <tr>
      <td>Arrived</td>
      <td>{{ $package->dateIn }}</td>
    </tr>

    @if ($package->isPickedUp())
    <tr>
      <td>Signed Out</td>
      <td>{{ $package->dateOut }}</td>
    </tr>
    <tr>
      <td>Signature</td>
      <td><img src="{{ $package->signature->url }}" width="500" height="100"/></td>
    </tr>
    @else
    <tr>
      <td>Status</td>
      <td>Waiting to be picked up.</td>
    </tr>
    @endif
  </table>

<br><br>

@stop
@section('javascriptFooter')

@stop
