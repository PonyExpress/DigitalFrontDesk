@extends('student.master')


@section('title')
#{{ $entry->id}} - {{ $entry->item->name }}
@stop

@section('navbar')
  <li><a href="{{ URL::to('student') }}">Student</a></li>
  <li class="sepratron"><a>/</a></li>
  <li><a href="{{ URL::to('student') }}">Item Log</a></li>
  <li class="sepratron"><a>/</a></li>
  <li class="active"><a href="{{ URL::to('student/items/' . $entry->id) }}">{{ $entry->item->name }}<span class="sr-only">(current)</span></a></li>
@stop

@section('content')
  <div class="title">Item Log #{{ $entry->id }}</div>

  <table class="responsiveTable zebra">
    <tr>
      <td>Resident</td>
      <td>{{ $entry->resident->informalName }}</td>
   </tr>
    <tr>
      <td>Address</td>
      <td>{{ $entry->resident->address }}</td>
    </tr>
   <tr>
      <td>Item</td>
      <td>{{ $entry->item->name }}</td>
    </tr>
    <tr>
      <td>Checked Out</td>
      <td>{{ $entry->dateOut }}</td>
    </tr>
    <tr>
      <td>Desk Worker Out</td>
      <td>{{ $entry->workerOut->initials }}</td>
    </tr>


    @if ($entry->isReturned())
      <tr>
        <td>Status</td>
        <td style="color:#0B610B">Returned</td>
      </tr>
      <tr>
        <td>Returned</td>
        <td>{{ $entry->dateIn }}</td>
      </tr>
      <tr>
        <td>Desk Worker In</td>
        <td>{{ $entry->workerIn->initials }}</td>
      </tr>
      <tr>
        <td>Signature</td>
        <td><img src="{{ $entry->signature->url }}" width="500" height="100"/></td>
      </tr>
    @else
      <tr>
        <td>Status</td>
        <td style="color:#B40404">Waiting to be returned</td>
      </tr>
    @endif
  </table>
<br><br>

@stop
