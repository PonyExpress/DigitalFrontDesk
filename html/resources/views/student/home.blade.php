@extends('student.master')


@section('title')
{{ $self->informalName }} Portal
@stop


@section('navbar')
  <li class="active"><a href="{{ URL::to('student') }}">Student<span class="sr-only">(current)</span></a></li>
@stop


@section('content')
  <img src="{!! $picture !!}" alt="" class="img-circle studentImage">
  <div class="title">{{ getGreeting() }}, {{ $self->first_name }}</div>
  <h3>{{ $self->id }}</h3>
  <h3>{{ $self->address }}</h3>
  <br/>

  <!-- New Packages -->
  <div class="panel panel-default displayPanel" style="display:inline-block;margin-right:10px;">
    <div class="panel-heading">
      <h3 class="panel-title" style="text-align:left">New Packages</h3>
    </div>
    <div class="panel-body">
      <table class="responsiveTable zebra">
        <thead>
          <tr>
            <th>ID</th>
            <th>Type</th>
            <th class="date">Date</th>
          </tr>
        </thead>
        <tbody class="clickableRows hoverRows" style="min-height:150px">
          @foreach ($newPackages as $package)
            <tr studentPackage="{{ $package->id }}">
              <td>{{ $package->number }}</td>
              <td>{{ $package->packageType }}</td>
              <td class="date">{{ $package->dateIn }}</td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>

  <!-- Old Packages -->
  <div class="panel panel-default displayPanel" style="display:inline-block;margin-left:10px;">
    <div class="panel-heading">
      <h3 class="panel-title" style="text-align:left">Package History</h3>
    </div>
    <div class="panel-body">
      <table class="responsiveTable zebra">
        <thead>
          <tr>
            <th>ID</th>
            <th>Type</th>
            <th class="date">Date</th>
          </tr>
        </thead>
        <tbody class="clickableRows hoverRows" style="min-height:150px">
          @foreach ($oldPackages as $package)
            <tr studentPackage="{{ $package->id }}" ref="{{ $self->id }}">
              <td>{{ $package->number }}</td>
              <td>{{ $package->packageType }}</td>
              <td class="date">{{ $package->dateIn }}</td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>

  <br/>

  <!-- Keys Loaned -->
  <div class="panel panel-default displayPanel" style="display:inline-block;margin-right:10px;">
    <div class="panel-heading">
      <h3 class="panel-title" style="text-align:left">Loaned Keys</h3>
    </div>
    <div class="panel-body">
      <table class="responsiveTable zebra">
        <thead>
          <th>ID</th>
          <th class="room">Room</th>
          <th class="date">Date</th>
        </thead>
        <tbody class="clickableRows hoverRows" style="min-height:150px">
          @foreach ($newKeys as $keylog)
            <tr studentKey="{{ $keylog->id }}" ref="{{ $self->id }}">
              <td>{{ $keylog->key->label }}</td>
              <td>{{ $keylog->key->room }}</td>
              <td class="date">{{ $keylog->dateOut }}</td>
            </tr>
          @endforeach
        </tbody>
      </table>

    </div>
  </div>

  <!-- Keys Loaned History -->
  <div class="panel panel-default displayPanel" style="display:inline-block;margin-left:10px;">
    <div class="panel-heading">
      <h3 class="panel-title" style="text-align:left">Key History</h3>
    </div>
    <div class="panel-body">
      <table class="responsiveTable zebra">
        <thead>
          <th>ID</th>
          <th class="room">Room</th>
          <th class="date">Date</th>
        </thead>
        <tbody class="clickableRows hoverRows" style="min-height:150px">
          @foreach ($oldKeys as $keylog)
            <tr studentKey="{{ $keylog->id }}" ref="{{ $self->id }}">
              <td>{{ $keylog->key->label }}</td>
              <td>{{ $keylog->key->room }}</td>
              <td class="date">{{ $keylog->dateOut }}</td>
            </tr>
          @endforeach
        </tbody>
      </table>

    </div>
  </div>

  <br/>

  <!-- Items Loaned -->
  <div class="panel panel-default displayPanel" style="display:inline-block;margin-right:10px;">
    <div class="panel-heading">
      <h3 class="panel-title" style="text-align:left">Loaned Items</h3>
    </div>
    <div class="panel-body">
      <table class="responsiveTable zebra">
        <thead>
          <th class="item">Item</th>
          <th class="date">Date</th>
        </thead>
        <tbody class="clickableRows hoverRows" style="min-height:150px">
          @foreach ($newItems as $itemlog)
            <tr studentItem="{{ $itemlog->id }}" ref="{{ $self->id }}">
              <td>{{ $itemlog->item->name }}</td>
              <td class="date">{{ $itemlog->dateOut }}</td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>


  <!-- Items Loaned History -->
  <div class="panel panel-default displayPanel" style="display:inline-block;margin-left:10px;">
    <div class="panel-heading">
      <h3 class="panel-title" style="text-align:left">Loaned Items</h3>
    </div>
    <div class="panel-body">
      <table class="responsiveTable zebra">
        <thead>
          <th class="item">Item</th>
          <th class="date">Returned</th>
        </thead>
        <tbody class="clickableRows hoverRows" style="min-height:150px">
          @foreach ($oldItems as $itemlog)
            <tr studentItem="{{ $itemlog->id }}" ref="{{ $self->id }}">
              <td>{{ $itemlog->item->name }}</td>
              <td class="date">{{ $itemlog->dateIn }}</td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>



@stop
