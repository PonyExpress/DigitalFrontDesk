@extends('student.master')

@section('title')
DFD Login
@stop

@section('content')
	<div class="title">Digital Front Desk</div>
	<form name="login">
		  <input type="text" class="form-control" placeholder="username" name="username">
			<br/>
			<input type="password" class="form-control" name="password">
			<br/>
			<button type="button" class="btn btn-default" name="login_btn">Login</button>
			<button type="button" class="btn btn-default" name="guest_btn" style="margin-left:20px">Guest</button>
	</form>
@stop
