@extends('student.master')


@section('title')
Key {{ $keylog->key->label }}
@stop


@section('navbar')
  <li><a href="{{ URL::to('student') }}">Student</a></li>
  <li class="sepratron"><a>/</a></li>
  <li><a href="{{ URL::to('student') }}">Key Log</a></li>
  <li class="sepratron"><a>/</a></li>
  <li class="active"><a href="{{ URL::to('student/keys/' . $keylog->id) }}">{{ $keylog->key->label }}</a><span class="sr-only">(current)</span></li>
@stop

@section('content')
  <div class="title">Key {{ $keylog->key->label }} @if ($keylog->isReturned()) History @endif</div>

  <table class="responsiveTable zebra">
    <tr>
      <td>Resident</td>
      <td>{{ $keylog->resident->informalName }}</td>
   </tr>
    <tr>
      <td>Address</td>
      <td>{{ $keylog->resident->address }}</td>
    </tr>
    <tr>
      <td>Key ID</td>
      <td>{{ $keylog->key->label }}</td>
    </tr>
    <tr>
      <td>Key Room</td>
      <td>{{ $keylog->key->room }}</td>
    </tr>
    <tr>
      <td>Time Out</td>
      <td>{{ $keylog->dateOut }}</td>
    </tr>
    <tr>
      <td>Signature Out</td>
      <td>
          <img src="{{ $keylog->signatureOut->url }}" width="500" height="100"/>
        </td>
    </tr>
    <tr>
      <td>Desk Worker Out</td>
      <td>{{ $keylog->workerOut->initials }}</td>
    </tr>

    @if ($keylog->isReturned())
      <tr>
        <td>Status</td>
        <td style="color:#0B610B">Returned</td>
      </tr>
      <tr>
        <td>Time In</td>
        <td>{{ $keylog->dateIn }}</td>
      </tr>
      <tr>
        <td>Signature</td>
        <td><img src="{{ $keylog->signatureIn->url }}" width="500" height="100"/></td>
      </tr>
    @else
      <tr>
        <td>Status</td>
        <td style="color:#B40404">Waiting to be returned</td>
      </tr>
    @endif
  </table>

<br><br>

@stop
