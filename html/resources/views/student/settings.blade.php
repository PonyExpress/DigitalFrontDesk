@extends('student.master')


@section('title')
{{ $self->informalName }} Settings
@stop


@section('navbar')
  <li><a href="{{ URL::to('student') }}">Student</a></li>
  <li class="sepratron"><a>/</a></li>
  <li class="active"><a href="{{ URL::to('student/settings') }}">Settings<span class="sr-only">(current)</span></a></li>
@stop


@section('header')
  <style>
    .settingsGroup{border-bottom:1px solid #eee;padding-bottom:10px;padding-top:10px}
    label{font-weight:normal;display:block;}
    small{display:block;font-size:15px;color:#666;}
    #alert_email {padding-top:20px;border-top:2px solid #eee;}
    #text {margin-top:60px;border-top: 3px solid #eee;}
    #phone_number, #phone_provider {display:none;}
    .btn{border:1px solid #ccc!important;}
    .btn-on, .btn-off{background-color:#fff;color:#333;font-size:15px}
    .btn-off.active{background-color:#A94442!important;color:#fff;}
    .btn-on.active{background-color:#3C763D!important;color:#fff;}
  </style>
  @if($settings->send_text)
    <style>#phone_number, #phone_provider {display:block;}</style>
  @endif
@stop


@section('content')
  <div class="title">Settings</div>

  {!! Form::open(['name' => 'settings_form']) !!}
    <div class="panel panel-default displayPanel">
      <div class="panel-heading" id="package_alerts">
        <h3 class="panel-title" style="text-align:left">Alerts and Notifications</h3>
      </div>
      <div class="panel-body" style="text-align:left">

        <!-- New Package Notification -->
        <div class="settingsGroup">
          <label>New Packages</label>
          <div class="btn-group" data-toggle="buttons" style="float:right;">
            <label class="btn btn-on  @if($settings->package_alerts) active @endif">
              <input type="radio" name="package_alerts" value="1" autocomplete="off"  @if($settings->package_alerts) checked @endif> On
            </label>
            <label class="btn btn-off @if(!$settings->package_alerts) active @endif">
              <input type="radio" name="package_alerts" value="0" autocomplete="off"  @if(!$settings->package_alerts) checked @endif> Off
            </label>
          </div>
          <label>
            <small>
              Recieve a notification when you recieve a new package<br/>
              (Default: On)
            </small>
          </label>
        </div>

        <!-- Package Checkout Notification -->
        <div class="settingsGroup" id="package_confirm">
          <label>Package Checkout Confirmation</label>
          <div class="btn-group" data-toggle="buttons" style="float:right;">
            <label class="btn btn-on  @if($settings->package_checkout_confirm) active @endif">
              <input type="radio" name="package_confirm" value="1" autocomplete="off"  @if($settings->package_checkout_confirm) checked @endif> On
            </label>
            <label class="btn btn-off @if(!$settings->package_checkout_confirm) active @endif">
              <input type="radio" name="package_confirm" value="0" autocomplete="off"  @if(!$settings->package_checkout_confirm ) checked @endif> Off
            </label>
          </div>
          <label>
            <small>
              Recieve a notification when a package you check out a package, along with a copy of the digital signature<br/>
              (Default: On)
            </small>
          </label>
        </div>

        <!-- Item Loan Notification -->
        <div class="settingsGroup">
          <label>Item Loans</label>
          <div class="btn-group" data-toggle="buttons" style="float:right;">
            <label class="btn btn-on  @if($settings->item_loan_alerts) active @endif">
              <input type="radio" name="item_loans" value="1" autocomplete="off"  @if($settings->item_loan_alerts) checked @endif> On
            </label>
            <label class="btn btn-off @if(!$settings->item_loan_alerts) active @endif">
              <input type="radio" name="item_loans" value="0" autocomplete="off"  @if(!$settings->item_loan_alerts) checked @endif> Off
            </label>
          </div>
          <label>
            <small>
              Recieve a notification when you checkout an item<br/>
              (Default: On)
            </small>
          </label>
        </div>

        <!-- Item Return Notification -->
        <div class="settingsGroup">
          <label>Item Return Confirmation</label>
          <div class="btn-group" data-toggle="buttons" style="float:right;">
            <label class="btn btn-on  @if($settings->item_return_alerts) active @endif">
              <input type="radio" name="item_return" value="1" autocomplete="off"  @if($settings->item_return_alerts) checked @endif> On
            </label>
            <label class="btn btn-off @if(!$settings->item_return_alerts) active @endif">
              <input type="radio" name="item_return" value="0" autocomplete="off"  @if(!$settings->item_return_alerts) checked @endif> Off
            </label>
          </div>
          <label>
            <small>
              Recieve a notification when you return an item, along with a copy of the digital signature<br/>
              (Default: On)
            </small>
          </label>
        </div>


        <!-- Key Loan Notification -->
        <div class="settingsGroup">
          <label>Key Loans</label>
          <div class="btn-group" data-toggle="buttons" style="float:right;">
            <label class="btn btn-on  @if($settings->key_loan_alerts) active @endif">
              <input type="radio" name="key_loans" value="1" autocomplete="off"  @if($settings->key_loan_alerts) checked @endif> On
            </label>
            <label class="btn btn-off @if(!$settings->key_loan_alerts) active @endif">
              <input type="radio" name="key_loans" value="0" autocomplete="off"  @if(!$settings->key_loan_alerts) checked @endif> Off
            </label>
          </div>
          <label>
            <small>
              Recieve a notification when you borrow a key<br/>
              (Default: On)
            </small>
          </label>
        </div>

        <!-- Key Return Notification -->
        <div class="settingsGroup">
          <label>Key Return Confirmation</label>
          <div class="btn-group" data-toggle="buttons" style="float:right;">
            <label class="btn btn-on  @if($settings->key_return_alerts) active @endif">
              <input type="radio" name="key_return" value="1" autocomplete="off"  @if($settings->key_return_alerts) checked @endif> On
            </label>
            <label class="btn btn-off @if(!$settings->key_return_alerts) active @endif">
              <input type="radio" name="key_return" value="0" autocomplete="off"  @if(!$settings->key_return_alerts) checked @endif> Off
            </label>
          </div>
          <label>
            <small>
              Recieve a notification when you return a key, along with a copy of the digital signature<br/>
              (Default: On)
            </small>
          </label>
        </div>


        <!-- SMS Notification -->
        <div class="settingsGroup" id="text">
          <label>Text Message Notification</label>
          <div class="btn-group" data-toggle="buttons" style="float:right;">
            <label class="btn btn-on  @if($settings->send_text) active @endif" id="send_text_on">
              <input type="radio" name="send_text" value="1" autocomplete="off"  @if($settings->send_text) checked @endif> On
            </label>
            <label class="btn btn-off @if(!$settings->send_text) active @endif" id="send_text_off">
              <input type="radio" name="send_text" value="0" autocomplete="off"  @if(!$settings->send_text) checked @endif> Off
            </label>
          </div>
          <label>
            <small>
              Recieve a SMS notification rather than an email notification<br/>
              (Default: Off)<br/>
            </small>
          </label>
        </div>

        <!-- Phone Number -->
        <div class="settingsGroup" id="phone_number">
          <label>Phone Number</label>
          <input class="form-control" type="tel" value="{{ $settings->phone_number }}" placeholder="0123456789" name="phone_number" maxlength="10">
          <label><small>The full 10-digit cell phone number you want to recieve a text alert on</small></label>
        </div>
        <!-- Phone Provider -->
        <div class="settingsGroup" id="phone_provider">
          <label>Service Provider</label>
          {!! Form::select('phone_provider', $providers, null, ['class' => 'form-control']) !!}
          <label><small>Please select your cell phone service provider, this is <strong>important</strong>, if the wrong provider is selected then you will not recieve notifications.</small><br/><small>Don't see your service provider listed? You can request that it be listed by filing out <a href="{{ URL::to('forms/cellphonecarrier') }}" target="_blank">this form</a>.</label>
        </div>



        <!-- Email for alerts -->
        <div class="settingsGroup" id="alert_email">
          <label>Notification Email</label>
          <input class="form-control" type="email" value="{{ $settings->alert_email }}" name="alert_email">
          <label><small>The email that will recieve notifications<br/>(Default: {{ $self->email }})</small></label>
          @if($settings->send_text)
          <label><small style="color: #A94442">You will not recieve email notifications since you have SMS notifications turned on</small></label>
          @endif
        </div>

        <input class="btn btn-primary form-control" type="submit" value="Save" id="formSubmit">
      </div>

    </div>
  {!! Form::close() !!}


@stop


@section('javascriptFooter')
  <script type="text/javascript">$('select[name=phone_provider]').val('{{ $settings->phone_provider }}');</script>
@stop
