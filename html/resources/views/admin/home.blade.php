@extends('admin.master')


@section('title')
Admin Panel
@stop

@section('navbar')
  <li><a href="{{ URL::to('admin') }}">Admin Panel</a></li>
@stop

@section('content')

  <div class="col-sm-4" href="{{ URL::to('admin/residents') }}">
    <div class="tile" color="red">
      <span class="bigSubText">Residents</span>
        <span class="subtext">Manage Residents</span>
    </div>
  </div>

  <div class="col-sm-4" href="{{ URL::to('admin/buildings') }}">
    <div class="tile" color="orange">
      <span class="bigSubText">Buildings</span>
        <span class="subtext">Manage Locations</span>
    </div>
  </div>

  <div class="col-sm-4">
    <div class="tile" color="teal">
      <span class="bigSubText">Items</span>
        <span class="subtext">Manage Location Items</span>
    </div>
  </div>

  <br/>

  <div class="col-sm-4">
    <div class="tile" color="pink">
      <span class="bigSubText">Desk Workers</span>
        <span class="subtext">Manage Resident Workers</span>
    </div>
  </div>

  @if(Auth::user()->admin->permissions->canEditSettings())
  <div class="col-sm-4">
    <div class="tile" color="teal">
      <span class="bigSubText">Settings</span>
        <span class="subtext">Change the Digital Front Desk Settings</span>
    </div>
  </div>
  @endif



  <br/>
  <table class="responsiveTable zebra">
    <thead>
        <tr>
          <th class="name">Name</th>
          <th>Item</th>
          <th>Building</th>
          <th class="date">Date</th>
        </tr>
      </thead>
      <tbody class="clickableRows hoverRows">


      </tbody>
  </table>

@stop
