<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title')</title>

    @include('global.css')
    <link href="{{ URL::to('css/adminportal.css') }}" rel="stylesheet">

		@yield('header')

    @include('global.javascript_header')

  </head>
  <body>
    <div id="body">
      @include('global.navbar')
      <div class="container" style="border-bottom:1px solid #ccc;padding-bottom:0px;margin-bottom:20px;">
        <div class="content" style="padding-bottom:0px;margin-bottom:0px">
          @include('admin.notifications')
        </div>
      </div>
      <div class="container">

        <div class="content">
          @include('global.errorSection')

          @yield('content')
        </div>
      </div>

    </div>
      @include('global.footer')

    @include('global.javascript_footer')

    @yield('javascriptFooter')
  </body>
</html>
