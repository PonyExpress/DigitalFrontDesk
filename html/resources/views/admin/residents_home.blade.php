@extends('admin.master')

@section('title')
Residents
@stop

@section('navbar')
  <li><a href="{{ URL::to('admin') }}">Admin Panel</a></li>
  <li class="sepratron"><a>/</a></li>
  <li class="active"><a href="{{ URL::to('admin/residents') }}">Residents<span class="sr-only">(current)</span></a></li>
@stop


@section('content')
  <div class="title">Residents</div>
  <br/>
  @if (Auth::user()->admin->permissions->canCreateResidents())
  <a href="{{ URL::to('admin/residents/create') }}"><button name="createResident" class="btn btn-primary">New Resident</button></a>
  <br/>
  @endif
  <br/>
  @include('global.resident_table')
@stop

@section('javascriptFooter')
  <script type="text/javascript" src="{{ URL::to('js/load_residents_click.js') }}"></script>
@stop
