@extends('admin.master')


@section('title')
{{ $building->name }}
@stop


@section('navbar')
  <li><a href="{{ URL::to('admin') }}">Admin Panel</a></li>
  <li class="sepratron"><a>/</a></li>
  <li><a href="{{ URL::to('admin/buildings') }}">Buildings</a></li>
  <li class="sepratron"><a>/</a></li>
  <li class="active"><a href="{{ URL::to('admin/buildings/' . $building->uID) }}">{{ $building->name }}<span class="sr-only">(current)</span></a></li>
@stop


@section('content')
  <div class="title">{{ $building->name }}</div>
  <br/>
  <a href="{{ URL::to('admin/buildings/' . $building->uID . '/modify') }}"><button name="modfiyBuilding" class="btn btn-primary">Edit Building</button></a>
  <br/><br/>
  <table class="responsiveTable zebra">
    <tr>
      <td>Name</td>
      <td>{{ $building->name }}</td>
    </tr>
    @if ($building->inGroup())
      <tr>
        <td>Group</td>
        <td><a href="{{ URL::to('admin/buildings/groups/' . $building->group->id) }}">{{ $building->groupName }}</a></td>
      </tr>
    @endif
    <tr>
      <td>Address</td>
      <td>{{ $building->address }}</td>
    </tr>
    @foreach ($building->buildingHasAsArray() as $param)
    <tr>
      <td>{{ $param['title'] }}</td>
      <td>{{ $param['value'] }}</td>
    </tr>
    @endforeach



  </table>
<br><br>

@stop
