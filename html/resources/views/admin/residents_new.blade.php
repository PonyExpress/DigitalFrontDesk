@extends('admin.master')


@section('title')
New Resident
@stop

@section('header')
<style>
.settingsGroup{border-bottom:1px solid #eee;padding-bottom:10px;padding-top:10px}
.settingsGroup label{font-weight:normal;display:block;}
.settingsGroup small{display:block;font-size:15px;color:#666;}
#desk_worker_permissions, #admin_permissions {display:none;}
.btn{border:1px solid #ccc!important;}
.btn-on, .btn-off{background-color:#fff;color:#333;font-size:15px}
.btn-off.active{background-color:#A94442!important;color:#fff;}
.btn-on.active{background-color:#3C763D!important;color:#fff;}

</style>
@stop

@section('navbar')
  <li><a href="{{ URL::to('admin') }}">Admin Panel</a></li>
  <li class="sepratron"><a>/</a></li>
  <li><a href="{{ URL::to('admin/residents') }}">Residents</a></li>
  <li class="sepratron"><a>/</a></li>
  <li class="active"><a href="{{ URL::to('admin/residents/create') }}">Create <span class="sr-only">(current)</span></a></li>
@stop


@section('content')
  <div class="title">Create a Resident</div>
  {!! Form::open(['name' => 'new_resident_form']) !!}
    <div class="form-group">
      <label>Student ID</label>
      <input type="text" name="resident_id" placeholder="Student ID" class="form-control" pattern="([0-9]+)" title="Student ID Number" value="{{ \Session::get('input_resident_id', '') }}" required autofocus>
    </div>
    <div class="form-group">
      <label>Name</label>
      <input type="text" name="first_name" class="form-control" placeholder="First Name" title="Resident's First Name" value="{{ \Session::get('input_first_name', '') }}" required>
      <input type="text" name="middle_name" class="form-control" placeholder="Middle Name (optional)" title="Resident's Middle Name" value="{{ \Session::get('input_middle_name', '') }}">
      <input type="text" name="last_name" class="form-control" placeholder="Last Name" title="Resident's Last Name" value="{{ \Session::get('input_last_name', '') }}" required>
    </div>
    <div class="form-group">
      <label>Address</label>
      <input type="text" class="form-control" name="room_number" placeholder="Room Number" pattern="([0-9]+)" value="{{ \Session::get('input_room_number', '') }}" required>
      <select id="building" name="building" class="form-control" required>
      @foreach($buildings as $building)
        <option value="{{ $building->uID }}" @if(\Session::get('input_building', '-1') == $building->uID) selected @endif >{{ $building->name }}</option>
      @endforeach
    </select>
    </div>

    <div class="form-group">
      <label>Email</label>
      <div class="input-group">
        <input type="text" name="email" class="form-control" placeholder="username" aria-describedby="basic-addon2" value="{{ \Session::get('input_email', '') }}" required>
        <span class="input-group-addon" id="basic-addon2">@mst.edu</span>
      </div>
    </div>

    @if ($self->permissions->canCreateWorkers())
      <!-- Desk Worker Options -->
      <div class="panel panel-default displayPanel">
        <div class="panel-heading" id="desk_worker_options">
          <h3 class="panel-title" style="text-align:left">Desk Worker Options</h3>
        </div>
        <div class="panel-body" style="text-align:left">
          <div class="settingsGroup">
            <label>Make a Desk Worker</label>
            <div class="btn-group" data-toggle="buttons" style="float:right;">
              <label class="btn btn-on " id="desk_worker_yes">
                <input type="radio" name="make_worker" value="1" autocomplete="off"> Yes
              </label>
              <label class="btn btn-off active" id="desk_worker_no">
                <input type="radio" name="make_worker" value="0" autocomplete="off" checked> No
              </label>
            </div>
            <label>
              <small>
                Changing this option to "Yes" will allow this resident access to the desk worker portal.<br/>
                (Default: No)<br/>
              </small>
            </label>
          </div>

          <div class="settingsGroup" id="desk_worker_permissions">

            <!-- New Packages -->
            <div class="settingsGroup">
              <label>Create Packages</label>
              <div class="btn-group" data-toggle="buttons" style="float:right;">
                <label class="btn btn-on  active">
                  <input type="radio" name="new_packages" value="1" autocomplete="off"  checked> Yes
                </label>
                <label class="btn btn-off">
                  <input type="radio" name="new_packages" value="0" autocomplete="off"> No
                </label>
              </div>
              <label>
                <small>
                  This permission option allows the desk worker to create new packages.<br/>
                  (Default: Yes)
                </small>
              </label>
            </div>

            <!-- Signout Packages -->
            <div class="settingsGroup">
              <label>Signout Packages</label>
              <div class="btn-group" data-toggle="buttons" style="float:right;">
                <label class="btn btn-on  active">
                  <input type="radio" name="signout_packages" value="1" autocomplete="off"  checked> Yes
                </label>
                <label class="btn btn-off">
                  <input type="radio" name="signout_packages" value="0" autocomplete="off"> No
                </label>
              </div>
              <label>
                <small>
                  This permission option allows the desk worker to sign out packages to students.<br/>
                  (Default: Yes)
                </small>
              </label>
            </div>

            <!-- Loan Keys -->
            <div class="settingsGroup">
              <label>Loan Keys</label>
              <div class="btn-group" data-toggle="buttons" style="float:right;">
                <label class="btn btn-on  active">
                  <input type="radio" name="loan_keys" value="1" autocomplete="off"  checked> Yes
                </label>
                <label class="btn btn-off">
                  <input type="radio" name="loan_keys" value="0" autocomplete="off"> No
                </label>
              </div>
              <label>
                <small>
                  This permission option allows the desk worker to loan out keys to residents.<br/>
                  (Default: Yes)
                </small>
              </label>
            </div>

            <!-- Return Keys -->
            <div class="settingsGroup">
              <label>Return Keys</label>
              <div class="btn-group" data-toggle="buttons" style="float:right;">
                <label class="btn btn-on  active">
                  <input type="radio" name="return_keys" value="1" autocomplete="off"  checked> Yes
                </label>
                <label class="btn btn-off">
                  <input type="radio" name="return_keys" value="0" autocomplete="off"> No
                </label>
              </div>
              <label>
                <small>
                  This permission option allows the desk worker to return keys loaned to residents.<br/>
                  (Default: Yes)
                </small>
              </label>
            </div>

            <!-- Loan Items -->
            <div class="settingsGroup">
              <label>Loan Items</label>
              <div class="btn-group" data-toggle="buttons" style="float:right;">
                <label class="btn btn-on  active">
                  <input type="radio" name="loan_items" value="1" autocomplete="off"  checked> Yes
                </label>
                <label class="btn btn-off">
                  <input type="radio" name="loan_items" value="0" autocomplete="off"> No
                </label>
              </div>
              <label>
                <small>
                  This permission option allows the desk worker to loan items to residents.<br/>
                  (Default: Yes)
                </small>
              </label>
            </div>

            <!-- Return Items -->
            <div class="settingsGroup">
              <label>Return Items</label>
              <div class="btn-group" data-toggle="buttons" style="float:right;">
                <label class="btn btn-on  active">
                  <input type="radio" name="return_items" value="1" autocomplete="off"  checked> Yes
                </label>
                <label class="btn btn-off">
                  <input type="radio" name="return_items" value="0" autocomplete="off"> No
                </label>
              </div>
              <label>
                <small>
                  This permission option allows the desk worker to return loaned items.<br/>
                  (Default: Yes)
                </small>
              </label>
            </div>

            <!-- Message Log -->
            <div class="settingsGroup">
              <label>Message Log</label>
              <div class="btn-group" data-toggle="buttons" style="float:right;">
                <label class="btn btn-on  active">
                  <input type="radio" name="message_log" value="1" autocomplete="off"  checked> Yes
                </label>
                <label class="btn btn-off">
                  <input type="radio" name="message_log" value="0" autocomplete="off"> No
                </label>
              </div>
              <label>
                <small>
                  This permission gives a desk worker access to the building message log for desk workers.<br/>
                  (Default: Yes)
                </small>
              </label>
            </div>

            <!--Punch Clock-->
            <div class="settingsGroup">
              <label>Punch Clock</label>
              <div class="btn-group" data-toggle="buttons" style="float:right;">
                <label class="btn btn-on  active">
                  <input type="radio" name="punch_clock" value="1" autocomplete="off"  checked> Yes
                </label>
                <label class="btn btn-off">
                  <input type="radio" name="punch_clock" value="0" autocomplete="off"> No
                </label>
              </div>
              <label>
                <small>
                  This permission option allows the desk worker to create entries on their punch clock.<br/>
                  (Default: Yes)
                </small>
              </label>
            </div>

            <!-- Send Emails -->
            <div class="settingsGroup">
              <label>Send Emails</label>
              <div class="btn-group" data-toggle="buttons" style="float:right;">
                <label class="btn btn-on  active">
                  <input type="radio" name="send_emails" value="1" autocomplete="off"  checked> Yes
                </label>
                <label class="btn btn-off">
                  <input type="radio" name="send_emails" value="0" autocomplete="off"> No
                </label>
              </div>
              <label>
                <small>
                  This permission option allows the desk worker to send emails to residents.<br/>
                  (Default: Yes)
                </small>
              </label>
            </div>

          </div> <!-- End of Desk Worker Permissions -->

        </div>
      </div> <!-- End Desk Worker Options -->
    @endif






    @if ($self->permissions->canCreateAdmins())
      <!-- Admin Options -->
      <div class="panel panel-default displayPanel">
        <div class="panel-heading" id="admin_options">
          <h3 class="panel-title" style="text-align:left">Administrator Options</h3>
        </div>
        <div class="panel-body" style="text-align:left">
          <div class="settingsGroup">
            <label>Make an Administrator</label>
            <div class="btn-group" data-toggle="buttons" style="float:right;">
              <label class="btn btn-on " id="admin_yes">
                <input type="radio" name="make_admin" value="1" autocomplete="off"> Yes
              </label>
              <label class="btn btn-off active" id="admin_no">
                <input type="radio" name="make_admin" value="0" autocomplete="off" checked> No
              </label>
            </div>
            <label>
              <small>
                Changing this option to "Yes" will allow this resident access to the admin panel.<br/>
                (Default: No)<br/>
              </small>
            </label>
          </div>

          <div class="settingsGroup" id="admin_permissions">

            <!-- App Settings -->
            <div class="settingsGroup">
              <label>App Settings</label>
              <div class="btn-group" data-toggle="buttons" style="float:right;">
                <label class="btn btn-on">
                  <input type="radio" name="prog_settings" value="1" autocomplete="off" > Yes
                </label>
                <label class="btn btn-off active">
                  <input type="radio" name="prog_settings" value="0" autocomplete="off" checked> No
                </label>
              </div>
              <label>
                <small>
                  Allows the admin the ability to change the application settings.<br/>
                  (Default: No)
                </small>
              </label>
            </div>

            <!-- Create Residents -->
            <div class="settingsGroup">
              <label>Create Residents</label>
              <div class="btn-group" data-toggle="buttons" style="float:right;">
                <label class="btn btn-on  active">
                  <input type="radio" name="create_residents" value="1" autocomplete="off"  checked> Yes
                </label>
                <label class="btn btn-off">
                  <input type="radio" name="create_residents" value="0" autocomplete="off"> No
                </label>
              </div>
              <label>
                <small>
                  Allows the admin to create new residents.<br/>
                  (Default: Yes)
                </small>
              </label>
            </div>

            <!-- Create Desk Workers -->
            <div class="settingsGroup">
              <label>Create Desk Workers</label>
              <div class="btn-group" data-toggle="buttons" style="float:right;">
                <label class="btn btn-on  active" id="create_workers_yes">
                  <input type="radio" name="create_workers" value="1" autocomplete="off"  checked> Yes
                </label>
                <label class="btn btn-off">
                  <input type="radio" name="create_workers" value="0" autocomplete="off"> No
                </label>
              </div>
              <label>
                <small>
                  Allows the admin to create new desk workers. This must be set to 'Yes' if the user can create administrators<br/>
                  (Default: Yes)
                </small>
              </label>
            </div>

            <!-- Create Admins -->
            <div class="settingsGroup">
              <label>Create Admins</label>
              <div class="btn-group" data-toggle="buttons" style="float:right;">
                <label class="btn btn-on" id="create_admins_yes">
                  <input type="radio" name="create_admins" value="1" autocomplete="off"> Yes
                </label>
                <label class="btn btn-off active">
                  <input type="radio" name="create_admins" value="0" autocomplete="off" checked> No
                </label>
              </div>
              <label>
                <small>
                  Allows the admin to create more administrators.<br/>
                  (Default: No)
                </small>
              </label>
            </div>

          </div> <!-- End of Admin Permissions Section -->

        </div>
      </div> <!-- End Admin Permissions -->
    @endif


    <button type="submit" class="btn btn-primary form-control" id="submitBtn">Create Resident</button>
  {!! Form::close() !!}
@stop


@section('javascriptFooter')
  @if ($self->permissions->canCreateWorkers())
    <script type="text/javascript">
      $(document).ready(function(){
        $("#desk_worker_yes").on('click', function(){
          $("#desk_worker_permissions").slideDown(300);
        });
        $("#desk_worker_no").on('click', function(){
          $("#desk_worker_permissions").slideUp(300);
          $('#admin_no').click();
        });

      });
    </script>
  @endif

  @if ($self->permissions->canCreateAdmins())
    <script type="text/javascript">
      $(document).ready(function(){
        $("#admin_yes").on('click', function(){
          $("#admin_permissions").slideDown(300);
          $('#desk_worker_yes').click();
        });
        $("#admin_no").on('click', function(){
          $("#admin_permissions").slideUp(300);
        });


        $('#create_admins_yes').on('click', function(){
          $('#create_workers_yes').click();
        });

      });
    </script>
  @endif

@stop
