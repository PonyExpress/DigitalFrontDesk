@extends('admin.master')


@section('title')
New Building
@stop

@section('header')
  <style>
    .settingsGroup{border-bottom:1px solid #eee;padding-bottom:10px;padding-top:10px}
    .settingsGroup label{font-weight:normal;display:block;}
    .settingsGroup small{display:block;font-size:15px;color:#666;}
    .btn{border:1px solid #ccc!important;}
    .btn-on, .btn-off{background-color:#fff;color:#333;font-size:15px}
    .btn-off.active{background-color:#A94442!important;color:#fff;}
    .btn-on.active{background-color:#3C763D!important;color:#fff;}
    #existing_group_options{display:none;}
    #select_a_group{display:none;}
    #new_group{display:none;}
  </style>
@stop

@section('navbar')
  <li><a href="{{ URL::to('admin') }}">Admin Panel</a></li>
  <li class="sepratron"><a>/</a></li>
  <li><a href="{{ URL::to('admin/buildings') }}">Buildings</a></li>
  <li class="sepratron"><a>/</a></li>
  <li class="active"><a href="{{ URL::to('admin/buildings/create') }}">New Building<span class="sr-only">(current)</span></a></li>
@stop

@section('content')
  <div class="title">New Building</div>
  {!! Form::open(['name' => 'new_building_form']) !!}
    <div class="form-group">
      <label>Name</label>
      <input type="text" name="name" class="form-control" placeholder="Building Name" title="Building Name" value="{{ \Session::get('input_building_name', '') }}" required>
    </div>
    <div class="form-group">
      <label>Address</label>
      <input type="text" class="form-control" name="address" placeholder="Address" value="{{ \Session::get('input_building_address', '') }}" required>
    </div>


    <!-- Building Group Options -->
    <div class="panel panel-default displayPanel" style="min-width:100%;">
      <div class="panel-heading" id="building_options">
        <h3 class="panel-title" style="text-align:left">Group Options</h3>
      </div>
      <div class="panel-body" style="text-align:left">

        <div class="settingsGroup">
          <label>Part of a Group</label>
          <div class="btn-group" data-toggle="buttons" style="float:right;">
            <label class="btn btn-on" id="in_group_yes">
              <input type="radio" name="in_a_group" value="1" autocomplete="off"> Yes
            </label>
            <label class="btn btn-off active" id="in_group_no">
              <input type="radio" name="in_a_group" value="0" autocomplete="off" checked> No
            </label>
          </div>
          <label>
            <small>
              Is this building part of a group of buildings?<br/>
              (Default: No)<br/>
            </small>
          </label>
        </div>


      <div class="settingsGroup" id="existing_group_options">
          <label>Existing Group</label>
          <div class="btn-group" data-toggle="buttons" style="float:right;">
            <label class="btn btn-on active" id="existing_group_yes">
              <input type="radio" name="existing_group" value="1" autocomplete="off" checked> Yes
            </label>
            <label class="btn btn-off" id="existing_group_no">
              <input type="radio" name="existing_group" value="0" autocomplete="off" > No
            </label>
          </div>
          <label>
            <small>
              Select "No" to create a new group for this building?<br/>
              (Default: Yes)<br/>
            </small>
          </label>
      </div>

      <div class="settingsGroup" id="select_a_group">
          <label>Select a Group</label>
          <select class="form-control" name="group">
            @foreach($groups as $group)
              <option value="{{ $group->id }}">{{ $group->name }}</option>
            @endforeach
          </select>
          <label>
            <small>
              Please choose a group the building belongs to, or select "No" for "Existing Group" to create a new group.
            </small>
          </label>
      </div>

      <div class="settingsGroup" id="new_group">
          <label>Create a Group</label>
          <input type="text" name="new_group" class="form-control" placeholder="Group Name" title="Group Name">
          <label>
            <small>
              Please enter the name of what you would like to call the new group.
            </small>
          </label>
      </div>

    </div> <!-- End Building Group Options -->
  </div>



    <!-- Building Options -->
    <div class="panel panel-default displayPanel" style="min-width:100%;">
      <div class="panel-heading" id="building_options">
        <h3 class="panel-title" style="text-align:left">Building Options</h3>
      </div>
      <div class="panel-body" style="text-align:left">
        @if ($self->permissions->canCreateBuildings())
          <div class="settingsGroup">
            <label>Packages</label>
            <div class="btn-group" data-toggle="buttons" style="float:right;">
              <label class="btn btn-on active" id="packages_yes">
                <input type="radio" name="has_packages" value="1" autocomplete="off" checked> On
              </label>
              <label class="btn btn-off " id="desk_worker_no">
                <input type="radio" name="has_packages" value="0" autocomplete="off"> Off
              </label>
            </div>
            <label>
              <small>
                This options allows desk workers to log packages.<br/>
                (Default: On)<br/>
              </small>
            </label>
          </div>


          <div class="settingsGroup">
            <label>Residents</label>
            <div class="btn-group" data-toggle="buttons" style="float:right;">
              <label class="btn btn-on active" id="packages_yes">
                <input type="radio" name="has_residents" value="1" autocomplete="off" checked> On
              </label>
              <label class="btn btn-off " id="desk_worker_no">
                <input type="radio" name="has_residents" value="0" autocomplete="off"> Off
              </label>
            </div>
            <label>
              <small>
                This options allows the building to have a resident database.<br/>
                (Default: On)<br/>
              </small>
            </label>
          </div>


          <div class="settingsGroup">
            <label>Keys</label>
            <div class="btn-group" data-toggle="buttons" style="float:right;">
              <label class="btn btn-on active" id="packages_yes">
                <input type="radio" name="has_keys" value="1" autocomplete="off" checked> On
              </label>
              <label class="btn btn-off " id="desk_worker_no">
                <input type="radio" name="has_keys" value="0" autocomplete="off"> Off
              </label>
            </div>
            <label>
              <small>
                This options allows desk workers to loan out keys.<br/>
                (Default: On)<br/>
              </small>
            </label>
          </div>


          <div class="settingsGroup">
            <label>Items</label>
            <div class="btn-group" data-toggle="buttons" style="float:right;">
              <label class="btn btn-on active" id="packages_yes">
                <input type="radio" name="has_items" value="1" autocomplete="off" checked> On
              </label>
              <label class="btn btn-off " id="desk_worker_no">
                <input type="radio" name="has_items" value="0" autocomplete="off"> Off
              </label>
            </div>
            <label>
              <small>
                This options allows desk workers to loan out items.<br/>
                (Default: On)<br/>
              </small>
            </label>
          </div>


          <div class="settingsGroup">
            <label>Punch Clock</label>
            <div class="btn-group" data-toggle="buttons" style="float:right;">
              <label class="btn btn-on active" id="packages_yes">
                <input type="radio" name="has_punchClock" value="1" autocomplete="off" checked> On
              </label>
              <label class="btn btn-off " id="desk_worker_no">
                <input type="radio" name="has_punchClock" value="0" autocomplete="off"> Off
              </label>
            </div>
            <label>
              <small>
                This options allows desk workers to log the hours they work.<br/>
                (Default: On)<br/>
              </small>
            </label>
          </div>


          <div class="settingsGroup">
            <label>Message Log</label>
            <div class="btn-group" data-toggle="buttons" style="float:right;">
              <label class="btn btn-on active" id="packages_yes">
                <input type="radio" name="has_messageLog" value="1" autocomplete="off" checked> On
              </label>
              <label class="btn btn-off " id="desk_worker_no">
                <input type="radio" name="has_messageLog" value="0" autocomplete="off"> Off
              </label>
            </div>
            <label>
              <small>
                This options allows desk workers to enter messages for other desk workers to see.<br/>
                (Default: On)<br/>
              </small>
            </label>
          </div>


          <div class="settingsGroup">
            <label>GameTrax</label>
            <div class="btn-group" data-toggle="buttons" style="float:right;">
              <label class="btn btn-on" id="gametrax_yes">
                <input type="radio" name="has_gameTrax" value="1" autocomplete="off"> On
              </label>
              <label class="btn btn-off active" id="desk_worker_no">
                <input type="radio" name="has_gameTrax" value="0" autocomplete="off" checked> Off
              </label>
            </div>
            <label>
              <small>
                This options enables the GameTrax portal for this building, so students can track which items (e.g., pool/ping pong tables) are available for checkout.<br/>
                (Default: Off)<br/>
              </small>
            </label>
          </div>

        @else
          You do not have permission to create buildings
        @endif
      </div>
    </div> <!-- End Building Options -->


    <button type="submit" class="btn btn-primary form-control" id="submitBtn">Create Building</button>
  {!! Form::close() !!}
@stop


@section('javascriptFooter')

  <script type="text/javascript">
    function data_confirm_ok_click(url){window.location = url;}
    $('#in_group_yes').on('click', function(){
      $('#existing_group_options').slideDown(300);
      $('#existing_group_yes').click();

    });
    $('#in_group_no').on('click', function(){
      $('#existing_group_options').slideUp(300);
      $('#select_a_group').slideUp(300);
      $('#new_group').slideUp(300);
    });
    $('#existing_group_yes').on('click', function(){
      $('#new_group').slideUp(300);
      $('#select_a_group').slideDown(300);
    });
    $('#existing_group_no').on('click', function(){
      $('#select_a_group').slideUp(300);
      $('#new_group').slideDown(300);
    });
  </script>

@stop
