<?php
  $permissions = $resident->worker->permissions;
?>

<!-- Desk Worker Options -->
<div class="panel panel-default displayPanel" style="min-width:100%;">
  <div class="panel-heading" id="desk_worker_options">
    <h3 class="panel-title" style="text-align:left">Desk Worker Options</h3>
  </div>
  <div class="panel-body" style="text-align:left">
    @if ($self->permissions->canCreateWorkers())
      <div class="settingsGroup">
        <label>Make a Desk Worker</label>
        <div class="btn-group" data-toggle="buttons" style="float:right;">
          <label class="btn btn-on @if($resident->isDeskWorker()) active @endif" id="desk_worker_yes">
            <input type="radio" name="make_worker" value="1" autocomplete="off" @if($resident->isDeskWorker()) checked @endif> Yes
          </label>
          <label class="btn btn-off @if(!$resident->isDeskWorker()) active @endif" id="desk_worker_no">
            <input type="radio" name="make_worker" value="0" autocomplete="off" @if(!$resident->isDeskWorker()) checked @endif> No
          </label>
        </div>
        <label>
          <small>
            Changing this option to "Yes" will allow this resident access to the desk worker portal.<br/>
            (Default: No)<br/>
          </small>
        </label>
      </div>

      <div class="settingsGroup" id="desk_worker_permissions">

        <!-- New Packages -->
        <div class="settingsGroup">
          <label>Create Packages</label>
          <div class="btn-group" data-toggle="buttons" style="float:right;">
            <label class="btn btn-on @if($permissions->canCreatePackages()) active @endif">
              <input type="radio" name="new_packages" value="1" autocomplete="off" @if($permissions->canCreatePackages()) checked @endif> Yes
            </label>
            <label class="btn btn-off @if(!$permissions->canCreatePackages()) active @endif">
              <input type="radio" name="new_packages" value="0" autocomplete="off" @if(!$permissions->canCreatePackages()) checked @endif> No
            </label>
          </div>
          <label>
            <small>
              This permission option allows the desk worker to create new packages.<br/>
              (Default: Yes)
            </small>
          </label>
        </div>

        <!-- Signout Packages -->
        <div class="settingsGroup">
          <label>Signout Packages</label>
          <div class="btn-group" data-toggle="buttons" style="float:right;">
            <label class="btn btn-on @if($permissions->canSignOutPackages()) active @endif">
              <input type="radio" name="signout_packages" value="1" autocomplete="off" @if($permissions->canSignOutPackages()) checked @endif> Yes
            </label>
            <label class="btn btn-off @if(!$permissions->canSignOutPackages()) active @endif">
              <input type="radio" name="signout_packages" value="0" autocomplete="off" @if(!$permissions->canSignOutPackages()) checked @endif> No
            </label>
          </div>
          <label>
            <small>
              This permission option allows the desk worker to sign out packages to students.<br/>
              (Default: Yes)
            </small>
          </label>
        </div>

        <!-- Loan Keys -->
        <div class="settingsGroup">
          <label>Loan Keys</label>
          <div class="btn-group" data-toggle="buttons" style="float:right;">
            <label class="btn btn-on @if($permissions->canLoanKeys()) active @endif">
              <input type="radio" name="loan_keys" value="1" autocomplete="off" @if($permissions->canLoanKeys()) checked @endif> Yes
            </label>
            <label class="btn btn-off @if(!$permissions->canLoanKeys()) active @endif">
              <input type="radio" name="loan_keys" value="0" autocomplete="off" @if(!$permissions->canLoanKeys()) checked @endif> No
            </label>
          </div>
          <label>
            <small>
              This permission option allows the desk worker to loan out keys to residents.<br/>
              (Default: Yes)
            </small>
          </label>
        </div>

        <!-- Return Keys -->
        <div class="settingsGroup">
          <label>Return Keys</label>
          <div class="btn-group" data-toggle="buttons" style="float:right;">
            <label class="btn btn-on @if($permissions->canReturnKeys()) active @endif">
              <input type="radio" name="return_keys" value="1" autocomplete="off"  @if($permissions->canReturnKeys()) checked @endif> Yes
            </label>
            <label class="btn btn-off @if(!$permissions->canReturnKeys()) active @endif">
              <input type="radio" name="return_keys" value="0" autocomplete="off" @if(!$permissions->canReturnKeys()) checked @endif> No
            </label>
          </div>
          <label>
            <small>
              This permission option allows the desk worker to return keys loaned to residents.<br/>
              (Default: Yes)
            </small>
          </label>
        </div>

        <!-- Loan Items -->
        <div class="settingsGroup">
          <label>Loan Items</label>
          <div class="btn-group" data-toggle="buttons" style="float:right;">
            <label class="btn btn-on @if($permissions->canLoanItems()) active @endif">
              <input type="radio" name="loan_items" value="1" autocomplete="off" @if($permissions->canLoanItems()) checked @endif> Yes
            </label>
            <label class="btn btn-off @if(!$permissions->canLoanItems()) active @endif">
              <input type="radio" name="loan_items" value="0" autocomplete="off" @if(!$permissions->canLoanItems()) checked @endif> No
            </label>
          </div>
          <label>
            <small>
              This permission option allows the desk worker to loan items to residents.<br/>
              (Default: Yes)
            </small>
          </label>
        </div>

        <!-- Return Items -->
        <div class="settingsGroup">
          <label>Return Items</label>
          <div class="btn-group" data-toggle="buttons" style="float:right;">
            <label class="btn btn-on @if($permissions->canReturnItems()) active @endif">
              <input type="radio" name="return_items" value="1" autocomplete="off" @if($permissions->canReturnItems()) checked @endif> Yes
            </label>
            <label class="btn btn-off @if(!$permissions->canReturnItems()) active @endif">
              <input type="radio" name="return_items" value="0" autocomplete="off" @if(!$permissions->canReturnItems()) checked @endif> No
            </label>
          </div>
          <label>
            <small>
              This permission option allows the desk worker to return loaned items.<br/>
              (Default: Yes)
            </small>
          </label>
        </div>

        <!-- Message Log -->
        <div class="settingsGroup">
          <label>Message Log</label>
          <div class="btn-group" data-toggle="buttons" style="float:right;">
            <label class="btn btn-on @if($permissions->messageLogAccess()) active @endif">
              <input type="radio" name="message_log" value="1" autocomplete="off"  @if($permissions->messageLogAccess()) checked @endif> Yes
            </label>
            <label class="btn btn-off  @if(!$permissions->messageLogAccess()) active @endif">
              <input type="radio" name="message_log" value="0" autocomplete="off"  @if(!$permissions->messageLogAccess()) checked @endif> No
            </label>
          </div>
          <label>
            <small>
              This permission gives a desk worker access to the building message log for desk workers.<br/>
              (Default: Yes)
            </small>
          </label>
        </div>

        <!--Punch Clock-->
        <div class="settingsGroup">
          <label>Punch Clock</label>
          <div class="btn-group" data-toggle="buttons" style="float:right;">
            <label class="btn btn-on @if($permissions->punchClockAccess()) active @endif">
              <input type="radio" name="punch_clock" value="1" autocomplete="off" @if($permissions->punchClockAccess()) checked @endif> Yes
            </label>
            <label class="btn btn-off @if(!$permissions->punchClockAccess()) active @endif">
              <input type="radio" name="punch_clock" value="0" autocomplete="off" @if(!$permissions->punchClockAccess()) checked @endif> No
            </label>
          </div>
          <label>
            <small>
              This permission option allows the desk worker to create entries on their punch clock.<br/>
              (Default: Yes)
            </small>
          </label>
        </div>

        <!-- Send Emails -->
        <div class="settingsGroup">
          <label>Send Emails</label>
          <div class="btn-group" data-toggle="buttons" style="float:right;">
            <label class="btn btn-on @if($permissions->canSendEmails()) active @endif">
              <input type="radio" name="send_emails" value="1" autocomplete="off" @if($permissions->canSendEmails()) checked @endif> Yes
            </label>
            <label class="btn btn-off @if(!$permissions->canSendEmails()) active @endif">
              <input type="radio" name="send_emails" value="0" autocomplete="off" @if(!$permissions->canSendEmails()) checked @endif> No
            </label>
          </div>
          <label>
            <small>
              This permission option allows the desk worker to send emails to residents.<br/>
              (Default: Yes)
            </small>
          </label>
        </div>

      </div> <!-- End of Desk Worker Permissions -->

    @else
      You do not have permission to create deskworkers
    @endif
  </div>
</div> <!-- End Desk Worker Options -->




    <!-- Admin Options -->
    <div class="panel panel-default displayPanel" style="min-width:100%;">
      <div class="panel-heading" id="admin_options">
        <h3 class="panel-title" style="text-align:left">Administrator Options</h3>
      </div>
      <div class="panel-body" style="text-align:left">
        @if ($self->permissions->canCreateAdmins())
        <div class="settingsGroup">
          <label>Make an Administrator</label>
          <div class="btn-group" data-toggle="buttons" style="float:right;">
            <label class="btn btn-on " id="admin_yes">
              <input type="radio" name="make_admin" value="1" autocomplete="off"> Yes
            </label>
            <label class="btn btn-off active" id="admin_no">
              <input type="radio" name="make_admin" value="0" autocomplete="off" checked> No
            </label>
          </div>
          <label>
            <small>
              Changing this option to "Yes" will allow this resident access to the admin panel.<br/>
              (Default: No)<br/>
            </small>
          </label>
        </div>

        <div class="settingsGroup" id="admin_permissions">

          <!-- App Settings -->
          <div class="settingsGroup">
            <label>App Settings</label>
            <div class="btn-group" data-toggle="buttons" style="float:right;">
              <label class="btn btn-on">
                <input type="radio" name="prog_settings" value="1" autocomplete="off" > Yes
              </label>
              <label class="btn btn-off active">
                <input type="radio" name="prog_settings" value="0" autocomplete="off" checked> No
              </label>
            </div>
            <label>
              <small>
                Allows the admin the ability to change the application settings.<br/>
                (Default: No)
              </small>
            </label>
          </div>

          <!-- Create Residents -->
          <div class="settingsGroup">
            <label>Create Residents</label>
            <div class="btn-group" data-toggle="buttons" style="float:right;">
              <label class="btn btn-on  active">
                <input type="radio" name="create_residents" value="1" autocomplete="off"  checked> Yes
              </label>
              <label class="btn btn-off">
                <input type="radio" name="create_residents" value="0" autocomplete="off"> No
              </label>
            </div>
            <label>
              <small>
                Allows the admin to create new residents.<br/>
                (Default: Yes)
              </small>
            </label>
          </div>

          <!-- Create Desk Workers -->
          <div class="settingsGroup">
            <label>Create Desk Workers</label>
            <div class="btn-group" data-toggle="buttons" style="float:right;">
              <label class="btn btn-on  active" id="create_workers_yes">
                <input type="radio" name="create_workers" value="1" autocomplete="off"  checked> Yes
              </label>
              <label class="btn btn-off">
                <input type="radio" name="create_workers" value="0" autocomplete="off"> No
              </label>
            </div>
            <label>
              <small>
                Allows the admin to create new desk workers. This must be set to 'Yes' if the user can create administrators<br/>
                (Default: Yes)
              </small>
            </label>
          </div>

          <!-- Create Admins -->
          <div class="settingsGroup">
            <label>Create Admins</label>
            <div class="btn-group" data-toggle="buttons" style="float:right;">
              <label class="btn btn-on" id="create_admins_yes">
                <input type="radio" name="create_admins" value="1" autocomplete="off"> Yes
              </label>
              <label class="btn btn-off active">
                <input type="radio" name="create_admins" value="0" autocomplete="off" checked> No
              </label>
            </div>
            <label>
              <small>
                Allows the admin to create more administrators.<br/>
                (Default: No)
              </small>
            </label>
          </div>

        </div> <!-- End of Admin Permissions Section -->

        @endif

      </div>
    </div> <!-- End Admin Permissions -->




@section('javascriptFooter')
  @if ($self->permissions->canCreateWorkers())
    <script type="text/javascript">
      $(document).ready(function(){
        $("#desk_worker_yes").on('click', function(){
          $("#desk_worker_permissions").slideDown(300);
        });
        $("#desk_worker_no").on('click', function(){
          $("#desk_worker_permissions").slideUp(300);
          $('#admin_no').click();
        });

      });
    </script>
  @endif

  @if ($self->permissions->canCreateAdmins())
    <script type="text/javascript">
      $(document).ready(function(){
        $("#admin_yes").on('click', function(){
          $("#admin_permissions").slideDown(300);
          $('#desk_worker_yes').click();
        });
        $("#admin_no").on('click', function(){
          $("#admin_permissions").slideUp(300);
        });

        $('#create_admins_yes').on('click', function(){
          $('#create_workers_yes').click();
        });

      });
    </script>
  @endif

@stop
