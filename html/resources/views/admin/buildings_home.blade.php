@extends('admin.master')

@section('title')
Buildings
@stop

@section('navbar')
  <li><a href="{{ URL::to('admin') }}">Admin Panel</a></li>
  <li class="sepratron"><a>/</a></li>
  <li class="active"><a href="{{ URL::to('admin/buildings') }}">Buildings<span class="sr-only">(current)</span></a></li>
@stop


@section('content')
  <div class="title">Buildings</div>
  <br/>
  @if (Auth::user()->admin->permissions->canCreateBuildings())
  <a href="{{ URL::to('admin/buildings/create') }}"><button name="createBuilding" class="btn btn-primary">New Building</button></a>
  <br/><br/>
  @endif
  <table class="responsiveTable zebra">
    <thead>
        <tr>
          <th class="name">Name</th>
          <th class="date">Address</th>
        </tr>
      </thead>
      <tbody class="clickableRows clickableKeyRows hoverRows">
        @foreach ($buildings as $building)
          <tr href="{{ URL::to('admin/buildings/' . $building->uID) }}">
            <td class="name">{{ $building->name }}</td>
            <td class="date">{{ $building->address }}</td>
          </tr>
        @endforeach

      </tbody>
  </table>

  <br/>

@stop

@section('javascriptFooter')

@stop
