<!-- New Message Log Posts -->
<div class="col-sm-4">
  <div class="tile" color="purple">
    <span class="bigSubText">34</span>
    <span class="subtext">New Messages</span>
  </div>
</div>

<!-- New Packages -->
<div class="col-sm-4">
  <div class="tile" color="blue">
    <span class="bigSubText">34</span>
    <span class="subtext">New Packages</span>
  </div>
</div>

<!-- New Emails Sent -->
<div class="col-sm-4">
  <div class="tile" color="orange">
    <span class="bigSubText">34</span>
    <span class="subtext">New Emails</span>
  </div>
</div>

<!-- Loaned Keys -->
<div class="col-sm-4">
  <div class="tile" color="red">
    <span class="bigSubText">34</span>
    <span class="subtext">Loaned Keys</span>
  </div>
</div>

<!-- Loaned Items -->
<div class="col-sm-4">
  <div class="tile" color="teal">
    <span class="bigSubText">34</span>
    <span class="subtext">Loaned Items</span>
  </div>
</div>
