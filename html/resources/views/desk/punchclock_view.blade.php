@extends('desk.master')


@section('title')
Punch Clock Entry
@stop

@section('navbar')
<li><a href="{{ URL::to('desk') }}">Front Desk</a></li>
<li class="sepratron"><a>/</a></li>
<li><a href="{{ URL::to('desk/punchclock') }}">Punch Clock</a></li>
<li class="sepratron"><a>/</a></li>
  <li class="active"><a href="{{ URL::to('desk/punchclock/' . $entry->id) }}">Entry #{{ $entry->id }}<span class="sr-only">(current)</span></a></li>
@stop

<?php
  $diff = $entry->startDateTime()->diffInHours($entry->endDateTime());
  $diff += ($entry->startDateTime()->diffInMinutes($entry->endDateTime()) % 60) / 60;
?>

@section('content')
  <div class="title">Punch Clock Entry #{{ $entry->id }}</div>

  <table class="responsiveTable zebra">

    <tr>
      <td>Start</td>
      <td>{{ $entry->startDate }}</td>
    </tr>
    <tr>
      <td>End</td>
      <td>{{ $entry->endDate }}</td>
    </tr>
    <tr>
      <td>Time Worked</td>
      <td>{{ $diff }} hours</td>
    <tr/>


  </table>

<br><br>

@stop
