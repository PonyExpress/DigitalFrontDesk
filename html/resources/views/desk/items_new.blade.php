@extends('desk.master')


@section('title')
Loan an Item
@stop

@section('navbar')
<li><a href="{{ URL::to('desk') }}">Front Desk</a></li>
<li class="sepratron"><a>/</a></li>
<li><a href="{{ URL::to('desk/items') }}">Item Log</a></li>
<li class="sepratron"><a>/</a></li>
<li class="active"><a href="{{ URL::to('desk/items/create') }}">Loan Item<span class="sr-only">(current)</span></a></li>
@stop


@section('content')
  <div class="title">Item Loan</div>
  <br/>
  {!! Form::open(['name' => 'new_item_loan_form']) !!}
    {!! Form::label('resident', 'Resident') !!}
    @include('global.resident_table')


    <div class="form-group" id="item_list">
      {!! Form::label('item', 'Item') !!}
        <input type="text" id="item_search" class="form-control search" placeholder="Search for an item by name">
      {!! Form::input('hidden', 'item_id', null, ['id' => 'selected_item', 'style' => 'height:0px;width:0px;display:hidden', 'required' => '']) !!}
      <br/>
      <table class="scroll zebra">
        <thead>
          <tr>
            <th class="hidden">ID</th>
            <th class="name sort" data-sort="item_search_name">Name</th>
            <th class="building sort" data-sort="item_search_building">Building</th>
           </tr>
        </thead>
        <tbody class="clickableRows hoverRows clickableItemRow list" id="item_table_body">

        </tbody>
      </table>
      <div id="item_loading">
        <img src="{{ URL::to('assets/desk/images/loading.gif') }}" />
      </div>
    </div>


    <div class="form-group">
    <input class="btn btn-primary form-control" type="submit" value="Loan Item" id="formSubmit" disabled="true">
    </div>

  {!! Form::close() !!}
@stop


@section('javascriptFooter')
  <script type="text/javascript">
    var selected_item = 0;

    // Ajax load items
    $(document).ready(function(){
      $.ajax({
        type: 'GET',
        url: '{{ URL::to('desk/items/all/html') }}',
        dataType: 'html'
      }).done(function(data){
        // Add html to the tabel
        $('#item_table_body').html(data);

        // Remove the loading gif
        $('#item_loading').html('');

        // JQuery for row selection
        $('.clickableItemRow[item]').on('click', function(){
          if ( selected_item != 0 && $(this).attr('item') == selected_item)
          {
            // Same row was selected twice, unselect it
            $('.clickableItemRow[selected]').removeAttr('selected');
            selected_item = 0;

            // Clear the selected residents value
            $('#selected_item').attr('value', '');

            // Disable the new package button
            $('#formSubmit').prop('disabled', true);
          }
          else
          {
            // New selected row, mark it
            $('.clickableItemRow[selected]').removeAttr('selected');
            selected_item = $(this).attr('item');
            $(this).attr('selected', 'selected');

            // Add the resident's ID to the selected resident form element
            $('#selected_item').attr('value', $(this).attr('item'));

            // Enable the submit button
            $('#formSubmit').prop('disabled', false);
          }
        });
      });
    }); // end Ajax request
  </script>

  <script type="text/javascript" src="{{ URL::to('js/load_residents.js') }}"></script>
@stop
