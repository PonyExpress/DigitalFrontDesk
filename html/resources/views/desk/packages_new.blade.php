@extends('desk.master')

@section('title')
New Package
@stop

@section('navbar')
<li><a href="{{ URL::to('desk') }}">Front Desk</a></li>
<li class="sepratron"><a>/</a></li>
<li><a href="{{ URL::to('desk/packages') }}">Package Log</a></li>
<li class="sepratron"><a>/</a></li>
<li class="active"><a href="{{ URL::to('desk/packages/create') }}">New Package<span class="sr-only">(current)</span></a></li>
@stop

@section('content')
  <div class="title">New Package</div>
    <h1 id="package_id_text">ID: <span id="package_id_number">{{$maxID % 500}}</span></h1>
  {!! Form::open(['name' => 'new_package_form']) !!}

      <input type="hidden" name="package_number" style="height:0px;width:0px;display:none;" value="{{ $maxID % 500 }}">
      <input type="hidden" name="another_package" style="height:0px;width:0px;display:none;" value="true">
      {!! Form::label('resident', 'Resident') !!}
      @include('global.resident_table')

    <div class="form-group">
      {!! Form::label('package_type', 'Package Type') !!}
      {!! Form::select('package_type', ['1' => 'Box', '2' => 'Envelope', '3' => 'Other'], null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group checkbox" title="Uncheck this box if you are only entering in one package">
      <label>
        <input type="checkbox" name="anotherPackage" checked="true">Add another package</label>
    </div>

    <div class="form-group">
    <input class="btn btn-primary form-control" type="submit" value="Create Package" id="formSubmit" disabled="true">
    </div>

  {!! Form::close() !!}
@stop


@section('javascriptFooter')
  <script type="text/javascript">
    var selected = 0;
    // Form submission (to avoid page reload because this page takes a while since it has so many residents to load)
    $(document).ready(function(){
      $('form[name=new_package_form]').submit(function(event){
        event.preventDefault();

        $.ajax({
          type: 'POST',
          url: '/desk/packages/create',
          data: $('form[name=new_package_form]').serialize(),
          dataType: 'json'

        }).done(function(data){
          if ($('input[name=anotherPackage]').prop('checked'))
          {
            // Give success message
            $('errors').html('<div class="alert alert-success dismisses" role="alert" style="display:none">Package succesfully logged! &#x1F60E;</div>');

            // Make sure the package number was not wrong
            if ($('#package_id_number').html() != data.package_number)
            {
              // It was wrong...
              $('errors').html($('errors').html() + '<div class="alert alert-info" role="alert" style="display:none">Sorry about that!<br/>It looks like the last package ID was changed to <strong>' + data.package_number + '</strong>, not ' + $('#package_id_number').html() + '.');
            }

            // Change the package id to the next number
            $('#package_id_text').html('ID: <span id="package_id_number">' + ((data.packageID + 1) % 500) + '</span>');
            $('input[name=package_number]').val((data.packageID + 1) % 500);

            // Scroll to top and display message(s)
            $('html, body').animate({scrollTop: 0}, "slow", function(){
               $('errors .alert').slideDown(800);
            });

            // Reset the from
            formReset();

            // Bring focus back to the search box
            $('#resident_search').focus();
          }else{
            // No more packages, redirect to the packages page
            window.location.href = "{{ URL::to('desk/packages') }}";
          }
        }).fail(function(data, msg){
          $('errors').html('<div class="alert alert-danger .alert-dismissible" role="alert" style="display:none"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Package not created  &#x1F631;</div>');

          // Scroll to top
          $('html, body').animate({scrollTop:0}, "slow", function(){
            // Make error visible
            $('errors .alert').slideDown(800);
          });

          formReset();
        });
      });

      // Resets the form
      function formReset(){
        $('#resident_search').val('').keyup(); // Reset the resident search and show all the residents again
        $('#selected_resident').val('');      // Reset the actual selected resident
        $('tr[selected]').removeAttr('selected');  // Remove attribute on selected resident
        $('#packageType').val(1);            // Reset the package type
        $('#formSubmit').attr('disabled', 'true');
      }
    });

    // Input box checked
    $('input[name=anotherPackage]').click(function(){
      $('input[name=another_package]').val(this.checked);
    });

    $('input[name=another_package]').val($('input[name=anotherPackage]').prop('checked'));


  </script>
  <script type="text/javascript" src="{{ URL::to('js/load_residents.js') }}"></script>

  <script type="text/javascript">
    function stopRKey(evt) {
      var evt = (evt) ? evt : ((event) ? event : null);
      var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
      if ((evt.keyCode == 13) && (node.type=="text"))  {return false;}
    }
    document.onkeypress = stopRKey;
  </script>
@stop
