@extends('desk.master')


@section('title')
Message Log
@stop

@section('navbar')
  <li><a href="{{ URL::to('desk') }}">Front Desk</a></li>
  <li class="sepratron"><a>/</a></li>
 <li class="active"><a href="{{ URL::to('desk/messages') }}">Message Log<span class="sr-only">(current)</span></a></li>
@stop

@section('content')
<div class="title">Message Log</div>
<a href="{{ URL::to('desk/messages/create') }}"><button name="newMessage" class="btn btn-primary">New Message</button></a><br/><br/>
<table class="zebra">
  <thead class="responsiveTable zebra">
      <tr>
        <th >Poster</th>
        <th style="width:400px">Message</th>
        <th class="date">Date</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($messages as $msg)
        <tr @if(!$msg->hasViewed()) style="background-color: #FCF8E3" @endif>
          <td>{{ $msg->poster->initials }}</td>
          <td>{{ $msg->message }}</td>
          <td class="date">{{ $msg->datePosted }}</td>
        </tr>

        @if(!$msg->hasViewed())
          {{ $msg->markAsViewed() }}
        @endif
      @endforeach

    </tbody>
</table>
@stop
