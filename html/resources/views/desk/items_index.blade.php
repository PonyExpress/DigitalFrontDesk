@extends('desk.master')


@section('title')
GameTrax - Digital Front Desk
@stop

@section('navbar')
  <li><a href="{{ URL::to('desk') }}">Front Desk</a></li>
  <li class="sepratron"><a>/</a></li>
 <li class="active"><a href="{{ URL::to('desk/items') }}">Item Log<span class="sr-only">(current)</span></a></li>
@stop

@section('content')
<div class="title">Item Log</div>
<a href="{{ URL::to('desk/items/all') }}"><button name="availableItems" class="btn btn-primary">Available Items</button></a>
<a href="{{ URL::to('desk/items/create') }}"><button name="newItemLoan" class="btn btn-primary">Loan an Item</button></a>
<br/><br/>
<br/>
<table class="responsiveTable zebra">
  <thead>
      <tr>
        <th class="name">Name</th>
        <th>Item</th>
        <th>Building</th>
        <th class="date">Date</th>
      </tr>
    </thead>
    <tbody class="clickableRows hoverRows">
      @foreach ($items as $entry)
        <tr item="{{ $entry->id }}">
          <td class="name">{{ $entry->resident->informalName }}</td>
          <td>{{ $entry->item->name }}</td>
          <td>{{ $entry->building->shortName }}</td>
          <td class="date">{{ $entry->dateOut }}</td>
        </tr>
      @endforeach

    </tbody>
</table>

@stop
