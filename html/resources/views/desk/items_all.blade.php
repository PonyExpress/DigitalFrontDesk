@extends('desk.master')


@section('title')
GameTrax - Digital Front Desk
@stop

@section('navbar')
  <li><a href="{{ URL::to('desk') }}">Front Desk</a></li>
  <li class="sepratron"><a>/</a></li>
  <li><a href="{{ URL::to('desk/items') }}">Items</a></li>
  <li class="sepratron"><a>/</a></li>
  <li class="active"><a href="{{ URL::to('desk/items/all') }}">All Items<span class="sr-only">(current)</span></a></li>
@stop

@section('content')
<div class="title">Building Items</div>
<a href="{{ URL::to('desk/items/create') }}"><button name="newItemLoan" class="btn btn-primary">Loan an Item</button></a>
<br/><br/>
<br/>
<table class="responsiveTable zebra">
  <thead>
      <tr>
        <th class="name">Item</th>
        <th>Quantity</th>
        <th>Available</th>
      </tr>
    </thead>
    <tbody class="clickableRows hoverRows">
      @foreach ($items as $item)
        <tr href="{{ URL::to('desk/items/view/' . $item->item->uID) }}">
          <td class="name">{{ $item->item->name }}</td>
          <td>{{ $item->quantity }}</td>
          <td>{{ $item->available }}</td>
        </tr>
      @endforeach

    </tbody>
</table>

@stop
