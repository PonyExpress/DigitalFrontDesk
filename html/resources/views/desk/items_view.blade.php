@extends('desk.master')


@section('title')
#{{ $entry->id}} - {{ $entry->item->name }}
@stop

@section('header')
  <!-- Javascript for signature pad Topaz Systems Inc. T-L460-HSB-R -->
  <script type="text/javascript" src="{{ URL::to('js/signature.js') }}"></script>
@stop

@section('navbar')
<li><a href="{{ URL::to('desk') }}">Front Desk</a></li>
<li class="sepratron"><a>/</a></li>
<li><a href="{{ URL::to('desk/items') }}">Item Log</a></li>
<li class="sepratron"><a>/</a></li>
  <li class="active"><a href="{{ URL::to('desk/items/' . $entry->id) }}">#{{ $entry->id}} - {{ $entry->item->name }}<span class="sr-only">(current)</span></a></li>
@stop

@section('content')
  <div class="title">Item Log #{{ $entry->id }} @if ($entry->isReturned()) History @endif</div>

  <table class="responsiveTable zebra">
    <tr>
      <td>Resident</td>
      <td><a href="{{ URL::to('desk/residents/'.$entry->resident_id) }}">{{ $entry->resident->informalName }}</a></td>
   </tr>
    <tr>
      <td>Address</td>
      <td>{{ $entry->resident->address }}</td>
    </tr>
   <tr>
      <td>Item</td>
      <td><a href="{{ URL::to('desk/items/view/'.$entry->config->uID) }}">{{ $entry->item->name }}</a></td>
    </tr>
    <tr>
      <td>Checked Out</td>
      <td>{{ $entry->dateOut }}</td>
    </tr>
    <tr>
      <td>Desk Worker Out</td>
      <td>{{ $entry->workerOut->initials }}</td>
    </tr>


    @if ($entry->isReturned())
    <tr>
      <td>Returned</td>
      <td>{{ $entry->dateIn }}</td>
    </tr>
    <tr>
      <td>Desk Worker In</td>
      <td>{{ $entry->workerIn->initials }}</td>
    </tr>
    <tr>
      <td>Signature</td>
      <td><img src="{{ URL::to('api/img/signatures/items/'.$entry->id.'/in') }}" width="500" height="100"/></td>
    </tr>
    @else
    <tr>
      <td>Signature</td>
      <td><canvas id="cnv" width="500" height="100"></canvas>
      <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAzUlEQVQ4jZ2TUQ3DMAxEH4RACIRCKIRCCIRCKINAKIRCKIRCGIRA2D5iN11qb9NOipRK58vZvsIdARiBGcjACixANLg3jMADeDonfypeOnIBDkPwEJcDsGnx3BEmo63ciRe5Ey8fqu5hM9o6Xy9iy0IQcWsup+rmFCsGHAdJXPR9e0i8D5Ykx7PvQVtn57cWLASo+y+i+Dc0vusXXsJJY6INxXMSaHnZLUKmTjcIYaclUDOSqHmInsWBuk51E2m7P4TjFl8x0X7hWe7mml/oHFUgJAYHTwAAAABJRU5ErkJggg==" title="Redo Signature" style="padding-left:2px;margin-top:-165px;cursor:pointer" onclick="javascript:startTablet()"/>
      <br/><div id="signMessage" style="color:#FF0000">Please wait!</div></td>
     </tr>
    @endif
  </table>

   @if (!$entry->isReturned())
    {!! Form::open(['url' => 'desk/items/'.$entry->id.'/return']) !!}
      <input type="hidden" name="ref" value="{{ Request::query('ref', '') }}" readonly></input>
      <textarea name="signatureValue" id="signatureValue" rows="0" cols="0" style="visibility:hidden;width:0px;height:0px;"></textarea>
      <br/>
      <input class="btn btn-primary form-control" type="submit" value="Return Item" id="formSubmit">
    {!! Form::close() !!}
  @endif
<br><br>

@stop
