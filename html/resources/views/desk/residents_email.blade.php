@extends('desk.master')


@section('title')
Email {{ $resident->informalName }}
@stop


@section('navbar')
  <li><a href="{{ URL::to('desk') }}">Front Desk</a></li>
  <li class="sepratron"><a>/</a></li>
  <li><a href="{{ URL::to('desk/residents') }}">Residents</a></li>
  <li class="sepratron"><a>/</a></li>
  <li><a href="{{ URL::to('desk/residents/' . $resident->id) }}">{{ $resident->formalName }}</a></li>
  <li class="sepratron"><a>/</a></li>
  <li class="active"><a href="{{ URL::to('desk/residents/'.$resident->id.'/email') }}">New Email<span class="sr-only">(current)</span></a></li>
@stop


@section('content')
  <div class="title">New Email</div>
  {!! Form::open(['name' => 'new_email_form']) !!}
    <input type="hidden" name="resident_id" value="{{ $resident->id }}" style="width:0px;display:none;" radonly>
    <div class="form-group">
      <label>To:</label>
      <input type="text" class="form-control" placeholder="username@mst.edu" value="{{ $resident->formalName }} &lt;{{ $resident->email }}&gt;" readonly>
    </div>
    <div class="form-group">
      <label>Subject</label>
      <input type="text" class="form-control" name="subject" placeholder="Packages available to pick up" value="{{ $subject }}">
    </div>
    <div class="form-group">
      <label>Body</label>
      <textarea class="form-control" rows="10" cols="100" name="body" >{{ $body }}</textarea>
    </div>
    <button type="submit" class="btn btn-primary form-control" id="submitBtn" try="1">Send</button>
  {!! Form::close() !!}
  <br/><br/><br/>
@stop


@section('javascriptFooter')
<script>
$("form[name=new_email_form]").submit(function(){return"1"==$("#submitBtn").attr("try")?($("errors").html('<div class="alert alert-info" role="alert" style="display:none" id="checkAgain">Please review the email, then hit the submit button again.</div>'),$("html, body").animate({scrollTop:0},"slow",function(){$("#checkAgain").slideDown(800)}),$("#submitBtn").attr("try","2"),!1):void 0});
</script>
@stop
