@extends('desk.master')

@section('title')
Punch Clock
@stop

@section('navbar')
<li><a href="{{ URL::to('desk') }}">Front Desk</a></li>
<li class="sepratron"><a>/</a></li>
<li><a href="{{ URL::to('desk/punchclock') }}">Punch Clock</a></li>
<li class="sepratron"><a>/</a></li>
<li class="active"><a href="{{ URL::to('desk/punchclock/create') }}">New Entry<span class="sr-only">(current)</span></a></li>
@stop

@section('header')
<link href="{{ URL::to('assets/desk/css/date-picker.min.css') }}" rel="stylesheet">
@stop

@section('content')
  <div class="title">New Time Entry</div>
  {!! Form::open(['name' => 'new_timelog_form']) !!}
    <div class="form-group">
      {!! Form::label('', 'Start Time') !!}<br/>
      <input id="date_picker" class="datepicker form-control" name="start_date" type="text" autofocuss value="{{ Carbon\Carbon::today()->format('d F, Y') }}" data-valuee="{{ Carbon\Carbon::today()->format('m-d-Y') }}" style="margin-bottom:20px">
      <?php
        $time = NULL;
        $time2 = NULL;
        if (date('i') >= 15 && date('i') < 45)
        {
          $time = date('h:30 A');
          $time2 = date('H:30 A');
        }
        else
        {
          $time = date('h:00 A');
          $time2 = date('H:00 A');
        }

      ?>
      <input id="start_time" class="timepicker form-control" name="start_time" type="text" autofocuss value="{{ $time }}" data-valuee="{{ $time2 }}">
    </div>


    <div class="form-group">
      {!! Form::label('', 'End Time') !!}<br/>
      <input id="date_picker" class="datepicker form-control" name="end_date" type="text" autofocuss value="{{ Carbon\Carbon::today()->format('d F, Y') }}" data-valuee="{{ Carbon\Carbon::today()->format('m-d-Y') }}" style="margin-bottom:20px">
      <input id="end_time" class="timepicker form-control" name="end_time" type="text" autofocuss value="{{ $time }}" data-valuee="{{ $time2 }}">
    </div>

    <div class="form-group">
      <input class="btn btn-primary form-control" type="submit" value="Submit Time" id="formSubmit">
    </div>

  {!! Form::close() !!}


  <div id="datepickercontainer"></div>
@stop


@section('javascriptFooter')
  <script type="text/javascript" src="{{ URL::to('js/date.js') }}"></script>
  <script>
$(function() {

        var $input = $( '.datepicker' ).pickadate({
            formatSubmit: 'mm/dd/yyyy',
            // min: [2015, 7, 14],
            container: '#datepickercontainer',
            // editable: true,
            closeOnSelect: false,
            closeOnClear: false,
        });

        var picker = $input.pickadate('picker');

        var $time_input = $( '.timepicker' ).pickatime({});;
       var time_picker = $time_input.pickatime('picker');

       $('.datepicker').removeAttr('readonly');
       $('.timepicker').removeAttr('readonly');
});
</script>
@stop
