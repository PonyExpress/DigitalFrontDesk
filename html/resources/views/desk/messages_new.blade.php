@extends('desk.master')


@section('title')
New Message
@stop


@section('navbar')
  <li><a href="{{ URL::to('desk') }}">Front Desk</a></li>
  <li class="sepratron"><a>/</a></li>
  <li><a href="{{ URL::to('desk/messages') }}">Message Log</a></li>
  <li class="sepratron"><a>/</a></li>
  <li class="active"><a href="{{ URL::to('desk/messages/create') }}">New Message<span class="sr-only">(current)</span></a></li>
@stop


@section('content')
  <div class="title">New Message</div>
  {!! Form::open(['name' => 'new_message_form']) !!}
    <div class="form-group">
      <label>Enter Message</label>
      <textarea class="form-control" rows="10" cols="100" name="message" ></textarea>
    </div>
    <button type="submit" class="btn btn-primary form-control" id="submitBtn" try="1">Submit</button>
  {!! Form::close() !!}
  <br/><br/><br/>
@stop


@section('javascriptFooter')
<script>
$("form[name=new_email_form]").submit(function(){return"1"==$("#submitBtn").attr("try")?($("errors").html('<div class="alert alert-info" role="alert" style="display:none" id="checkAgain">Please review the message, then hit the submit button again.</div>'),$("html, body").animate({scrollTop:0},"slow",function(){$("#checkAgain").slideDown(800)}),$("#submitBtn").attr("try","2"),!1):void 0});
</script>
@stop
