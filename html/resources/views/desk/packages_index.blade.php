@extends('desk.master')


@section('title')
Packages - Digital Front Desk
@stop

@section('navbar')
  <li><a href="{{ URL::to('desk') }}">Front Desk</a></li>
  <li class="sepratron"><a>/</a></li>
 <li class="active"><a href="{{ URL::to('desk/packages') }}">Package Log<span class="sr-only">(current)</span></a></li>
@stop

@section('content')
<div class="title">Package Log</div>
<a href="{{ URL::to('desk/packages/create') }}"><button name="newPackage" class="btn btn-primary">New Package</button></a>
@if ($blasts > 0)
<button name="blastEmails" class="btn btn-primary">Send Notifications</button>
@endif

<br/><br/>
Click on a package to sign it out.
<br/>
<table class="responsiveTable zebra">
  <thead>
      <tr>
        <th>ID</th>
        <th class="name">Name</th>
        <th>Type</th>
        <th>Room</th>
        <th>Building</th>
        <th class="date">Date</th>
      </tr>
    </thead>
    <tbody class="clickableRows hoverRows">
      @foreach ($packages as $package)
        <tr package="{{ $package->id }}" <?php if($package->needsReminder()) {?> id="needsReminder" <?php } ?>>
          <td>{{ $package->number }}</td>
          <td class="name">{{ $package->resident->formalName }}</td>
          <td>{{ $package->packageType }}</td>
          <td>{{ $package->resident->room_number }}</td>
          <td>{{ $package->building->shortName }}</td>
          <td class="date">{{ $package->dateIn }}</td>
        </tr>
      @endforeach

    </tbody>
</table>


@stop
