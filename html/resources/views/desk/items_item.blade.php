@extends('desk.master')


@section('title')
{{ $entry->item->name }}
@stop

@section('navbar')
<li><a href="{{ URL::to('desk') }}">Front Desk</a></li>
<li class="sepratron"><a>/</a></li>
<li><a href="{{ URL::to('desk/items/all') }}">Items</a></li>
<li class="sepratron"><a>/</a></li>
<li class="active"><a href="{{ URL::to('desk/items/view/' . $entry->uID) }}">{{ $entry->item->name }}<span class="sr-only">(current)</span></a></li>
@stop

@section('content')
  <div class="title">{{ $entry->item->name }}</div>

  <table class="responsiveTable zebra">
    <tr>
      <td>Item</td>
      <td><a href="{{ URL::to('desk/items/view/'.$entry->uID) }}">{{ $entry->item->name }}</a></td>
   </tr>
    <tr>
      <td>Building</td>
      <td>{{ $entry->building->name }}</td>
    </tr>
   <tr>
      <td>Quantity</td>
      <td>{{ $entry->quantity }}</td>
    </tr>
    <tr>
      <td>Available</td>
      <td>{{ $entry->available }}</td>
    </tr>
  </table>

  @if(!is_null($loans))
  <br/>
    <div class="panel panel-default displayPanel">
      <div class="panel-heading" id="desk_worker_options">
        <h3 class="panel-title" style="text-align:left">Item Loans</h3>
      </div>
      <div class="panel-body" style="text-align:left">
        <table class="responsiveTable zebra">
          <thead>
              <tr>
                <th class="name">Resident</th>
                <th class="date">Date</th>
              </tr>
            </thead>
            <tbody class="clickableRows hoverRows">
              @foreach ($loans as $loan)
                <tr href="{{ URL::to('desk/items/' . $loan->id) }}">
                  <td class="name">{{ $loan->resident->informalName }}</td>
                  <td class="date">{{ $loan->dateOut }}</td>
                </tr>
              @endforeach

            </tbody>
        </table>
      </div>
    </div>
  @endif
<br><br>

@stop
