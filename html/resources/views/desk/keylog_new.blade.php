@extends('desk.master')

@section('title')
New Key Loan
@stop

@section('header')
  <!-- Javascript for signature pad Topaz Systems Inc. T-L460-HSB-R -->
  <script type="text/javascript" src="{{ URL::to('js/signature.js') }}"></script>
@stop



@section('navbar')
  <li><a href="{{ URL::to('desk') }}">Front Desk</a></li>
  <li class="sepratron"><a>/</a></li>
  <li><a href="{{ URL::to('desk/keys') }}">Key Log</a></li>
  <li class="sepratron"><a>/</a></li>
  <li class="active"><a href="{{ URL::to('desk/keys/create') }}">New Key Loan<span class="sr-only">(current)</span></a></li>
@stop

@section('content')
  <div class="title">Key Loan</div>

  {!! Form::open(['name' => 'new_key_form']) !!}
    <div class="form-group" id="key_list">
      {!! Form::label('key', 'key') !!}
      <input type="text" id="key_search" class="form-control search" placeholder="Search by a key number, room number, or suite number" autofocus>
      {!! Form::input('hidden', 'key_id', null, ['id' => 'selected_key', 'style' => 'height:0px;width:0px;display:hidden', 'required' => '']) !!}
      <br/>
      <table class="scroll zebra responsiveTable">
        <thead>
          <tr>
            <th class="hidden">ID</th>
            <th class="name sort" data-sort="key_search_label">ID</th>
            <th class="room sort" data-sort="key_search_room">Room</th>
            <th class="building sort" data-sort="key_search_building">Building</th>
           </tr>
        </thead>
        <tbody class="clickableKeyRow clickableRows hoverRows key_table list" id="key_table_body">

        </tbody>
      </table>
      <div id="key_loading">
        <img src="{{ URL::to('assets/desk/images/loading.gif') }}" />
      </div>

    </div>
    <br/><br/>
    {!! Form::label('resident', 'Resident') !!}
    @include('global.resident_table')

    <!-- Signature to sign out -->
    <div class="form-group">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Signature</h3>
        </div>
        <div class="panel-body">
          <td><canvas id="cnv" width="500" height="100"></canvas>
          <!-- Refresh Button -->
          <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAzUlEQVQ4jZ2TUQ3DMAxEH4RACIRCKIRCCIRCKINAKIRCKIRCGIRA2D5iN11qb9NOipRK58vZvsIdARiBGcjACixANLg3jMADeDonfypeOnIBDkPwEJcDsGnx3BEmo63ciRe5Ey8fqu5hM9o6Xy9iy0IQcWsup+rmFCsGHAdJXPR9e0i8D5Ykx7PvQVtn57cWLASo+y+i+Dc0vusXXsJJY6INxXMSaHnZLUKmTjcIYaclUDOSqHmInsWBuk51E2m7P4TjFl8x0X7hWe7mml/oHFUgJAYHTwAAAABJRU5ErkJggg==" title="Redo Signature" style="padding-left:2px;margin-top:-165px;cursor:pointer" onclick="javascript:startTablet()"/>
          <br/><div id="signMessage" style="color:#FF0000">Please wait!</div>
        </div>
      </div>

      <textarea name="signatureValue" id="signatureValue" rows="0" cols="0" style="visibility:hidden;width:0px;height:0px;" required></textarea>
    </div>

    <div class="form-group">
      <input class="btn btn-primary form-control" type="submit" value="Loan Key" id="formSubmit" >
    </div>

  {!! Form::close() !!}
@stop


@section('javascriptFooter')
  <script type="text/javascript">
    var selected_key = 0;

    $(document).ready(function(){
      // Ajax for loading keys from database
      $.ajax({
        type: 'GET',
        url: '{{ URL::to('desk/keys/all/html') }}',
        dataType: 'html'
      }).done(function(data){
        // Add html to the tabel
        $('#key_table_body').html(data);

        // Remove the loading gif
        $('#key_loading').html('');

        // JQuery for row selection
        $('.clickableKeyRow[key]').on('click', function(){
          if ( selected_key != 0 && $(this).attr('key') == selected_key)
          {
            // Same row was selected twice, unselect it
            $('.clickableKeyRow[selected]').removeAttr('selected');
            selected_key = 0;

            // Clear the selected residents value
            $('#selected_key').attr('value', '');

            // Disable the new package button
            $('#formSubmit').prop('disabled', true);
          }
          else
          {
            // New selected row, mark it
            $('.clickableKeyRow[selected]').removeAttr('selected');
            selected_key = $(this).attr('key');
            $(this).attr('selected', 'selected');

            // Add the resident's ID to the selected resident form element
            $('#selected_key').attr('value', $(this).attr('key'));


          }
        });
      });
    });


  </script>
  <script type="text/javascript" src="{{ URL::to('js/load_residents.js') }}"></script>
\\
@stop
