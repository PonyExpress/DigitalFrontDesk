@extends('desk.master')

@section('title')
Key {{ $keylog->key->label }}
@stop

@section('header')
  <!-- Javascript for signature pad Topaz Systems Inc. T-L460-HSB-R -->
  <script type="text/javascript" src="{{ URL::to('js/signature.js') }}"></script>
@stop

@section('navbar')
  <li><a href="{{ URL::to('desk') }}">Front Desk</a></li>
  <li class="sepratron"><a>/</a></li>
  <li><a href="{{ URL::to('desk/keys') }}">Key Log</a></li>
  <li class="sepratron"><a>/</a></li>
  <li><a href="{{ URL::to('desk/keys/' . $keylog->id) }}">{{ $keylog->key->label }}<span class="sr-only">(current)</span></a></li>
@stop

@section('content')
  <div class="title">Key {{ $keylog->key->label }} @if ($keylog->isReturned()) History @endif</div>

  <table class="responsiveTable zebra">
    <tr>
      <td>Resident</td>
      <td><a href="{{ URL::to('desk/residents/'.$keylog->resident->id) }}">{{ $keylog->resident->informalName }}</a></td>
   </tr>
    <tr>
      <td>Address</td>
      <td>{{ $keylog->resident->room_number }}&nbsp;{{ $keylog->resident->building->name }}</td>
    </tr>
   <tr>
      <td>Key ID</td>
      <td>{{ $keylog->key->label }}</td>
    </tr>
    <tr>
      <td>Key Room</td>
      <td>{{ $keylog->key->room }}</td>
    </tr>
    <tr>
      <td>Time Out</td>
      <td>{{ $keylog->dateOut }}</td>
    </tr>
    <tr>
      <td>Signature Out</td>
      <td>
        <img src="{{ $keylog->signatureOut->url }}" width="500" height="100"/>
      </td>
    </tr>
    <tr>
      <td>Desk Worker Out</td>
      <td>{{ $keylog->workerOut->initials }}</td>
    </tr>

    @if ($keylog->isReturned())
    <tr>
      <td>Time In</td>
      <td>{{ $keylog->dateIn }}</td>
    </tr>
    <tr>
      <td>Signature In</td>
      <td><img src="{{ $keylog->signatureIn->url }}" width="500" height="100"/></td>
    </tr>
    <tr>
      <td>Desk Worker In</td>
      <td>{{ $keylog->workerIn->initials }}</td>
    </tr>
    @else
    <!-- Sign out the package -->
      <tr>
        <td>Signature In</td>
        <td><canvas id="cnv" width="500" height="100"></canvas>
        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAzUlEQVQ4jZ2TUQ3DMAxEH4RACIRCKIRCCIRCKINAKIRCKIRCGIRA2D5iN11qb9NOipRK58vZvsIdARiBGcjACixANLg3jMADeDonfypeOnIBDkPwEJcDsGnx3BEmo63ciRe5Ey8fqu5hM9o6Xy9iy0IQcWsup+rmFCsGHAdJXPR9e0i8D5Ykx7PvQVtn57cWLASo+y+i+Dc0vusXXsJJY6INxXMSaHnZLUKmTjcIYaclUDOSqHmInsWBuk51E2m7P4TjFl8x0X7hWe7mml/oHFUgJAYHTwAAAABJRU5ErkJggg==" title="Redo Signature" style="padding-left:2px;margin-top:-165px;cursor:pointer" onclick="javascript:startTablet()"/>
        <br/><div id="signMessage" style="color:#FF0000">Please wait!</div></td>
      </tr>
    @endif
  </table>

  @if (!$keylog->isReturned())
    {!! Form::open(['url' => 'desk/keys/'.$keylog->id.'/return']) !!}
      <input type="hidden" name="ref" value="{{ Request::query('ref', '') }}" readonly></input>
      <textarea name="signatureValue" id="signatureValue" rows="0" cols="0" style="visibility:hidden;width:0px;height:0px;"></textarea>
      <br/>
      <input class="btn btn-primary form-control" type="submit" value="Return Key" id="formSubmit">
    {!! Form::close() !!}
  @endif
<br><br>

@stop
