@extends('desk.master')


@section('title')
Punch Clock
@stop

@section('navbar')
  <li><a href="{{ URL::to('desk') }}">Front Desk</a></li>
  <li class="sepratron"><a>/</a></li>
 <li class="active"><a href="{{ URL::to('desk/punchclock') }}">Punch Clock<span class="sr-only">(current)</span></a></li>
@stop

@section('content')
<div class="title">Punch Clock</div>
<a href="{{ URL::to('desk/punchclock/create') }}"><button name="newTimeEntry" class="btn btn-primary">New Time Entry</button></a>

<br/><br/>
<table class="responsiveTable zebra">
  <thead>
      <tr>
        <th class="date">Start Date</th>
        <th class="date">End Date</th>
        <th class="date">Hours Worked</th>
      </tr>
    </thead>
    <tbody class="clickableRows hoverRows">
      @foreach ($entries as $entry)
        <tr punch="{{ $entry->id }}">
          <td class="date">{{ $entry->startDate }}</td>
          <td class="date">{{ $entry->endDate }}</td>
          <?php
          $diff = $entry->startDateTime()->diffInHours($entry->endDateTime());
          $diff += ($entry->startDateTime()->diffInMinutes($entry->endDateTime()) % 60) / 60;
          ?>
          <td class="date">{{ $diff }}</td>
        </tr>
      @endforeach

    </tbody>
</table>


@stop
