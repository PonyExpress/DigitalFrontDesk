@extends('desk.master')


@section('title')
Package {{ $package->number }}
@stop

@section('header')
  <!-- Javascript for signature pad Topaz Systems Inc. T-L460-HSB-R -->
  <script type="text/javascript" src="{{ URL::to('js/signature.js') }}"></script>
@stop

@section('navbar')
<li><a href="{{ URL::to('desk') }}">Front Desk</a></li>
<li class="sepratron"><a>/</a></li>
<li><a href="{{ URL::to('desk/packages') }}">Package Log</a></li>
<li class="sepratron"><a>/</a></li>
  <li class="active"><a href="{{ URL::to('desk/packages/' . $package->id) }}">{{ $package->id }}<span class="sr-only">(current)</span></a></li>
@stop

@section('content')
  <div class="title">Package #{{ $package->number }} @if ($package->isPickedUp()) History @endif</div>

  <table class="responsiveTable zebra">
    <tr>
      <td>Resident</td>
      <td><a href="{{ URL::to('desk/residents/'.$package->resident_id) }}">{{ $package->resident->informalName }}</a></td>
   </tr>
    <tr>
      <td>Address</td>
      <td>{{ $package->resident->address }}</td>
    </tr>
   <tr>
      <td>Type</td>
      <td>{{ $package->packageType }}</td>
    </tr>
    <tr>
      <td>Arrived</td>
      <td>{{ $package->dateIn }} @if ($package->needsReminder()) <button type="button" class="btn btn-warning" name="packageReminder">Send Reminder</button> @endif</td>
    </tr>
    @if (!is_null($package->reminded_on))
    <tr>
      <td>Reminded On</td>
      <td>{{ $package->reminded_on }}</td>
    </tr>
    @endif
    <tr>
      <td>Desk Worker In</td>
      <td>{{ $package->workerIn->initials }}</td>
    </tr>


    @if ($package->isPickedUp())
    <tr>
      <td>Signed Out</td>
      <td>{{ $package->dateOut }}</td>
    </tr>
    <tr>
      <td>Desk Worker Out</td>
      <td>{{ $package->workerOut->initials }}</td>
    </tr>
    <tr>
      <td>Signature</td>
      <td><img src="{{ $package->signature->url }}" width="500" height="100"/></td>
    </tr>
    @else
    <tr>
      <td>Signature</td>
      <td><canvas id="cnv" width="500" height="100"></canvas>
      <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAzUlEQVQ4jZ2TUQ3DMAxEH4RACIRCKIRCCIRCKINAKIRCKIRCGIRA2D5iN11qb9NOipRK58vZvsIdARiBGcjACixANLg3jMADeDonfypeOnIBDkPwEJcDsGnx3BEmo63ciRe5Ey8fqu5hM9o6Xy9iy0IQcWsup+rmFCsGHAdJXPR9e0i8D5Ykx7PvQVtn57cWLASo+y+i+Dc0vusXXsJJY6INxXMSaHnZLUKmTjcIYaclUDOSqHmInsWBuk51E2m7P4TjFl8x0X7hWe7mml/oHFUgJAYHTwAAAABJRU5ErkJggg==" title="Redo Signature" style="padding-left:2px;margin-top:-165px;cursor:pointer" onclick="javascript:startTablet()"/>
      <br/><div id="signMessage" style="color:#FF0000">Please wait!</div></td>
     </tr>
    @endif
  </table>

   @if (!$package->isPickedUp())
    {!! Form::open(['url' => 'desk/packages/'.$package->id.'/signout']) !!}
      <input type="hidden" name="ref" value="{{ Request::query('ref', '') }}" readonly></input>
      <textarea name="signatureValue" id="signatureValue" rows="0" cols="0" style="visibility:hidden;width:0px;height:0px;"></textarea>
      <br/>
      <input class="btn btn-primary form-control" type="submit" value="Signout Package" id="formSubmit">
    {!! Form::close() !!}
  @endif
<br><br>

@stop
