@extends('desk.master')


@section('title')
Keys - Digital Front Desk
@stop

@section('navbar')
  <li><a href="{{ URL::to('desk') }}">Front Desk</a></li>
  <li class="sepratron"><a>/</a></li>
  <li class="active"><a href="{{ URL::to('desk/keys') }}">Key Log<span class="sr-only">(current)</span></a></li>
@stop

@section('content')
<div class="title">Key Log</div>
<a href="{{ URL::to('desk/keys/create') }}"><button name="newKey" class="btn btn-primary">New Key Loan</button></a>
<br/><br/>
Click on a key to return it
<br/>
<table class="responsiveTable zebra">
  <thead>
      <tr>
        <th>ID</th>
        <th class="name">Name</th>
        <th class="room">Room</th>
        <th>Building</th>
        <th class="date">Date</th>
      </tr>
    </thead>
    <tbody class="clickableRows clickableKeyRows hoverRows">
      @foreach ($keys as $keylog)
        <tr key="{{ $keylog->id }}">
          <td>{{ $keylog->key->label }}</td>
          <td class="name">{{ $keylog->resident->formalName }}</td>
          <td class="room">{{ $keylog->key->room }}</td>
          <td>{{ $keylog->building->name }}</td>
          <td class="date">{{ $keylog->dateOut }}</td>
        </tr>
      @endforeach

    </tbody>
</table>


@stop
