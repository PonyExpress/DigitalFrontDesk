@extends('desk.master')


@section('title')
Dollies - Digital Front Desk
@stop

@section('navbar')
  <li><a href="{{ URL::to('desk') }}">Front Desk</a></li>
  <li class="sepratron"><a>/</a></li>
  <li class="active"><a href="{{ URL::to('desk/dollies') }}">Dolly Log<span class="sr-only">(current)</span></a></li>
@stop

@section('content')
<div class="title">Dolly Log</div>
<a href="{{ URL::to('desk/dollies/create') }}"><button name="newDollies" class="btn btn-primary">New Dolly Loan</button></a>
<br/><br/>
Click on a dolly to return it
<br/>
<table class="responsiveTable zebra">
  <thead>
      <tr>
        <th>ID</th>
        <th class="name">Name</th>
        <th class="room">Room</th>
        <th>Building</th>
        <th class="date">Date</th>
      </tr>
    </thead>
    <tbody class="clickableRows clickableDollyRows hoverRows">
      @foreach ($dollies as $dollylog)
        <tr key="{{ $dollylog->id }}">
          <td>{{ $dollylog->dolly->label }}</td>
          <td class="name">{{ $dollylog->resident->formalName }}</td>
          <td class="room">{{ $dollylog->dolly->room }}</td>
          <td>{{ $dollylog->building->name }}</td>
          <td class="date">{{ $dollylog->dateOut }}</td>
        </tr>
      @endforeach

    </tbody>
</table>


@stop
