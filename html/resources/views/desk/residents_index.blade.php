@extends('desk.master')

@section('title')
Residents
@stop

@section('navbar')
  <li><a href="{{ URL::to('desk') }}">Front Desk</a></li>
  <li class="sepratron"><a>/</a></li>
  <li class="active"><a href="{{ URL::to('desk/residents') }}">Residents<span class="sr-only">(current)</span></a></li>
@stop


@section('content')
  <div class="title">Residents</div>
  <br/><br/>
  @include('global.resident_table')
@stop

@section('javascriptFooter')
  <script type="text/javascript" src="{{ URL::to('js/load_residents_click.js') }}"></script>
@stop
