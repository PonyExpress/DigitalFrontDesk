<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title')</title>

    @include('global.css')
    <link href="{{ URL::to('css/digitalfrontdesk.css') }}" rel="stylesheet">

		@yield('header')


    @include('global.javascript_header')

  </head>
  <body>
    <div id="body">
      @include('global.navbar')
      <div class="container">
        <div class="content">
          @include('global.errorSection')

          @yield('content')
        </div>
      </div>

    </div>
      @include('global.footer')

    @include('global.javascript_footer')
    <script src="{{ URL::to('assets/desk/js/digitalfrontdesk.min.js') }}"></script>

    @yield('javascriptFooter')
  </body>
</html>
