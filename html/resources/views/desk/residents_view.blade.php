@extends('desk.master')


@section('title')
{{ $resident->informalName }}
@stop


@section('navbar')
  <li><a href="{{ URL::to('desk') }}">Front Desk</a></li>
  <li class="sepratron"><a>/</a></li>
  <li><a href="{{ URL::to('desk/residents') }}">Residents</a></li>
  <li class="sepratron"><a>/</a></li>
  <li class="active"><a href="{{ URL::to('desk/residents/' . $resident->id) }}">{{ $resident->formalName }}<span class="sr-only">(current)</span></a></li>
@stop


@section('content')
  <div class="title">{{ $resident->nameWithInitial }}</div>
  <div class="residentInfo">
    {{ $resident->id }} <br/>
    {{ $resident->address }}<br/>
    <a href="{{ URL::to('desk/residents/'.$resident->id.'/email') }}" title="Email Resident">{{ $resident->email }}</a>
  </div>
  <br/>

  <!-- New Packages -->
  <div class="panel panel-default displayPanel" style="display:inline-block;margin-right:10px;">
    <div class="panel-heading">
      <h3 class="panel-title" style="text-align:left">New Packages</h3>
    </div>
    <div class="panel-body">
      <table class="responsiveTable zebra">
        <thead>
          <tr>
            <th>ID</th>
            <th>Type</th>
            <th class="date">Date</th>
          </tr>
        </thead>
        <tbody class="clickableRows hoverRows" style="min-height:150px">
          @foreach ($newPackages as $package)
            <tr href="{{ URL::to('desk/packages/' . $package->id) }}" ref="{{ $resident->id }}">
              <td>{{ $package->number }}</td>
              <td>{{ $package->packageType }}</td>
              <td class="date">{{ $package->dateIn }}</td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>

  <!-- Old Packages -->
  <div class="panel panel-default displayPanel" style="display:inline-block;margin-left:10px;">
    <div class="panel-heading">
      <h3 class="panel-title" style="text-align:left">Package History</h3>
    </div>
    <div class="panel-body">
      <table class="responsiveTable zebra">
        <thead>
          <tr>
            <th>ID</th>
            <th>Type</th>
            <th class="date">Date</th>
          </tr>
        </thead>
        <tbody class="clickableRows hoverRows" style="min-height:150px">
          @foreach ($oldPackages as $package)
            <tr href="{{ URL::to('desk/packages/' . $package->id) }}" ref="{{ $resident->id }}">
              <td>{{ $package->number }}</td>
              <td>{{ $package->packageType }}</td>
              <td class="date">{{ $package->dateIn }}</td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>

  <br/>

  <!-- Keys Loaned -->
  <div class="panel panel-default displayPanel" style="display:inline-block;margin-right:10px;">
    <div class="panel-heading">
      <h3 class="panel-title" style="text-align:left">Loaned Keys</h3>
    </div>
    <div class="panel-body">
      <table class="responsiveTable zebra">
        <thead>
          <th>ID</th>
          <th class="room">Room</th>
          <th class="date">Date</th>
        </thead>
        <tbody class="clickableRows clickableKeyRows hoverRows" style="min-height:150px">
          @foreach ($newKeys as $keylog)
            <tr href="{{ URL::to('desk/keys/' . $keylog->id) }}" ref="{{ $resident->id }}">
              <td>{{ $keylog->key->label }}</td>
              <td>{{ $keylog->key->room }}</td>
              <td class="date">{{ $keylog->dateOut }}</td>
            </tr>
          @endforeach
        </tbody>
      </table>

    </div>
  </div>

  <!-- Keys Loaned History -->
  <div class="panel panel-default displayPanel" style="display:inline-block;margin-left:10px;">
    <div class="panel-heading">
      <h3 class="panel-title" style="text-align:left">Key History</h3>
    </div>
    <div class="panel-body">
      <table class="responsiveTable zebra">
        <thead>
          <th>ID</th>
          <th class="room">Room</th>
          <th class="date">Date</th>
        </thead>
        <tbody class="clickableRows clickableKeyRows hoverRows" style="min-height:150px">
          @foreach ($oldKeys as $keylog)
            <tr href="{{ URL::to('desk/keys/' . $keylog->id) }}" ref="{{ $resident->id }}">
              <td>{{ $keylog->key->label }}</td>
              <td>{{ $keylog->key->room }}</td>
              <td class="date">{{ $keylog->dateOut }}</td>
            </tr>
          @endforeach
        </tbody>
      </table>

    </div>
  </div>

  <br/>

  <!-- Items Loaned -->
  <div class="panel panel-default displayPanel" style="display:inline-block;margin-right:10px;">
    <div class="panel-heading">
      <h3 class="panel-title" style="text-align:left">Loaned Items</h3>
    </div>
    <div class="panel-body">
      <table class="responsiveTable zebra">
        <thead>
          <th>Item</th>
          <th class="date">Date</th>
        </thead>
        <tbody class="clickableRows clickableKeyRows hoverRows" style="min-height:150px">
          @foreach ($newItems as $itemlog)
            <tr href="{{ URL::to('desk/items/' . $itemlog->id) }}" ref="{{ $resident->id }}">
              <td>{{ $itemlog->item->name }}</td>
              <td class="date">{{ $itemlog->dateOut }}</td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>




@stop
