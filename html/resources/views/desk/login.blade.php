@extends('desk.master')
@section('title')
DFD Login
@stop


@section('content')
	<div class="title">Please Sign In</div>
	<form name="login">
		  <input type="text" class="form-control" placeholder="name" name="email" >
			<br/>
			<input type="password" class="form-control" name="password">
			<br/>
			<button type="button" class="btn btn-default" name="login_btn">Login</button>

      <a href="{{ URL::to('desk/google/register') }}"><button type="button" class="btn btn-default" name="guest_bt" style="margin-left:20px">Create an Account</button></a>
	</form>

@stop
