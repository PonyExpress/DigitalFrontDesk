@extends('master')

@section('title')
DigitalFrontDesk
@stop

@section('content')
<div class="title">Missouri S&T Digital Front Desk</div>
<br/><br/>
<ul style="font-size:30px!important">
  <li><a href="/desk">Desk Worker Portal</a></li>
  <li><a href="/student">Student Desk Portal</a></li>
</ul>

  @if ($loggedOut == 'true')
    <iframe src="https://accounts.google.com/logout" style="display:none"></iframe>
  @endif

@stop
