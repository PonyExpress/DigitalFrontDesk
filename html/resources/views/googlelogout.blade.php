@extends('desk.master')


@section('title')
Please Wait
@stop



@section('content')
<div class="title">Redirecting...</div>
<h4><a href="{{ URL::to($path.'/login') }}">Click here if you are not redirected</a></h4>
<br/><br/>
<span style="font-size:12px">Or, you will automatically be redirect in <span id="countDown">10</span> seconds.</span>
<iframe src="https://accounts.google.com/logout" style="display:none" id="theFrame"></iframe>
@stop


@section('javascriptFooter')
  <script>
    document.getElementById('theFrame').onload = function(){window.location = "{{ URL::to($path.'/login') }}";};

    // Redirect after ten seconds
    $(function(){
      var counter = 10;
      setInterval(function(){
        counter--;

        $('#countDown').html(counter);

        if (counter <= 0)
          window.location = "{{ URL::to($path.'/login') }}";

      }, 1000);

    });
  </script>
@stop
