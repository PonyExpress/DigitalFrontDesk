@extends('docs.master')


@section('title')
About Digital Front Desk
@stop

@section('navbar')
  <li class="active"><a href="{{ URL::to('docs') }}">Documentation<span class="sr-only">(current)</span></a></li>
@stop

@section('content')
<div class="title">Documentation</div>
<p>This documentation is here to help guide students and desk workers through using Digital Front Desk, if they are unfamiliar with it.</p>
<br/><br/>
<h3 style="margin-bottom:20px">Categories</h3>
<p style="margin-bottom:10px"><a href="{{ URL::to('docs/about') }}">About Digital Front Desk</a></p>
<p style="margin-bottom:10px"><a href="{{ URL::to('docs/desk') }}">Desk Worker Portal</a></p>
<p style="margin-bottom:10px"><a href="{{ URL::to('docs/student') }}">Student Portal</a></p>
<p style="margin-bottom:10px"><a href="{{ URL::to('docs/gametrax') }}">GameTrax Portal</a></p>
<p style="margin-bottom:10px"><a href="{{ URL::to('docs/api') }}">Student API</a></p>
@stop
