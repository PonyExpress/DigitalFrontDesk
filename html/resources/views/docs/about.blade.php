@extends('docs.master')


@section('title')
About Digital Front Desk
@stop

@section('navbar')
  <li><a href="{{ URL::to('docs') }}">Documentation</a></li>
  <li class="sepratron"><a>/</a></li>
  <li class="active"><a href="{{ URL::to('docs/about') }}">About<span class="sr-only">(current)</span></a></li>

@stop

@section('content')
<div class="title">About</div>
<p>
Digital Front Desk is an online solution for everything that the front desk in student housing should need.
<p>
<br/>
<p>
Designed and developed by students at <a href="http://mst.edu" target="_blank">Missouri University of Science &amp; Technology</a> during 2015 - 2016, as a student project for the Better Campus Competition, Digital Front Desk aims to make the lives <br/>of Desk Workers all around the world easier.
</p>
<br/>
<p>

</p>
@stop
