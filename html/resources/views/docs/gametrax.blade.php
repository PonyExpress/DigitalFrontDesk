@extends('docs.master')


@section('title')
GameTrax Documentation
@stop

@section('navbar')
  <li><a href="{{ URL::to('docs') }}">Documentation</a></li>
  <li class="sepratron"><a>/</a></li>
  <li class="active"><a href="{{ URL::to('docs/gametrax') }}">GameTrax<span class="sr-only">(current)</span></a></li>
@stop

@section('content')
<div class="title">GameTrax Documentation</div>
<p>This documentation is here to help guide students and desk workers through using Digital Front Desk, if they are unfamiliar with it.</p>

@stop
