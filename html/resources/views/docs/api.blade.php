@extends('docs.master')


@section('title')
API Documentation
@stop

@section('navbar')
  <li><a href="{{ URL::to('docs') }}">Documentation</a></li>
  <li class="sepratron"><a>/</a></li>
  <li class="active"><a href="{{ URL::to('docs/api') }}">API<span class="sr-only">(current)</span></a></li>
@stop

@section('content')
<div class="title">Student API Documentation</div>
<p>This documentation is here to help guide students and desk workers through using Digital Front Desk, if they are unfamiliar with it.</p>
<br/><br/>
<h3 style="margin-bottom:20px">Version</h3>
<p style="margin-bottom:10px"><a href="{{ URL::to('docs/api/v1') }}">Version 1</a></p>

@stop
