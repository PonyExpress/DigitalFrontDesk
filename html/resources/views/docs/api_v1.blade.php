@extends('docs.master')


@section('title')
API v1 Documentation
@stop

@section('header')
<style>h1{border-bottom:1px solid #ccc;padding-bottom:5px}p{text-align:left;margin:5px!important;}h2{font-size:28px;}p{letter-spacing:0.02em;}body{overflow-x:hidden;}</style>
@stop

@section('navbar')
  <li><a href="{{ URL::to('docs') }}">Documentation</a></li>
  <li class="sepratron"><a>/</a></li>
  <li><a href="{{ URL::to('docs/api') }}">API</a></li>
  <li class="sepratron"><a>/</a></li>
  <li class="active"><a href="{{ URL::to('docs/api/v1') }}">Version 1<span class="sr-only">(current)</span></a></li>
@stop


@section('content')
<div class="row">
  <div class="col-xs-2 pull-left bs-docs-sidebar" role="complementary" style="text-align:left">
    <ul class="nav nav-stacked affix" id="sidebar">
      <li class="active">
          <a href="#overview">Overview</a>
      </li>
      <li>
          <a href="#config">Configuration</a>
          <ul class="nav nav-stacked">
              <li><a href="#config_api_key">API Key</a></li>
          </ul>
      </li>
      <li><a href="#objects">Return Objects</a>
        <ul class="nav nav-stacked">
          <li><a href="#objects_collection">Collection</a></li>
          <li><a href="#objects_building">Building</a>
            <ul class="nav nav-stacked">
              <li><a href="#objects_building_group">&nbsp;&nbsp;Group</a></li>
            </ul>
          </li>
          <li><a href="#objects_person">Resident</a></li>
          <li><a href="#objects_package">Package</a></li>
          <!-- Todo: Keys and Stuff -->
          <li><a href="#objects_timestamp">Timestamp</a></li>
          <li><a href="#objects_signature">Signature</a></li>
        </ul>
      </li>
      <li>
        <a href="#endpoints">API Endpoints</a>
        <ul class="nav nav-stacked">
          <li><a href="#endpoints_details">API Details</a></li>
          <li><a href="#endpoints_user">User Information</a></li>
          <li><a href="#endpoints_buildings">Buildings</a>
            <ul class="nav nav-stacked">
              <li><a href="#endpoints_buildings_properties">&nbsp;&nbsp;Properties</a></li>
              <li><a href="#endpoints_buildings_all">&nbsp;&nbsp;All Buildings</a></li>
              <li><a href="#endpoints_buildings_one">&nbsp;&nbsp;Specific Building</a></li>
            </ul>
          </li>
          <li><a href="#endpoints_packages">Packages</a>
            <ul class="nav nav-stacked">
              <li><a href="#endpoints_packages_number">&nbsp;&nbsp;Number vs ID</a></li>
              <li><a href="#endpoints_packages_all">&nbsp;&nbsp;All</a></li>
              <li><a href="#endpoints_packages_newold">&nbsp;&nbsp;New and Old</a></li>
              <li><a href="#endpoints_packages_one">&nbsp;&nbsp;Single</a></li>
            </ul>
          </li>

        </ul>
      </li>


    </ul>
  </div>


  <div class="col-xs-10" role="main" style="text-align:left!important">
    <section id="overview" class="group">
      <div class="title" style="width:100%">API Version 1 Documentation</div>
      <p>The documentation below is on version 1 on the Digital Front Desk API.</p><br/>
      <p>The goal of having this public API available to residents is to encourage students to develop their own applications which access data available from the API.</p><br/>
    </section>


    <section id="config" class="group">
      <h1>Configuration</h1>
      <p>The home page for the desk worker portal is the main hub for navigating through each log.</p><br/>
      <p>Here, you will find icons to all the pages that you, as a desk worker, have permission to access.</p><br/>

      <div id="config_api_key" class="subgroup">
          <h2>Get Your API Key</h2>
      </div>

    </section> <!-- End config -->


    <section id="objects" class="group">
      <h1>Return Objects</h1>
      <p>Different endpoints will return different objects, each representing something different, some might return a collection of objects. This section is a guide to understanding those objects and what the values in them mean.</p><br/>
      <p>All endpoints return objects in JSON format, however, if you do not use a premade class for accessing the API you need to make sure to check if values are strings or integers.</p>



      <div id="objects_collection" class="subgroup">
        <h2>Collection</h2>
        <p>A collection is simply a group of objects, or an array of objects. Simple as that, they have the following format:</p>
        <pre class="well">{
  "object": "collection,
  "type": {object_type_in_collection},
  "data": {collection_data},
  "url": {endpoint}
}</pre>
        <p>The {colletion_data} element is where the actual collection is, the above sample is just the collection object.</p>
        <br/>
        <p>The actual collection resembles the following:</p>
        <pre class="well">{
  {index}: {object},
   .
   .
   .
   {nth_index}: {object}
}</pre>
        <p>Where {index} is the array index for that object, usually starts at 0, and {nth_index} is the last object, and has the highest index of n.</p>
      </div> <!-- End Object Collections -->


      <div id="objects_building" class="subgroup">
        <h2>Building</h2>
        <p>The Building Object represents buildings.
        <pre class="well">{
  "object": "building",
  "id": {building_id},
  "name": {building_name},
  "address": {building_address},
  "group": {building_group_object},
  "url": {endpoint}
}</pre>

        <div id="objects_building_group" class="subgroup">
          <h3>Building Group</h3>
          <p>Buildings can belong to groups, the group object has the following structure:</p>
          <pre class="well">{
  "object": "building_group",
  "id": {group_id},
  "name": {group_name}
}</pre>
          <p>This is a pretty simple object. Buildings that are in groups (one Resident Director or similar names) will have a group with a common name.</p><br/>
          <p>If a building does not belong to a group, then its group object will be <em>null</em>.</p>
        </div> <!-- End building group object -->

      </div> <!-- End Building Object -->


      <div id="objects_person" class="subgroup">
        <h2>Resident</h2>
        <p>The Resident Object is used to represent residents in a building, it has the following structure:</p>
        <pre class="well">{
  "object": "resident",
  "id": {id},
  "role": {permission_level},
  "name": {
    "first": {first_name},
    "middle": {middle_name},
    "last": {last_name},
    "full": {first_name} + " " + {last_name}
  },
  "address": {
    "room_number": {room_number},
    "building": {building_object}
    }
  },
  "email": {school_email},
  "url": {endpoint}
}</pre>
        <p>A permission level can be one of the following:</p>
        <div class="well">
          resident<br/>
          worker<br/>
          administrator
        </div>
        <p>If a resident has the worker permission level then they are a worker at the front desk and have access to the desk portal.</p>
      </div> <!-- End Person object -->


      <div id="objects_package" class="subgroup">
        <h2>Packages</h2>
        <p>The following format is used to represent a package:</p>
        <pre class="well">{
  "object": "package",
  "id": {package_id},
  "number": {package_number},
  "type": {package_type},
  "timestamps": {
    "in": {timestamp_object},
    "out": {timestamp_object}
  },
  "addressed_to": {resident_object},
  "signatures": {signatures_object}
}</pre>
        <p>When working with packages you will see two properties that might be kind of confusing, <em>{package_id}</em> and <em>{package_number}</em>. Although these two properties sound like they are the same thing, they are not.</p><br/>
        <p><em>{package_id}</em> is a <strong>unique</strong> identifier given to packages, while the <em>{package_number}</em> is not unique, and will repeat every x-amount of times, using the following equation:</p>
        <div class="well">
          {package_number} = {package_id} % x
        </div>
        <p>Where x is a value that is customizable by an administrator.</p><br/>
      </div> <!-- End Packages Object -->


      <div id="objects_timestamp" class="subgroup">
        <h2>Tiimestamps</h2>
        <p>Timestamps are not an object that will be returned by themeselves, but inside of other objects, they just need some clarification.</p>
        <p>A timestamp object has the following structure:</p>
        <pre class="well">{
  "object": "timestamp",
  "non-raw": {nonraw_timestamp},
  "raw": {raw_timestamp}
}</pre>
        <p>The raw timestamp has the following format:</p>
        <div class="well">
          Year-MonthNumber-Day Hour:Minute:Second<br/><br/>
          Example: 2016-01-29 07:35:54
        </div>
        <p>The non-raw timestamps have a more friendly format:</p>
        <div class="well">
          Month Day, Year, Hour:Minute (AM|PM)<br/><br/>
          Example: January 29, 2016 7:35 AM
        </div>
        <p>If a package has not been picked up by a student, or a key has not returned then the respective timestamp (out and in) will be <em>null</em>.</p>
      </div> <!-- End Timestamps Object -->


      <div id="objects_signature" class="subgroup">
        <h2>Signature</h2>
        <p>Signatures are saved when residents sign out or check items in at the front desk. These can later be used if any problems or conflicts occure. They use the following format:</p>
        <pre class="well">{
  "object": "signature",
  "url": {signature_image_url},
  "timestamps": {
    "signed": {time_stamp_object}
  }
}</pre>
        <p>The URL element is the url that the signature can be viewed at.</p>
      </div> <!-- End Signatures Object -->


    </section> <!-- End Return Objects -->


    <section id="endpoints" class="group">
      <h1>API Endpoints</h1>
      <p>This section contains information about the different endpoints for v1 of the DFD API.</p><br/>
      <p>All endpoints will return data in JSON format.</p><br/>

      <div id="endpoints_details" class="subgroup">
        <h2>API Details</h2>
        <p>Sometimes you might need to know what version of the API you are using, to do this you'll need to use one of the endpoints that retrieve details about the api, such as the version.</p><br/>
        <p>The following endpoints all return the same details about the API:</p>
        <div class="well">
          /v1/<br/>
            /v1/version<br/>
            /v1/about<br/>
            /v1/info<br/>
            /v1/api<br/>
        </div>
        <p>
          The return value will resemble the following format:
        </p>
        <pre class="well">{
  "object": "application_details",
  "application": {application_name},
  "author": {
    "team": {team_name},
    "year": {development_years},
    "school": {
      "name": {school_name},
      "city": {school_location},
      "url": {school_website_url}
    }
  },
  "version": {api_version},
  "documentation": {api_documentation_url},
  "timezone": "America/Chicago",
  "url": {endpoint}
}</pre>
        <p>
          Again, this is just basic information about the API for your use.
        </p>
      </div> <!-- End API Details -->


      <div id="endpoints_user" class="subgroup">
        <h2>User Information</h2>
        <p>To get information about the user using the API (i.e., you), use one of the following endpoints:</p>
        <div class="well">
          /v1/me<br/>
          /v1/user<br/>
          /v1/user/{id}<br/>
          <br/>
          {id} is the student ID of the user (can not retrieve other user's information)
        </div>
        <p>This will return a <a href="#objects_person">Resident Object</a>.</p>
      </div> <!-- End user information -->


      <div id="endpoints_buildings" class="subgroup">
        <h2>Buildings</h2>
        <p>There are two ways to get information about a building, you can either get a collection of all the buildings, or view details about a specific building.</p>
        <br/>

        <div id="endpoints_buildings_properties" class="subgroup">
          <h3>Building Properties</h3>
          <p>Understanding the building object is fairly simple, buildings have a name, an address, and an ID to make them unique.</p>
          <br/><br/>
          <p>But buildings can also belong to a group. Almost every campus has them, a group of dormitory buildings that are extremely similar, and might have a vary similar name. Or, if there are multiple buildings that is overlooked by one Resident Director, then these buildings are most likely in a group.</p>
        </div> <!-- End Building Properties -->

        <div id="endpoints_buildings_all" class="subgroup">
          <h3>All Buildings</h3>
          <p>To get information about all the buildings with a desk portal, use one of the following endpoints:</p>
          <div class="well">
            /v1/buildings<br/>
            /v1/buildings/all
          </div>
          <p>The return data will be a <a href="#objects_collection">Collection Object</a> of <a href="#objects_building">Building Objects</a></p>

        </div> <!-- End All Buildings -->


        <div id="endpoints_buildings_one" class="subgroup">
          <h3>Specific Building</h3>
          <p>Alternatively to getting information for all the buildings, you can get information for one building, using one of these endpoints:</p>
          <div class="well">
            /v1/building/{building_id}<br/>
            /v1/buildings/view/{building_id}
          </div>
          <p>Notice, that one end point has <em>building</em> and the other has <em>buildings</em>.</p><br/>
          <p>This will return a <a href="#objects_building">Building Object</a>.</p>
        </div> <!-- End Specific Building -->

      </div> <!-- End Buildings -->


      <div id="endpoints_packages" class="subgroup">
        <h2>Packages</h2>
        <p>Packages, similar to buildings, can be returned in a collection, or you can just get information about one specific package.</p><br/>

        <div id="endpoints_packages_all" class="subgroup">
          <h3>All Packages</h3>
          <p>Using one of the following endpoints:</p>
          <div class="well">
            /v1/packages<br/>
            /v1/packages/all
          </div>
          <p>Will return your entire package history, along with any new packages you have waiting for you.</p><br/>
          <p>It gives you a <a href="#objects_collection">Collection Object</a> with the following data:</p>
          <pre class="well">{
  "new_packages": {collection_of_new_packages},
  "old_packages": {collection_of_old_packages}
}</pre>
        <p>Notice that this collection actually has two more collections inside of it.</p>
        </div> <!-- End ALl Packages -->

        <div id="endpoints_packages_newold" class="subgroup">
          <h3>New/Old Packages</h3>
          <p>Unlike other endpoints, there is only one way to retrieve <em>only</em> new or old packages, by using the following endpoint:</p>
          <div class="well">
            /v1/packages/new<br/>
            /v1/packages/old
          </div>
          <p>This returns a <a href="#objects_collection">Collection Object</a> of <a href="#objects_package">Package Objects</a> addressed to you, new or old depending on the end point.</p><br/>
          <p>Old packages are packages that you have picked up, new packages are packages waiting to be picked up.</p>

        </div> <!-- End New and Old Packages -->


        <div id="endpoints_packages_one" class="subgroup">
          <h3>A Single Package</h3>
          <p>Two endpoints can be used to get information for a single package:</p>
          <div class="well">
            /v1/package/{package_id}<br/>
            /v1/packages/view/{package_id}
          </div>
          <p><strong>Note:</strong> you can not view details about packages that are not addressed to you</p>
          <br/>
          <p>These endpoints will return a <a href="#objects_package">Package Object</a>.</p>
        </div> <!-- End Single Package -->
      </div>

    </section> <!-- End Endpoints Section -->

    <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
    <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
    <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
    <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
    <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
    <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
    <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
    <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
    <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
  </div>

</div>
@stop
@section('javascriptFooter')
<script type="text/javascript">
$('body').scrollspy({
    target: '.bs-docs-sidebar',
    offset: 200
});
</script>
@stop
