@extends('docs.master')


@section('title')
Desk Documentation
@stop

@section('header')
<style>h1{border-bottom:1px solid #ccc;padding-bottom:5px}p{text-align:left;margin:5px!important;}h2{font-size:28px;}p{letter-spacing:0.02em;}body{overflow-x:hidden;}</style>
@stop

@section('navbar')
  <li><a href="{{ URL::to('docs') }}">Documentation</a></li>
  <li class="sepratron"><a>/</a></li>
  <li class="active"><a href="{{ URL::to('docs/desk') }}">Desk<span class="sr-only">(current)</span></a></li>
@stop

@section('content')
<div class="row">
  <div class="col-xs-2 pull-left bs-docs-sidebar" role="complementary" style="text-align:left">
    <ul class="nav nav-stacked affix" id="sidebar">
      <li class="active">
          <a href="#overview">Overview</a>
      </li>
      <li>
          <a href="#home_page">Home Page</a>
          <ul class="nav nav-stacked">
              <li><a href="#home_page_icons">Icons</a></li>
          </ul>
      </li>
      <li>
        <a href="#package_log">Package Log</a>
        <ul class="nav nav-stacked">
          <li><a href="#package_log_home">Viewing Packages</a></li>
          <li><a href="#package_log_logging">Logging Packages</a></li>
          <li><a href="#package_log_notifications">Notifications</a></li>
          <li><a href="#package_log_signing">Signing Out Packages</a></li>
        </ul>
      </li>
        <!-- Same for Group B & C -->
    </ul>
  </div>


  <div class="col-xs-10" role="main" style="text-align:left!important">
    <section id="overview" class="group">
      <div class="title" style="width:100%">Desk Worker Portal Documentation</div>
      <p>This documentation details what a desk worker can perform when using the desk worker portal. You can use the navbar to the left to jump to sections, or scroll down through the sections.</p><br/>
      <p>It's important to understand, that since different buildings and locations have different desk requirements, that a desk worker portal for Location A might not be the same as the portal for Location B.</p><br/>
      <p>The documenation for this section is going to go through a few of the most popular ones. All the logs are generally pretty close to being the same, so if you read about these then you'll be fine to explore other logs.</p>
    </section>


    <section id="home_page" class="group">
      <h1>The Home Page</h1>
      <p>The home page for the desk worker portal is the main hub for navigating through each log.</p><br/>
      <p>Here, you will find icons to all the pages that you, as a desk worker, have permission to access.</p><br/>

      <div id="home_page_icons" class="subgroup">
          <h2>Icons</h2>
      </div>

    </section>



    <section id="package_log" class="group">
      <h1>The Package Log</h1>
      <p>The package log is one of the most used logs at most front desks, which can recieve dozen of packages a day. It's not a secret that this can create a huge bottleneck in the speed that a front desk can handle packages and other student interactions.</p><br/>
      <p>But Digital Front Desk aims to reduce, or even eliminate the bottlenecks, especially for packages.</p>


      <div id="package_log_home" class="subgroup">
          <h2>Viewing Packages</h2>
          <p>When you first get to the package log page, you'll see a table with some, or none, listed items in it. This is a list of packages that have not been picked up by students.</p>
          <br/>
          <p>Packages are sorted by date, older ones are at the top. After 7 days, a package might appear in red, meaning that it is old and the resident might need a reminder, as seen in the example below.</p><br/>
          <div class="image">
            <img src="{{ URL::to('assets/docs/images/desk/001.png') }}">
            <div class="caption">An example scenario with an old package.</div>
          </div>
          <p>To send a reminder, click on the package row to go to it's signout page, and then click on the orange/yellow "Send Reminder" button, confirm that you want to remind the resident if needed.</p>
          <div class="image">
            <img src="{{ URL::to('assets/docs/images/desk/005.png') }}">
            <div class="caption">The Send Reminder button</div>
          </div>
      </div>


      <div id="package_log_logging" class="subgroup">
          <h2>Logging New Packages</h2>
          <p>You'll also notice on the package log home page the big "New Package" button, if you click this you'll be able to log in new packages as they arrive. Below, you can see what the new package form looks like.</p>
          <div class="image">
            <img src="{{ URL::to('assets/docs/images/desk/002.png') }}">
            <div class="caption">The New Package Form</div>
          </div>
          <p>Create a new package from this page is easy, all you need is three things (really two things), and you're all set!</p><br/>
          <p>First, you search for the resident in the "Search by name, room number, or student ID" box. You can type in their last name, first name, room number, or student ID. And the table below will narrow down in real time as you type.</p><br/><p>Once you have found the resident that the package is addressed, to, click on their row and they should highlight green. You'll also notice that the "Log Package" button at the bottom is now blue, and you can click on it.</p><br/>
          <p>Next, choose the type of package that it is.<br/>Then, if there are more packages to enter after this one, make sure that the "Add another package" box is checked. If this is checked, then after submitting the package you'll land right back on the new package form again.</p>
      </div>

      <div id="package_log_notifications" class="subgroup">
        <h2>Package Notifications</h2>
          <p>Once all the packages have been logged, you can go back to the package home page to see them. You should see a "Send Notifications" button.</p>
          <div class="image">
            <img src="{{ URL::to('assets/docs/images/desk/003.png') }}">
            <div class="caption">The Send Notifications Button</div>
          </div>
          <p>It is important that you click on this button after logging all of the packages. Since students can receive email or text notifications when they recieve packages, this button is responsible for sending those notifications. <br/>Instead of sending them right when the package is logged, they are saved for later, and should be sent when all packages have been logged.</p>
      </div>

      <div id="package_log_signing" class="subgroup">
        <h2>Signing Out Packages</h2>
        <p>Now that all the packages have been logged, and residents have been notified that they have a package waiting for them, you'll need to know how to sign a package out to a student.</p>
        <br/>
        <p>The very first thing you should do is take their student ID card to verify that they are who they say they are. Next, swipe their card on the card scanner. This should automagically redirect you to their student information page. <strong>For this to work, the website must be open, and be the active window on the computer.</strong></p><br/>
        <p>If the student has any new packages, you'll see each one listed on the left-hand side of their information page.</p>
        <p>To sign a package out to the student, just simply click on each new package they have, this must be done one at a time.</p>
        <div class="image">
          <img src="{{ URL::to('assets/docs/images/desk/004.png') }}">
          <div class="caption">The Package View Page</div>
        </div>
        <p>Once you click on a package, you'll be taken to the package view page, as seen above. Here, you can see details about the package such as which desk worker logged the package, and the package type. At the top, you'll see "Package #X". Now, you should proceed to find package #X and confirm that it is addressed to the resident.</p><br/>
        <p>The next step is to have the resident sign their name on the digital signature pad, then have them hit the "OK" button on the signature pad. If the student needs to redo their signature after hitting "OK" the refresh button near their signature (on the package page) will reset the signature pad and have them sign again.</p>

      </div>
    </section>

    <section id="package_log_sign1ing" class="group">
      <h3>Group C</h3>
      <div id="GroupCSub1" class="subgroup">
          <h4>Group C Sub 1</h4>
      </div>
      <div id="GroupCSub2" class="subgroup">
          <h4>Group C Sub 2</h4>
      </div>
    </section>
    <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
    <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
    <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
    <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
    <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
    <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
    <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
    <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
    <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
  </div>

</div>
@stop
@section('javascriptFooter')
<script type="text/javascript">
$('body').scrollspy({
    target: '.bs-docs-sidebar',
    offset: 200
});
</script>
@stop
