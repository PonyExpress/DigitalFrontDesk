@extends('forms.master');


@section('title')
Thank You
@stop


@section('content')
  <div class="title">Thank You</div>
  Your feedback is very much appreciated.
@stop
