<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title')</title>

    @include('global.css')


		@yield('header')


    @include('global.javascript_header')

  </head>
  <body>
    <div class="container">
      <div class="content">

        @yield('content')
      </div>
    </div>

    @include('global.javascript_footer')

    @yield('javascriptFooter')
  </body>
</html>
