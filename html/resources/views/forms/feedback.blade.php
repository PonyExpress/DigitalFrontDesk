@extends('forms.master');


@section('title')
Feedback
@stop


@section('content')
  <div class="title">Submit Feedback</div>
  <br/>
  Your feedback is <strong>always</strong> 100% anonymous.<br/><br/>
  {!! Form::open(['name' => 'feedback_form']) !!}

    <div class="form-group">
      <label>Please Enter Your Feedback</label>
      <textarea class="form-control" name="content" rows="3" required autofocus></textarea>
    </div>

    <button type="submit" class="btn btn-primary form-control" id="submitBtn">Submit</button>
  {!! Form::close() !!}
@stop
