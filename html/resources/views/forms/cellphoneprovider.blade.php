@extends('forms.master');


@section('title')
Suggest a Serivce Provider
@stop


@section('content')
  <div class="title">Suggest a Service Provider</div>
  <br/>
  Your feedback is <strong>always</strong> 100% anonymous.<br/><br/>
  {!! Form::open(['name' => 'new_package_form']) !!}

    <div class="form-group">
      <label>Cell Phone Provider Name</label>
      <input type="text" name="provider_name" placeholder="Service Provider" class="form-control"required autofocus>
    </div>

    <button type="submit" class="btn btn-primary form-control" id="submitBtn">Submit</button>
  {!! Form::close() !!}
@stop
