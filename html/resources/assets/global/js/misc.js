console.log('Digital Front Desk');
console.log('Developed by Michael Rouse and the Pony Express team')
console.log('Created for the 2015-2016 Better Campus Competition at Missouri University of Science and Technology');

// Will automatically scroll up non-error flash messages
$('.dismisses').delay(5000).slideUp(300);



/**
 * This allows any element to work as a link if it has an href tag
 */
$(document).ready(function(){register_links();}); $(document).ajaxStop(function(){register_links();});
function register_links()
{
  $('[href]').on('click', function(e){
    if ($(this).get(0).tagName != 'A')
    {
      e.preventDefault();
      // Scroll to an element if href is # or .
      if ($(this).attr('href').substr(0, 1) == '#' || $(this).attr('href').substr(0, 1) == '.')
      {
        var href = $(this).attr('href');

        // Scroll to the element
        scroll_to(href, 1);

        return;
      }


      // Get all the attributes from the element
      var url = $(this).attr('href');
      var target = ($(this).attr('target') === undefined) ? '_self' : $(this).attr('target');
      var specs = ($(this).attr('specs') === undefined) ? '' : $(this).attr('specs');
      var replace = ($(this).attr('replace') === undefined) ? false : ($(this).attr('replace').toString().toLowerCase() == 'true');

      if (url === undefined) { return; }

      // Open the window
      window.open(url, target, specs, replace);
    }
  });


  // Scroll to an element if the url has a has
  if (window.location.hash != '')
  {
    scroll_to(window.location.hash, 1);
  }
}

function scroll_to(element, speed, offset)
{
  // Default parameters
  speed = speed || 500;
  offset = offset || 70;

  if ($(element).length != 0)
  {
    $('html, body').animate({
      scrollTop: ($(element).offset().top) - offset
    }, speed);

    window.location.hash = element;
  }else{
    console.log('[Error] Tried to scroll to element ' + element + ' but it does not exist.');
  }

  return;
}


$(document).ready(function(){confirm_modal();}); $(document).ajaxStop(function(){confirm_modal();});
function confirm_modal()
{
	$('[modal-confirm]').on('click', function(ev) {
		var href = $(this).attr('modal-url');
    var title = $(this).attr('modal-title');
    var content = $(this).attr('modal-text');
		if (!$('#dataConfirmModal').length) {
			$('body').append('<div id="dataConfirmModal" class="modal fade" tabindex="-1" role="dialog"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> <h4 class="modal-title">' + title + '</h4> </div><div class="modal-body"> <p>' + content + '</p></div><div class="modal-footer"> <button type="button" class="btn btn-default" data-dismiss="modal" >No</button> <button type="button" class="btn btn-primary" id="dataConfirmOK">Yes</button> </div></div></div></div>');
		}
		$('#dataConfirmModal').modal({show:true});
    $('#dataConfirmOK').on('click', function(){data_confirm_ok_click(href);});
		return false;
	});
}


function is_int(value){
  if((parseFloat(value) == parseInt(value)) && !isNaN(value)){
      return true;
  } else {
      return false;
  }
}



/**
 Hide the navbar when scrolling down
 */
 $('.navbar-fixed-top').autoHidingNavbar();
