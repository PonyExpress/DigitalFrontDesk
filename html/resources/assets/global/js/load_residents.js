var selected_resident = 0;
//var resident_rows = $('#searchableResidentBody').find("tr");

// Ajax load residents
// And redirect when a resident row is clicked
$(document).ready(function(){
  $.ajax({
    type: 'GET',
    url: BASE_URL + '/desk/residents/all/html',
    dataType: 'html'
  }).done(function(data){
    // Add html to the tabel
    $('#resident_table_body').html(data);
  //  resident_rows = $('#searchableResidentBody').find("tr");

    // Remove the loading gif
    $('#resident_loading').html('');

    $('.clickableResidentRow[resident]').on('click', function(){
      if ( selected_resident != 0 && $(this).attr('resident') == selected_resident)
      {
        // Same row was selected twice, unselect it
        $('.clickableResidentRow[selected]').removeAttr('selected');
        selected_resident = 0;

        // Clear the selected residents value
        $('#selected_resident').attr('value', '');

        // Disable the new package button
        $('#formSubmit').prop('disabled', true);
      }
      else
      {
        // New selected row, mark it
        $('.clickableResidentRow[selected]').removeAttr('selected');
        selected_resident = $(this).attr('resident');
        $(this).attr('selected', 'selected');

        // Add the resident's ID to the selected resident form element
        $('#selected_resident').attr('value', $(this).attr('resident'));

        // Enable the submit button
        if ($('#selected_keyt').val() != '' && $('#signatureValue').val() != '')
        {
          $('#formSubmit').prop('disabled', false);
        }
      }
    });
  });
}); // end Ajax request
