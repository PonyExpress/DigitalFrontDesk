var selected_resident = 0;

// Ajax load residents
// And redirect when a resident row is clicked
$(document).ready(function(){
  $.ajax({
    type: 'GET',
    url: BASE_URL + '/desk/residents/all/html',
    dataType: 'html'
  }).done(function(data){
    // Add html to the tabel
    $('#resident_table_body').html(data);

    // Remove the loading gif
    $('#resident_loading').html('');

    // JQuery for row selection
    $('tr[resident]').on('click', function(){
        window.location.href = '/' + APP_PATH + '/residents/' + $(this).attr('resident');
    });
  });
}); // end Ajax request
