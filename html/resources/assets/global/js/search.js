var resident_list;
var key_list;
var item_list;

$(document).ready(function(){
  $(document).ajaxStop(function(){
    if ($('#resident_table_body').length != 0)
    {
      resident_search();
    }

    if ($('#key_table_body').length != 0)
    {
      key_search();
    }

    if ($('#item_table_body').length != 0)
    {
      item_search();
    }

  });
});


/**
 * Resident List Search
 */
function resident_search()
{
  var options = {
    valueNames: ['resident_search_id', 'resident_search_name', 'resident_search_room'],
    page: 10000
  };

  resident_list = new List('resident_list', options);

  // Show message if search returned nothing
  resident_list.on('searchComplete', function(){
    if ($('#resident_table_body').html() == ''){
      $('#resident_loading').html('No residents were found.');
    }else{
      $('#resident_loading').html('');
    }
  });
}



/**
 * Key List Search
 */
function key_search()
{
  var options = {
    valueNames: ['key_search_label', 'key_search_room'],
    page: 10000
  };

  key_list = new List('key_list', options);

  // Shows message if search returned nothing
  key_list.on('searchComplete', function(){
    if ($('#key_table_body').html() == ''){
      $('#key_loading').html('No Keys were found.');
    }else{
       $('#key_loading').html('');
     }
  });
}



/**
 * Item List Search
 */
function item_search()
{
  var options = {
    valueNames: ['item_search_name'],
    page: 10000
  };

  item_list = new List('item_list', options);

  // Show message if search returns nothing
  item_list.on('searchComplete', function(){
    if ($('#item_table_body').html() == ''){
      $('#item_loading').html('No items were found.');
    }else{
      $('#item_loading').html('');
    }
  });
}
