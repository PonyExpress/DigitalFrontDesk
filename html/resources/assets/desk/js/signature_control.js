var tmr;
var eventTmr;
var lcdX=0;
var lcdY=0;
var scrn=0;
var lcdSize=0;
var data="";
var data2="";

function startTablet()
{
  document.getElementById('signatureValue').value = "";
  document.getElementById("formSubmit").disabled = true; // Disable form submit button
  document.getElementById('signMessage').style.visibility = "visible";
  document.getElementById('signMessage').style.height = "auto";

  try
  {
    var retmod = 0;
    SetTabletState(1);
    retmod = TabletModelNumber();
    SetTabletState(0);

    if(retmod == 11 || retmod == 12 || retmod == 15)
    {
      var ctx = document.getElementById('cnv').getContext('2d');

      eventTmr = setInterval( SigWebEvent, 20 );
      tmr = SetTabletState(1, ctx, 50) || tmr;

      SetLCDCaptureMode(2);

      LcdRefresh(0, 0, 0, 240, 64);
      SetJustifyMode(0);
      KeyPadClearHotSpotList();
      ClearSigWindow(1);
      SetDisplayXSize(500);
      SetDisplayYSize(100);
      SetImageXSize(500);
      SetImageYSize(100);
      SetLCDCaptureMode(2);

      LCDSendGraphicUrl(1, 2, 0, 20, "http://www.sigplusweb.com/SigWeb/Sign.bmp");
		  LCDSendGraphicUrl(1, 2, 207, 4, "http://www.sigplusweb.com/SigWeb/OK.bmp");
		  LCDSendGraphicUrl(1, 2, 15, 4, "http://www.sigplusweb.com/SigWeb/CLEAR.bmp");

      lcdSize = LCDGetLCDSize();
      lcdX = lcdSize & 0xffff;
      lcdY = (lcdSize >> 16) & 0xffff;

      ClearTablet();

      LcdRefresh(2, 0, 0, 240, 64);
      ClearTablet();
      KeyPadClearHotSpotList();
      KeyPadAddHotSpot(2, 1, 10, 5, 53, 17);   //CLEAR
      KeyPadAddHotSpot(3, 1, 197, 5, 19, 17);  //OK
      LCDSetWindow(2, 22, 236, 40);
      SetSigWindow(1, 0, 22, 240, 40);

      onSigPenUp = function ()
      {
        processPenUp();
      };

      $('#signMessage').html('Please have the resident sign on the signature pad.<br/>Then hit "OK."');
      SetLCDCaptureMode(2);
    }
    else
    {
      // Error
      $('#signMessage').html('Error: Please plug in the digital signature pad, then refresh.');

    }
  }
  catch (e)
  {
    // Error
    $('#signMessage').html('Error: You need to have SigWeb installed, please click <a href="https://ro3mstbetter.managed.mst.edu/assets/desk/sigweb.exe">Here to Download</a> the file, then run the file.');
  }

}

function processPenUp()
{
  // Clear button
  if(KeyPadQueryHotSpot(2) > 0)
  {
    ClearSigWindow(1);
    LcdRefresh(1, 10, 0, 53, 17);

    LcdRefresh(2, 0, 0, 240, 64);
    ClearTablet();
  }

  // Okay button
  if(KeyPadQueryHotSpot(3) > 0)
  {
    ClearSigWindow(1);
    LcdRefresh(1, 210, 3, 14, 14);

    if(NumberOfTabletPoints() > 0)
    {

      // Thank you and credits
      LcdRefresh(0, 0, 0, 240, 64);
      //LCDWriteString(0, 2, (lcdX / 2) - 30, 15, "9pt Arial", 15, "Thank you.");
      //LCDWriteString(0, 2, 5, 45, "8pt Arial", 15, "Dev. by Michael Rouse & Jake Zenk, 2015-2016");
      SetSigCompressionMode(1);
      AutoKeyStart();
      SetAutoKeyData(data);
      SetAutoKeyData(data2);
      AutoKeyFinish();
      SetEncryptionMode(2);

      GetSigImageB64(function(str){document.getElementById('signatureValue').value = str;});

      clearInterval(eventTmr);
      setTimeout(endCapture, 2000);

      // Enable the form submit button
      document.getElementById("formSubmit").disabled = false;
      document.getElementById('signMessage').style.visibility = "hidden";
      document.getElementById('signMessage').style.height = "0px";
    }
    else
    {
      // No signature, clear screen and start again
      LcdRefresh(0, 0, 0, 240, 64);
      ClearTablet();
      LcdRefresh(2, 0, 0, 240, 64);
      SetLCDCaptureMode(2);
    }
  }

  ClearSigWindow(1);
}


function endCapture()
{
  LcdRefresh(0, 0, 0, 240, 64);
  LCDSetWindow(0, 0, 240, 64);
  SetSigWindow(1, 0, 0, 240, 64);
  KeyPadClearHotSpotList();
  SetLCDCaptureMode(1);
  SetTabletState(0, tmr);
}

window.onload = function()
{


  startTablet();
};

window.onunload = function ()
{
  endCapture();
};
