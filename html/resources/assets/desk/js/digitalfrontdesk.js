// This will check to watch for clicks on the packages page, and redirects to the page about the package
$('tr[package]').click(function(){
  if ($(this).attr('ref'))
  {
    // Redirect with a reference (this will return back to a student page)
    window.location.href = "/desk/packages/" + $(this).attr('package') + "?ref=" + $(this).attr('ref');
  }
  else
  {
    window.location.href = "/desk/packages/" + $(this).attr('package');
  }
});


// Wait for click on key row
$('tr[key]').click(function(){
  if ($(this).attr('ref'))
  {
    // Redirect with a reference (this will return back to a student page)
    window.location.href = "/desk/keys/" + $(this).attr('key') + "?ref=" + $(this).attr('ref');
  }
  else
  {
    window.location.href = "/desk/keys/" + $(this).attr('key');
  }
});


// Click on an item log
$('tr[item]').click(function(){
  if ($(this).attr('ref'))
  {
    // Redirect with reference
    window.location.href = "/desk/items/" + $(this).attr('item') + "?ref=" + $(this).attr('ref');
  }
  else {
    window.location.href = "/desk/items/" + $(this).attr('item');
  }
});


// Click on a Punch Clock entry
$('tr[punch]').click(function(){
  if ($(this).attr('ref'))
  {
    // Redirect with reference
    window.location.href = "/desk/punchclock/" + $(this).attr('punch') + "?ref=" + $(this).attr('ref');
  }
  else
  {
    window.location.href = "/desk/punchclock/" + $(this).attr('punch');
  }
});



// Send out an email blast to residents with new packages
$('button[name=blastEmails]').click(function(){
  bootbox.confirm("Are you sure you want to send out an email notification to residents with new packages?", function(result){
    if (result)
    {
      window.location = "https://mstdfd.com/desk/packages/blast";
    }
  })
});


// Send reminder to a resident about a package
$('button[name=packageReminder]').click(function(){
  bootbox.confirm("Are you sure you want to send out an email reminder for this package?", function(result){
    if (result)
    {
      //window.location = "{{ URL::to('desk/packages/'.$package->id.'/reminder') }}";
    }
  })
});
