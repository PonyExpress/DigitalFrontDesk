$("#send_text_on").on('click', function(){
  $('#alert_email').slideUp(300);
  $("#phone_number").slideDown(300);
  $("#phone_provider").slideDown(300);
});
$("#send_text_off").on('click', function(){
  $("#phone_number").slideUp(300);
  $("#phone_provider").slideUp(300);
  $('#alert_email').slideDown(300);
});

// Limit input to only numbers on telephone input
$('input[type=tel]').keydown(function(e) {
  var a=[8,9,13,16,17,18,20,27,35,36,37,38,39,40,45,46,91,92];
  var k = e.which;
  for (i = 48; i < 58; i++)
      a.push(i);
  for (i = 96; i < 106; i++)
      a.push(i);
  if (!(a.indexOf(k)>=0))
      e.preventDefault();
});

$('.btn-toggle').click(function() {
  $(this).find('.btn').toggleClass('active');
  $(this).find('.btn').toggleClass('btn-default');
});

$("#newAPIKeyBtn").click(function(){bootbox.confirm("Are you sure you want to generate a new API key? This will cause any applications using your current API key to stop working.",function(result){if (result){window.location = "https://mstdfd.com/student/settings/newapikey";}})});
