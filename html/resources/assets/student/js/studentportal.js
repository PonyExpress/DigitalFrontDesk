// This will check to watch for clicks on the packages page, and redirects to the page about the package
$('tr[studentPackage]').on('click', function(){
  window.location.href = "/student/packages/" + $(this).attr('studentPackage');

});

// Wait for click on key row
$('tr[studentKey]').on('click', function(){
  window.location.href = "/student/keys/" + $(this).attr('studentKey');
});

// Wait for click on item row
$('tr[studentItem]').on('click', function(){
  window.location.href = '/student/items/' + $(this).attr('studentItem');
});
