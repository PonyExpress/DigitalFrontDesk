<?php

namespace App;

use Auth;
use Session;

use Illuminate\Database\Eloquent\Model;
use App\Exceptions\RedirectException;


use App\Resident;

use Carbon\Carbon;


/**
 * Class to represent the settings for a Resident
 *
 */
class CellPhoneProvidersSuggestions extends Model
{
  protected $primaryKey = 'id';
  protected $table = 'cell_phone_providers_suggestions';

  public $timestamps = false;

  protected $fillable = ['content'];

}
