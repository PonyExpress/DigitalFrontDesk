<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;


use App\Resident;
use App\DeskWorker;
use App\Administrators;
use App\KeyLog;
use App\Key;
use App\PackageLog;





/**
 * This is the class that represents signed in users,
 * Can be accessed using Auth::user()
 *
 */
class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
  use Authenticatable, Authorizable, CanResetPassword;

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'users';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['name', 'email', 'resident_id', 'worker_id', 'password', 'picture'];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['password', 'remember_token'];


  private $my_resident = null;
  private $my_worker = null;
  private $my_admin = null;
  private $my_privileges = null;
  private $my_settings = null;

  private $functions = ['resident', 'worker', 'admin', 'permissions', 'settings']; // List of function names without parenthesis

  private $permissions = ['new_packages', 'signout_packages', 'loan_keys', 'return_keys', 'loan_items', 'return_items',
                          'message_log', 'send_emails']; // List of permissions on the user


  /**
   * Gets the resident object the signed in user represents
   *
   * @return App\Resident
   */
  private function resident()
  {
    if (is_null($this->my_resident))
      $this->my_resident = Resident::find($this->attributes['resident_id']);

    return $this->my_resident;
  }


  /**
   * Gets the desk worker the signed in user represents
   *
   * @return App\DeskWorker
   */
  private function worker()
  {
    if (is_null($this->my_worker))
      $this->my_worker = DeskWorker::find($this->attributes['worker_id']);//$this->belongsTo('App\DeskWorker', 'worker_id', 'id');

    return $this->my_worker;
  }


  /**
   * Gets the admin object of the user
   *
   * @return null or an admin object
   */
  private function admin()
  {
    if (is_null($this->my_admin))
      $this->my_admin = Administrators::where('worker_id', '=', $this->attributes['worker_id'])->first();

    return $this->my_admin;
  }


  /**
   * Tells whether the user is an admin or not
   *
   * @return True if an admin
   */
  public function isAdmin()
  {
    // Make sure the user has a desk worker first
    if (!is_null($this->worker()))
      return !(is_null($this->admin()));

    // No desk worker ID so they can't be an admin
    return false;
  }


  /**
   * Tells whether the user is a desk worker or not
   *
   * @return True if a desk worker
   */
  public function isDeskWorker()
  {
    return (!is_null($this->worker())); // Will be true if the user is a desk worker
  }


  /**
   * Used to access the permissions of a worker
   *
   * @return App\WorkerPermissions
   */
  private function permissions()
  {
    if (is_null($this->my_permissions) || !is_object($this->my_permissions) || get_class($this->my_permissions) != "App\WorkerPermissions")
      $this->my_permissions = $this->worker()->permissions;

    return $this->my_permissions;
  }


  /**
   * Used to access the resident's settings
   *
   * @return App\ResidentSettings
   */
  private function settings()
  {
    if (is_null($this->my_settings) || !is_object($this->my_settings) || get_class($this->my_settings) != "App\ResidentSettings")
      $this->my_settings =  $this->resident()->settings();

    return $this->my_settings;
  }


  /**
   * Allows for $user->building to actually call $user->building()
   * And other function calls to be made without the parenthesis
   *
   * This will not work on scope functions though, or functions that have
   * parameters
   *
   * @param string $key the variable to lookup
   * @return yes
   */
  public function __get($key)
  {
    if (in_array($key, $this->functions))
      return $this->$key();

    // Try the parent __get function
    return parent::__get($key);
  }

}





/**
 *
 *
 * @param
 * @return
 */
