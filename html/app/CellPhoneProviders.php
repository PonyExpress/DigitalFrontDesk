<?php

namespace App;

use Auth;
use Session;

use Illuminate\Database\Eloquent\Model;
use App\Exceptions\RedirectException;


use App\Resident;

use Carbon\Carbon;


/**
 * Class to represent the settings for a Resident
 *
 */
class CellPhoneProviders extends Model
{
  protected $primaryKey = 'id';
  protected $table = 'cell_phone_providers';

  public $timestamps = false;

  protected $fillable = ['id', 'name', 'text_url'];


  public static function asArray()
  {
    $providers = CellPhoneProviders::all();

    $arr = [];

    foreach($providers as $provider)
    {
      $arr[$provider['id']] = $provider['name'];
    }

    return $arr;
  }

}











/**
 *
 *
 * @param
 * @return
 */
