<?php

namespace App;

use Auth;
use Session;

use Illuminate\Database\Eloquent\Model;

// Exceptions
use App\Exceptions\RedirectException;

// Classes/Models
use App\Building;
use App\DeskWorker;
use App\Administrators;
use App\ResidentSettings;


/**
 * Class to represent a Resident
 *
 */
class Resident extends Model
{
  private $my_building = NULL; // Object that points to the building the resident lives in
  private $my_settings = NULL;
  private $my_worker = NULL;
  private $my_admin = NULL;

  protected $primaryKey = 'id';
  protected $table = 'residents';

  public $timestamps = false;

  protected $fillable = ['id', 'first_name', 'middle_name', 'last_name', 'room_number', 'building_id', 'email', 'removed', 'hidden'];

  private $functions = ['settings', 'building', 'address', 'formalName', 'formalNameWithInitial', 'informalName', 'fullName', 'nameWithInitial', 'initials', 'worker', 'admin']; // List of function names without parenthesis


  /* -------------------------------------------------
                   DATABASE FUNCTIONS
   ------------------------------------------------ */

   /**
    * Creates a resident and the resident settings
    *
    * @param Array $information - the information about the resident
    * @return App\Resident
    */
  public static function createResident($information)
  {
    // Create the resident
    $new_resident = Resident::create($information);
    $new_resident->id = intval($information['id']);
    $new_resident->save();

    // Create Settings for the Residen

    $settings = ResidentSettings::findOrCreate($new_resident);
    $settings->newAPIKey();

    return $new_resident;
  }



  /* -------------------------------------------------
                    SCOPE FUNCTIONS
   ------------------------------------------------- */

  /**
   * Makes query return residents by last name in ASCENDING order
   *
   * @param QueryBuilder $query
   * @return the modified query
  */
  public function scopeLastNameASC($query)
  {
    return $query->orderBy('last_name', 'ASC');
  }


  /**
   * Makes query return residents by last name in DESCENDING order
   *
   * @param QueryBuilder $query
   * @return the modifed query
   */
  public function scopeLastNameDESC($query)
  {
    return $query->orderBy('last_name', 'DESC');
  }


  /**
   * Narrows a query to only a single building
   *
   * @param QueryBulder $query
   * @return the modified query
   */
  public function scopeInBuilding($query)
  {
    $building = Auth::user()->worker->building;

    // Add the direct building the user works for
    $query->where('building_id', '=', $building->uID);

    // Add any other sub-buildings (RC2, RC3)
    $buildings = Building::where('id', '=', $building->id)->get()->toArray();

    foreach($buildings as $theBuilding)
    {
      // Don't add the same building twice
      if ($theBuilding['uID'] != $building->uID)
        $query->orWhere('building_id', '=', $theBuilding['uID']);
    }

    return $query;
  }


  /**
   * Narrows a query to residents with a certain email address
   *
   * @param QueryBulder $query
   * @param String $email
   * @return the modified query
   */
  public function scopeEmail($query, $email)
  {
    return $query->where('email', '=', $email);
  }


  /**
   * Narrows a query to residents that have not been removed
   *
   * @param QueryBulder $query
   * @return the modified query
   */
  public function scopeNotRemoved($query)
  {
    if (Auth::user()->isAdmin())
      return $query;
    else
      return $query->where('removed', '=', '0');
  }


  /**
   * Narrows a query to only show residents
   *
   * @param QueryBulder $query
   * @return the modified query
   */
  public function scopeNotHidden($query)
  {
    if (Auth::user()->isAdmin())
      return $query;
    else
      return $query->where('hidden', '=', '0');
  }




  /* -------------------------------------------------
                 NON-SCOPE FUNCTIONS
   ------------------------------------------------- */

  /**
   * Gets the full name of the resident informally (First Last)
   *
   * @return string
  */
  private function informalName()
  {
    return ucFirst($this->first_name) . " " . ucFirst($this->last_name) . (($this->removed()) ? " [REMOVED]" : "");
  }


  /**
   * Gets the full name of the Resident formally (Last, First)
   *
   * @return string
   */
  private function formalName()
  {
    return ucFirst($this->last_name) . ", " . ucFirst($this->first_name) . (($this->removed()) ? " [REMOVED]" : "");
  }


  /**
   * Gets the full name of the Resident formally with middle initial (Last, First I.)
   *
   * @return string
   */
  private function formalNameWithInitial()
  {
    if (is_null($this->middle_name))
      return $this->formalName();
    else
      return ucFirst($this->last_name) . ", " . ucFirst($this->first_name) . " " . ucFirst(substr($this->middle_name, 0, 1)) . "." . (($this->removed()) ? " [REMOVED]" : "");
  }


  /**
   * Generates complete full name
   *
   * @return string
   */
  private function fullName()
  {
    if (is_null($this->middle_name))
      return $this->informalName();
    else
      return ucFirst($this->first_name) . " " . ucFirst($this->middle_name) . " " . ucFirst($this->last_name) . (($this->removed()) ? " [REMOVED]" : "");
  }


  /**
   * Generates students full name with a middle initial
   *
   * @return string
   */
  private function nameWithInitial()
  {
    if (is_null($this->middle_name))
      return $this->informalName();
    else
      return ucFirst($this->first_name) . " " . ucFirst(substr($this->middle_name, 0, 1)) . ". " . ucFirst($this->last_name) . (($this->removed()) ? " [REMOVED]" : "");
  }


  /**
   * Generates the intials of the resident
   *
   * @return string
   */
  public function initials()
  {
    return strtoupper(substr($this->attributes['first_name'], 0, 1) . ((!is_null($this->attributes['middle_name'])) ? substr($this->attributes['middle_name'], 0, 1) : '') . substr($this->attributes['last_name'], 0, 1)) . (($this->removed()) ? " [REMOVED]" : "");
  }



  /**
   * Gets the building that the resident lives in
   *
   * @return App\Buidling
   */
  private function building()
  {
    if (is_null($this->my_building))
      $this->my_building = Building::find($this->attributes['building_id']);

    return $this->my_building;
  }


  /**
   * Gets the address of the resident
   *
   * @return string
   */
  private function address()
  {
    return $this->attributes['room_number'] . ' ' . $this->building()->name;
  }


  /**
   * Checks if a resident is a desk worker
   *
   * @return true if they are a desk worker
   */
  public function isDeskWorker()
  {
    return !($this->worker()->isDummy());
  }


  /**
   * Gets the worker object
   *
   * @return App\DeskWorker
   */
  private function worker()
  {
    if (is_null($this->my_worker))
      $this->my_worker = DeskWorker::findOrDummy($this->attributes['id']);//here('resident_id', '=', $this->attributes['id'])->first();

    return $this->my_worker;
  }


  /**
   * Checks if the resident is an administrator
   *
   * @return true if they are an admin
   */
  public function isAdmin()
  {

    return !($this->admin()->isDummy());
  }


  /**
   * Gets the admin object
   *
   * @return App\Administrators
   */
  private function admin()
  {
    if (is_null($this->my_admin))
      $this->my_admin = Administrators::findOrDummy($this->attributes['id']);

    return $this->my_admin;
  }


  /**
   * Gets the settings for the resident, will create settings if none are found
   *
   * @return App\ResidentSettings
   */
  private function settings()
  {
    if(is_null($this->my_settings))
      $this->my_settings = ResidentSettings::findOrCreate($this);

    return $this->my_settings;
  }


  /**
   * Checks if the resident has been removed
   *
   * @return boolean
   */
  public function isRemoved()
  {
    return false;//(!Auth::user()->isAdmin() && $this->attributes['removed'] == '1');
  }
  public function removed()
  {
    return false;
    //return ($this->attributes['removed'] == '1');
  }


  /**
   * Checks if the resident is a student
   *
   * @return boolean
   */
   public function isHidden()
   {
     return ($this->attributes['hidden'] != 0);
   }

  /**
   * Soft deletes a model
   *
   * @return nothing
   */
  public function remove()
  {
    // Mark the object as removed
    $this->update(['removed' => 1]);
    $this->save();

    // Mark the admin object as removed if they are an admin
    if ($this->isAdmin())
    {
      $this->admin()->update(['removed' => 1]);
      $this->worker()->save();
    }

    // Mark the worker object as removed
    if ($this->isDeskWorker())
    {
      $this->worker()->update(['removed' => 1]);
      $this->worker()->save();
    }

  }


  /**
   * Un-removes a resident
   *
   * @return nothing
   */
  public function restore()
  {
    // Mark the object as removed
    $this->update(['removed' => 0]);
    $this->save();

    // Mark the admin object as removed if they are an admin
    if ($this->isAdmin())
    {
      $this->admin()->update(['removed' => 0]);
      $this->worker()->save();
    }

    // Mark the worker object as removed
    if ($this->isDeskWorker())
    {
      $this->worker()->update(['removed' => 0]);
      $this->worker()->save();
    }

  }






  /**
   * Allows for $resident->building to actually call $resident->building()
   * And other function calls to be made without the parenthesis
   *
   * This will not work on scope functions though, or functions that have
   * parameters
   *
   * @param string $key the variable to lookup
   * @return yes
   */
  public function __get($key)
  {
    if (in_array($key, $this->functions))
      return $this->$key();

    // Not a function, go to the parent (for $this->attribute[$key])
    return parent::__get($key);

  }


  /**
   * Finds a resident, returns an error if it failed
   *
   * @throws App\Exceptions\RedirectException
   * @param int $id The ID of the resident to find
   * @param string $returnPath (Default: 'desk/residents') The path to return to if it fails
   * @return resident or redirect
   */
  public static function findOrError($id, $returnPath = null)
  {
    if (is_null($returnPath))
      $returnPath = Session::get('app_path', 'desk') . '/residents';

    $entry = Resident::find($id);

    // Return the Resident if found
    if (!is_null($entry) && !$entry->isRemoved())
      return $entry;


    // Throw resident not found error
    throw new RedirectException('Resident was not found.', $returnPath, 'error', true);
  }
}





/**
 *
 *
 * @param
 * @return
 */
