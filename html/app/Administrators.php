<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Resident;
use App\DeskWorker;
use App\AdminPermissions;


/**
 * Class for representing administrators
 *
 */
class Administrators extends Model
{
  protected $primaryKey = 'id';
  protected $table = 'administrators';

  public $timestamps = false;

  // Fillable stuff
  protected $fillable = ['resident_id', 'worker_id', 'removed'];

  private $my_resident = NULL;
  private $my_worker = NULL;
  private $my_privileges = NULL;

  private $functions = ['resident', 'worker', 'permissions'];


  /* -------------------------------------------------
                   DATABASE FUNCTIONS
   ------------------------------------------------ */

   /**
    * Creates an admin and the admin permissions
    *
    * @param Array $information - the information about the admin
    * @param Array $permissions - the permissions for the new admin
    * @return App\Administrators
    */
    public static function createAdmin($information, $permissions = [])
    {
      // Create the worker
      $new_admin = Administrators::create($information);

      // Create the permissions
      if ($permissions == [] || !array_key_exists('admin_id', $permissions))
        $permissions['admin_id'] = $new_admin->id;

      $admin_permissions = AdminPermissions::create($permissions);


      return $new_admin;
    }




  /* -------------------------------------------------
                 NON-SCOPE FUNCTIONS
   ------------------------------------------------- */

   /**
    * Returns the worker object or returns a dummy object that has no permissions
    *
    * @param $id The id of the resident to find a worker for
    * @return App\DeskWorker
    */
   public static function findOrDummy($id)
   {
     $admin = Administrators::where('resident_id', '=', $id)->first();

     if (is_null($admin))
       $admin = Administrators::where('resident_id', '=', 0)->first();

     return $admin;
   }


   /**
    * Tells if the worker object is a dummy instance
    *
    * @return boolean
    */
   public function isDummy()
   {
     return ($this->attributes['resident_id'] == 0 && $this->attributes['id'] == 0);
   }


  /**
   * Gets the resident object of this admin
   *
   * @return App\Resident the admin represents
  */
  private function resident()
  {
    if (is_null($this->my_resident))
      $this->my_resident = Resident::find($this->attributes['resident_id']);

    return $this->my_resident;
  }


  /**
   * Gets the worker object of this admin
   *
   * @return App\DeskWorker the admin Represents
  */
  private function worker()
  {
    if (is_null($this->my_resident))
      $this->my_resident = DeskWorker::find($this->attributes['worker_id']);

    return $this->my_resident;
  }


  /**
   * Gets the permissions of the desk worker
   *
   * @return App\WorkerPermissions
   */
  private function permissions()
  {
    if (is_null($this->my_privileges))
      $this->my_privileges = AdminPermissions::find($this->attributes['id']);

    return $this->my_privileges;
  }




  /**
   * Allows for $resident->building to actually call $resident->building()
   * And other function calls to be made without the parenthesis
   *
   * This will not work on scope functions though, or functions that have
   * parameters
   *
   * @param string $key the variable to lookup
   * @return yes
   */
  public function __get($key)
  {
    if (in_array($key, $this->functions))
      return $this->$key();

    // Not a function, go to the parent (for $this->attribute[$key])
    return parent::__get($key);
  }
}


/**
 *
 *
 * @param
 * @return
 */
