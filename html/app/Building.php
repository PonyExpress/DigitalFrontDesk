<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\BuildingGroups;



/**
 * Class to represent a Building
 *
 */
class Building extends Model
{
  protected $primaryKey = 'uID';
  protected $table = 'buildings';

  public $timestamps = false;

  // Fillable stuff
  protected $fillable = ['uID', 'id', 'name', 'address', 'has_packages', 'has_residents', 'has_keys', 'has_items', 'has_punchClock', 'has_gametrax'];

  private $functions = ['shortName', 'groupName', 'group', 'gameTrax']; // List of functions to make accessible from the __get magic operator

  private $my_gameTrax = null;



  public function scopeName($query, $name)
  {
    return $query->where('name', '=', $name);
  }

  /**
   * Gets the maximum group id for the table
   *
   * @param QueryBuilder $query
   * @return the modified query
   */
  public function scopeMaxID($query)
  {
    return $query->whereRaw('id = (select max(`id`) from `package_log`)');
  }

  /* -------------------------------------------------
                   NON-SCOPE FUNCTIONS

    These are just plain-ol-boring functions that any Package
      object can call, nothing magical about these.Okay

     ------------------------------------------------- */


  /**
   * Gets a short name for a building
   *
   * @return string
  */
  private function shortName()
  {
    // Put the name into an array, each element begins at a space
    $words = explode(" ", $this->attributes['name']);

    $shortname = "";

    // Loop through each word in the name
    foreach ($words as $word)
    {
      if (is_numeric($word))
      {
        $shortname .= $word . " ";
      }
      else
      {
        // Add first letter of the word if it's not hall
        if ($word != "Hall")
        {
          $shortname .= substr($word, 0, 1); // Add just the first letter of the word to the shortname
        }
        else
        {
          $shortname .= $word . " "; // Adds "Hall" to the running shortname
        }
      }

    }

    return $shortname;
  }


  /**
   * Gets the group name for a building
   *
   * @return string
  */
  private function groupName()
  {
    return ($this->inGroup()) ? $this->group()->name : $this->attributes['name'];
  }


  /**
   * Tells if a building is in a group
   *
   * @return boolean
  */
  public function inGroup()
  {
    return !(is_null($this->group()));
  }


  /**
   * Returns the group
   *
   * @return App\BuildingGroups
  */
  private function group()
  {
    if(is_null($this->my_group))
      $this->my_group = BuildingGroups::find($this->attributes['id']);

    return $this->my_group;
  }



  /**
   * Tells if a building has packages
   *
   * @return boolean
   */
  public function hasPackages()
  {
    return ($this->attributes['has_packages'] != 0);
  }


  /**
   * Tells if a building has residents
   *
   * @return boolean
   */
  public function hasResidents()
  {
    return ($this->attributes['has_residents'] != 0);
  }


  /**
   * Tells if a building has keys
   *
   * @return boolean
   */
  public function hasKeys()
  {
    return ($this->attributes['has_keys'] != 0);
  }


  /**
   * Tells if a building has items
   *
   * @return boolean
   */
  public function hasItems()
  {
    return ($this->attributes['has_items'] != 0);
  }


  /**
   * Tells if a building has a worker message log
   *
   * @return boolean
   */
  public function hasMessageLog()
  {
    return ($this->attributes['has_messageLog'] != 0);
  }


  /**
   * Tells if a building has a worker punch clock
   *
   * @return boolean
   */
  public function hasPunchClock()
  {
    return ($this->attributes['has_punchClock'] != 0);
  }


  /**
   * Tells if a building has GameTrax enabled or not
   *
   * @return boolean
   */
  public function hasGameTrax()
  {
    return ($this->attributes['has_gameTrax'] != 0);
  }


  /**
   * Tells if a building only has GameTrax
   *
   * @return boolean
   */
  public function onlyGameTrax()
  {
    return ($this->hasGameTrax() && !$this->hasPackages() && !$this->hasResidents() && !$this->hasKeys() && !$this->hasItems());
  }


  /**
   * Returns the GameTrax configuration of a building
   *
   * @return App\GameTraxConfig or null
   */
  private function gameTrax()
  {
    if ($this->hasGameTrax())
    {
      //if(is_null($this->my_gameTrax))
        //$this->my_gameTrax = new GameTraxBuilding($this->attributes['uID']); // Get the GameTrax configuration
    }

    return $this->my_gameTrax;
  }


  /**
   * Gets all the has stuff as an array
   *
   * @return Array
   */
  public function buildingHasAsArray()
  {
    $building = [];
    $building[0] = ['title' => 'Packages', 'value' => ($this->hasPackages()) ? 'Enabled' : 'Disabled' ];
    $building[1] = ['title' => 'Residents', 'value' => ($this->hasResidents()) ? 'Enabled' : 'Disabled' ];
    $building[2] = ['title' => 'Keys', 'value' => ($this->hasKeys()) ? 'Enabled' : 'Disabled' ];
    $building[3] = ['title' => 'Items', 'value' => ($this->hasItems()) ? 'Enabled' : 'Disabled' ];
    $building[4] = ['title' => 'Punch Clock', 'value' => ($this->hasPunchClock()) ? 'Enabled' : 'Disabled' ];
    $building[5] = ['title' => 'Message Log', 'value' => ($this->hasMessageLog()) ? 'Enabled' : 'Disabled' ];
    $building[6] = ['title' => 'GameTrax', 'value' => ($this->hasGameTrax()) ? 'Enabled' : 'Disabled' ];

    return $building;
  }


  /**
   * Allows for $buidling->shortName to actually call $building->shortName()
   * And other function calls to be made without the parenthesis
   *
   * This will not work on scope functions though, or functions that have
   * parameters
   *
   * @param string $key the variable to lookup
   * @return yes
   */
  public function __get($key)
  {
    if (in_array($key, $this->functions))
      return $this->$key();

    // Try the parent __get function
    return parent::__get($key);
  }

}

/**
 *
 *
 * @param
 * @return
 */
