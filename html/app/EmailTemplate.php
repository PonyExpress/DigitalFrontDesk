<?php

namespace App;

use App\EmailLog;

use Mail;

use Auth;

use App\Resident;

use Illuminate\Database\Eloquent\Model;

use Exceptions;
use App\Exceptions\EmailException;

use Carbon\Carbon;


/**
 * Handles sending an email based on a template
 *
 * @throws Exceptions\EmailException for an error in sending
 *
 */
class EmailTemplate
{
  private $template; // Array of content
  private $resident; // Resident object
  private $info;

  /**
   * Class Constructor
   *
   * @param string $_template the name of the template to use, must only have a {{ $firstname }} variable
   * @param App\Resident $_resident Object of the resident you want to send an email to
   */
  public function __construct($_template, $_resident = NULL, $_details = [])
  {
    // Check to see if a resident exists with the email
    if(is_null($_resident))
      throw new EmailException('No resident specified', 100);

    // $details is okay, proceed
    $this->resident = Resident::find($_resident);

    $this->template = $_template;

    $this->info = $_details;
  }

  /**
   * Send an email using the template
   *
   * @todo Do something with this return value...
   *
   * @throws Exceptions\EmailException for an error in sending
   *
   * @return true
   */
  public function send()
  {
    if ($this->resident->settings->package_alerts)
    {
      // Throw exception if the resident is empty (no harm in checking again...)
      if (is_null($this->resident))
        throw new EmailException('No resident specified.', 99);

      // Put resident details in an array
      $detials = [];
      $details['email'] = $this->resident->settings->email;
      $details['name'] = $this->resident->informalName;

      // Send the email
      if ($this->resident->settings->send_text)
      {
        $this->template .= '_text';

        // Send the text
        Mail::send($this->template, ['firstname' => $this->resident->first_name, 'info' => $this->info], function($message) use ($details){
          $message->to($details['email'], $details['name']);
        });
      }
      else
      {
        $details['subject'] = $this->info['subject'];

        // Send the email
        Mail::send($this->template, ['firstname' => $this->resident->first_name, 'info' => $this->info], function($message) use ($details){
          $message->to($details['email'], $details['name'])->subject($details['subject']);
        });
      }
    }
    return true;
  }
}





/**
 *
 *
 * @param
 * @return
 */
