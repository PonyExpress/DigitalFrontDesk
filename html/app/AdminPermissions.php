<?php

namespace App;

use Auth;

use App\Administrators;
use App\Building;
use App\Resident;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


/**
 * This class handles all the permissions for an admin, and permission checking
 *
 */
class AdminPermissions extends Model
{
  protected $primaryKey = 'admin_id';
  protected $table = 'administrator_permissions';

  public $timestamps = false;

  private $functions = ['admin'];

  protected $fillable = ['admin_id', 'prog_settings', 'create_buildings', 'create_residents', 'create_workers', 'create_admins'];


  private $my_admin = null;




  /**
   * Returns an object of the admin
   *
   * @return App\Administrators
   */
  private function admin()
  {
    if (is_null($this->my_admin))
      $this->my_admin = Administrators::find($this->admin_id);

    return $this->my_admin;
  }

  /**
   * Tells if the admin can create buildings
   *
   * @return boolean
   */
  public function canCreateBuildings()
  {
    // Return their permissions allow it
    return ($this->attributes['create_buildings'] != 0 || $this->admin()->isDummy());
  }


  /**
   * Tells if the admin can create users
   *
   * @return boolean
   */
  public function canCreateWorkers()
  {
    // Return their permissions allow it
    return ($this->attributes['create_workers'] != 0 || $this->admin()->isDummy());
  }


  /**
   * Tells if an admin can edit the digital front desk settings
   *
   * @return boolean
   */
  public function canEditSettings()
  {
    // return the permission value
    return ($this->attributes['prog_settings'] != 0 || $this->admin()->isDummy());
  }


  /**
   * Tells if the admin can create admins
   *
   * @return boolean
   */
  public function canCreateAdmins()
  {
    // Return their permissions allow it
    return ($this->attributes['create_admins'] != 0 || $this->admin()->isDummy());
  }


  /**
   * Tells if the admin can create residents
   *
   * @return boolean
   */
  public function canCreateResidents()
  {
    // Return their permissions allow it
    return ($this->attributes['create_residents'] != 0 || $this->admin()->isDummy());
  }





  /**
   * Returns the permissions as an array
   *
   * @return array
   */
  public function asArray()
  {
    $permissions = [];
    $permissions['prog_settings'] = ($this->prog_settings != 0  || $this->admin()->isDummy());
    $permissions['create_residents'] = ($this->create_residents != 0 || $this->admin()->isDummy()) ;
    $permissions['create_workers'] = ($this->create_workers != 0 || $this->admin()->isDummy());
    $permissions['create_admins'] = ($this->create_admins != 0 || $this->admin()->isDummy());

    return $permissions;
  }


  /**
   * Allows for $resident->building to actually call $resident->building()
   * And other function calls to be made without the parenthesis
   *
   * This will not work on scope functions though, or functions that have
   * parameters
   *
   * @param string $key the variable to lookup
   * @return yes
   */
  public function __get($key)
  {
    if (in_array($key, $this->functions))
      return $this->$key();

    // Not a function, go to the parent (for $this->attribute[$key])
    return parent::__get($key);
  }
}







/**
 *
 *
 * @param
 * @return
 */
