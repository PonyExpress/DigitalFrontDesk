<?php

namespace App;

use Auth;
use Session;

use Illuminate\Database\Eloquent\Model;
use App\Exceptions\RedirectException;


use App\Resident;
use App\CellPhoneProviders;
use App\Utilities;
use App\APIUsers;

use Carbon\Carbon;


/**
 * Class to represent the settings for a Resident
 *
 */
class ResidentSettings extends Model
{
  protected $primaryKey = 'resident_id';
  protected $table = 'residents_settings';

  public $timestamps = false;

  private $functions = ['resident', 'package_alerts', 'package_checkout_confrim', 'item_loan_alerts', 'item_return_alerts', 'key_loan_alerts',
                        'key_return_alerts', 'send_text', 'email'];

  protected $fillable = ['resident_id', 'package_alert', 'package_checkout_confirm', 'item_loan_alert', 'item_return_alert', 'key_loan_alert',
                         'key_return_alert', 'phone_alert', 'phone_number', 'phone_provider', 'alert_email', 'api_user'];

  private $my_resident = NULL;
  private $my_apiuser = NULL;




  /**
   * Will create settings for a resident if they do not exist
   *
   * @param App\Resident $resident (Reference) - The resident to find or create settings for
   * @return App\ResidentSettings
   */
  public static function findOrCreate(&$resident)
  {
    $settings = ResidentSettings::find($resident->id);

    // Create the settings if they do not exist
    if (is_null($settings))
    {
      $user = APIUsers::generate($resident->id);
      $settings = ResidentSettings::create(['resident_id' => $resident->id, 'alert_email' => $resident->email]);
    }

    if (is_null($settings->api_user))
    {
      $user = APIUsers::generate($resident->id);
      $settings->save();
    }

    return $settings;
  }




  /**
   * Returns an object of the resident the settings are for
   *
   * @return App\Resident
   */
  private function resident()
  {
    if (is_null($this->my_resident))
      $this->my_resident = Resident::find($this->resident_id);

    return $this->my_resident;
  }

  /**
   * Tells if the resident wishes to recieve alerts on new packages
   *
   * @return Boolean
   */
  private function package_alerts()
  {
    return ($this->package_alert != 0); // Make anything but 0 true
  }


  /**
   * Tells if the resident wishes to recieve alerts when they checkout a package
   *
   * @return Boolean
   */
  private function package_checkout_confrim()
  {
    return ($this->package_checkout_confirm != 0);
  }


  /**
   * Tells if the resident wishes to recieve alerts when they are loaned an item
   *
   * @return Boolean
   */
  private function item_loan_alerts()
  {
    return ($this->item_loan_alert != 0);
  }


  /**
   * Tells if the resident wishes to recieve alerts when they return an item
   *
   * @return Boolean
   */
  private function item_return_alerts()
  {
    return ($this->item_return_alert != 0);
  }


  /**
   * Tells if the resident wishes to recieve alerts when they get loaned a key
   *
   * @return Boolean
   */
  private function key_loan_alerts()
  {
    return ($this->key_loan_alert != 0);
  }


  /**
   * Tells if the resident wishes to recieve alerts when they return an item
   *
   * @return Boolean
   */
  private function key_return_alerts()
  {
    return ($this->key_return_alert != 0);
  }


  /**
   * Tells if the resident wishes to recieve phone alerts
   *
   * @return Boolean
   */
  private function send_text()
  {
    return ($this->phone_alert != 0);
  }


  /**
   * Returns the email that the resident wishes to receive alerts to
   *
   * @return String
   */
  private function email()
  {
    // Return the phone number if they want to recieve texts not email
    if ($this->send_text())
    {
      if (!is_null($this->phone_provider))
      {
        $provider = CellPhoneProviders::find($this->phone_provider);

        if (!is_null($provider))
          return $this->phone_number . "@" . $provider->text_url;
      }
    }

    if (is_null($this->alert_email))
    {
      $this->alert_email = $this->resident()->email;
      $this->save();
    }

    return $this->alert_email;
  }

  /**
   * Verifies new settings and updates the settings
   *
   * @param Array $new (Reference) - the new settings
   * @return boolean
   */
  public function updateSettings(&$new)
  {
    // Make sure $new is an array
    if (is_array($new))
    {
      // Package alerts
      if (array_key_exists('package_alerts', $new))
        $this->package_alert = ($new['package_alerts'] != 0) ? 1 : 0;

      // Package Checkout Confrim
      if (array_key_exists('package_confirm', $new))
        $this->package_checkout_confirm = ($new['package_confirm'] != 0) ? 1 : 0;

      // Item Loan
      if (array_key_exists('item_loan', $new))
        $this->item_loan_alert = ($new['item_loan'] != 0) ? 1 : 0;

      // Item Return
      if (array_key_exists('item_return', $new))
        $this->item_return_alert = ($new['item_return'] != 0) ? 1 : 0;

      // key Loan
      if (array_key_exists('key_loan', $new))
        $this->key_loan_alert = ($new['key_loan'] != 0) ? 1 : 0;

      // Key Return
      if (array_key_exists('key_return', $new))
        $this->key_return_alert = ($new['key_return'] != 0) ? 1 : 0;

      // Send text alerts
      if (array_key_exists('send_text', $new))
      {
        $this->phone_alert = ($new['send_text'] != 0) ? 1 : 0;

        if ($new['send_text'])
        {
          $this->phone_number = $new['phone_number'];
          $this->phone_provider = $new['phone_provider'];
        }
      }


      // Email for alerts
      if (array_key_exists('alert_email', $new))
        $this->alert_email = $new['alert_email'];

      // Save the new settings
      $this->save();

      return true;
    }

    // Nope!
    return false;
  }









  /**
   * Allows for $resident->building to actually call $resident->building()
   * And other function calls to be made without the parenthesis
   *
   * This will not work on scope functions though, or functions that have
   * parameters
   *
   * @param string $key the variable to lookup
   * @return yes
   */
  public function __get($key)
  {
    if (in_array($key, $this->functions))
      return $this->$key();

    // Not a function, go to the parent (for $this->attribute[$key])
    return parent::__get($key);
  }

}


/**
 *
 *
 * @param
 * @return
 */
