<?php

namespace App;

use Auth;


use App\DeskWorker;
use App\Building;
use App\Resident;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


/**
 * Class to represent a key
 *
 * NOT AN ENETRY IN THE KEYLOG
 *
 */
class Key extends Model
{

  private $my_building = NULL; // Object that equals the resident that the package belongs to

  protected $primaryKey = 'id';
  protected $table = 'keys';

  public $timestamps = false;

  protected $fillable = ['label', 'room', 'building_id'];

  private $functions = ['building']; // List of function names without parenthesis


  /* -------------------------------------------------
                    SCOPE FUNCTIONS
   ------------------------------------------------- */

  /**
   * Narrows a query to keys in a specific building
   *
   * @param QueryBuilder $query
   * @param App\Building $building builidng to find keys for
   * @return modified query
   */
  public function scopeForBuilding($query)
  {
    $building = Auth::user()->worker->building;

    // Add the direct building the user works for
    $query->where('building_id', '=', $building->uID);

    // Add any other sub-buildings (RC2, RC3)
    $buildings = Building::where('id', '=', $building->id)->get()->toArray();

    foreach($buildings as $theBuilding)
    {
      // Don't add the same building twice
      if ($theBuilding['uID'] != $building->uID)
        $query->orWhere('building_id', '=', $theBuilding['uID']);
    }

    return $query;
  }


  /**
   * Gets the maximum ID of the key table
   *
   * @param Builder $query
   * @return Modified query builder
   */
  public function scopeMaxID($query)
  {
    return $query->whereRaw('id = (select max(`id`) from `keys`)');
  }





  /* -------------------------------------------------
                   NON-SCOPE FUNCTIONS

    These are just plain-ol-boring functions that any Package
      object can call, nothing magical about these.

     ------------------------------------------------- */

  /**
   * Gets the building
   *
   * @return App\Building of the building the key belongs to
  */
  private function building()
  {
    if ($this->my_building == NULL)
      $this->my_building = Building::find($this->attributs['building_id']);//$this->belongsTo('App\Building', 'id', 'building_id');

    return $this->my_building;
  }


   /**
    * Allows for $resident->building to actually call $resident->building()
    * And other function calls to be made without the parenthesis
    *
    * This will not work on scope functions though, or functions that have
    * parameters
    *
    * @param string $key the variable to lookup
    * @return yes
    */
   public function __get($key)
   {
     if (in_array($key, $this->functions))
       return $this->$key();

     // Not a function, go to the parent (for $this->attribute[$key])
     return parent::__get($key);

   }
}


/**
 *
 *
 * @param
 * @return
 */
