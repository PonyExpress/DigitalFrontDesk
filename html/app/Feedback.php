<?php

namespace App;

use Auth;
use Session;

use Illuminate\Database\Eloquent\Model;
use App\Exceptions\RedirectException;


use App\Resident;

use Carbon\Carbon;


/**
 * Class to represent the settings for a Resident
 *
 */
class Feedback extends Model
{
  protected $primaryKey = 'id';
  protected $table = 'feedback';

  public $timestamps = false;

  protected $fillable = ['content'];

}
