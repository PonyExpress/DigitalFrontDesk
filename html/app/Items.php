<?php

namespace App;

use Auth;

use App\Building;
use App\Resident;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


/**
 * Responsible for handling items
 *
 */
class Items extends Model
{
  protected $primaryKey = 'uID';
  protected $table = 'items';

  public $timestamps = false;

  // Fillable stuff
  protected $fillable = ['uID', 'name'];


}


/**
 *
 *
 * @param
 * @return
 */
