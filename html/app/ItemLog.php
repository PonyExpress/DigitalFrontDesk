<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Session;

use App\DeskWorker;
use App\Resident;
use App\Building;
use App\Items;
use App\ItemsConfig;

use App\Exceptions\RedirectException;
use App\Exception\PermissionsException;

use App\Utilities;



/**
 * Class to represent the item log
 *
 */
class ItemLog extends Model
{
  private $my_resident = NULL;
  private $my_building = NULL;
  private $my_workerIn = NULL;
  private $my_workerOut = NULL;
  private $my_config = NULL;
  private $my_item = NULL;
  private $my_signature = NULL;

  protected $primaryKey = 'id';
  protected $table = 'item_log';

  public $timestamps = false;

  protected $fillable = ['resident_id', 'item_config_id', 'time_in', 'time_out', 'worker_in', 'worker_out',
                         'signature_in', 'building_id'];

  private $functions = ['resident', 'config', 'item', 'building', 'dateIn', 'dateOut', 'workerIn', 'workerOut', 'signature'];


  /* -------------------------------------------------
                      SCOPE FUNCTIONS
     ------------------------------------------------- */


  /**
   * Narrows a query to items logs for a certain item
   *
   * @param QueryBuilder $query
   * @return modified query
   */
  public function scopeLoanedItem($query, $item_id)
  {
    return $query->where('item_config_id', '=', $item_id);
  }


  /**
   * Narrows a query to items that have not been returned
   *
   * @param QueryBuilder $query
   * @return modified query
  */
  public function scopeNotReturned($query)
  {
    return $query->whereRaw('`time_in` IS NULL');
  }


  /**
   * Narrows a query to items that have been returned
   *
   * @param QueryBuilder $query
   * @return modified query
   */
  public function scopeReturned($query)
  {
    return $query->whereRaw('`time_in` IS NOT NULL');
  }


  /**
   * Narrows a scope to items loaned to a student
   *
   * @param QueryBuilder $query
   * @param int $id the ID of the resident
   * @return the modified query builder
   */
  public function scopeLoanedTo($query, $id)
  {
    return $query->where('resident_id', '=', $id);
  }


  /**
   * Narrows a query to return oldest item checkouts first
   *
   * @param QueryBuilder $query
   * @return the modified query
   */
  public function scopeOldest($query)
  {
    return $query->orderBy('time_out', 'ASC');
  }


  /**
   * Forces query to return newer item checkouts first
   *
   * @param QueryBuilder $query
   * @return the modified query
   */
  public function scopeNewest($query)
  {
    return $query->orderBy('time_out', 'DESC');
  }


  /**
   * Gets packages just in the worker's building
   *
   * @param QueryBuilder $query
   * @return the modified $query
   */
  public function scopeInBuilding($query)
  {
    $building = Auth::user()->worker->building;

    // Add the direct building the user works for
    $query->where('building_id', '=', $building->uID);

    // Add any other sub-buildings (RC2, RC3)
    $buildings = Building::where('id', '=', $building->id)->get()->toArray();

    foreach($buildings as $theBuilding)
    {
      // Don't add the same building twice
      if ($theBuilding['uID'] != $building->uID)
        $query->orWhere('building_id', '=', $theBuilding['uID']);
    }

    return $query;
  }



  /* -------------------------------------------------
                   NON-SCOPE FUNCTIONS

    These are just plain-ol-boring functions that any Package
      object can call, nothing magical about these.

     ------------------------------------------------- */

  /**
   * Gets the resident who signed out the item
   *
   * @return App\Resident
  */
  private function resident()
  {
    if (is_null($this->my_resident))
      $this->my_resident = Resident::find($this->attributes['resident_id']);

    return $this->my_resident;
  }


  /**
   * Gets the building the item belongs to
   *
   * @return App\Building
  */
  private function building()
  {
    if (is_null($this->my_building))
      $this->my_building = Building::find($this->attributes['building_id']);

    return $this->my_building;
  }


  /**
   * Gets the item details
   *
   * @return App\Items
   */
  public function item()
  {
    if (is_null($this->my_item))
      $this->my_item = $this->config()->item;

    return $this->my_item;
  }

  /**
   * Gets the item config detauls
   *
   * @return App\ItemsConfig
   */
  public function config()
  {
    if (is_null($this->my_config))
      $this->my_config = ItemsConfig::find($this->attributes['item_config_id']);

    return $this->my_config;
  }


  /**
   * Checks if an item has been returned
   *
   * @return boolean
   */
  public function isReturned()
  {
    return (!is_null($this->attributes['time_in']));
  }


  /**
   * Gets a friendly date that the item was loaned
   *
   * @return string
   */
  private function dateOut()
  {
    return date("F d, Y, h:i A", strtotime($this->attributes['time_out']));
  }


  /**
   * Gets a friendly date the item was returned
   *
   * @return string
   */
  private function dateIn()
  {
    if ($this->isReturned()){
      return date("F d, Y, h:i A", strtotime($this->attributes['time_in']));
    }else{
      // Not returned, shouldn't have a time in date
      return "";
    }
  }


  /**
   * Gets the worker who checked the item back in
   *
   * @return App\DeskWorker
   */
  private function workerIn()
  {
    if (is_null($this->my_workerIn))
      $this->my_workerIn = DeskWorker::find($this->attributes['worker_in']);

    return $this->my_workerIn;
  }


  /**
   * Gets the worker who signed the item out to the resident
   *
   * @return App\DeskWorker
   */
  private function workerOut()
  {
    if (is_null($this->my_workerOut))
      $this->my_workerOut = DeskWorker::find($this->attributes['worker_out']);

    return $this->my_workerOut;
  }


  /**
   * Gets the signature
   *
   * @return App\Signatures
   */
  private function signature()
  {
    if (is_null($this->my_signature))
      $this->my_signature = Signatures::type('App\ItemLog')->id($this->attributes['id'])->direction('in')->first();

    return $this->my_signature;
  }




  /**
   * Will mark an item as returned
   *
   * @param Request $input the form input
   * @return void
   */
  public function returnItem($input)
  {
    // Check user permissions first (just in case)
    if (Auth::user()->permissions->canReturnItems())
    {
      $this->time_in = Carbon::now();
      $this->worker_in = Auth::user()->worker_id;

      // save the signature
      $sign = Signatures::create(['object' => 'App\ItemLog', 'object_id' => $this->id, 'direction' => 'in', 'value' => $input['signatureValue']]);
      $this->signature = $sign->id;
      $this->save();
    }

    return;
  }


  /**
   * Used to create a new item log entry
   *
   * @throws App\Exceptions\RedirectException
   * @throws App\Exceptions\PermissionsException
   * @param App\Resident $resident (Reference) the resident to loan an item to
   * @param App\Item $item the item to loan to the resident
   * @return the new package log entry
   */
  public static function createEntry(&$resident, &$item)
  {
    if (!is_object($resident))
      return;

    if (!is_object($item))
      return;

    $itemConfig = ItemsConfig::getItem($item->uID);

    // Make sure the user has permissions
    if (Auth::user()->worker->permissions->canLoanItems())
    {
      if (!is_null($itemConfig))
      {
        $item_info = [];
        $item_info['resident_id'] = $resident->id;
        $item_info['item_config_id'] = $itemConfig->uID;
        $item_info['time_out'] = Carbon::now();
        $item_info['worker_out'] = Auth::user()->worker_id;
        $item_info['building_id'] = $resident->building_id;

        $newItem = ItemLog::create($item_info);

        // Make 1 less item available
        $itemConfig->checkout();

        return $newItem;
      }

      // Item not found, error
      throw new RedirectException('That item is not available.', Session::get('app_path', 'desk') . '/items', 'error', true);
    }

    // No permissions
    throw new PermissionsException(Session::get('app_path', 'desk') . '/items');

  }











  /**
   * Allows for $package->dateIn to actually call $package->dateIn()
   *
   * Allows other function calls to be made without (), does not work on scope functions or
   * functions with parameters
   *
   * @param string $key
   * @return the function, or calls parent __get
   */
  public function __get($key)
  {
    if (in_array($key, $this->functions))
      return $this->$key(); // Isn't PHP Awesome???

    // Go to the parent
    return parent::__get($key);
  }



  /**
   * Finds a package, returns an error if it failed
   *
   * @throws App\Exceptions\RedirectException
   * @param int $id The ID of the package to find
   * @param string $returnPath (Default: 'desk/packages') The path to return to if it fails
   * @return App\Package or redirect
   */
  public static function findOrError($id, $returnPath = 'desk/items')
  {
    $entry = ItemLog::find($id);

    if (!is_null($entry))
      return $entry;

    // Generate the error
    $error = [];
    $error['type'] = 'error_dismisses';
    $error['msg'] = 'Item Log Entry was not found.';

    throw new RedirectException($error, $returnPath);
  }

}


/**
 *
 *
 * @param
 * @return
 */
