<?php

namespace App;

use Illuminate\Database\Eloquent\Model;



/**
 * Class to represent a user on the API
 *
 */
class APIUsers extends Model
{
  protected $primaryKey = 'resident_id';
  protected $table = 'api_users';

  public $timestamps = false;

  // Fillable stuff
  protected $fillable = ['id', 'resident_id', 'api_key'];
  private $functions = ['key'];



  /**
   * Gets the API key
   *
   * @return string
   */
  private function key()
  {
    return $this->attributes['api_key'];
  }



  /**
   * Will return a
   *
   * @return string
   */
  public static function generate($resident_id)
  {
    $results = APIUsers::find($resident_id);

    // Check if a user was found
    if (!is_null($results))
      return $results;

    // No user found, create one
    $key = APIUsers::generate_key($resident_id);
    return APIUsers::create(['resident_id' => $resident_id, 'api_key' => $key]);
  }


  /**
   * Generates a new API key for a user
   *
   * @return string
   */
  public function newKey()
  {
    $this->api_key = APIUSers::generate_key($this->resident_id);
    $this->save();
  }


  /**
   * Takes a resident ID and generates an API key for them
   *
   * @return string
   */
  public static function generate_key($resident_id)
  {
    return sha1(rand(0, 9978199) . $resident_id . rand(9978199, 12498498749849));
  }


  /**
   * Allows for function calles without using ()
   *
   * @param string $key the variable to lookup
   * @return yes
   */
  public function __get($key)
  {
    if (in_array($key, $this->functions))
      return $this->$key();

    // Try the parent __get function
    return parent::__get($key);
  }
}




/**
 *
 *
 * @param
 * @return
 */
