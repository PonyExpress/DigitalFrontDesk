<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\PackageLog;
use App\KeyLog;
use App\ItemLog;



/**
 * Class to represent a Signature
 *
 */
class Signatures extends Model
{
  protected $primaryKey = 'id';
  protected $table = 'signatures';

  public $timestamps = false;

  // Fillable stuff
  protected $fillable = ['id', 'object', 'object_id', 'direction', 'value'];

  private $functions = ['parent', 'image', 'url']; // List of functions to make accessible from the __get magic operator

  private $my_parent = null;


  /**
   * Narrows a query to signatures for a specific type
   *
   * @return Query
   */
  public function scopeType($query, $type)
  {
    return $query->where('object', '=', $type);
  }


  /**
   * Narrows a query to signatures for a specific type
   *
   * @return Query
   */
  public function scopeId($query, $id)
  {
    return $query->where('object_id', '=', $id);
  }


  /**
   * Narrows a query to signatures for a specific type
   *
   * @return Query
   */
  public function scopeDirection($query, $dir)
  {
    return $query->where('direction', '=', $dir);
  }




  /**
   * Gets the parent object
   *
   * @return object
   */
  private function parent()
  {
    if (is_null($this->my_parent))
    {
      switch (strtolower($this->attributes['object']))
      {
        case 'app\itemlog':
          $this->my_parent = ItemLog::find(intval($this->attributes['object_id']));
          break;

        case 'app\keylog':
          $this->my_parent = KeyLog::find(intval($this->attributes['object_id']));
          break;

        case 'app\packagelog':
          $this->my_parent = PackageLog::find(intval($this->attributes['object_id']));
          break;

        case 'app\dollylog':
          $this->my_parent = DollyLog::find(intval($this->attributes['object_id']));
          break;
      }
    }

    return $this->my_parent;
  }


  /**
   * Returns the base64 image URL of the signature
   *
   * @return String
   */
  private function image()
  {
    return 'data:image/png;base64,' . $this->attributes['value'];
  }

  /**
   * Returns the url for the image
   *
   * @return String
   */
  private function url()
  {
    $type = $this->attributes['object'];
    switch (strtolower($type))
    {
      case 'app\itemlog':
        $type = 'item';
        break;

      case 'app\keylog':
        $type = 'key';
        break;

      case 'app\packagelog':
        $type = 'package';
        break;
        
      case 'app\dollylog':
        $type = 'dolly';
        break;
    }

    return \URL::to('signature/' . $type . '/' . $this->attributes['object_id'] . '/' . $this->attributes['direction'] . '.png');
  }








  /**
   * Allows to access private functions without using parenthesis
   *
   * @param string $key the variable to lookup
   * @return yes
   */
  public function __get($key)
  {
    if (in_array($key, $this->functions))
      return $this->$key();

    // Try the parent __get function
    return parent::__get($key);
  }
}





/**
 *
 *
 * @param
 * @return
 */
