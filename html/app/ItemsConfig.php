<?php

namespace App;

use Auth;

use App\Building;
use App\Resident;
use App\Items;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


/**
 * Responsible for handling items
 *
 */
class ItemsConfig extends Model
{
  protected $primaryKey = 'uID';
  protected $table = 'items_config';

  public $timestamps = false;

  // Fillable stuff
  protected $fillable = ['uID', 'building_id', 'building_group', 'item_id', 'quantity', 'available'];

  private $functions = ['building', 'item', 'quantity', 'available'];

  private $my_item = null;



  /**
   * Narrows a query to only items in the worker's buildin g
   *
   * @param Builder $query
   * @return the narrowed query
   */
  public function scopeInBuilding($query)
  {
    return $query->where('building_group', '=', Auth::user()->worker->building->id);
  }


  /**
   * Narrows a query to only available items
   *
   * @param Builder $query
   * @return the narrowed query
   */
  public function scopeAvailable($query)
  {
    return $query->where('available', '>', '0');
  }


  public static function getItem($id)
  {
    return ItemsConfig::inBuilding()->available()->where('item_id', '=', $id)->first();
  }








  /**
   * Gets the building a config belongs to
   *
   * @return App\Building
   */
  public function building ()
  {
    return Building::find($this->attributes['building_id']);
  }

  /**
   * Gets the item details
   *
   * @return App\Items
   */
  public function item()
  {
    if (is_null($this->my_item))
      $this->my_item = Items::find($this->attributes['item_id']);

    return $this->my_item;
  }


  /**
   * Gets the quantity
   *
   * @return int
   */
  public function quantity()
  {
    return $this->attributes['quantity'];
  }


  /**
   * Gets the number of available items
   *
   * @return int
   */
  public function available()
  {
    return $this->attributes['available'];
  }


  /**
   * Gets the item details and stuff as an array
   *
   * @return array
   */
  public function asArray()
  {
    $item = [];
    $item['item'] = $this->item();
    $item['building'] = $this->building();
    $item['quantity'] = $this->quantity();
    $item['available'] = $this->available();
    return $item;
  }


  public function checkout()
  {
    $this->attributes['available'] = $this->attributes['available'] - 1;

    if ($this->attributes['available'] < 0)
      $this->attributes['available'] = 0;

    if ($this->attributes['available'] > $this->attributes['quantity'])
      $this->attributes = $this->attributes['quantity'];


    $this->save();
  }



  /**
   * Allows for functions and properties to be called in a much
   * prettier way
   *
   * @param string $key the variable/function to lookup
   * @return the value
   */
  public function __get($key)
  {
    if (in_array($key, $this->functions))
      return $this->$key();

    // Not a function, go to the parent (for $this->attribute[$key])
    return parent::__get($key);
  }

}


/**
 *
 *
 * @param
 * @return
 */
