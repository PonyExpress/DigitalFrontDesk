<?php

namespace App;

use Auth;

use App\DeskWorker;
use App\Building;
use App\Resident;
use App\PunchClock;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


/**
 * This class handles all the permissions for a worker, and permission checking
 *
 */
class WorkerPermissions extends Model
{
  protected $primaryKey = 'worker_id';
  protected $table = 'desk_worker_permissions';

  public $timestamps = false;

  private $functions = ['worker'];

  protected $fillable = ['worker_id', 'new_packages', 'signout_packages', 'loan_keys', 'return_keys', 'loan_items', 'return_items',
                          'message_log', 'punch_clock', 'send_emails'];


  private $my_worker = null;




  /**
   * Returns an object of the worker
   *
   * @return App\DeskWorker
   */
  private function worker()
  {
    if (is_null($this->my_worker))
      $this->my_worker = DeskWorker::find($this->worker_id);

    return $this->my_worker;
  }


  /**
   * Tells if the user can compose emails
   *
   * @return boolean
   */
  public function canComposeEmails()
  {
    if ($this->worker()->isDummy()) { return true; }

    if ($this->worker()->building->hasResidents())
    {
      // Return true if a user is an admin
      if ($this->worker()->isAdmin())
        return true;

      // Return their permissions (not an admin)
      return ($this->attributes['send_emails'] != 0);
    }

    // No residents
    return false;
  }


  /**
   * Tells if the user can loan out keys
   *
   * @return boolean
   */
  public function canLoanKeys()
  {
    if ($this->worker()->isDummy()) { return true; }

    if ($this->worker()->building->hasKeys())
    {
      // Return true if an admin
      if ($this->worker()->isAdmin())
        return true;

      // Return if they have appropriate permissions
      return ($this->attributes['loan_keys'] != 0);
    }

    // No keys
    return false;
  }


  /**
   * Tells if the user can return loaned keys
   *
   * @return boolean
   */
  public function canReturnKeys()
  {
    if ($this->worker()->isDummy()) { return true; }

    if ($this->worker()->building->hasKeys())
    {
      // Return true if an admin
      if ($this->worker()->isAdmin())
        return true;

      // Return true if their permissions allow it
      return ($this->attributes['return_keys'] != 0);
    }

    // No keys
    return false;
  }


  /**
   * Tells if the user can create packages
   *
   * @return boolean
   */
  public function canCreatePackages()
  {
    if ($this->worker()->isDummy()) { return true; }

    if ($this->worker()->building->hasPackages())
    {
      // Return true if an admin
      if ($this->worker()->isAdmin())
        return true;

      // Return true if their permissions allow it
      return ($this->attributes['new_packages'] != 0);
    }

    // No packages
    return false;
  }


  /**
   * Tells if the user can signout packages
   *
   * @return boolean
   */
  public function canSignOutPackages()
  {
    if ($this->worker()->isDummy()) { return true; }

    if ($this->worker()->building->hasPackages())
    {
      // Return true if an admin
      if ($this->worker()->isAdmin())
        return true;

      // Return true if their permissions allow it
      return ($this->attributes['signout_packages'] != 0);
    }

    // No packaging system
    return false;
  }


  /**
   * Tells if the user can loan items
   *
   * @return boolean
   */
  public function canLoanItems()
  {
    if ($this->worker()->isDummy()) { return true; }

    if ($this->worker()->building->hasItems() || $this->worker()->building->hasGameTrax())
    {
      // Return true if an admin
      if ($this->worker()->isAdmin())
        return true;

      // Return true if their permissions allow it
      return ($this->attributes['loan_items'] != 0);
    }

    return false;
  }


  /**
   * Tells if the user can return items
   *
   * @return boolean
   */
  public function canReturnItems()
  {
    if ($this->worker()->isDummy()) { return true; }

    if ($this->worker()->building->hasItems() || $this->worker()->building->hasGameTrax())
    {
      // Return true if an admin
      if ($this->worker()->isAdmin())
        return true;

      // Return true if their permissions allow it
      return ($this->attributes['return_items'] != 0);
    }

    return false;
  }


  /**
   * Tells if the user can visit the message log
   *
   * @return boolean
   */
  public function messageLogAccess()
  {
    if ($this->worker()->isDummy()) { return true; }

    if ($this->worker()->building->hasMessageLog())
    {
      // Return true if an admin
      if ($this->worker()->isAdmin())
        return true;

      // Return true if their permissions allow it
      return ($this->attributes['message_log'] != 0);
    }

    return false;
  }


  /**
   * Punch Clock Access
   *
   * @return boolean
   */
  public function punchClockAccess()
  {
    if ($this->worker()->isDummy()) { return true; }

    if ($this->worker()->building->hasMessageLog() || true)
    {
      // Return true if an admin
      if ($this->worker()->isAdmin())
        return true;

      // Return true if their permissions allow it
      return ($this->attributes['punch_clock'] != 0);
    }

    return false;
  }


  /**
   * Tells if the worker can send emails0
   *
   * @return boolean
   */
  public function canSendEmails()
  {
    if ($this->worker()->isDummy()) { return true; }

    if (true)
    {
      // Return true if an admin
      if ($this->worker()->isAdmin())
        return true;

      // Return true if their permissions allow it
      return ($this->attributes['send_emails'] != 0);
    }

    return false;
  }






  /**
   * Tells whether a worker can view something
   *
   * This function uses PHPs awesome function get_class(), which will return
   * a string value of the type of class something is. Then calls a private
   * function based on the class.
   *
   * Example:
   *   $var = new App\Resident;
   *   echo get_class($var);
   *
   * That example would echo "App\Resident"
   *  Docs: http://php.net/manual/en/function.get-class.php
   *
   * @param Object $object - The object to check if the user can view it
   * @return boolean
   */
  public function canView($object)
  {
    // First verify that the parameter is an object, and not null
    if (is_object($object) && !is_null($object))
    {
      $class = get_class($object); // Get the class that the object is

      // App\Resident
      if ($class == "App\Resident")
        return $this->canViewResident($object);

      // App\Package
      if ($class == "App\PackageLog")
        return $this->canViewPackage($object);

      // App\KeyLog
      if ($class == "App\KeyLog")
        return $this->canViewKeyLog($object);

      // App\PunchCard
      if ($class == 'App\PunchClock')
        return $this->canViewPunchClock($object);

      // App\DollyLog
      if ($class == 'App\DollyLog')
        return true;

      // App\Building
      //if ($class == "App\Building")
        //return $this->canViewBuilding($object);

      // App\Key
      //if ($class == "App\Key")
        //return $this->canViewKey($object);

    }

    // object is invalid, deny
    return false;
  }


  /**
   * Tells if a user can view a certain resident
   *
   * @param App\Resident $resident
   * @return boolean
   */
  private function canViewResident($resident)
  {
    if ($this->worker()->building->hasResidents() && !$resident->isRemoved())
    {
      // Check if the building IDs are different
      if ($resident->building->id != $this->worker()->building->id)
        return ($this->worker()->isAdmin()); // Allow access to different building only if user is an admin

      // Same building, return true
      return true;
    }

    // No residents in the building
    return false;
  }


  /**
   * Tells if a user can view a certain package
   *
   * @param App\Package $package the package to check if viewable
   * @return boolean
   */
  private function canViewPackage($package)
  {
    if ($this->worker()->building->hasPackages() && !$package->resident->isRemoved())
    {
      // Check if buildings are different
      if ($package->building->id != $this->worker()->building->id)
        return ($this->worker()->isAdmin()); // Only allow if user is admin

      return true; // Same buiding
    }

    // No packages
    return false;
  }


  /**
   * Tells if a user can access a certain key log
   *
   * @param App\KeyLog $keylog
   * @return boolean
   */
  private function canViewKeyLog($keylog)
  {
    if ($this->worker()->building->hasKeys() && !$keylog->resident->isRemoved())
    {
      // Check if they are for the same building
      if ($keylog->building->id != $this->worker()->building->id)
        return ($this->worker()->isAdmin());

      return true;
    }

    // No key log
    return false;
  }


  /**
   * Tells if a user can access a certain punch clock entry
   *
   * @param App\PunchClock $punch
   * @return boolean
   */
  private function canViewPunchClock($punch)
  {
    if ($this->worker()->building->hasPunchClock())
    {
      // Check if the punch clock entry belongs to the signed in user
      if ($punch->worker_id != $this->worker()->worker_id)
        return $this->worker()->isAdmin();

      return true;
    }

    return false;
  }



  /**
   * Returns the permissions as an array
   *
   * @return array
   */
  public function asArray()
  {
    $permissions = [];
    $permissions['new_packages'] = ($this->new_packages != 0);
    $permissions['signout_packages'] = ($this->signout_packages != 0);
    $permissions['loan_keys'] = ($this->loan_keys != 0);
    $permissions['return_keys'] = ($this->return_keys != 0);
    $permissions['loan_items'] = ($this->loan_items != 0);
    $permissions['return_items'] = ($this->return_items != 0);
    $permissions['message_log'] = ($this->message_log != 0);
    $permissions['punch_clock'] = ($this->punch_clock != 0);
    $permissions['send_emails'] = ($this->send_emails != 0);

    return $permissions;
  }


  /**
   * Allows for $resident->building to actually call $resident->building()
   * And other function calls to be made without the parenthesis
   *
   * This will not work on scope functions though, or functions that have
   * parameters
   *
   * @param string $key the variable to lookup
   * @return yes
   */
  public function __get($key)
  {
    if (in_array($key, $this->functions))
      return $this->$key();

    // Not a function, go to the parent (for $this->attribute[$key])
    return parent::__get($key);
  }
}





/**
 *
 *
 * @param
 * @return
 */
