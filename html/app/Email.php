<?php

namespace App;

use App\Resident;
use App\EmailLog;

use Mail;

use Auth;
use SocialAuth;

use Illuminate\Database\Eloquent\Model;

use Exceptions;
use App\Exceptions\EmailException;

use Carbon\Carbon;


/**
 * Handles sending an email with text (no template)
 *
 * @throws Exceptions\EmailException for an error in sending
 *
 */
class Email
{
  private $content; // Content in the email

  private $resident;

  private $errorCode;

  /**
   * Email Constructor
   *
   * This will build an email using $details and $resident
   *
   * @todo Add in resident settings on if they want to receive notifications and an alternate email if wanted
   * @throws Exceptions\EmailException for an error in the email composing
   * @see ResidentController::sendEmail()
   * @param array $details the details of the emial to compose
   * @param App\Resident $resident the resident object to send to
   */
  public function __construct($details, $resident)
  {
    // Throw exception if $details is not an array or if it's empty
    if (!is_array($details) || empty($details) || is_null($resident))
    {
      // Everything is just wrong, wrong, wrong!
      throw new EmailException('Invalid parameters providied for the Email class', 99);
    }
    else
    {
      // Make sure $details has required array keys
      if (!array_key_exists('recipient', $details))
        throw new EmailException('The parameter accepted by the Email class must have a key named \'recipient\'', 51);

      if (!array_key_exists('subject', $details))
        throw new EmailException('The parameter accepted by the Email class must have a key named \'subject\'', 52);

      if (!array_key_exists('body', $details))
        throw new EmailException('The parameter accepted by the Email class must have a key named \'body\'', 53);

      // Check to see if a resident exists with the email
      if(is_null($resident))
        throw new EmailException('No resident specified', 100);

      // Make sure recipient email and resident email match
      if ($resident->email != $details['recipient'])
        throw new EmailException('Email of the specified resident does not match the email you wish to send.', 505);


      // $details is okay, proceed
      $this->resident = $resident;

      $this->content = $details;
      $this->content['name'] = $resident->formalName;
    }

    $this->errorCode = 0;
  }


  /**
   * Send an email
   *
   * @todo Do something with this return value...
   * @throws Exceptions\EmailException for an error in sending
   * @return true
   */
  public function send()
  {
    // Throw exception if the string is empty
    if ($this->isEmpty())
      throw new EmailException($this->errorMessage(), $this->errorCode);

    if (is_null($this->resident))
      throw new EmailException('No resident specified.', 99);

    // Send the email
    $details = $this->content;

    // Send the email
    Mail::send('emails.template', ['body' => $this->content['body']], function($message) use ($details)
              {
                $message->to($details['recipient'], $details['name'])->subject($details['subject']);
              });


    // Log that the email happened in the database (for Admin use)
    $logData = ['resident_id' => $this->resident->id, 'subject' => $this->content['subject'],
                'body' => preg_replace('/\r\n|\r|\n/', '<br/>', $this->content['body'])];

    // Create the email log
    EmailLog::newEntry($logData);


    return true;
  }


  /**
   * Will return true if the email is empty
   *
   * If it is empty then send() will throw an exception
   * This function will set an error code according to what is empty
   *
   * @return true if the email doesn't have any content
   */
  private function isEmpty()
  {
    $to = $this->content['recipient'];
    $to = preg_replace('/\s+/', '', $to); // Replace all white space with nothing

    $subject = $this->content['subject'];
    $subject = preg_replace('/\s+/', '', $subject); // Replace all white space with nothing

    $body = $this->content['body'];
    $body = preg_replace('/\s+/', '', $body); // Replace all white space with nothing

    // Set Error Code(s) if needed
    if ($body == "")
      $this->errorCode = 3;

    if ($subject == "")
      $this->errorCode = 2;

    if ($to == "")
      $this->errorCode = 1;


    // Return true if ANY of the items (body, subject, to) are empty
    return ($this->errorCode != 0);
  }



  /**
   * Will retrieve a friendly message for the current errorCode in this class
   *
   * @return string of the error message
   */
  private function errorMessage()
  {
    // $messages[errorCode] will return the error message
    // $messages[0] is empty because when $this->errorCode is 0 there are no error states happening
    $messages = [ 0 => '',
                  1 => 'Email not sent because the Recipient field is empty.',
                  2 => 'Email not sent because the Subject field is empty.',
                  3 => 'Email not sent because the Body field is empty.',
                  4 => '',
                  5 => '',
                ];

    return $messages[$this->errorCode];
  }


  /**
   * Allows for getting/setting of the resident object to send an email to
   *
   * @param App\Resident $newRes (Default: null) the new resident object to send to
   * @return
   */
  public function resident($newRes = null)
  {
    if ($newRes == null)
      return $this->resident;
    else
      $this->resident = $newRes;
  }


  /**
   * Returns an error code
   *
   * @return returns the current errorCode
   */
  public function error()
  {
    return $this->errorCode;
  }


  /**
   * Checks for an error
   *
   * @return true if an error exists in the class
   */
  public function isError()
  {
    return ($this->errorCode != 0);
  }


  /**
   * Allos for getting of stuff in the $content array
   *
   * @param string $key the key to return
   * @return the key value in $this->content
   */
  public function __get($key)
  {
    if (array_key_exists($key, $this->content))
      return $this->content[$key];
    else
      return "";
  }
}





/**
 *
 *
 * @param
 * @return
 */
