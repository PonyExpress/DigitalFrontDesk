<?php

namespace App;

use Auth;

use App\Building;
use App\Resident;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


/**
 * Responsible for handling items
 *
 */
class DollyConfig extends Model
{
  protected $primaryKey = 'uID';
  protected $table = 'dolly_config';

  public $timestamps = false;

  // Fillable stuff
  protected $fillable = ['uID', 'building_id', 'quantity', 'available'];

  private $functions = ['building', 'quantity', 'available'];

  private $my_dolly = null;


  /**
   * Gets packages just in the worker's building
   *
   * @param QueryBuilder $query
   * @return the modified $query
   */
  public function scopeInBuilding($query)
  {
    $building = Auth::user()->worker->building;

    // Add the direct building the user works for
    $query->where('building_id', '=', $building->uID);

    // Add any other sub-buildings (RC2, RC3)
    $buildings = Building::where('id', '=', $building->id)->get()->toArray();

    foreach($buildings as $theBuilding)
    {
      // Don't add the same building twice
      if ($theBuilding['uID'] != $building->uID)
        $query->orWhere('building_id', '=', $theBuilding['uID']);
    }

    return $query;
  }


  /**
   * Narrows a query to only available items
   *
   * @param Builder $query
   * @return the narrowed query
   */
  public function scopeAvailable($query)
  {
    return $query->where('available', '>', '0');
  }



  /**
   * Gets the building a config belongs to
   *
   * @return App\Building
   */
  public function building ()
  {
    return Building::find($this->attributes['building_id']);
  }


  /**
   * Gets the quantity
   *
   * @return int
   */
  public function quantity()
  {
    return $this->attributes['quantity'];
  }


  /**
  * Gets the item details and stuff as an array
  *
  * @return array
  */
  public function asArray()
  {
    $item = [];
    $item['building'] = $this->building();
    $item['quantity'] = $this->quantity();
    $item['available'] = $this->available();
    return $item;
  }


  public function checkout()
  {
    $this->attributes['available'] = $this->attributes['available'] - 1;

    if ($this->attributes['available'] < 0)
      $this->attributes['available'] = 0;

    if ($this->attributes['available'] > $this->attributes['quantity'])
      $this->attributes = $this->attributes['quantity'];


    $this->save();
  }



  /**
   * Allows for functions and properties to be called in a much
   * prettier way
   *
   * @param string $key the variable/function to lookup
   * @return the value
   */
  public function __get($key)
  {
    if (in_array($key, $this->functions))
      return $this->$key();

    // Not a function, go to the parent (for $this->attribute[$key])
    return parent::__get($key);
  }

}
