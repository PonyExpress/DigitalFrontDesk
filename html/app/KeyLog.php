<?php

namespace App;

use Auth;

use Session;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

use App\Key;
use App\Building;
use App\Resident;
use App\PackageLog;
use App\Signatures;

use App\Exceptions\RedirectException;

use App\Utilities;


/**
 * Class to represent an entry in the key_log table
 *
 */
class KeyLog extends Model
{
  private $my_resident = NULL; // Object that equals the resident that the package belongs to
  private $my_key = NULL; // Object for the key
  private $my_building = NULL;
  private $my_workerIn = NULL;
  private $my_workerOut = NULL;
  private $my_signatureOut = NULL;
  private $my_signatureIn = NULL;

  protected $primaryKey = 'id';
  protected $table = 'key_log';

  public $timestamps = false;

  protected $fillable = ['resident_id', 'key_id', 'time_in', 'time_out', 'worker_in', 'worker_out', 'signature_in', 'signature_out', 'building_id'];

  private $functions = ['resident', 'building', 'key', 'dateIn', 'dateOut', 'workerIn', 'workerOut', 'signatureIn', 'signatureOut']; // List of function names without parenthesis



  /* -------------------------------------------------
                    SCOPE FUNCTIONS
   ------------------------------------------------- */

  /**
   * Narrows the scope of a query to keys that have not been returned
   *
   * @param Builder $query
   * @return the modified $query Builder
   */
  public function scopeNotReturned($query)
  {
    return $query->whereRaw('`time_in` IS NULL');
  }


  /**
   * Narrows the scope of a query to keys that have been returned
   *
   * @param Builder $query
   * @return The modifed $query
   */
  public function scopeReturned($query)
  {
    return $query->whereRaw('`time_in` IS NOT NULL');
  }


  /**
   * Narrows query to keys that were loaned to a certain resident
   *
   * @param QueryBuilder $query
   * @param int $id the ID of the resident to find keys loaned to
   * @return The modified Query
   */
  public function scopeLoanedTo($query, $id)
  {
    return $query->whereRaw("`resident_id` = '".$id."'");
  }


  /**
   * Narrows query to return the oldest key logs first
   *
   * @param QueryBuilder $query
   * @return the modified $query
   */
  public function scopeOldest($query)
  {
    return $query->orderBy('time_out', 'ASC');
  }


  /**
   * Narrows query to show newer key log entries first
   *
   * @param QueryBuilder $query
   * @return the modified query
   */
  public function scopeNewest($query)
  {
    return $query->orderBy('time_out', 'DESC');
  }


  /**
   * Returns the maximum ID in a key log
   *
   * @param QueryBuilder $query
   * @return the modified $query
   */
  public function scopeMaxID($query)
  {
    return $query->whereRaw('id = (select max(`id`) from `key_log`)');
  }


  /**
   * Gets KeyLogs just in the worker's building
   *
   * @param QueryBuilder $query
   * @return the modified $query
   */
  public function scopeInBuilding($query)
  {
    $building = Auth::user()->worker->building;

    // Add the direct building the user works for
    $query->where('building_id', '=', $building->uID);

    // Add any other sub-buildings (RC2, RC3)
    $buildings = Building::where('id', '=', $building->id)->get()->toArray();

    foreach($buildings as $theBuilding)
    {
      // Don't add the same building twice
      if ($theBuilding['uID'] != $building->uID)
        $query->orWhere('building_id', '=', $theBuilding['uID']);
    }

    return $query;
  }




  /* -------------------------------------------------
                   NON-SCOPE FUNCTIONS

    These are just plain-ol-boring functions that any Key checkout
      object can call, nothing magical about these.

     ------------------------------------------------- */

  /**
   * Returns a reference object to the resident a key log entry is for
   *
   * @return App\Resident object
   */
  private function resident()
  {
    if ($this->my_resident == NULL)
      $this->my_resident = Resident::find($this->attributes['resident_id']);//$this->belongsTo('App\Resident', 'resident_id', 'id');

    return $this->my_resident;
  }

  /**
   * Gets the building the key log is in
   *
   * @return App\Building object
   */
  private function building()
  {
    if (is_null($this->my_building))
      $this->my_building = Building::find($this->attributes['building_id']);

    return $this->my_building;
  }


  /**
   * Gets the key that was checked out
   *
   * @return App\Key object
   */
  private function key()
  {
    if (is_null($this->my_residet))
    {
      $this->my_key = Key::find($this->attributes['key_id']);//$this->belongsTo('App\Key', 'key_id', 'id');
    }

    return $this->my_key;
  }


  /**
   * Finds out if the key has been returned
   *
   * @return True if the key has been returned
   */
  public function isReturned()
  {
    return (!is_null($this->attributes['time_in']));
  }


  /**
   * Shows friendly date of the time the key was returned
   *
   * @return string
   */
  private function dateIn()
  {
    return date("F d, Y, h:i A", strtotime($this->attributes['time_in']));
  }


  /**
   * Shows friendly date of the time the key was loaned
   *
   * @return string
   */
  private function dateOut()
  {
    return date("F d, Y, h:i A", strtotime($this->attributes['time_out']));
  }


  /**
   * Gets the worker who the key was returned to
   *
   * @return App\DeskWorker
   */
  private function workerIn()
  {
    if (is_null($this->my_workerIn))
    {
      $this->my_workerIn = DeskWorker::find($this->attributes['worker_in']);
    }
    return $this->my_workerIn;
  }


  /**
   * Gets the desk worker who loaned the key
   *
   * @return App\DeskWorker
   */
  private function workerOut()
  {
    if (is_null($this->my_workerOut))
    {
      $this->my_workerOut = DeskWorker::find($this->attributes['worker_out']);
    }
    return $this->my_workerOut;
  }


  /**
   * Gets the signature
   *
   * @return App\Signatures
   */
  private function signatureOut()
  {
    if (is_null($this->my_signatureOut))
      $this->my_signatureOut = Signatures::type('App\KeyLog')->id($this->attributes['id'])->direction('out')->first();

    return $this->my_signatureOut;
  }


  /**
   * Gets the signature
   *
   * @return App\Signatures
   */
  private function signatureIn()
  {
    if (is_null($this->my_signatureIn))
      $this->my_signatureIn = Signatures::type('App\KeyLog')->id($this->attributes['id'])->direction('in')->first();

    return $this->my_signatureIn;
  }



  /**
   * Returns a package
   *
   * @param Request $input the form input
   */
  public function returnKey($input)
  {
    // Make sure user can return keys
    if (Auth::user()->permissions->canReturnKeys())
    {
      // Has permissions
      $this->time_in = Carbon::now(); // Date and time value of when the package was signed out
      $this->worker_in = Auth::user()->worker_id;

      // Save the signature
      $sign = Signatures::create(['object' => 'App\ItemLog', 'object_id' => $this->id, 'direction' => 'in', 'value' => $input['signatureValue']]);
      $this->signature_in = $sign->id;
      $this->save();
    }
  }




  /**
   * Allows for $keylog->building to actually call $keylog->building()
   * And other function calls to be made without the parenthesis
   *
   * This will not work on scope functions though, or functions that have
   * parameters
   *
   * @param string $key the variable to lookup
   * @return yes
   */
  public function __get($key)
  {
    if (in_array($key, $this->functions))
      return $this->$key();

    // Not a function, go to the parent (for $this->attribute[$key])
    return parent::__get($key);
  }


  /**
   * Finds a keylog, returns an error if it failed
   *
   * @throws App\Exceptions\RedirectException
   * @param int $id The ID of the keylog entry to find
   * @param string $returnPath (Default: 'desk/keys') The path to return to if it fails
   * @return App\KeyLog or redirect
   */
  public static function findOrError($id, $returnPath = 'desk/keys')
  {
    $entry = KeyLog::find($id);

    if (!is_null($entry))
      return $entry;

    // Generate the error
    throw new RedirectException('Key Log entry was not found.', $returnPath, 'error', true);

  }

}



/**
 *
 *
 * @param
 * @return
 */
