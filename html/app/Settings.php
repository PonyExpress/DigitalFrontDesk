<?php

namespace App;

use Illuminate\Database\Eloquent\Model;



/**
 * This is the class that represents signed in users,
 * Can be accessed using Auth::user()
 *
 */
class Settings extends Model {

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'app_settings';
  protected $primaryKey = 'id';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['organization', 'time_out_time'];

  private $functions = ['organization', 'time_out'];


  private function organization()
  {
    if (!isset($this->attributes['organization']) || is_null($this->attributes['organization']))
      return "Organization";

    return $this->attributes['organization'];
  }

  private function time_out()
  {
    if (!isset($this->attributes['time_out_time']) || is_null($this->attributes['time_out_time']))
      return "1200000"; // Default time is 20 minutes

    return $this->attributes['time_out_time'];
  }

  public function __get($key)
  {
    if (in_array($key, $this->functions))
      return $this->$key();

    // Not a function, go to the parent (for $this->attribute[$key])
    return parent::__get($key);

  }

}





/**
 *
 *
 * @param
 * @return
 */
