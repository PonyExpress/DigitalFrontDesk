<?php

namespace App;

use Auth;

use Session;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

use App\Key;
use App\Building;
use App\Resident;
use App\PackageLog;
use App\DeskWorker;
use App\MessageLogViews;


use App\Exceptions\RedirectException;

use App\Utilities;


/**
 * Class to represent an entry in the message log
 *
 */
class MessageLog extends Model
{
  protected $primaryKey = 'id';
  protected $table = 'message_log';

  public $timestamps = false;

  protected $fillable = ['worker_id', 'message', 'building_id', 'date_posted'];

  private $functions = ['poster', 'building', 'datePosted']; // List of function names without parenthesis

  private $my_worker = NULL;
  private $my_building = NULL;


  /**
   * Gets KeyLogs just in the worker's building
   *
   * @param QueryBuilder $query
   * @return the modified $query
   */
  public function scopeInBuilding($query)
  {
    $building = Auth::user()->worker->building;

    // Add the direct building the user works for
    $query->where('building_id', '=', $building->uID);

    // Add any other sub-buildings (RC2, RC3)
    $buildings = Building::where('id', '=', $building->id)->get()->toArray();

    foreach($buildings as $theBuilding)
    {
      // Don't add the same building twice
      if ($theBuilding['uID'] != $building->uID)
        $query->orWhere('building_id', '=', $theBuilding['uID']);
    }

    return $query;
  }


  public static function getUnviewedMessages()
  {
    $worker = Auth::user()->worker;

    $lastViewedMessage = MessageLogViews::where('worker_id', $worker->id)->orderBy('date_viewed', 'DESC')->limit(1)->get();

    if (sizeof($lastViewedMessage) == 0)
    {
      // All Messages are unviewed
      return sizeof(MessageLog::inBuilding()->orderBy('date_posted', 'DESC')->get());
    }

    // Return all messages newer than the date of the last viewed message
    return sizeof(MessageLog::where('date_posted', '>', $lastViewedMessage[0]->date_viewed)->inBuilding()->get());

  }


  /**
   * Returns a reference object to the resident a key log entry is for
   *
   * @return App\Resident object
   */
  private function poster()
  {
    if ($this->my_worker == NULL)
      $this->my_worker = DeskWorker::find($this->attributes['worker_id']);//$this->belongsTo('App\Resident', 'resident_id', 'id');

    return $this->my_worker;
  }


  /**
   * Gets the building the key log is in
   *
   * @return App\Building object
   */
  private function building()
  {
    if (is_null($this->my_building))
      $this->my_building = Building::find($this->attributes['building_id']);

    return $this->my_building;
  }


  /**
   * Shows friendly date of the time the key was returned
   *
   * @return string
   */
  private function datePosted()
  {
    return date("F d, Y, h:i A", strtotime($this->attributes['date_posted']));
  }


  public function hasViewed()
  {
    return Auth::user()->worker->hasViewedMessage($this->id);
  }


  public function markAsViewed()
  {
    MessageLogViews::create(['message_id' => $this->id, 'worker_id' => Auth::user()->worker_id, 'date_viewed' => Carbon::now()]);
  }



















  /**
   * Allows for $keylog->building to actually call $keylog->building()
   * And other function calls to be made without the parenthesis
   *
   * This will not work on scope functions though, or functions that have
   * parameters
   *
   * @param string $key the variable to lookup
   * @return yes
   */
  public function __get($key)
  {
    if (in_array($key, $this->functions))
      return $this->$key();

    // Not a function, go to the parent (for $this->attribute[$key])
    return parent::__get($key);

  }


}
