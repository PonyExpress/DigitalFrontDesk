<?php

namespace App;

use Auth;
use SocialAuth;
use Carbon\Carbon;
use App\EmailTemplate;


use Illuminate\Database\Eloquent\Model;

/**
 * Used for logging emails that residents send to students
 * It doesn't do much, but the class is needed
 *
 */
class EmailBlast extends Model
{
  protected $primaryKey = 'id';
  protected $table = 'email_blast';

  public $timestamps = false;

  protected $fillable = ['resident_id', 'building_id'];



  /**
   * Gets email blasts just in the worker's building
   *
   * @param QueryBuilder $query
   * @return the modified $query
   */
  public function scopeInBuilding($query)
  {
    $building = Auth::user()->worker->building;

    // Add the direct building the user works for
    $query->where('building_id', '=', $building->uID);

    // Add any other sub-buildings (RC2, RC3)
    $buildings = Building::where('id', '=', $building->id)->get()->toArray();

    foreach($buildings as $theBuilding)
    {
      // Don't add the same building twice
      if ($theBuilding['uID'] != $building->uID)
        $query->orWhere('building_id', '=', $theBuilding['uID']);
    }

    return $query;
  }



  /**
   * Puts a resident in a queue for an email
   *
   * @param App\Resident $resident A resident OBJECT, the resident who will be getting an email
   * @return nothing
   */
  public static function enqueue(&$resident)
  {

    // Create an entry in the email_blast table for the resident
    EmailBlast::create(['resident_id' => $resident->id, 'building_id' => $resident->building->id]);
  }


  /**
   * Sends all the email blasts
   *
   * @return nothing
   */
  public static function blast()
  {
    // Get all the blasts for a building
    $emails = EmailBlast::inBuilding()->get();

    foreach ($emails as $blast)
    {
      try
      {
        // Create a draft of an email with the email template
        $email = new EmailTemplate('emails.package', $blast->resident_id, ['subject' => 'You have recieved a package.']);

        $email->send(); // Send the email

        $blast->delete();
      }
      catch (EmailException $e)
      {
        // Do nothing... (Shhhhhh)
      }
    }
  }



}


/**
 *
 *
 * @param
 * @return
 */
