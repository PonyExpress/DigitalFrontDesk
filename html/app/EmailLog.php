<?php

namespace App;

use Auth;
use SocialAuth;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

/**
 * Used for logging emails that residents send to students
 * It doesn't do much, but the class is needed
 *
 */
class EmailLog extends Model
{
  protected $primaryKey = 'id';
  protected $table = 'email_log';

  public $timestamps = false;

  protected $fillable = ['resident_id', 'subject', 'body', 'time_sent', 'worker_id'];


  /**
   * Create a email_log entry
   *
   * @param array $details the contents of the log
   * @return nothing
   */
  public static function newEntry($details)
  {
    if (array_key_exists('resident_id', $details) && array_key_exists('subject', $details) && array_key_exists('body', $details))
    {
      $details['time_sent'] = Carbon::now();
      $details['worker_id'] = Auth::user()->worker_id;
      $details['building_id'] = Auth::user()->worker->building_id;

      // Create the entry
      EmailLog::create($details);
    }

    return;
  }
}


/**
 *
 *
 * @param
 * @return
 */
