<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Administrators;
use App\Resident;
use App\Building;
use App\WorkerPermissions;
use App\MessageLogViews;


/**
 * Class to represent Desk Worker privileges, and other stuff
 *
 */
class DeskWorker extends Model
{
  protected $primaryKey = 'id';
  protected $table = 'desk_workers';

  public $timestamps = false;

  private $my_building = null;
  private $my_resident = null;
  private $my_admin = null;
  private $my_privileges = null;


  protected $fillable = ['resident_id', 'building_id', 'initials', 'removed'];

  private $functions = ['resident', 'building', 'permissions', 'admin']; // Function names without () for the __get function




  /* -------------------------------------------------
                   DATABASE FUNCTIONS
   ------------------------------------------------ */

   /**
    * Creates a desk worker and the desk worker permissions
    *
    * @param Array $information - the information about the worker
    * @param Array $permissions - the permissions for the worker
    * @return App\DeskWorker
    */
  public static function createWorker($information, $permissions = [])
  {
    // Create the worker
    $new_worker = DeskWorker::create($information);

    // Create the permissions
    if ($permissions == [] || !array_key_exists('worker_id', $permissions))
      $permissions['worker_id'] = $new_worker->id;

    $worker_permissions = WorkerPermissions::create($permissions);


    return $new_worker;
  }




  /* -------------------------------------------------
                   NON-SCOPE FUNCTIONS

    These are just plain-ol-boring functions that any Package
      object can call, nothing magical about these.Okay

     ------------------------------------------------- */


  /**
   * Returns the worker object or returns a dummy object that has no permissions
   *
   * @param $id The id of the resident to find a worker for
   * @return App\DeskWorker
   */
  public static function findOrDummy($id)
  {
    $worker = DeskWorker::where('resident_id', '=', $id)->first();

    if (is_null($worker))
      $worker = DeskWorker::find(0);

    return $worker;
  }


  /**
   * Tells if the worker object is a dummy instance
   *
   * @return boolean
   */
  public function isDummy()
  {
    return ($this->attributes['resident_id'] == 0 && $this->attributes['id'] == 0);
  }


  /**
   * This will return an object of type App\Resident
   *
   * Note, since the resident_id on the residents table is a reference to
   * the buildings table,
   * this function can be called by doing $desk_worker->resident->id without parenthesis on building
   *
   * @return Object of the resident the desk worker is
   */
  private function resident()
  {
    if (is_null($this->my_resident))
      $this->my_resident = Resident::find($this->attributes['resident_id']);

    return $this->my_resident;
  }


  /**
   * This will return an object of type App\Building
   *
   * Note, since the building_id on the desk_workers table is a reference to
   * the buildings table,
   * this function can be called by doing $desk_worker->building->id without parenthesis on building
   *
   * @return Object of the building the desk worker belongs to
   */
  private function building()
  {
    if (is_null($this->my_building))
      $this->my_building = Building::find($this->attributes['building_id']);

    return $this->my_building;
  }


  /**
   * Gets the permissions of the desk worker
   *
   * @return App\WorkerPermissions
   */
  private function permissions()
  {
    if (is_null($this->my_privileges))
      $this->my_privileges = WorkerPermissions::find($this->attributes['id']);

    return $this->my_privileges;
  }


  public function hasViewedMessage($message_id)
  {
    $views = MessageLogViews::where('message_id', $message_id)->where('worker_id', $this->id)->get();

    return (sizeof($views) != 0);
  }


  /**
   * Gets the user admin object
   *
   * @return App\Administrators
   */
  private function admin()
  {
    if (is_null($this->my_admin))
      $this->my_admin = Administrators::where('worker_id', '=', $this->attributes['id'])->first();

    return $this->my_admin;
  }


  /**
   * Tells if a desk worker is also an admin
   *
   * @return boolean
   */
  public function isAdmin()
  {
    return !(is_null($this->admin()));
  }



  /**
   * Allows for $resident->building to actually call $resident->building()
   * And other function calls to be made without the parenthesis
   *
   * This will not work on scope functions though, or functions that have
   * parameters
   *
   * @param string $key the variable to lookup
   * @return yes
   */
  public function __get($key)
  {
    if (in_array($key, $this->functions))
      return $this->$key();

    // Not a function, go to the parent (for $this->attribute[$key])
    return parent::__get($key);
  }

}



/**
 *
 *
 * @param
 * @return
 */
