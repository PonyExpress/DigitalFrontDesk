<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

use App\DeskWorker;
use App\Resident;
use App\Building;
use App\Signatures;

use App\Exceptions\RedirectException;

use App\Utilities;



/**
 * Class to represent a package item
 *
 */
class PackageLog extends Model
{
  private $my_resident = NULL; // Object that equals the resident that the package belongs to
  private $my_building = NULL;
  private $my_workerIn = NULL;
  private $my_workerOut = NULL;
  private $my_signature = null;

  protected $primaryKey = 'id';
  protected $table = 'package_log';

  public $timestamps = false;

  protected $fillable = ['resident_id', 'package_number', 'type', 'time_in', 'time_out', 'worker_in',
                         'worker_out', 'signature', 'building_id', 'reminded_on'];

  private $functions = ['resident', 'building', 'dateIn', 'dateOut', 'workerIn', 'workerOut', 'packageType', 'number', 'signature'];


  /* -------------------------------------------------
                      SCOPE FUNCTIONS

      IMPORTANT: scope functions are SUPER MAGICAL!

      Seriously, for example the function scopeNotPickedUp()
        is actually called by doing Package::notPickedUp()

      You don't actually have to pass anything for the $query
        parameter, Laravel fills this in for you. Any parameters after
        $query are actual parameters though

      Which is some magical stuff because Laravel converts
        that stuff automagically.



      Scopes CAN be combined, for example if somewhere in the code has:
        Package::notPickedUp()->belongsTo('666')->get();

      That will return all the packages belonging to the resident with the ID 666
        that have not been picked up, pretty cool.


      Also, just assume how Laravel actually makes this:
        $package->package_id (or any attribute) into:
        $package->attributes['package_id']  (this is the actual place that the value is stored, and MUST be used when calling something from inside the class)

     ------------------------------------------------- */

  /**
   * Narrows a query to packages that have not been picked up
   *
   * @param QueryBuilder $query
   * @return modified query
  */
  public function scopeNotPickedUp($query)
  {
    return $query->whereRaw('`time_out` IS NULL');
  }


  /**
   * Narrows a query to packages that have been picked up
   *
   * @param QueryBuilder $query
   * @return modified query
   */
  public function scopePickedUp($query)
  {
    return $query->whereRaw('`time_out` IS NOT NULL');
  }


  /**
   * Narrows a scope to packages addressed to a certian resident
   *
   * @param QueryBuilder $query
   * @param int $id the ID of the resident to find packages for
   * @return the modified query builder
   */
  public function scopeAddressedTo($query, $id)
  {
    return $query->where('resident_id', $id);
  }


  /**
   * Narrows a query to return oldest packages first
   *
   * @param QueryBuilder $query
   * @return the modified query
   */
  public function scopeOldest($query)
  {
    return $query->orderBy('time_in', 'ASC');
  }


  /**
   * Forces query to return newer packages first
   *
   * @param QueryBuilder $query
   * @return the modified query
   */
  public function scopeNewest($query)
  {
    return $query->orderBy('time_in', 'DESC');
  }


  /**
   * Gets the maximum unique ID from the pacakges table
   *
   * @param QueryBuilder $query
   * @return the modified query
   */
  public function scopeMaxID($query)
  {
    return $query->whereRaw('id = (select max(`id`) from `package_log`)');
  }


  /**
   * Gets packages just in the worker's building
   *
   * @param QueryBuilder $query
   * @return the modified $query
   */
  public function scopeInBuilding($query)
  {
    $building = Auth::user()->worker->building;

    // Add the direct building the user works for
    $query->where('building_id', '=', $building->uID);

    // Add any other sub-buildings (RC2, RC3)
    $buildings = Building::where('id', '=', $building->id)->get()->toArray();

    foreach($buildings as $theBuilding)
    {
      // Don't add the same building twice
      if ($theBuilding['uID'] != $building->uID)
        $query->orWhere('building_id', '=', $theBuilding['uID']);
    }

    return $query;
  }



  /* -------------------------------------------------
                   NON-SCOPE FUNCTIONS

    These are just plain-ol-boring functions that any Package
      object can call, nothing magical about these.

     ------------------------------------------------- */

  /**
   * Gets the resident the package is addressed to
   *
   * @return App\Resident the package is for
  */
  private function resident()
  {
    if (is_null($this->my_resident))
      $this->my_resident = Resident::find($this->attributes['resident_id']);

    return $this->my_resident;
    //return $this->belongsTo('App\Resident', 'resident_id', 'id');
  }


  /**
   * Gets the building the package is addressed to
   *
   * @return App\Building the package is for
  */
  private function building()
  {
    if (is_null($this->my_building))
      $this->my_building = Building::find($this->attributes['building_id']);

    return $this->my_building;
    //return $this->belongsTo('App\Building', 'building_id', 'uID');
  }


  /**
   * Checks if a package has been picked up
   *
   * @return true if the package has been picked up, false otherwise
   */
  public function isPickedUp()
  {
    return (!is_null($this->attributes['time_out']));
  }


  /**
   * Gets a friendly name for the package type
   *
   * @return string for the package type name
   */
  private function packageType()
  {
    switch ($this->attributes['type'])
    {
      case 1:
        return "Box";
      case 2:
        return "Envelope";
      default:
        return "Other";
    }
  }


  /**
   * Gets a friendly date that the package was checked in
   *
   * @return string
   */
  private function dateIn()
  {
    return date("F d, Y, h:i A", strtotime($this->attributes['time_in']));
  }


  /**
   * Gets a friendly date the package was signed out at
   *
   * @return string
   */
  private function dateOut()
  {
    if ($this->isPickedUp()){
      return date("F d, Y, h:i A", strtotime($this->attributes['time_out']));
    }else{
      // Not picked up, shouldn't have a timeout date
      return "";
    }
  }


  /**
   * Gets the package number (not the unique)
   *
   * The package number is what desk workers will write on packages, it's just the unique ID of the package modded by 500 (350?)
   *
   * @return integer or string, I'm not sure
   */
  private function number()
  {
    return $this->attributes['package_number'];
  }


  /**
   * Gets the worker who checked the package in
   *
   * @return App\DeskWorker
   */
  private function workerIn()
  {
    if (is_null($this->my_workerIn))
      $this->my_workerIn = DeskWorker::find($this->attributes['worker_in']);

    return $this->my_workerIn;
    //return $this->belongsTo('App\DeskWorker', 'worker_in', 'id');
  }


  /**
   * Gets the worker who signed the package out to the resident
   *
   * @return App\DeskWorker
   */
  private function workerOut()
  {
    if (is_null($this->my_workerOut))
      $this->my_workerOut = DeskWorker::find($this->attributes['worker_out']);

    return $this->my_workerOut;
    //return $this->belongsTo('App\DeskWorker', 'worker_out', 'id');
  }


  /**
   * Gets the signature
   *
   * @return App\Signatures
   */
  private function signature()
  {
    if (is_null($this->my_signature))
      $this->my_signature = Signatures::type('App\PackageLog')->id($this->attributes['id'])->direction('out')->first();

    return $this->my_signature;
  }



  /**
   * Tells if the resident needs a reminder about the package
   *
   * @return boolean
   */
  public function needsReminder()
  {
    // Check if package has been signed out
    if (!is_null($this->attributes['time_out']))
      return false; // No reminder

    // Return false if the package does not belong to the resident's building and they are not an admin
    if (!Auth::user()->isAdmin())
    {
      if ($this->building->id != Auth::user()->worker->building->id)
        return false;
    }

    $arrived = $this->attributes['time_in'];

    // Check if they have been notified before, if they have, use that as the $arrived time
    if (!is_null($this->attributes['reminded_on']))
      $arrived = $this->attributes['reminded_on'];

    // Turn $arrived into a Carbon instance
    $arrived = new Carbon($arrived);

    // Say the package needs a reminder if it has not been picked up after 7 days.
    return ($arrived->diff(Carbon::now())->days >= 7);
  }


  /**
   * Will mark a package as signed out
   *
   * @param Request $input the form input
   * @return void
   */
  public function signOut($input)
  {
    // Check user permissions first (just in case)
    if (Auth::user()->permissions->canSignOutPackages())
    {
      // Update values
      $this->time_out = Carbon::now(); // Date and time value of when the package was signed out
      $this->worker_out = Auth::user()->worker_id;

      // Save the signature
      $sign = Signatures::create(['object' => 'App\PackageLog', 'object_id' => $this->id, 'direction' => 'out', 'value' => $input['signatureValue']]);
      $this->signature = $sign->id;
      $this->save();

      // Alert the user if they have the settings for it
      //if ($this->resident->settings->package_checkout_confrim)

    }

    return;
  }


  /**
   * Used to create a new package entry
   *
   * @throws App\Exceptions\RedirectException
   * @param App\Resident $resident (Reference) the resident to create a package for
   * @param int $package_type the type of package to create
   * @return the new package log entry
   */
  public static function createEntry(&$resident, $package_type)
  {
    if (!is_object($resident))
      return;

    // Make sure the user can create packages
    if (Auth::user()->permissions->canCreatePackages())
    {
      // Fill in the reset of the package information
      $package_info = [];
      $package_info['resident_id'] = $resident->id;
      $package_info['type'] = $package_type;
      $package_info['time_in'] = Carbon::now();
      $package_info['worker_in'] = Auth::user()->worker_id;
      $package_info['building_id'] = $resident->building_id; // Make the package for the resident's building

      // Use the package's parent object to create the package
      $package = PackageLog::create($package_info);

      if (!is_null($package))
      {
        // Set the package number
        $package->update(['package_number' => ($package->id % 500)]);

        // Return the package
        return $package;
      }

      // Something broke
      return null;
    }

    // No permissions
    return null;
  }











  /**
   * Allows for $package->dateIn to actually call $package->dateIn()
   *
   * Allows other function calls to be made without (), does not work on scope functions or
   * functions with parameters
   *
   * @param string $key
   * @return the function, or calls parent __get
   */
  public function __get($key)
  {
    if (in_array($key, $this->functions))
      return $this->$key(); // Isn't PHP Awesome???

    // Go to the parent
    return parent::__get($key);
  }



  /**
   * Finds a package, returns an error if it failed
   *
   * @throws App\Exceptions\RedirectException
   * @param int $id The ID of the package to find
   * @param string $returnPath (Default: 'desk/packages') The path to return to if it fails
   * @return App\Package or redirect
   */
  public static function findOrError($id, $returnPath = 'desk/packages')
  {
    $entry = PackageLog::find($id);

    if (!is_null($entry))
      return $entry;

    // Generate the error
    throw new RedirectException('Packages was not found.', $returnPath, 'error', true);
  }

}


/**
 *
 *
 * @param
 * @return
 */
