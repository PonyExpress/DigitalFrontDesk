<?php

namespace App;





/**
 * Misc Tools to use whenever.
 *
 */
class Utilities
{

  /**
   * Tells if a string is empty
   *
   * @param Strin g $string_value
   * @return bool
   */
  public static function empty_str($string_value)
  {
    $string_value = preg_replace('/(\s*)/', '', $string_value);

    return ($string_value == '');
  }




  /**
   * Verifies that all the required keys exist in an array
   *
   * @param Array $keys - the list of keys that are REQUIRED to be in the next paramerter
   * @param Array $input - The POST input from the form
   * @return bool
   */
  public static function verify_array($keys, $input)
  {
    foreach ($keys as $key)
      if (!array_key_exists($key, $input))
        return false; // Array is not valid

    return true; // Array is valid
  }
}




/**
 *
 *
 * @param
 * @return
 */
