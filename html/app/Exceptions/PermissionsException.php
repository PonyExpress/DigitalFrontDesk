<?php

namespace App\Exceptions;

use Exception;
use Session;



/**
 * Exception error for when the user does not have the correct permissions to view/do something
 *
 */
class PermissionsException extends Exception
{
  public $redirect_path;
  Public $session_msg;



  /**
   * Constructor
   *
   * @param string $redirectPath the path that will be redirected to when the exception is handled
   * @param string $msg (Default: 'You do not have the required permissions to perform that action.') the error message
   * @param boolean $dismisses (Default: true) true if the error message should dismiss
   */
  public function __construct($redirectPath, $msg = 'You do not have the required permissions to perform that action.', $dismisses = true)
  {
    // Generate the error type (dismissable if needed)
    $type = 'error';
    if ($dismisses)
      $type .= '_dismisses';

    // Set flash message
    Session::flash($type, $msg);

    // Set the redirect path
    $this->redirect_path = $redirectPath;
  }
}




/**
 *
 *
 * @param
 * @return
 */
