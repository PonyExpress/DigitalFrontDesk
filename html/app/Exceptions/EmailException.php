<?php
namespace App\Exceptions;

use Exception;

/*
 * Name.........: EmailException
 * Description..: Class for email exceptions to be caught and thrown
 *
 *
 * The exception class has everything built in that you should need, for the documentation
 * see: http://php.net/manual/en/language.exceptions.extending.php
 *
 *
 * When throwing, use this syntax:
 *    throw new EmailExcept(message, code);
 */
 class EmailException extends Exception
 {

 }
