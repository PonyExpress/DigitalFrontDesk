<?php

namespace App\Exceptions;

use Exception;
use Session;


/**
 * Handles an error that should immedietly redirect from something
 *
 */
class RedirectException extends Exception
{
  public $redirect_path;
  Public $session_msg;


  /**
   * Constructor
   *
   * @param array $error - should have the error type and the error message
   * @param string $redirectPath - the path to redirect to
   */
  public function __construct($message, $redirectPath, $type = DFLT, $dismisses = DFLT)
  {
    if (is_default($type))
      $type = 'error';

    if (is_default($dismisses))
      $dismisses = false;

    if ($dismisses && !(strpos($type, '_dismisses') === false))
      $type = str_replace('_dismisses', '', $type);

    // Set flash messages if needed
    Session::flash($type . (($dismisses) ? '_dismisses' : ''), $message);

    $this->redirect_path = $redirectPath;
  }


}


/**
 *
 *
 * @param
 * @return
 */
