<?php

namespace App;

use Auth;

use Session;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

use App\Key;
use App\Building;
use App\Resident;
use App\PackageLog;
use App\DeskWorker;
use App\Exceptions\RedirectException;

use App\Utilities;


/**
 * Class to represent an entry in the message log
 *
 */
class MessageLogViews extends Model
{
  protected $primaryKey = 'id';
  protected $table = 'message_log_views';

  public $timestamps = false;

  protected $fillable = ['message_id', 'worker_id', 'date_viewed'];
}


/**
 *
 *
 * @param
 * @return
 */
