<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


// Root Routes
Route::get('/', function () {
    $loggedOut = Session::get('loggedOut', '');
    Session::forget('loggedOut', '');
    return view('welcome', ['loggedOut' => $loggedOut]);
});
Route::get('home', function () { return redirect('/'); });



// =======================
//     Login & Logout
// =======================
Route::get('login/forward/{path}', 'Auth\AuthController@googleLogout'); // This will logout any users on any Google site (forces to enter password)
                                                                        // And then it will redirect to /{path}/login
Route::get('login', function() { return redirect('login/forward/home'); }); // Regular login w/out any special site (like desk or student)
Route::get('{path}/login', 'Auth\AuthController@chooseLogin'); // Will call whatever login function based on {path}
Route::get('login/callback', 'Auth\AuthController@loginCallback'); // Login the user, and save to Users table

Route::get('logout', 'Auth\AuthController@logout');
Route::get('logout/{param}', 'Auth\AuthController@logout'); // Logout, will return to /home and have a hidden iframe that will log out of all Google as well





// =======================
//          API
// =======================
Route::get('desk/residents/all', 'API\DeskAPIController@allResidents'); // Retrieve all residents
Route::get('desk/residents/all/html', 'API\DeskAPIController@allResidentsHTML'); // Retrieve all residents in HTML format
Route::get('desk/keys/all/html', 'API\DeskAPIController@allKeysHTML');
Route::get('desk/items/all/html', 'API\DeskAPIController@allItemsHTML');


Route::get('api/img/signatures/{type}/{id}/{direction}.png', 'API\SignatureAPI@getSignatureImage');
Route::get('api/img/signatures/{type}/{id}/{direction}', 'API\SignatureAPI@getSignatureImage');
Route::get('sig/{type}/{id}/{direction}.png', 'API\SignatureAPI@getSignatureImage');





// =======================
//          Desk
// =======================
Route::get('desk', 'Desk\DeskController@home');  // Home page

//Route::get('desk', function(){echo "FUCK YOU!"; exit;});
// Residents
Route::get('desk/residents', 'Desk\ResidentController@home');  // View all residents
Route::get('desk/residents/{resident}', 'Desk\ResidentController@show'); // View a single residents information
Route::get('desk/residents/{resident}/email', 'Desk\ResidentController@composeEmail'); // Form page to compose an email
Route::post('desk/residents/{resident}/email', 'Desk\ResidentController@sendEmail'); // POST page to send the composed email

// Packages
Route::get('desk/packages', 'Desk\PackageLogController@home'); // Packages home page
Route::get('desk/packages/create', 'Desk\PackageLogController@createPackage'); // Create a package
Route::post('desk/packages/create', 'Desk\PackageLogController@postCreatePackage'); // Save the new package
Route::get('desk/packages/blast', 'Desk\PackageLogController@sendBlast');
Route::get('desk/packages/{id}', 'Desk\PackageLogController@show');  // See details about a package/checkout the package
Route::post('desk/packages/{id}/signout', 'Desk\PackageLogController@signOutPackage'); // POST stuff for signing out a package
Route::get('desk/packages/{id}/reminder', 'Desk\PackageLogController@sendReminder'); // Send a reminder to a resident about their package.


// Keys
Route::get('desk/keys', 'Desk\KeyLogController@home'); // Keys page home
Route::get('desk/keys/create', 'Desk\KeyLogController@loanKey'); // New key checkout
Route::post('desk/keys/create', 'Desk\KeyLogController@postLoanKey'); // POST Save key checkout
Route::get('desk/keys/{id}', 'Desk\KeyLogController@show'); // Single key loan view
Route::post('desk/keys/{id}/return', 'Desk\KeyLogController@returnKey'); // Return a key

// Items
Route::get('desk/items', 'Desk\ItemLogController@home');
Route::get('desk/items/all', 'Desk\ItemLogController@all');
Route::get('desk/items/create', 'Desk\ItemLogController@loanItem');
Route::post('desk/items/create', 'Desk\ItemLogController@postLoanItem');
Route::get('desk/items/{id}', 'Desk\ItemLogController@show');
Route::post('desk/items/{id}/return', 'Desk\ItemLogController@returnItem');
Route::get('desk/items/view/{id}', 'Desk\ItemLogController@viewItem');

// Messages
Route::get('desk/messages', 'Desk\MessageLogController@home');
Route::get('desk/messages/create', 'Desk\MessageLogController@createMessage');
Route::post('desk/messages/create', 'Desk\MessageLogController@postCreateMessage');

// Punch Clock
Route::get('desk/punchclock', 'Desk\PunchClockController@home');
Route::get('desk/punchclock/create', 'Desk\PunchClockController@createEntry');
Route::post('desk/punchclock/create', 'Desk\PunchClockController@postCreateEntry');
Route::get('desk/punchclock/{id}', 'Desk\PunchClockController@viewEntry');

// Dollies
Route::get('desk/dollies', 'Desk\DollyLogController@home');





// =======================
//     Student Portal
// =======================
Route::get('student', 'Student\StudentController@home');

Route::get('student/settings', 'Student\StudentController@settings');
Route::post('student/settings', 'Student\StudentController@postSettingsSave');
Route::get('student/settings/newapikey', 'Student\StudentController@newAPIkey');

Route::get('student/packages/{id}', 'Student\PackageLogController@show');
Route::get('student/keys/{id}', 'Student\KeyLogController@show');
Route::get('student/items/{id}', 'Student\ItemLogController@show');





// =======================
//        GameTrax
// =======================
Route::get('gametrax', function(){
  return view('gametrax.home');
});





// =======================
//        Admin Portal
// =======================
Route::get('admin', 'Admin\AdminController@home');

// Residents
Route::get('admin/residents', 'Admin\ResidentsController@home');
Route::get('admin/residents/create', 'Admin\ResidentsController@create');
Route::post('admin/residents/create', 'Admin\ResidentsController@postCreate');
Route::get('admin/residents/{id}', 'Admin\ResidentsController@show');
Route::get('admin/residents/{id}/modify', 'Admin\ResidentsController@modify');
Route::post('admin/residents/{id}/modify', 'Admin\ResidentsController@postModify');
Route::get('admin/residents/{id}/remove', 'Admin\ResidentsController@remove');

// Building Groups
Route::get('admin/buildings/groups/{id}', 'Admin\BuildingsController@showGroup');

// Buildings
Route::get('admin/buildings', 'Admin\BuildingsController@home');
Route::get('admin/buildings/create', 'Admin\BuildingsController@create');
Route::post('admin/buildings/create','Admin\BuildingsController@postCreate');
Route::get('admin/buildings/{id}', 'Admin\BuildingsController@show');
Route::get('admin/buildings/{id}/modify', 'Admin\BuildingsController@modify');
Route::post('admin/buildings/{id}/modify', 'Admin\BuildingsController@postModify');

// Desk Workers









// ========================
//       Signatures
// ========================
Route::get('signature/{type}/{id}/{direction}/{mod}', 'SignaturesController@show'); // Show base64 value or base64 uri for the signature depending on the value of $mod
Route::get('signature/{type}/{id}/{direction}.png', 'SignaturesController@show'); // Show the signature using a .png extension
Route::get('signature/{type}/{id}/{direction}', 'SignaturesController@show'); // Show the signature without an extension
Route::get('desk/{type}/{id}/signature/{direction}.png', 'SignaturesController@show');







// ========================
//    Forms & Feedback
// ========================
Route::get('forms', function(){return redirect('/');});
Route::get('forms/thankyou', 'FormsController@thankyou');

// Cell Phone Carrier Suggestions
Route::get('forms/cellphonecarrier', 'FormsController@cellphonecarrier');
Route::post('forms/cellphonecarrier', 'FormsController@postCellphonecarrier');

// Feedback
Route::get('forms/feedback', 'FormsController@feedback');
Route::post('forms/feedback', 'FormsController@postFeedback');



// ========================
//  About & Documentation
// ========================
Route::get('docs', 'DocsController@home');
Route::get('about', function(){return redirect('docs/about');});
Route::get('docs/about', 'DocsController@about');
Route::get('docs/desk', 'DocsController@desk');
Route::get('docs/student', 'DocsController@student');

// API Documentation
Route::get('docs/api', 'DocsController@api');
Route::get('docs/api/v1', 'DocsController@api_v1');









Route::controllers([
  'auth' => 'Auth\AuthController',
  'password' => 'Auth\PasswordController',
]);
