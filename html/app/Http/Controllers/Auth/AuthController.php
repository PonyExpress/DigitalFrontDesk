<?php

namespace App\Http\Controllers\Auth;

use Session;
use App\User;
use App\Resident;
use App\DeskWorker;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Socialite;

use SocialAuth;
use SocialNorm\Exceptions\ApplicationRejectedException;
use SocialNorm\Exceptions\InvalidAuthorizationCodeException;
use Auth;
use Redirect;

use App\Exceptions\RedirectException;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'password' => 'required',
        ]);
    }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
      return false;
      //return User::create(['password' => bcrypt($data['password'])]);
    }


    /**
     * Chooses what login to take bassed on path
     *
     * @param  string  $path - The path to take for a login
     * @return the login function
     */
    public function chooseLogin($path = "home")
    {
      // Go to the google logout page if it has not been visited yet
      if (!Session::has('google_logout'))
        return $this->googleLogout();

      // Pick a login function based on what $path is
      if ($path == "student")
        return $this->studentLogin();

      if ($path == "admin")
        return $this->adminLogin(); // No admin panel at this time

      if ($path == "desk")
        return $this->deskLogin();

      if ($path == "docs")
        return $this->docsLogin();

      // Default option is home login
      return $this->login();
    }


    /**
     * Will authorize with Google, login with google, and redirect to the home page
     *
     * @return the SocialAuth authorization info
     */
    public function login()
    {
      // Go to the google logout page if it has not been visited yet
      if (!Session::has('google_logout'))
        return $this->googleLogout();

      // Set the page to redirect to if not already set
      if (!Session::has('login_redirect_uri'))
        Session::set('login_redirect_uri', 'home');

      Session::forget('google_logout');

      return SocialAuth::authorize('google');
    }


    /**
     * Will authorize with Google, then redirect to the student portal after login
     *
     * @return the SocialAuth authorization info
     */
    public function studentLogin()
    {
      // Go to the google logout page if it has not been visited yet
      if (!Session::has('google_logout'))
        return $this->googleLogout();

      // Set the page to redirect to if not already set
      if (!Session::has('login_redirect_uri'))
        Session::set('login_redirect_uri', 'student');

      Session::forget('google_logout');

      return SocialAuth::authorize('google');
    }


    /**
     * Will authorize with Google, then redirect to the desk portal after login
     *
     * @return SocialAuth authorization info
     */
    public function deskLogin()
    {
      // Go to the google logout page if it has not been visited yet
      if (!Session::has('google_logout'))
        return $this->googleLogout();

      // Set the page to redirect to if not already set
      if (!Session::has('login_redirect_uri'))
        Session::set('login_redirect_uri', 'desk');

      Session::forget('google_logout');

      return SocialAuth::authorize('google');
    }


    /**
     * Will authorize with Google, then redirect to the admin portal after login
     *
     * @return SocialAuth authorization info
     */
    public function adminLogin()
    {
      // Go to the google logout page if it has not been visited yet
      if (!Session::has('google_logout'))
        return $this->googleLogout();

      // Set the page to redirect to if not already set
      if (!Session::has('login_redirect_uri'))
        Session::set('login_redirect_uri', 'admin');

      Session::forget('google_logout');

      return SocialAuth::authorize('google');
    }


    /**
     * Will authorize with Google, then redirect to the documentation after login
     *
     * @return the SocialAuth authorization info
     */
    public function docsLogin()
    {
      // Go to the google logout page if it has not been visited yet
      if (!Session::has('google_logout'))
        return $this->googleLogout();

      // Set the page to redirect to if not already set
      if (!Session::has('login_redirect_uri'))
        Session::set('login_redirect_uri', 'docs');

      Session::forget('google_logout');

      return SocialAuth::authorize('google');
    }


    /**
     * Will perofrm a user logout
     *
     * @return redirects to the home page
     */
    public function logout($param = "")
    {
      Auth::logout();

      //$this->getLogout();

      Session::set('loggedOut', 'true');

      if ($param == "inactive" || $param == "inactivity")
        Session::flash('info', 'You have been logged out due to inactivity.');


      // Redirect back to the Digital Front Desk Portal
      return redirect('/');
    }




    /**
     * Will load the google logout page which will log a user out of Google Accounts
     *
     * This is kind of a sketchy way of doing this... But since we do not want Google to remember
     * passwords on the login page, we must impliment this, the googlelogout view page just has an iframe
     * that has a source of https://accounts.google.com/logout which will log a user out of any Google
     * account that they are currently logged into.
     * This will force the user to re-enter their password the next time they go to login.
     *
     * @param string $path The login path the user wants to redirect to, defaults to home (redirects to home/login)
     * @return the googlelogout view
     */
    public function googleLogout($path ="home")
    {
      Session::set('google_logout', 'true');

      return view('googlelogout', ['path' => $path]);
    }


    /**
     * Is performed after the user gets authorized from Google.
     *
     * This function is responsible for handling the login. It will check to
     * see if the email on the Google account ends in @mst.edu or @mail.mst.edu,
     * if it doesn't then the user will be automatically logged out :)
     * Once again, another kind of sketchy way to do this, but there isn't another way.
     *
     * @return will redirect to the Session varibale login_redirect_uri set in the login functions
     */
    public function loginCallback()
    {
      try {
        SocialAuth::login('google', function($user, $details)
        {
          // Make sure that the hosted domain is correct
          // This is needed because if the hosted domain is not mst.edu or mail.mst.edu
          // Then we do not want to create an entry in the users table
          if (!is_null($details->raw()) && isset($details->raw()['hd']) && ($details->raw()['hd'] == 'mst.edu' || $details->raw('hd') == 'mail.mst.edu'))
          {
            $changed = false;

            // Only update stuff if it has changed (for some reason)
            if ($user->name != $details->full_name)
            {
              $user->name = $details->full_name;
              $changed = true;
            }

            if ($user->email != $details->email)
            {
              $user->email = $details->email;
              $changed = true;
            }

            if ($user->picture != htmlspecialchars(urldecode($details->avatar)))
            {
              $user->picture = htmlspecialchars(urldecode($details->avatar));
              $changed = true;
            }

            // Find a resident with the same email address
            $resident = Resident::where('email', '=', $user->email)->first();

            if (!is_null($resident))
            {
              // Check if the resident found is the same
              if ($user->resident_id != $resident->id)
              {
                $user->resident_id = $resident->id;
                $changed = true;
              }

              // Find a desk worker with the same resident ID
              $worker = DeskWorker::where('resident_id', '=', $user->resident_id)->first();

              if (!is_null($worker))
              {
                // Check if the worker stuff has changed
                if ($user->worker_id != $worker->id)
                {
                  $user->worker_id = $worker->id;
                  $changed = true;
                }
              }
              else
              {
                // No worker found
                $user->worker_id = null;
                $changed = true;
              }
            }

            // Save the user if they have changed
            if ($changed)
              $user->save();
          }

          $hd = explode('@', $details->raw()['email'])[1];

          if ($hd != 'mst.edu' && $hd != 'mail.mst.edu')
          {
            Session::set('error', 'You need to log in with your <strong>@mst.edu</strong> email address.');

            throw new RedirectException('Please make sure you are logging in with your <strong>mst.edu</strong> email address.', '/logout');
          }

      });
    } catch (ApplicationRejectedException $e) {
      // User rejected application
      Session::set('info', 'To use this application, you must allow access to your profile information.<br/>We do not, and will not, read your emails or any other personal information.');

      return redirect('logout');

    } catch (InvalidAuthorizationCodeException $e) {
      // Authorization was attempted with invalid
      // code,likely forgery attempt
      Session::set('info', 'Something happened... Please try again.');

      return redirect('logout');

    } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
      // Model not found error, stupid, just logout
      Session::set('error', 'You are unable to log in with that email address.<br/>Please make sure you are using your <strong>@mst.edu</strong> email address.');
      return redirect('logout');
    }

    // Current user is now available via Auth facade
    $user = Auth::user();

    if ($user->resident->removed())
    {
      Session::set('error', 'The login information you provided does not belong to a valid resident.<br/>If you feel that this is incorrect, please contact your head resident.');
      return redirect('logout');
    }

    if (!is_null(Auth::user()))
    {
      // Confirm the hosted domain is the same
      $domain = $hd = explode('@', $user->email)[1];

      if ($domain != "mst.edu" && $domain != "mail.mst.edu")
      {
        Session::set('error', 'You need to log in with your @mst.edu email address.');
        return redirect('logout');
      }

      // Redirect to appropriate location
      $redirect = Session::get('login_redirect_uri', 'home');
      Session::forget('login_redirect_uri');

      return redirect($redirect);
    }

    // User not logged in
    return redirect('home');
  }

} // End class



/**
 *
 *
 * @param
 * @return
 */
