<?php

namespace App\Http\Controllers\Desk;

use Illuminate\Http\Request;
use Auth;
use Session;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\PackageLog;
use App\Resident;
use App\Building;
use App\MessageLog;
use App\Exceptions\PermissionsException;

use Carbon\Carbon;

 /**
  * This controller is for the desk stuff
  *
  * It only really does the desk home page...
  *
  */
class MessageLogController extends Controller
{
  public function __construct()
  {
    $this->middleware('deskAuth');
  }


  /**
   * Goes the the desk home page
   *
   * @return view of desk/home.blade.php
   */
  public function home()
  {
    $log = MessageLog::inBuilding()->orderBy('date_posted', 'DESC')->take(50)->get();

    return view('desk.messages', ['self' => Auth::user()->worker, 'messages' => $log]);
  }


  /**
   * Goes to the compose a message view
   *
   * @return view
   */
  public function createMessage()
  {
    return view('desk.messages_new');
  }


  /**
   * Submits a message to the database
   *
   * @param App\Request $request
   * @throws App\Exceptions\PermissionsException if not correct permissions
   * @return desk view
   */
  public function postCreateMessage(Request $request)
  {
    $input = $request->all();

    if (Auth::user()->worker->permissions->message_log)
    {
      $details = [];
      $details['worker_id'] = Auth::user()->worker_id;
      $details['message'] = (string)$input['message'];
      $details['building_id'] = Auth::user()->worker->building_id;
      $details['date_posted'] = Carbon::now();

      MessageLog::create($details);

      Session::flash('success', 'Message posted to the log');
      return redirect('desk/messages');
    }

    // No permissions
    throw new PermissionsException('desk');
  }

}




/**
 *
 *
 * @param
 * @return
 */
