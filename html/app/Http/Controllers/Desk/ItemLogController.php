<?php

namespace App\Http\Controllers\Desk;

use Request;
use Session;

use Auth;
use SocialAuth;

use App\Http\Requests;
use App\Http\Requests\NewItemLoanRequest;
use App\Http\Controllers\Controller;

use App\PackageLog;
use App\Resident;
use App\Building;
use App\Items;
use App\ItemsConfig;
use App\ItemLog;
use App\EmailTemplate;
use App\Signatures;

use App\Exceptions\PermissionsException;
use App\Exceptions\RedirectException;

use Carbon\Carbon;
use Mail;


 /**
  * This controller is for the Item Loan Desk Portal
  */
class ItemLogController extends Controller
{
  public function __construct()
  {
    $this->middleware('deskAuth');
  }


  /**
   * Home page for the GameTrax desk portal
   *
   * @return view of checked out items
   */
  public function home()
  {
    $items = ItemLog::notReturned()->inBuilding()->get();

    return view('desk.items_index', ['items' => $items]);
  }


  /**
   * Shows all items for the desk workers building
   *
   * @return view of all the items and searching them
   */
  public function all()
  {
    $items = ItemsConfig::inBuilding()->get();

    return view('desk.items_all', ['items' => $items]);
  }


  /**
   * Shows an entry in the item log
   *
   * @param int $id the ID of the item log entry
   * @return the view or redirect to desk/items
   */

  public function show($id)
  {
    $entry = ItemLog::find($id);

    if (!is_null($entry))
      return view('desk.items_view', ['entry' => $entry]);

    // Item not found, return
    return redirect('desk/items');
  }


  /**
   * Views an item that is available at a location
   *
   * @param int $id the ID of the item
   * @return the item view or redirect back to desk/items
   */
  public function viewItem($id)
  {
    $entry = ItemsConfig::find($id);

    if (is_null($entry))
      throw new RedirectException('That item was not found', 'desk/items');

    $loans = ItemLog::loanedItem($entry->uID)->notReturned()->get();

    return view('desk.items_item', ['entry' => $entry, 'loans' => $loans]);

  }


  /**
   * Loads the form to loan a new item
   *
   * @throws App\Exception\PermissionsException if no permissions
   * @return view or redirect (on error)
   */
  public function loanItem()
  {
    if (Auth::user()->worker->permissions->canLoanItems())
      return view('desk.items_new');

    // No permissions
    throw new PermissionsException('desk/items');
  }


  /**
   * Creates an entry in the item log
   *
   * @throws App\Exception\PermissionsException if no permissions
   * @return
   */
  public function postLoanItem(NewItemLoanRequest $request)
  {
    // Make sure the user has permission to do this
    if (Auth::user()->worker->permissions->canLoanItems())
    {
      $input = $request->all();

      $resident = Resident::findOrError($input['resident_id'], 'desk/items');
      $item = Items::find($input['item_id']);

      if (!is_null($item))
      {
        $entry = ItemLog::createEntry($resident, $item);

        // Alert the resident if they have settings for it
        if ($resident->settings->item_loan_alerts)
        {
          $email = new EmailTemplate('emails.item_loan', $resident->id, ['subject' => 'You have checked an item out', 'item_name' => $item->name]);
          $email->send();
        }

        // Success
        Session::flash('success', 'Item loaned was logged.');
        return redirect('desk/items');
      }

      // Error with finding item
      throw new RedirectException('Selected item was not found.', 'desk/items');

    }

    // No permisisons
    throw new PermissionsException('desk/items');
  }


  /**
   * Returns a loaned item
   *
   * @param int $id - the ID of the item to return
   * @throws App\Exceptions\PermissionsException if no permissions
   * @return redirect back to desk/items
   */
  public function returnItem($id)
  {
    $item = ItemLog::find($id);

    if (!is_null($item))
    {
      if (Auth::user()->worker->permissions->canReturnItems())
      {
        $input = Request::all();

        $item->returnItem($input);

        // Increase availabliltiy of that item
        $item->config->available = $item->config->available + 1;
        $item->config->save();

        // Alert the resident the item has been returned
        if ($item->resident->settings->item_return_alerts)
        {
          $email = new EmailTemplate('emails.item_return', $item->resident->id, ['subject' => 'You have returned an item',
                                                                           'item_name' => $item->config->item->name,
                                                                           'item_id' => $item->id]);
          $email->send();
        }

        Session::flash('success', 'Item returned!');

        if ($input['ref'] != "")
          // Return to the resident that the page was navigated to from
          return redirect('desk/residents/'.$input['ref']);

        return redirect('desk/items');
      }

      // No permissions
      throw new PermissionsException('desk/items');
    }

    // No permissions
    throw new PermissionsException('desk/items');
  }
}



/**
 *
 *
 * @param
 * @return
 */
