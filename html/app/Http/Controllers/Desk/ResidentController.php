<?php

namespace App\Http\Controllers\Desk;

use Request;
use Session;

use App\Http\Requests\NewEmailRequest;

use Auth;
use SocialAuth;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\DeskAPIController;
use App\PackageLog;
use App\Resident;
use App\Building;
use App\KeyLog;
use App\Key;
use Carbon\Carbon;
use App\Email;
use App\ItemLog;

use App\Exceptions\EmailException;
use App\Exceptions\PermissionsException;



/**
 * This class handles routes for desk/resident and stuff
 *
 */
class ResidentController extends Controller
{
  public function __construct()
  {
    $this->middleware('deskAuth');
    $this->middleware('deskResidentsAuth');
  }



  /**
   * Shows all the residents
   *
   * @return view
   */
  public function home()
  {
    return view('desk.residents_index');
  }


  /**
   * Shows the information for a single resident
   *
   * This function will make sure the resident exists
   *
   * @param string $id the ID of the resident to show details about
   * @throws App\Exceptions\PermissionsException if not correct permissions
   * @return view
   */
  public function show($id)
  {
    // Get resident info
    $resident = Resident::findOrError($id);

    // Make sure the user can view this resident
    if (Auth::user()->permissions->canView($resident))
    {
      // Get packages
      $newPackages = PackageLog::notPickedUp()->addressedTo($resident->id)->oldest()->get();
      $oldPackages = PackageLog::pickedUp()->addressedTo($resident->id)->newest()->limit(20)->get();

      // Get Keys
      $newKeys = KeyLog::notReturned()->loanedTo($resident->id)->oldest()->get();
      $oldKeys = KeyLog::returned()->loanedTo($resident->id)->newest()->get();

      // Get items
      $newItems = ItemLog::notReturned()->loanedTo($resident->id)->oldest()->limit(20)->get();

      // Return the view of the resident
      return view('desk.residents_view', ['resident' => $resident, 'newPackages' => $newPackages, 'oldPackages' => $oldPackages,
                                          'newKeys' => $newKeys, 'oldKeys' => $oldKeys, 'newItems' => $newItems]);
    }

    // Cannot view, redirect
    throw new PermissionsException('desk/residents');
  }


  /**
   * Will show the page to compose an email to that resident
   *
   * @param int $id the id of the resident to sent an email to
   * @throws App\Exceptions\PermissionsException if not correct permissions
   * @return the compose new email view
   */
  public function composeEmail($id)
  {
    // Get the resident info
    $resident = Resident::findOrError($id);

    // Check if the user can view this resident
    if (Auth::user()->permissions->canView($resident))
    {
      // Check if the user can compose an email
      if (Auth::user()->permissions->canComposeEmails())
      {
        // Permissions are good, continue
        $subject = Session::get('email_subject', '');
        $body = Session::get('email_body', '');

        // Return the view
        return view('desk.residents_email', ['resident' => $resident, 'subject' => $subject, 'body' => $body]);

      }

      // Permission was denied to send emails, redirect
      throw new PermissionsException('desk/residents/' . $id);
    }

    // Cannot view the resident, redirect
    throw new PermissionsException('desk/residents');
  }


  /**
   * Handles the POST data from the compose email page, and sends the email
   *
   * @uses Request\NewEmailRequest::class() for validating form input
   * @uses App\Email::class() for sending an email
   *
   * @param NewEmailRequest $request used to retrieve and validate form input
   * @param int $id the ID of the resident to send an email to
   * @throws App\Exceptions\PermissionsException if not correct permissions
   * @return will redirect back to compose email if an error, or to the resident page if successfull
   */
  public function sendEmail(NewEmailRequest $request, $id)
  {
    // make sure the resident is real
    $resident = Resident::findOrError($id);

    // Make sure the user can view this resident
    if (Auth::user()->permissions->canView($resident))
    {
      // Make sure the resident can send an email
      if (Auth::user()->permissions->canComposeEmails())
      {
        // Permissions are good, continue
        $input = $request->all();

        if ($input['resident_id'] == $id)
        {
          // Try and send the email
          try
          {
            // Create the email object with all the details
            $email = new Email(['recipient' => $resident->email, 'subject' => $input['subject'], 'body' => $input['body']], $resident);

            // Attempt to send
            if ($email->send())
            {
              // Email has been sent successfully
              Session::flash('success', 'The email has been sent');
            }
            else
            {
              // Unknown error, probably shouldn't happen with all the exception throwing goin on in the Email class
              Session::flash('info', 'Whoops, something went wrong, please try again.');

              // Temp. flash variables with email and body content
              Session::flash('email_subject', $input['subject']);
              Session::flash('email_body', $input['body']);

              // Redirect back to compose email
              return redirect()->action('Desk\ResidentController@composeEmail', [$resident]);
            }

          }
          catch (EmailException $e)
          {
            // Return to the email form if the exception is that something was blank
            if ($e->getCode() <= 10)
            {
              // Empty-field exception
              Session::flash('error_dismisses', $e->getMessage());

              // Temp. flash variables with email and body content
              Session::flash('email_subject', $input['subject']);
              Session::flash('email_body', $input['body']);

              // Go to the view with stuff
              return redirect()->action('Desk\ResidentController@composeEmail', [$resident]);
            }
            else
            {
              // Other exception
              Session::flash('error', $e->getMessage());
            }
          } // end catch
        }
        else
        {
          // ID from the form does not match the ID in the URL... Uh oh, gotcha cheaters!
          Session::flash('info', 'Whoops, something went wrong. Please try again.');
        }

        return redirect('desk/residents/'. $resident->id);

      } // End email permission check

      // Permission denied to send emails
      throw new PermissionsException('desk/residents/'.$id);
    }

    // Permisson denied to view the resident
    throw new PermissionsException('desk/residents');

  } // End send email






} // End class


/**
 *
 *
 * @param
 * @return
 */
