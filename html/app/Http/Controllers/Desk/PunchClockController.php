<?php

namespace App\Http\Controllers\Desk;

use Illuminate\Http\Request;

use Auth;
use SocialAuth;

use App\Http\Requests;
use App\Http\Controllers\Controller;


use App\PunchClock;

use Carbon\Carbon;


use App\Exceptions\PermissionsException;



 /**
  * Controller for the punch clock
  */
class PunchClockController extends Controller
{
  public function __construct()
  {
    $this->middleware('deskAuth');
    $this->middleware('deskPunchClockAuth');
  }



  /**
   * Punchclock home page
   *
   * @return view
   */
  public function home()
  {
    $entries = PunchClock::myEntries()->take(20)->get();
    return view('desk/punchclock_home', ['entries' => $entries]);
  }


  /**
   * Form to create an entry in the punch clock
   *
   * @return view
   */
  public function createEntry()
  {
    return view('desk.punchclock_new');
  }


  /**
   * Puts a punch clock entry into the database
   *
   * @param App\Request $request
   * @throws App\Exceptions\PermissionsException if not correct permissions
   * @return redirect
   */
  public function postCreateEntry(Request $request)
  {
    $input = $request->all();

    $details = [];
    $details['worker_id'] = Auth::user()->worker_id;
    $details['start_date'] = $input['start_date'];
    $details['start_time'] = $input['start_time'];
    $details['end_date'] = $input['end_date'];
    $details['end_time'] = $input['end_time'];
    $details['building_id'] = Auth::user()->worker->building_id;

    PunchClock::punch($details);

    return redirect('desk/punchclock');
  }


  /**
   * Gets the view of a specific punch clock entry
   *
   * @param int $id - The ID of the punch clock entry
   * @throws App\PermissionsException
   * @return views
   */
  public function viewEntry($id)
  {
    $entry = PunchClock::findOrError($id);

    // Check if user can view the package
    if (Auth::user()->permissions->canView($entry))
    {
      // Has Required Permissions
      return view('desk.punchclock_view', ['entry' => $entry]);
    }

    // No permissions
    throw new PermissionsException('desk/punchclock');
  }

}




/**
 *
 *
 * @param
 * @return
 */
