<?php

namespace App\Http\Controllers\Desk;

use Request;

use Auth;
use SocialAuth;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\PackageLog;
use App\Resident;
use App\Building;
use App\MessageLog;

use Carbon\Carbon;



 /**
  * This controller is for the desk stuff
  *
  * It only really does the desk home page...
  *
  */
class DeskController extends Controller
{
  public function __construct()
  {
    $this->middleware('deskAuth', ['except' => 'deskLogin']);
  }


  /**
   * Goes the the desk home page
   *
   * @return view of desk/home.blade.php
   */
  public function home()
  {

    $new_messages = MessageLog::getUnviewedMessages();

    return view('desk.home', ['new_msgs' => $new_messages]);
  }



  public function deskLogin()
  {
    return view('desk.login');
  }


}




/**
 *
 *
 * @param
 * @return
 */
