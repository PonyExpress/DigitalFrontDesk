<?php

namespace App\Http\Controllers\Desk;

use Request;
use Session;

use Auth;
use SocialAuth;

use App\Http\Requests;
use App\Http\Requests\NewPackageRequest;

use App\Http\Controllers\Controller;
use App\Http\Controllers\API\DeskAPIController;

use App\PackageLog;
use App\Resident;
use App\Building;
use App\EmailBlast;
use App\EmailTemplate;
use App\Exceptions\PermissionsException;

use Carbon\Carbon;


/**
 * This class handles all routes and functions related to packages on the desk portal, such as creating packages,
 * or signing a package out to a resident.
 *
 */
class PackageLogController extends Controller
{
  public function __construct()
  {
    $this->middleware('deskAuth');
    $this->middleware('deskPackageLogAuth');
  }


  /**
   * Goes to the index page for packages, here all packages that have not been picked up are shown
   *
   * @return the view of unpicked up packages
   */
  public function home()
  {
    // Get unpicked up packages, sorted by the oldest first
    $packages = PackageLog::notPickedUp()->inBuilding()->oldest()->get();
    $blasts = EmailBlast::inBuilding()->get();

    // Return the view with the packages as a variable
    return view('desk.packages_index', ['packages' => $packages, 'blasts' => sizeof($blasts)]);
  }


  /**
   * Shows a single entry in the package log
   *
   * This will check if a package with the id of $id exists, if it does
   * it will return the view showing package log details about that package
   *
   * @param int $id the ID of the packagelog entry to show
   * @throws App\Exceptions\PermissionsException if not correct permissions
   * @return
   */
  public function show($id)
  {
    // Get the package info
    $package = PackageLog::findOrError($id);

    // Check if user can view the package
    if (Auth::user()->permissions->canView($package))
    {
      // Has Required Permissions
      return view('desk.packages_view', ['package' => $package]);
    }

    // No permissions
    throw new PermissionsException('desk/packages');
  }


  /**
   * This will show the new package page
   *
   * @throws App\Exceptions\PermissionsException if not correct permissions
   * @return the new package view
   */
  public function createPackage()
  {
    // Check the User's permissions
    if (Auth::user()->permissions->canCreatePackages())
    {
      // Has appropriate permissions
      $maxID = PackageLog::maxID()->get();
      $id = 0;

      if (isset($maxID[0]))
        $id = $maxID[0]['id'];

      return view('desk.packages_new', ['maxID' => $id + 1]);
    }

    // No permissions
    throw new PermissionsException('desk/packages');
  }


  /**
   * This handles POST data coming from the new package page
   *
   * This returns JSON data because only an Ajax request should be making it
   *
   * @uses Request\NewPackageRequest for verifying form input
   * @uses Carbon\Carbon for getting the current time stamp
   *
   * @param NewPackageRequest $request verifies and retrieves user input
   * @return json data for the Ajax call
   */
  public function postCreatePackage(NewPackageRequest $request)
  {
    // Check the User's permissions
    if (Auth::user()->permissions->canCreatePackages())
    {
      $input = $request->all();

      // Make sure the resident is real
      $resident = Resident::find($input['resident_id']); // findOrFail will prevent people from using inspect element to create packages for
                                                         // residents that do not exist :)
      // Do nothing if the resident was not found
      if(is_null($resident))
        return;

      // JSON return data
      $json = [];
      $json['resident'] = $resident->id;
      $json['building_id'] = $resident->building_id;

      // Create a new package log
      $package = PackageLog::createEntry($resident, $input['package_type']);

      if (!is_null($package))
      {
        // Add the resident to the email blast queue if they want new package notifications
        if ($resident->settings->package_alerts)
          EmailBlast::enqueue($resident);

        // Success flash message
        // This runs when another_package is false because the AJAX will give it's own success message
        if ($input['another_package'] != 'true')
        {
          Session::flash('success', 'Package succesfully logged! &#x1F60E;');

          // Make sure the page gave the right package id
          if(($package->id % 500) != $input['package_number'])
          {
            Session::flash('info', 'Sorry about that!<br/>It looks like the last package ID was changed to <strong>'.($package->id % 500) .
                                    '</strong>, not '.$input['package_number'].'.');
          }
        }

        // If the user cannot create a package, this will cause an error so it will show the Ajax generated error message
        $json['packageID'] = $package->id;
        $json['package_number'] = $package->number;

        return json_encode($json);
      }

      // Something broke and the package could not be created
      return;
    }

    // No permission to sign out a package
    return;
  }


  /**
   * This handles POST data coming from the package view page
   *
   * This is called when a worker submits the form to sign out a package
   *
   * @uses Carbon\Carbon to get the current timestamp
   *
   * @param int $id the ID of the package to sign out
   * @throws App\Exceptions\PermissionsException if not correct permissions
   * @return redirects to reference page or the package log entry page
   */
  public function signOutPackage($id)
  {
    $package = PackageLog::findOrError($id);

    // Make sure the user has access to the package first
    if (Auth::user()->permissions->canView($package))
    {
      // Check if the user can sign out packages
      if (Auth::user()->permissions->canSignOutPackages())
      {
        // Has Permissions
        $input = Request::all();

        // Sign out the package
        $package->signOut($input);

        Session::flash('success', 'Package signed out!');


        // Send the resident a package signout confirmation email/text
        if ($package->resident->settings->package_checkout_confirm)
        {
          $email = new EmailTemplate('emails.package_checkout', $package->resident->id, ['subject' => 'Your package has been picked up',
                                                                                         'package_id' => $package->id]);
          $email->send();
        }


        if ($input['ref'] != "")
          // Return to the resident that the page was navigated to from
          return redirect('desk/residents/'.$input['ref']);

      }
      else
      {
        // No signout permission
        throw new PermissionsException('desk/packages/' . $id);
      }

      // Return to the package page
      return redirect('desk/packages/'.$id);
    }

    // No access to the package
    throw new PermissionsException('desk/packages');
  }


  /**
   * This is used to send a notification email to the resident that they have a package
   *
   * @todo Check user settings database to see if that resident wants to receive packages, also have an alternate email option
   * @todo Convert to use the Email class instead of Mail::send() directly
   *
   * @return redirect to desk/packages
   */
  public function sendBlast()
  {
    if (!is_null(EmailBlast::all()))
    {
      EmailBlast::blast();

      Session::flash('success', 'Residents with packages that have arrived today have been notified.');
      return redirect('desk/packages');
    }
  }


  /**
   * Used to send a reminder email that a resident has a package waiting for them.
   *
   * @param int $id the id of package you want to send the notification to
   * @return package to the package id page
   */
  public function sendReminder($id)
  {
    $package = PackageLog::findOrError($id);

    // Make sure the package should be getting a reminder
    if ($package->needsReminder())
    {
      // Send the reminder
      $email = new EmailTemplate('emails.package_reminder', $package->resident_id);
      $email->send();

      $package->reminded_on = Carbon::now();
      $package->save();

      Session::flash('success', 'Package reminder has been sent.');
    }
    else
    {
      // Does not need reminder
      Session::flash('info', 'Please wait a few more days before sending a reminder.');
    }

    return redirect('desk/packages/'.$id);
  }
}








/**
 *
 *
 * @param
 * @return
 */
