<?php

namespace App\Http\Controllers\Desk;

use Request;
use Session;

use Auth;
use SocialAuth;

use App\Http\Requests;
use App\Http\Requests\NewKeyLogRequest;

use App\Http\Controllers\Controller;

use App\PackageLog;
use App\Resident;
use App\Building;
use App\KeyLog;
use App\Key;
use App\EmailTemplate;
use App\Utilities;
use App\Signatures;

use App\Exceptions\PermissionsException;

use Carbon\Carbon;

use Mail;


/**
 * Class for the key log routes and what not
 *
 */
class KeyLogController extends Controller
{
  public function __construct()
  {
    $this->middleware('deskAuth');
    $this->middleware('deskKeyLogAuth');
  }


  /**
   * Will go the page that shows keys that are checked out
   *
   * @return key index view (all checked out keys)
   */
  public function home()
  {
    // Get keys that have not been returned
    $keys = KeyLog::notReturned()->inBuilding()->get();

    return view('desk.keylog_index', ['keys' => $keys]);
  }


  /**
   * Used to return the view of a single entry in the keylog
   *
   * This function will return back to desk/keys if the keylog entry $id
   * is not found
   *
   * @param int $id the ID of the keylog entry to retrieve
   * @throws App\Exceptions\PermissionsException if not correct permissions
   * @return view showing the key log information
   */
  public function show($id)
  {
    $keylog = KeyLog::findOrError($id);

    // Make sure the user can view the key log entry
    if (Auth::user()->permissions->canView($keylog))
    {
      // Permission granted
      return view('desk.keylog_view', ['keylog' => $keylog]);
    }

    // Access denied, no permissions
    throw new PermissionsException('desk/keys');
  }


  /**
   * Returns the view to create a new keylog entry
   *
   * @throws App\Exceptions\PermissionsException if not correct permissions
   * @return the new keylog entry page
   */
  public function loanKey()
  {
    // Check if a user has permission to loan new keys
    if (Auth::user()->permissions->canLoanKeys())
    {
      // Permission allowed
      return view('desk.keylog_new');
    }

    // No permissions
    throw new PermissionsException('desk/keys');
  }



  /**
   * Handle post data after form submission on loanKey()
   *
   * This function handles the submission of the form that will loan a key to a student
   *
   * @uses Requests\NewKeyLogRequest::class() to validate form input
   *
   * @param NewKeyLogRequest $request used to validate the form input and get the form input
   * @throws App\Exceptions\PermissionsException if not correct permissions
   * @return redirectes to desk/keys
   */
  public function postLoanKey(NewKeyLogRequest $request)
  {
    // Check if a user has permission to loan new keys
    if (Auth::user()->permissions->canLoanKeys())
    {
      // JSON return data
      $json = [];

      // Get all the input from the form
      $input = $request->all();

      // Make sure the resident is real
      $resident = Resident::findOrError($input['resident_id']); // findOrFail is great
      $key = Key::findOrFail($input['key_id']); // find the key, or fail

      // Create a new key log (automagically puts it in the database)
      $keyentry = KeyLog::create(['resident_id' => $input['resident_id'], 'key_id' => $input['key_id'],  'time_out' => Carbon::now(),
                                  'worker_out' => Auth::user()->worker_id,
                                  'building_id' => Auth::user()->worker->building_id]);

      // Save the signature
      $sign = Signatures::create(['object' => 'App\KeyLog', 'object_id' => $keyentry->id, 'direction' => 'out', 'value' => $input['signatureValue']]);
      $keyentry->signature_out = $sign->id;
      $keyentry->save();

      
      // Notify the resident that was loaned the key
      if ($resident->settings->key_loan_alerts)
      {
        $email = new EmailTemplate('emails.key_loan', $resident->id, ['subject' => 'You have been loaned a key.',
                                                                      'key_label' => $key->label,
                                                                      'key_id' => $keyentry->id]);
        $email->send();
      }

      // Success flash message
      Session::flash('success', 'Key loan succesfully logged! &#x1F60E;');

    }
    else
    {
      // No permission
      throw new PermissionsException('desk/keys');
    }

    // No permission for the action
    return redirect('desk/keys');
  }



  /**
   * This Handles post data coming from the view() page, if the keylog entry has not been returned
   *
   * If the key is not returned, there will be a form on the view() key page to return a key
   * This function is called when that form is submitted, it gets called when the URL
   * http://r03mstbetter.managed.mst.edu/desk/keys/{id}/return is called with a POST submission
   *
   * @param int $id the ID of the keylog entry to mark as returned
   * @throws App\Exceptions\PermissionsException if not correct permissions
   * @return redirects to wherever.
   */
  public function returnKey($id)
  {
    $keylog = KeyLog::findOrError($id);

    // Check if the user can view the key
    if (Auth::user()->permissions->canView($keylog))
    {
      // Check the user permissions
      if (Auth::user()->permissions->canReturnKeys())
      {
        // Get input from the POST request
        $input = Request::all();

        // Return the key
        $keylog->returnKey($input);

        // Send a notification to the resident
        if ($keylog->resident->settings->key_return_alerts)
        {
          $email = new EmailTemplate('emails.key_return', $keylog->resident->id, ['subject' => 'You returned a key',
                                                                                  'key_label' => $keylog->key->label,
                                                                                  'key_id' => $keylog->id]);
          $email->send();
        }

        Session::flash('success', 'Key succesfully Returned!');

        // Return to where the user came from or back to the key log entry
        if ($input['ref'] != "")
          // Return to the resident that the page was navigated to from
          return redirect('desk/residents/'.$input['ref']);

      }
      else
      {
        // No permission to return keys
        throw new PermissionsException('desk/keys/' . $keylog->id);
      }

      return redirect('desk/keys/'.$keylog->id);
    }

    // No permission  to view the page
    throw new PermissionsException('desk/keys');
  }

}




/**
 *
 *
 * @param
 * @return
 */
