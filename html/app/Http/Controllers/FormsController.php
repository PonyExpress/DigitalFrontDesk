<?php

namespace App\Http\Controllers;

use App\Http\Requests;

use Auth;
use SocialAuth;
use Session;

use App\CellPhoneProvidersSuggestions;
use App\Feedback;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

 /**
  * Controller for forms
  *
  */
class FormsController extends Controller
{
  public function __construct()
  {
  }



  /**
   * Form Thank You
   *
   * @return view
   */
  public function thankyou()
  {
    if (Session::has('feedback_submitted', '') != '')
      return view('forms.thankyou');
    else
      return redirect('/');
  }


  /**
   * Form to request a new Cell Phone Service Provider be listed
   *
   * @return view
   */
  public function cellphonecarrier()
  {
    return view('forms.cellphoneprovider');
  }


  /**
   * Submits cell phone carrier request form
   *
   * @return view
   */
  public function postCellphonecarrier(Request $request)
  {
    $input = $request->all();

    CellPhoneProvidersSuggestions::create(['content' => $input['provider_name']]);

    // Setting the feedback_submitted allows the user to see the thank you page
    Session::flash('feedback_submitted', 'true');

    return redirect('forms/thankyou');
  }


  /**
   * Feedback form
   *
   * @return view
   */
  public function feedback()
  {
    return view('forms.feedback');
  }


  /**
   * Post Feedback Form
   *
   * @param Request $request
   * @return redirect
   */
  public function postFeedback(Request $request)
  {
    $input = $request->all();

    Feedback::create(['content' => $input['content']]);

    // Setting the feedback_submitted allows the user to see the thank you page
    Session::flash('feedback_submitted', 'true');

    return redirect('forms/thankyou');
  }
}






/**
 *
 *
 * @param
 * @return
 */
