<?php

namespace App\Http\Controllers\Student;

use Auth;

use Request;
use Session;

use App\Http\Requests;

use App\Http\Controllers\Controller;
use App\Http\Controllers\API\APIController;

use App\KeyLog;
use App\Key;
use App\Resident;
use App\Building;

use Carbon\Carbon;

use Mail;

/**
 * This is the key log controller for the Student Portal
 *
 * It needs it's own because it doesn't need as much functionality as the desk one
 *
 */
class KeyLogController extends Controller
{
  public function __construct() { $this->middleware('studentAuth'); }

  /**
   * Will show a single key of the student
   *
   * This will make sure that the package belongs to the currently signed in user
   * @param int $id The id of key log entry
   * @return the view
   */
  public function show($id)
  {
    // Get the package info
    $entry = KeyLog::findOrError($id, 'student');

    
    // Make sure the package belongs to the user
    if (!is_null(Auth::user()))
    {
      if ($entry->resident_id != Auth::user()->resident_id)
      {
        // Does not belong to resident, redirect back to student
        Session::flash('error_dismisses', 'Key Log entry not found.');
        return redirect('student');
      }
    }

    // Return the package view with the package information
    return view('student.keylog_view', ['keylog' => $entry]);
  }
}



/**
 *
 *
 * @param
 * @return
 */
