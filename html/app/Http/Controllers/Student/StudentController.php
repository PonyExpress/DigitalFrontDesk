<?php

namespace App\Http\Controllers\Student;

//use Request;
use Session;
use Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\PackageLog;
use App\Resident;
use App\Building;
use App\KeyLog;
use App\Key;
use App\ItemLog;
use App\Items;
use App\ItemsConfig;
use App\CellPhoneProviders;

use Carbon\Carbon;
use Mail;


/**
 * The main controller for the student portal
 *
 */
class StudentController extends Controller
{

  public function __construct()
  {
    $this->middleware('studentAuth');
  }


  /**
   * Shows the home page of the student
   *
   * The student home page is MUCH more different than the desk home page.
   * Here, the studnet will see what packages they have waiting to be picked up, and more.
   *
   * @return the view
   */
  public function home()
  {
    $self = Auth::user()->resident;
    $picture = Auth::user()->picture;

    $newPackages = PackageLog::notPickedUp()->addressedTo($self->id)->take(20)->get();
    $oldPackages = PackageLog::pickedUp()->addressedTo($self->id)->take(10)->get();

    $newKeys = KeyLog::notReturned()->loanedTo($self->id)->oldest()->take(20)->get();
    $oldKeys = KeyLog::returned()->loanedTo($self->id)->newest()->take(10)->get();

    $newItems = ItemLog::notReturned()->loanedTo($self->id)->oldest()->take(20)->get();
    $oldItems = ItemLog::returned()->loanedTo($self->id)->newest()->take(10)->get();


	  return view('student.home', ['self' => $self, 'picture' => $picture, 'newPackages' => $newPackages, 'oldPackages' => $oldPackages,
                                  'newKeys' => $newKeys, 'oldKeys' => $oldKeys, 'newItems' => $newItems, 'oldItems' => $oldItems]);

  }


  /**
   * Gets a users settings
   *
   * @return Settings page view
   */
  public function settings()
  {
    $self = Auth::user()->resident;

    // List of cell phone providers
    $providers = CellPhoneProviders::asArray();

    return view('student.settings', ['self' => $self, 'settings' => $self->settings, 'providers' => $providers]);
  }


  /**
   * Saves a users settings
   *
   * @param Request $request the request
   * @return redirect
   */
  public function postSettingsSave(Request $request)
  {
    $input = $request->all();

    // Get the users settings
    $settings = Auth::user()->resident->settings;

    // Create an array to hold the new settings
    $newSettings = [];
    $newSettings['package_alerts'] = $input['package_alerts'];
    $newSettings['package_confirm'] = $input['package_confirm'];
    $newSettings['item_loan'] = $input['item_loans'];
    $newSettings['item_return'] = $input['item_return'];;
    $newSettings['send_text'] = $input['send_text'];

    if ($newSettings['send_text'])
    {
      if (strlen($input['phone_number']) != 10 || !is_numeric($input['phone_number']))
      {
        Session::flash('error_dismisses', 'The phone number you entered was not valid');
        return redirect('student/settings');
      }
      $newSettings['phone_number'] = $input['phone_number'];
      $newSettings['phone_provider'] = $input['phone_provider'];
    }

    $newSettings['alert_email'] = $input['alert_email'];

    // Update settings
    if ($settings->updateSettings($newSettings))
      Session::flash('success', 'Your settings have been updated.');
    else
      Session::flash('error_dismisses', 'Your settings did not update.');


    // Redirect back to the student settings page
    return redirect('student/settings');

  }


  /**
   * Creates a new API key for a resident
   *
   * @return redirect to student/settings
   */

  public function newAPIKey()
  {
    $settings = Auth::user()->resident->settings;
    $settings->newAPIKey();

    // Redirect back to the student settings page
    return redirect('student/settings');
  }

}



/**
 *
 *
 * @param
 * @return
 */
