<?php

namespace App\Http\Controllers\Student;

use Auth;

use Request;
use Session;

use App\Http\Requests;

use App\Http\Controllers\Controller;
use App\Http\Controllers\API\APIController;

use App\Items;
use App\ItemsConfig;
use App\ItemLog;
use App\Resident;
use App\Building;

use Carbon\Carbon;

use Mail;

/**
 * This is the item log controller for the Student Portal
 *
 * It needs it's own because it doesn't need as much functionality as the desk one
 *
 */
class ItemLogController extends Controller
{
  public function __construct() { $this->middleware('studentAuth'); }

  /**
   * Will show a single loaned item of the student
   *
   * This will make sure that the item belongs to the currently signed in user
   * @param int $id The id of item log entry
   * @return the view
   */
  public function show($id)
  {
    // Get the package info
    $item = ItemLog::findOrError($id, 'student');

    // Make sure the package belongs to the user
    if (!is_null(Auth::user()))
    {
      if ($item->resident_id != Auth::user()->resident_id)
      {
        // Does not belong to resident, redirect back to student
        Session::flash('error_dismisses', 'Loaned Item not found.');
        return redirect('student');
      }
    }

    // Return the package view with the package information
    return view('student.items_view', ['entry' => $item]);
  }
}



/**
 *
 *
 * @param
 * @return
 */
