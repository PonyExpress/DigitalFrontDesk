<?php

namespace App\Http\Controllers;

use App\Http\Requests;

use Auth;
use SocialAuth;
use Session;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;



 /**
  * Controller for documentation and about page
  *
  */
class DocsController extends Controller
{

  /**
   * Shows the home page
   *
   * @return view
   */
  public function home()
  {
    return view('docs.home');
  }


  /**
   * Shows the about page
   *
   * @return view
   */
  public function about()
  {
    return view('docs.about');
  }


  /**
   * Shows the desk documentation
   *
   * @return view
   */
  public function desk()
  {
    return view('docs.desk');
  }


  /**
   * Shows the student docs
   *
   * @return view
   */
  public function student()
  {
    return view('docs.student');
  }


  /**
   * Shows the gametrax documentation
   *
   * @return view
   */
  public function gametrax()
  {
    return view('docs.gametrax');
  }


  /**
   * Shows the api documentation
   *
   * @return view
   */
  public function api()
  {
    return view('docs.api');
  }


  /**
   * Shows the api documentation
   *
   * @return view
   */
  public function api_v1()
  {
    return view('docs.api_v1');
  }


}



/**
 *
 *
 * @param
 * @return
 */
