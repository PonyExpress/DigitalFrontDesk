<?php

namespace App\Http\Controllers\API;

use Request;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\PackageLog;
use App\Resident;
use App\Building;
use App\Key;
use App\KeyLog;
use App\Items;
use App\ItemLog;
use App\ItemsConfig;




/**
 * This class is the controller for the Desk API
 *
 */
class SignatureAPI extends Controller
{

  /**
   * Will return the signature image
   *
   * This function is pretty hectic, it's main purpose is to return signature images from the disk
   * (Stored in /var/PonyExpress/signatures/{type}/{direction})
   * It gives them a friendly URL, like http://r03mstbetter.managed.mst.edu/api/img/signatures/packages/1/out.png
   *
   * If a file does not exist, it will see if there is a base64 string saved in the database and tries to create
   * a file if there is one in the database.
   *
   * @see PackagesController::signOutPackage()
   * @see KeyController::saveKey()
   * @see KeyController::returnKey()
   *
   * @param string $type The type (package, key, item, etc...)
   * @param string $id The id to get the signature for
   * @param string $direction What file to get (in.png for signature in or out.png for signature out)
   * @return nothing
   */
  public function getSignatureImage($type, $id, $direction)
  {
    $path = '/var/PonyExpress/signatures/'.$type.'/'.$id;
    $image = $path.'/'.$direction.'.png';

    $image64 = null;
    $base64 = false;

    $entry = null;

    // Make sure the thing is real
    if ($type == "packages")
      $entry = PackageLog::find($id);
    else if ($type == "keys")
      $entry = KeyLog::find($id);
    else if ($type == "items")
      $entry = ItemLog::find($id);

    if (is_null($entry))
    {
      return;
    }
    else
    {
      // Check if the file exists
      if (!file_exists($image))
      {
        // No file, check the database for a base64 string
        if ($type == "packages")
        {
          // Check for signature
          if (is_null($entry->signature))
            return; // No signature

          $base64 = true;
          $image64 = $entry->signature;
        }
        else if ($type == "keys")
        {
          // Search the key log
          if ($direction == "out")
          {
            // Check for out signature
            if (is_null($entry->signature_out))
              return; // No signature

            $base64 = true;
            $image64 = $entry->signature_out;
          }
          else
          {
            // Check for in signature
            if (is_null($entry->signature_in))
              return; // No signature

            $base64 = true;
            $image64 = $entry->signature_in;
          }
        }
      }
    }

    if ($base64)
    {
      // Try and create the file
      if(!file_exists($path))
      {
        if (mkdir($path, 0777, true))
        {
          file_put_contents($image, base64_decode($image64));
        }
      }
      else
      {
        file_put_contents($image, base64_decode($image64));
      }

      // Check one last time to see if the image exists
      if (!file_exists($image))
      {
        // Doesn't exist, just display base64 data
        //header('Content-Type: image/png');
        //header('Content-length: '.filesize(base64_decode($image64)));
        echo '<img src="data:image/png;base64,'.$image64.'"/>';
        exit;
      }
      // File was created! Yay!
    }

    // Display the image on the disk
    return view('api.image', ['image' => $image]);
   }
}



/**
 *
 *
 * @param
 * @return
 */
