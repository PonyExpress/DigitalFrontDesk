<?php

namespace App\Http\Controllers\API;

use Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\PackageLog;
use App\Resident;
use App\Building;
use App\Key;
use App\KeyLog;

/*
  ====================== READ ME ======================

  In the API Class below, none of the functions use functions of objects like
    $resident->building->name

  This is because if we were to do that, when printing out an entire list of residents,
  it would have to query the database for every resident just to get building information.

  It is much quicker, and smarter, to pull the buildings into an array, and just use $resident->building_id
  to access information about the building.

  And just like that, the function allResidents() only needs to query the database twice, not 4,000+ times.

  These functions are supposed to be QUICK! And you need to shed every millisecond you can from them.

  To test speeds, modify the speedTest() function to time whatever it is you want to.
  Use $start = microtime(true); and $stop = microtime(true);
  And then do ($stop - $start) to get the total amount of time taken.
*/





/**
 * This class will control the API for student stuff
 *
 * This might never be needed, I'm not sure yet.
 *
 */
class StudentAPIController extends Controller
{
  public function __construct()
  {
    $this->middleware('studentAPIAuth');
  }


  /**
   *
   *
   * @param
   * @return
   */
  public function home()
  {
    return json_encode(['code' => 'Ohh yea']);
  }

}
