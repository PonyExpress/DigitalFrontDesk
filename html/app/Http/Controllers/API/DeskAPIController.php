<?php

namespace App\Http\Controllers\API;

use Request;
use Session;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\PackageLog;
use App\Resident;
use App\Building;
use App\Key;
use App\KeyLog;
use App\Items;
use App\ItemLog;
use App\ItemsConfig;

/*
  ====================== READ ME ======================

  In the API Class below, none of the functions use functions of objects like
    $resident->building->name

  This is because if we were to do that, when printing out an entire list of residents,
  it would have to query the database for every resident just to get building information.

  It is much quicker, and smarter, to pull the buildings into an array, and just use $resident->building_id
  to access information about the building.

  And just like that, the function allResidents() only needs to query the database twice, not 4,000+ times.

  These functions are supposed to be QUICK! And you need to shed every millisecond you can from them.

  To test speeds, modify the speedTest() function to time whatever it is you want to.
  Use $start = microtime(true); and $stop = microtime(true);
  And then do ($stop - $start) to get the total amount of time taken.
*/





/**
 * This class is the controller for the Desk API
 *
 */
class DeskAPIController extends Controller
{
  public function __construct()
  {
    $this->middleware('deskAPIAuth',  ['except' => 'getSignatureImage']);
  }


  /**
   * Gets a list of residents in JSON format
   *
   * This will gather the entire list of residents and format them using json_encode
   *
   * @example http://r03mstbetter.managed.mst.edu/api/desk/residents/all
   * @param bool $encode (default: true) Will return the data using json_encode if it's set to true
   * @return nothing - the output is echo'd in the functino
   */
  static public function allResidents($encode = true)
  {
    // Gather all residents and buildings
    $residents = Resident::notRemoved()->notHidden()->inBuilding()->lastNameASC()->get();
    $buildings = Building::all()->toArray();

    // Output the JSON (not actually JSON, it just looks like JSON haha)
    echo '{';
    foreach ($residents as $resident)
    {
      echo '"'.$resident->id.'":{';

      echo '"id":"'.$resident->id.'",';
      echo '"name":{"first":"'.$resident->first_name.'","last":"'.$resident->last_name.'"},';
      echo '"address":{"building":"'.$resident->building_id.'","name":"'.$buildings[$resident->building_id - 1]['name'].'","address":"'.$buildings[$resident->building_id - 1]['address'].'"}';

      echo '},';
    }
    echo '}';

    return;
  }


  /**
   * Gets a list of residents in HTML format (for tables)
   *
   * This will gather a list of residents and format them ready for use in an HTML table on the
   * website, used for Ajax requests.
   *
   * @example resources/views/desk/packages_new.blade.php
   * @return nothing - the HTML is echo'd in the function
   */
  static public function allResidentsHTML()
  {
    $residents = Resident::notRemoved()->notHidden()->inBuilding()->lastNameASC()->get();
    $buildings = Building::all()->toArray();

    // Generate HTML
    foreach ($residents as $resident)
    {
      $html = '<tr class="clickableRow clickableResidentRow" resident="'.$resident->id.'">';
        $html .= '<td class="resident_search_id hidden">'.$resident->id.'</td>';
        $html .= '<td class="name resident_search_name">'.$resident->formalName.'</td>';
        $html .= '<td class="room resident_search_room">'.$resident->room_number.'<span style="display:none;width:0px;height:0px">'.substr($resident->room_number, 0, -1).'0</span></td>';
        $html .= '<td class="building resident_search_building" >'.$buildings[$resident->building_id- 1]['name'].'</td>';
      $html .= '</tr>';

      echo $html;
    }
    exit;

  }


  /**
   * Will return a list of keys in HTML format, used for Ajax calls
   *
   * This function is here, because on a final version there will be the same amount of keys
   * as there are residents that need to be dynamically loaded using Ajax.
   *
   * @example resources/views/desk/keys_new.blade.php
   * @return nothing - HTML is echo'd in the function
   */
   static public function allKeysHTML()
   {
     $keys = Key::forBuilding()->get();
     $buildings = Building::all()->toArray();

     $html = "";

     // Generate HTML
     foreach ($keys as $key)
     {
       $html = '<tr class="clickableRow clickableKeyRow" key="'.$key->id.'">';
        $html .= '<td class="hidden">'.$key->id.'</td>';
        $html .= '<td class="name key_search_label">'.$key->label.'</td>';
        $html .= '<td class="room key_search_room">'.$key->room.'<span style="display:none;width:0px;height:0px">'.substr($key->room, 0, -1).'0</span></td>';
        $html .= '<td class="building key_search_building">'.$buildings[$key->building_id - 1]['name'].'</td>';
      $html .= '</tr>';

      echo $html;
     }
     exit;

   }



  public function allItemsHTML()
  {
    $items = ItemsConfig::inBuilding()->available()->get();
    $buildings = Building::all()->toArray();

    $html = "";

    // Generate HTML
    foreach ($items as $item)
    {
      $html = '<tr class="clickableRow clickableItemRow" item="'.$item->item->uID.'">';
       $html .= '<td class="name item_search_name">'.$item->item->name.'</td>';
       $html .= '<td class="building item_search_building">'.$buildings[$item->building_id - 1]['name'].'</td>';
     $html .= '</tr>';
     echo $html;

    }
    exit;
    
  }

}



/**
 *
 *
 * @param
 * @return
 */
