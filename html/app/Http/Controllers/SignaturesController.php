<?php

namespace App\Http\Controllers;

use Request;

use Auth;
use SocialAuth;

use App\Signatures;

use App\Http\Controllers\Controller;



 /**
  * Controller for signatures
  *
  */
class SignaturesController extends Controller
{
  public function __construct()
  {
  }


  /**
   * Shows a signature
   *
   * @return View
   */
  public function show($type, $id, $dir, $mod = 'img')
  {
    switch (strtolower($type))
    {
      case 'packages':
      case 'package':
        $type = 'App\PackageLog';
        break;

      case 'items':
      case 'item':
        $type = 'App\ItemLog';
        break;

      case 'keys':
      case 'key':
        $type = 'App\KeyLog';
        break;

      case 'dolly':
      case 'dollies':
        $type = 'App\DollyLog';
        break;
    }

    $signature = Signatures::type($type)->id($id)->direction($dir)->first();


    if (is_null($signature))
    {
      echo "Signature does not exist.";
      exit;
    }
    else
    {
      switch ($mod)
      {
        case 'raw':
          echo $signature->value;
          break;

        case 'value':
        case 'val':
          echo $signature->image;
          break;

        default:
          header('Content-Type: image/png');
          echo base64_decode($signature->value);
          break;
      }

      exit;
    }
  }


}







/**
 *
 *
 * @param
 * @return
 */
