<?php

namespace App\Http\Controllers\Admin;

use Auth;
use SocialAuth;

use App\Http\Requests;
use App\Http\Controllers\Controller;


use App\Building;
use App\BuildingGroups;
use App\Utilities;

use App\Exceptions\RedirectException;
use App\Exceptions\PermissionsException;

use Carbon\Carbon;

use Illuminate\Http\Request;


 /**
  * This controller is for the desk stuff
  *
  * It only really does the desk home page...
  *
  */
class BuildingsController extends Controller
{
  public function __construct()
  {
    $this->middleware('adminAuth');
  }


  /**
   * Goes the the admin home page
   *
   * @return view of admin/home
   */
  public function home()
  {
    $buildings = Building::all();

    return view('admin.buildings_home', ['buildings' => $buildings]);
  }


  /**
   * Shows Details about a building
   *
   * @return view
   */
  public function show($id)
  {
    $building = Building::find($id);

    if (is_null($building))
      throw new RedirectException('Building does not exist.', 'admin/buildings');

    return view('admin.buildings_view', ['building' => $building]);
  }


  /**
   * Modifies a building
   *
   * @return view
   */
  public function modify($id)
  {
    $building = Building::find($id);

    if (is_null($building))
      throw new RedirectException('Building does not exist.', 'admin/buildings');

    $groups = BuildingGroups::all();

    return view('admin.buildings_modify', ['self' => Auth::user()->admin, 'building' => $building, 'groups' => $groups]);
  }


  /**
   * Submission of the modify building form
   *
   * @throws App\Exceptions\PermissionsException if not correct permissions
   * @throws App\Exception\RedirectException if invalid input
   *
   * @param App\Request $request
   * @return redirect to building's page
   */
  public function postModify(Request $request)
  {
    $input = $request->all();

    // Make sure the user has appropriate permissions
    if (!Auth::user()->admin->permissions->canCreateBuildings())
      throw new PermissionsException('admin/buildings');

    // Make sure all the correct items exist in the form input
    $fields = ['_token', 'building_uID', 'name', 'address', 'in_a_group', 'existing_group', 'group', 'new_group', 'has_packages', 'has_residents', 'has_keys', 'has_items', 'has_punchClock', 'has_messageLog'];
    if (!Utilities::verify_array($fields, $input))
      throw new RedirectException('Something went wrong, please try again.', 'admin/buildings');

    // Make sure the building for the uID exists
    $building = Building::find(intval($input['building_uID']));
    if (is_null($building))
      throw new RedirectException('That building does not exist, please try again.', 'admin/buildings');

    // Check to see if any required fields were somehow left empty
    if (Utilities::empty_str($input['name']))
      throw new RedirectException('Building name can not be empty.', 'admin/buildings/' . $building->uID . '/modify');

    if (Utilities::empty_str($input['address']))
      throw new RedirectException('Building address can not be empty.', 'admin/buildings/' . $building->uID . '/modify');

    // Prepare the new information
    $details = [];
    $details['name'] = $input['name'];
    $details['address'] = $input['address'];
    $details['has_packages'] = intval($input['has_packages']);
    $details['has_items'] = intval($input['has_items']);
    $details['has_residents'] = intval($input['has_residents']);
    $details['has_keys'] = intval($input['has_keys']);
    $details['has_punchClock'] = intval($input['has_punchClock']);
    $details['has_messageLog'] = intval($input['has_messageLog']);

    // Check if the building is apart of a group
    if (intval($input['in_a_group']) != 1)
    {
      $details['id'] = $building->uID * -1; // Not in a group, By making the group ID the negative value of the uID it ensures that this building will have a unique group ID
    }
    else
    {
      // Building is in a group, check if new group
      if (intval($input['existing_group']) != 1)
      {
        // Create the new group
        if (Utilities::empty_str($input['new_group']))
          throw new RedirectException('New Group Name can not be empty', 'admin/buildings/' . $building->uID . '/modify');

        // Check if a building group with that name already exists
        $groups = BuildingGroups::name($input['new_group'])->first();
        if(!is_null($groups))
          throw new RedirectException('A building group with that name already exists.', 'admin/buildings/' . $building->uID . '/modify');

        // Create the group
        $new_group = BuildingGroups::create(['name' => $input['new_group']]);
        $details['id'] = $new_group->id;
      }
      else
      {
        // Make sure the existing group is real
        $group = BuildingGroups::find(intval($input['group']));

        if (is_null($group))
          throw new RedirectException('That building group could not be found, please try again.', 'admin/buildings/' . $building->uID . '/modify');

        $details['id'] = $group->id;
      }
    }

    // Update the building
    $building->update($details);
    $building->save(); // I am pretty sure that update() also does a save() on the object, but oh well, just in-case




    return redirect('admin/buildings/' . $building->uID);
  }


  /**
   * Form to create a building
   *
   * @return view
   */
  public function create()
  {
    $groups = BuildingGroups::all();

    return view('admin.buildings_new', ['self' => Auth::user()->admin, 'groups' => $groups]);
  }


  /**
   * Create the new building
   *
   * @param App\Request $request
   * @return redirect
   */
  public function postCreate(Request $request)
  {
    $input = $request->all();

    // Make sure the user has appropriate permissions
    if (!Auth::user()->admin->permissions->canCreateBuildings())
      throw new PermissionsException('admin/buildings');

    // Make sure all the correct items exist in the form input
    $fields = ['_token', 'name', 'address', 'in_a_group', 'existing_group', 'group', 'new_group', 'has_packages', 'has_residents', 'has_keys', 'has_items', 'has_punchClock', 'has_messageLog'];
    if (!Utilities::verify_array($fields, $input))
      throw new RedirectException('Something went wrong, please try again.', 'admin/buildings');

    // Make sure the building for the uID exists
    $building = Building::name($input['name'])->first();
    if (!is_null($building))
      throw new RedirectException('A building with that name already exists', 'admin/buildings/create');

    // Check to see if any required fields were somehow left empty
    if (Utilities::empty_str($input['name']))
      throw new RedirectException('Building name can not be empty.', 'admin/buildings/create');

    if (Utilities::empty_str($input['address']))
      throw new RedirectException('Building address can not be empty.', 'admin/buildings/create');

    // Prepare the new information
    $details = [];
    $details['name'] = $input['name'];
    $details['address'] = $input['address'];
    $details['has_packages'] = intval($input['has_packages']);
    $details['has_items'] = intval($input['has_items']);
    $details['has_residents'] = intval($input['has_residents']);
    $details['has_keys'] = intval($input['has_keys']);
    $details['has_punchClock'] = intval($input['has_punchClock']);
    $details['has_messageLog'] = intval($input['has_messageLog']);

    $building = Building::create($details);


    // Check if the building is apart of a group
    if (intval($input['in_a_group']) != 1)
    {
      $building->id = $building->uID * -1; // Not in a group, By making the group ID the negative value of the uID it ensures that this building will have a unique group ID
    }
    else
    {
      // Building is in a group, check if new group
      if (intval($input['existing_group']) != 1)
      {
        // Create the new group
        if (Utilities::empty_str($input['new_group']))
          throw new RedirectException('New Group Name can not be empty', 'admin/buildings/' . $building->uID . '/modify');

        // Check if a building group with that name already exists
        $groups = BuildingGroups::name($input['new_group'])->first();
        if(!is_null($groups))
          throw new RedirectException('A building group with that name already exists.', 'admin/buildings/' . $building->uID . '/modify');

        // Create the group
        $new_group = BuildingGroups::create(['name' => $input['new_group']]);

        $building->id = $new_group->id;
      }
      else
      {
        // Make sure the existing group is real
        $group = BuildingGroups::find(intval($input['group']));

        if (is_null($group))
          throw new RedirectException('That building group could not be found, please try again.', 'admin/buildings/' . $building->uID . '/modify');

        $building->id = $group->id;
      }
    }

    // Save the building
    $building->save(); // I am pretty sure that update() also does a save() on the object, but oh well, just in-case


    return redirect('admin/buildings/' . $building->uID);

  }


  /**
   * Shows a building group
   *
   * @return view
   */
  public function showGroup($id)
  {
    $building = BuildingGroups::find($id);

    if (is_null($building))
      throw new RedirectException('Building Group does not exist.', 'admin/buildings');

    return view('admin.buildings_view', ['building' => $building]);
  }

}




/**
 *
 *
 * @param
 * @return
 */
