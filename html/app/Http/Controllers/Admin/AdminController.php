<?php

namespace App\Http\Controllers\Admin;

use Request;

use Auth;
use SocialAuth;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\PackageLog;
use App\Resident;
use App\Building;
use App\MessageLog;

use Carbon\Carbon;



 /**
  * This controller is for the desk stuff
  *
  * It only really does the desk home page...
  *
  */
class AdminController extends Controller
{
  public function __construct()
  {
    $this->middleware('adminAuth');
  }


  /**
   * Goes the the admin home page
   *
   * @return view of admin/home
   */
  public function home()
  {
    
    return view('admin.home');
  }

}




/**
 *
 *
 * @param
 * @return
 */
