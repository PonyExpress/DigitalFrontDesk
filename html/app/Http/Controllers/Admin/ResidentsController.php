<?php

namespace App\Http\Controllers\Admin;

use Session;
use Auth;
use SocialAuth;

use App\Http\Requests;

use App\Http\Controllers\Controller;

use App\PackageLog;
use App\Resident;
use App\Building;
use App\KeyLog;
use App\Key;
use App\Email;
use App\Utilities;
use App\DeskWorker;
use App\Administrators;
use App\ItemLog;
use App\User;

use Carbon\Carbon;

use App\Exceptions\RedirectException;
use App\Exceptions\PermissionsException;

use Illuminate\Http\Request;





 /**
  * This controller is for the desk stuff
  *
  * It only really does the desk home page...
  *
  */
class ResidentsController extends Controller
{
  public function __construct()
  {
    $this->middleware('adminAuth');
  }


  /**
   * Goes the the admin home page
   *
   * @return view of admin/home
   */
  public function home()
  {
    return view('admin.residents_home');
  }



  /**
   * Shows the information for a single resident
   *
   * This function will make sure the resident exists
   *
   * @param string $id the ID of the resident to show details about
   * @return view
   */
  public function show($id)
  {
    // Get resident info
    $resident = Resident::findOrError($id);

    // Get packages
    $newPackages = PackageLog::notPickedUp()->addressedTo($resident->id)->oldest()->get();
    $oldPackages = PackageLog::pickedUp()->addressedTo($resident->id)->newest()->limit(20)->get();

    // Get Keys
    $newKeys = KeyLog::notReturned()->loanedTo($resident->id)->oldest()->get();
    $oldKeys = KeyLog::returned()->loanedTo($resident->id)->newest()->get();

    // Get items
    $newItems = ItemLog::notReturned()->loanedTo($resident->id)->oldest()->limit(20)->get();

    // Return the view of the resident
    return view('admin.residents_view', ['resident' => $resident, 'newPackages' => $newPackages, 'oldPackages' => $oldPackages,
                                        'newKeys' => $newKeys, 'oldKeys' => $oldKeys, 'newItems' => $newItems]);
  }


  /**
   * Loads form to create residents
   *
   * @throws App\Exceptions\PermissionException
   * @return view
   */
  public function create()
  {
    // Redirect if the admin does not have permissions
    if (!Auth::user()->admin->permissions->canCreateResidents())
      throw new PermissionException('admin/residents');

    $buildings = Building::all();

    // Load the view to create a new resident
    return view('admin.residents_new', ['self' => Auth::user()->admin, 'buildings' => $buildings]);
  }


  /**
   * Submission of the new resident form
   *
   * @throws App\Exceptions\PermissionsException if not correct permissions
   * @throws App\Exception\RedirectException if invalid input
   *
   * @param App\Request $request
   * @return redirect to resident page
   */
  public function postCreate(Request $request)
  {
    $input = $request->all();

    // Redirect if the admin doesn't have valid permissions
    if (!Auth::user()->admin->permissions->canCreateResidents())
      throw new PermissionsException('admin/residents');


    // Make sure all the correct items exist, so the form HTML cannot be tampered with
    $required_keys = ['_token', 'resident_id', 'first_name', 'middle_name', 'last_name', 'room_number', 'building', 'email', 'make_worker', 'new_packages', 'signout_packages', 'loan_keys',
                      'return_keys', 'loan_items', 'return_items', 'message_log', 'punch_clock', 'send_emails', 'make_admin', 'prog_settings', 'create_residents', 'create_workers',
                      'create_admins'];

    if (!Utilities::verify_array($required_keys, $input))
      throw new RedirectException('Something went wrong, please try again.', 'admin/residents/create');


    // Keep the input for page refresh, so we can display the input again if an error needs to be shown
    foreach($input as $key => $value)
      Session::flash('input_' . $key, $value);


    // Make sure required input is not empty
    if (Utilities::empty_str($input['first_name']))
      throw new RedirectException('Invalid First Name', 'admin/residents/create');

    if (Utilities::empty_str($input['last_name']))
      throw new RedirectException('Invalid Last Name', 'admin/residents/create');

    if (Utilities::empty_str($input['email']))
      throw new RedirectException('Invalid Email', 'admin/residents/create');


    // Check if the student ID is already in use
    $_resident = Resident::find(intval($input['resident_id']));
    if (!is_null($_resident))
      throw new RedirectException('A resident with that ID already exists, click <a href="' . \URL::to('admin/residents/' . $_resident->id) .'" target="_blank">here</a> to view them.', 'admin/residents/create');

    // Check if a resident with that email was found
    $_resident = Resident::email($input['email'] . '@mst.edu')->first();
    if (!is_null($_resident))
      throw new RedirectException('A resident with that email already exists, click <a href="' . \URL::to('admin/residents/' . $_resident->id) .'" target="_blank">here</a> to view them.', 'admin/residents/create');

    // Make sure the building exists
    $_building = Building::find($input['building']);
    if(is_null($_building))
      throw new RedirectException('That building could not be found.', 'admin/residents/create');


    // Create the Resident
    $details = [];
    $details['id'] = intval($input['resident_id']);
    $details['first_name'] = $input['first_name'];
    $details['middle_name'] = (Utilities::empty_str($input['middle_name'])) ? NULL : $input['middle_name'];
    $details['last_name'] = $input['last_name'];
    $details['room_number'] = $input['room_number'];
    $details['building_id'] = $input['building'];
    $details['email'] = $input['email'] . '@mst.edu';

    // Create the actual resident
    $new_resident = Resident::createResident($details);

    // Throw error if the resident was not created
    if (is_null($new_resident))
      throw new RedirectException('Something went wrong, the resident could not be created. Please try again.', 'admin/residents/create');

    // Make the user a desk worker if needed
    if (Auth::user()->admin->permissions->canCreateWorkers())
    {
      if (intval($input['make_worker']) == 1 || intval($input['make_admin']) == 1)
      {
        // Create a desk worker for the resident
        $details = [];
        $details['resident_id'] = $new_resident->id;
        $details['building_id'] = $new_resident->building_id;
        $details['admin_id'] = 0;
        $details['initials'] = $new_resident->initials;

        $permissions = [];
        $permissions['new_packages'] = intval($input['new_packages']);
        $permissions['signout_packages'] = intval($input['signout_packages']);
        $permissions['loan_keys'] = intval($input['loan_keys']);
        $permissions['return_keys'] = intval($input['return_keys']);
        $permissions['loan_items'] = intval($input['loan_items']);
        $permissions['return_items'] = intval($input['return_items']);
        $permissions['message_log'] = intval($input['message_log']);
        $permissions['punch_clock'] = intval($input['punch_clock']);
        $permissions['send_emails'] = intval($input['send_emails']);

        // Create the Desk Worker and the Desk Worker Permissions
        $worker = DeskWorker::createWorker($details, $permissions);
      }
    }


    // Make the user an admin if needed
    if (Auth::user()->admin->permissions->canCreateAdmins())
    {
      if (intval($input['make_admin']) == 1)
      {
        // Make the resident an admin
        $details = [];
        $details['resident_id'] = $new_resident->id;
        $details['worker_id'] = $worker->id;

        $permissions = [];
        $permissions['prog_settings'] = intval($input['prog_settings']);
        $permissions['create_residents'] = intval($input['create_residents']);
        $permissions['create_workers'] = intval($input['create_workers']);
        $permissions['create_admins'] = intval($input['create_admins']);

        // Make sure that the admin can create workers if they can create admins
        if($permissions['create_admins'] != 0)
          $permissions['create_workers'] == 1;

        // Create the admin and admin permissions
        $admin = Administrators::createAdmin($details, $permissions);

        // Update the worker with the admin id
        $worker->admin_id = $admin->id;
        $worker->save();
      }
    }

    // Redirect to the resident's page
    return redirect('admin/residents/' . $new_resident->id);
  }


  /**
   * Allows for the editing of resident information
   *
   * @param string $id the ID of the resident to show details about
   * @return view
   */
  public function modify($id)
  {
    // Get resident info
    $resident = Resident::findOrError($id);

    $buildings = Building::all();

    return view('admin.residents_modify', ['resident' => $resident, 'buildings' => $buildings, 'self' => Auth::user()->admin, 'permissions' => Auth::user()->worker->permissions]);
  }


  /**
   * Submission of the new resident form
   *
   * @throws App\Exceptions\PermissionsException if not correct permissions
   * @throws App\Exception\RedirectException if invalid input
   *
   * @param App\Request $request
   * @return redirect to resident page
   */
  public function postModify(Request $request)
  {
    $input = $request->all();

    // Redirect if the admin doesn't have valid permissions
    if (!Auth::user()->admin->permissions->canCreateResidents())
      throw new PermissionsException('admin/residents');


    // Make sure all the correct items exist, so the form HTML cannot be tampered with
    $required_keys = ['_token', 'resident_id', 'first_name', 'middle_name', 'last_name', 'room_number', 'building', 'email', 'make_worker', 'new_packages', 'signout_packages', 'loan_keys',
                      'return_keys', 'loan_items', 'return_items', 'message_log', 'punch_clock', 'send_emails', 'make_admin', 'prog_settings', 'create_residents', 'create_workers',
                      'create_admins'];

    if (!Utilities::verify_array($required_keys, $input))
      throw new RedirectException('Something went wrong, please try again.', 'admin/residents');


    // Keep the input for page refresh, so we can display the input again if an error needs to be shown
    foreach($input as $key => $value)
      Session::flash('input_' . $key, $value);


    // Make sure required input is not empty
    if (Utilities::empty_str($input['resident_id']))
      throw new RedirectException('Invalid Student ID', 'admin/residents');

    if (Utilities::empty_str($input['first_name']))
      throw new RedirectException('Invalid First Name', 'admin/residents/' . $input['resident_id'] . '/modify');

    if (Utilities::empty_str($input['last_name']))
      throw new RedirectException('Invalid Last Name', 'admin/residents/' . $input['resident_id'] . '/modify');

    if (Utilities::empty_str($input['email']))
      throw new RedirectException('Invalid Email', 'admin/residents/' . $input['resident_id'] . '/modify');


    // Check if the student ID is already in use
    $new_resident = Resident::find(intval($input['resident_id']));
    if (is_null($new_resident))
      throw new RedirectException('A resident with that ID does not exist, try creating the resident first', 'admin/residents/create');


    // Make sure the building exists
    $_building = Building::find($input['building']);
    if(is_null($_building))
      throw new RedirectException('That building could not be found.', 'admin/residents/' . $new_resident->id . '/modify');


    // Create the Resident
    $details = [];
    $old_id = $new_resident->id;

    $details['id'] = intval($input['resident_id']);
    $details['first_name'] = $input['first_name'];
    $details['middle_name'] = (Utilities::empty_str($input['middle_name'])) ? NULL : $input['middle_name'];
    $details['last_name'] = $input['last_name'];
    $details['room_number'] = $input['room_number'];
    $details['building_id'] = $input['building'];
    $details['email'] = $input['email'] . '@mst.edu';

    // Check if the student ID has changed
    if ($new_resident->id != intval($input['resident_id']))
    {
      // Need to change all copies of the old ID to the new ID in all of the databases...
    }

    // Create the actual resident
    $new_resident->update($details);
    $new_resident->save();

    // Make the user a desk worker if needed
    if (Auth::user()->admin->permissions->canCreateWorkers() && (Auth::user()->resident_id != $new_resident->id || Auth::user()->resident_id != $old_id))
    {
      if (intval($input['make_worker']) == 1 || intval($input['make_admin']) == 1)
      {
        $worker = DeskWorker::where('resident_id', '=', $new_resident->id)->first();

        // Create a desk worker for the resident
        $details = [];
        $details['resident_id'] = $new_resident->id;
        $details['building_id'] = $new_resident->building_id;
        $details['admin_id'] = 0;
        $details['initials'] = $new_resident->initials;

        $permissions = [];
        $permissions['new_packages'] = intval($input['new_packages']);
        $permissions['signout_packages'] = intval($input['signout_packages']);
        $permissions['loan_keys'] = intval($input['loan_keys']);
        $permissions['return_keys'] = intval($input['return_keys']);
        $permissions['loan_items'] = intval($input['loan_items']);
        $permissions['return_items'] = intval($input['return_items']);
        $permissions['message_log'] = intval($input['message_log']);
        $permissions['punch_clock'] = intval($input['punch_clock']);
        $permissions['send_emails'] = intval($input['send_emails']);


        // Create the Desk Worker and the Desk Worker Permissions
        if (is_null($worker))
          $worker = DeskWorker::createWorker($details, $permissions);
        else
        {
          $worker->update($details);
          $worker->save();
          $worker->permissions->update($permissions);
          $worker->permissions->save();
        }

        $user = User::where('resident_id', '=', $new_resident->id)->first();
        $user->worker_id = $worker->id;
        $user->save();

      }
    }


    // Make the user an admin if needed

    if (Auth::user()->admin->permissions->canCreateAdmins() && (Auth::user()->resident_id != $new_resident->id || Auth::user()->resident_id != $old_id))
    {
      if (intval($input['make_admin']) == 1)
      {
        $admin = Administrators::where('resident_id', '=', $new_resident->id)->first();

        // Make the resident an admin
        $details = [];
        $details['resident_id'] = $new_resident->id;
        $details['worker_id'] = $worker->id;

        $permissions = [];
        $permissions['prog_settings'] = intval($input['prog_settings']);
        $permissions['create_residents'] = intval($input['create_residents']);
        $permissions['create_workers'] = intval($input['create_workers']);
        $permissions['create_admins'] = intval($input['create_admins']);

        // Make sure that the admin can create workers if they can create admins
        if($permissions['create_admins'] != 0)
          $permissions['create_workers'] == 1;

        // Create the admin and admin permissions
        if (is_null($admin))
          $admin = Administrators::createAdmin($details, $permissions);
        else
        {
          $admin->update($details);
          $admin->save();
          $admin->permissions->update($permissions);
          $admin->permissions->save();
        }

        // Update the worker with the admin id
        $worker->admin_id = $admin->id;
        $worker->save();
      }
    }

    // Redirect to the resident's page
    return redirect('admin/residents/' . $new_resident->id);
  }


  /**
   * Removes a resident
   *
   * @param String $id - The id of the resident to remove
   * @return redirect
   */
  public function remove($id)
  {
    $resident = Resident::find($id);

    if(is_null($resident) && Auth::user()->resident_id != $resident->id)
    {
      Session::flash('error_dismisses', 'That resident was not found.');
    }
    else
    {
      $resident->remove();
      Session::flash('success', 'The resident was removed.');
    }

    return redirect('admin/residents');
  }

}




/**
 *
 *
 * @param
 * @return
 */
