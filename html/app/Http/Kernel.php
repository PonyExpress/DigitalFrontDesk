<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \App\Http\Middleware\EncryptCookies::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        \App\Http\Middleware\VerifyCsrfToken::class,
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'deskAPIAuth' => \App\Http\Middleware\API\DeskAPIAuthenticate::class,
        'studentAPIAuth' => \App\Http\MiddleWare\API\StudentAPIAuthenticate::class,

        'deskAuth' => \App\Http\Middleware\Desk\DeskAuthenticate::class,
        'deskPackageLogAuth' => \App\Http\Middleware\Desk\PackageLogAuthenticate::class,
        'deskKeyLogAuth' => \App\Http\Middleware\Desk\KeyLogAuthenticate::class,
        'deskResidentsAuth' => \App\Http\Middleware\Desk\ResidentsAuthenticate::class,
        'deskPunchClockAuth' => \App\Http\Middleware\Desk\PunchClockAuthenticate::class,

        'studentAuth' => \App\Http\Middleware\Student\StudentAuthenticate::class,



        'adminAuth' => \App\Http\Middleware\Admin\AdminAuthenticate::class,

        'auth' => \App\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
    ];
}
