<?php

namespace App\Http\Middleware\API;

use Session;

use Closure;
use Illuminate\Contracts\Auth\Guard;


use Auth;
use SocialAuth;



/**
 * Authentication middleware for the student API
 *
 */
class StudentAPIAuthenticate
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      // Return an error if there is not a user logged in
      if (is_null(Auth::user()))
      {
        // Not logged in, do not give access
        $result = [];
        $result['code'] = '202';
        $result['error'] = "Authentication failed, please log in.";

        return json_encode($result);
      }


      return $next($request);
    }
}





/**
 *
 *
 * @param
 * @return
 */
