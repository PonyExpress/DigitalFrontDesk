<?php

namespace App\Http\Middleware\API;

use Session;

use Closure;
use Illuminate\Contracts\Auth\Guard;


use Auth;
use SocialAuth;


/**
 * AUthentication middleware for the Desk API
 *
 */
class DeskAPIAuthenticate
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      // Check if a user is logged in
      if (is_null(Auth::user()))
      {
        // Not logged in, do not give access
        $result = [];
        $result['code'] = '202';
        $result['error'] = "Authentication failed, please log in.";

        return json_encode($result);
      }

      // Return an error if the user is not a desk worker
      if (is_null(Auth::user()->worker_id))
      {
        $result = [];
        $result['code'] = '403';
        $result['error'] = 'User account not permitted to access this API.';

        return json_encode($result);
      }

      return $next($request);
    }
}








/**
 *
 *
 * @param
 * @return
 */
