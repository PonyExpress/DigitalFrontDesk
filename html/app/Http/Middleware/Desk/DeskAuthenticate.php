<?php

namespace App\Http\Middleware\Desk;

use Session;

use Closure;
use Illuminate\Contracts\Auth\Guard;


use App\Exceptions\PermissionsException;

use Auth;
use SocialAuth;


/**
 * This is the middleware that will authenticate a user before shwoing anything on the desk worker portal
 *
 */
class DeskAuthenticate
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      // Redirect to the desk login if no one is logged in
      if ($this->auth->guest())
      {

        Session::set('login_redirect_uri', $request->path());
        return redirect('login/forward/desk');
      }

      // Return to the student page if not a desk worker
      if (!$request->user()->isDeskWorker())
      {
        throw new PermissionsException('student');
      }


      \Session::set('app_path', 'desk');

      //  var_dump($request->route()->parameters());
      if (empty(($request->route()->parameters())))
      {
        //echo "No parameters.<br/>";
      }

      //exit;
      return $next($request);
    }
}



/**
 *
 *
 * @param
 * @return
 */
