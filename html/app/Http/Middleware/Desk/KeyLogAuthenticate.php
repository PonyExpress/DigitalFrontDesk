<?php

namespace App\Http\Middleware\Desk;

use Session;

use Closure;
use Illuminate\Contracts\Auth\Guard;


use Auth;
use SocialAuth;


/**
 * This is the middleware that will authenticate a user before shwoing anything on the desk worker portal
 *
 */
class KeyLogAuthenticate
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      // Make sure the user's building has a key log
      if (!$request->user()->worker->building->hasKeys())
        return redirect('desk');

      
      //exit;
      return $next($request);
    }
}



/**
 *
 *
 * @param
 * @return
 */
