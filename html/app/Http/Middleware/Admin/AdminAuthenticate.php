<?php

namespace App\Http\Middleware\Admin;

use Session;

use Closure;
use Illuminate\Contracts\Auth\Guard;


use App\Exceptions\PermissionsException;

use Auth;
use SocialAuth;


/**
 * This is the middleware that will authenticate a user before shwoing anything on the desk worker portal
 *
 */
class AdminAuthenticate
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      // Redirect to the desk login if no one is logged in
      if ($this->auth->guest())
      {
        Session::set('login_redirect_uri', $request->path());
        return redirect('login/forward/admin');
      }


      // Return to the desk worker page if they are a desk worker, or the student page if not a desk worker, if they are not an admin
      if (!$request->user()->isAdmin())
      {
        if (!$request->user()->isDeskWorker())
          throw new PermissionsException('student'); // Return to the student page
        else
          throw new PermissionsException('desk'); // Return to the desk worker page
      }

      // Mark the apps location for redirects and navigation
      \Session::set('app_path', 'admin');

      //  var_dump($request->route()->parameters());
      if (empty(($request->route()->parameters())))
      {
        //echo "No parameters.<br/>";
      }

      //exit;
      return $next($request);
    }
}



/**
 *
 *
 * @param
 * @return
 */
