<?php

namespace App\Http\Middleware\Student;

use Session;

use Closure;
use Illuminate\Contracts\Auth\Guard;

use Auth;
use SocialAuth;


/**
 * This is the middleware that will authenticate the user before showing anything on the student portal
 *
 */
class StudentAuthenticate
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      // Go to the login page if there is not a user signed in
      if ($this->auth->guest())
      {
        Session::set('login_redirect_uri', $request->path()); // Remember the path they were trying for.
        return redirect('login/forward/student');
      }

      return $next($request);
    }
}
