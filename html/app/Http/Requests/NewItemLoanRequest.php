<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;


/**
 * Handles input for a new package request
 *
 */
class NewItemLoanRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
      // TODO: authenticate that the user is a desk worker
      return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'resident_id' => 'required',
           'item_id' => 'required',
        ];
    }
}



/**
 *
 *
 * @param
 * @return
 */
