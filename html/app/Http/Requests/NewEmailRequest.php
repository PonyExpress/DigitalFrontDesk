<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;


/**
 * Verifies input for a new email
 *
 */
class NewEmailRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
      // TODO: authenticate that the user is a desk worker
      return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }
}



/**
 *
 *
 * @param
 * @return
 */
