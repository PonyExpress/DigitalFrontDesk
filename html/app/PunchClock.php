<?php

namespace App;

use Auth;


use App\DeskWorker;
use App\Building;
use App\Resident;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


use App\Exceptions\RedirectException;


/**
 * Class to represent a PunchClock for the desk worker
 *
 *
 */
class PunchClock extends Model
{
  protected $primaryKey = 'id';
  protected $table = 'punch_clock';

  public $timestamps = false;

  protected $fillable = ['worker_id', 'start_date', 'end_date', 'building_id'];

    private $functions = ['startDate', 'endDate'];


  /* -------------------------------------------------
                    SCOPE FUNCTIONS
   ------------------------------------------------- */

  /**
   * Narrows a query to punch clock for a the user's building
   *
   * @param QueryBuilder $query
   * @return modified query
   */
  public function scopeForBuilding($query)
  {
    $building = Auth::user()->worker->building;

    // Add the direct building the user works for
    $query->where('building_id', '=', $building->uID);

    // Add any other sub-buildings (RC2, RC3)
    $buildings = Building::where('id', '=', $building->id)->get()->toArray();

    foreach($buildings as $theBuilding)
    {
      // Don't add the same building twice
      if ($theBuilding['uID'] != $building->uID)
        $query->orWhere('building_id', '=', $theBuilding['uID']);
    }

    return $query;
  }



  /**
   * Narrows a query to punch clock entries for logged in worker
   *
   * @param QueryBuilder $query
   * @return modified query
   */
  public function scopeMyEntries($query)
  {
    return $query->where('worker_id', '=', Auth::user()->worker_id);
  }

  /**
   * Narrows a query to a date range
   *
   * @param QueryBuilder $query
   * @return modified query
   */
  public function scopeDateRange($query)
  {
    return $query->where('worker_id', '=', Auth::user()->worker_id);
  }



  /* -------------------------------------------------
                  NON-SCOPE FUNCTIONS
   ------------------------------------------------- */

  /**
   * Creates an entry in the punch clock
   *
   * @param Array $details - the input
   * @throws App\Exceptions\RedirectException
   * @return boolean
   */
  public static function punch($details)
  {
    $months = ['January,' => '01', 'February,' => '02', 'March,' => '03', 'April,' => '04', 'May,' => '05', 'June,' => '06', 'July,' => '07', 'August,' => '08', 'September,' => '09',
                'October,' => '10', 'November,' => '11', 'December,' => '12'];

    // Format the Start Date and End Date
    $raw['start_date'] = explode(' ', $details['start_date'])[2] . '-' . $months[explode(' ', $details['start_date'])[1]] . '-' . explode(' ', $details['start_date'])[0] . ' ';
    if (explode(' ', $details['start_time'])[1] == 'PM')
      $raw['start_date'] .= ((int)explode(':', $details['start_time'])[0] % 12) + 12;
    else
      $raw['start_date'] .= ((int)explode(':', $details['start_time'])[0] % 12);
    $raw['start_date'] .= ':' . preg_replace('/ (AM|am|PM|pm)/', '', explode(':', $details['start_time'])[1]) . ':00';


    $raw['end_date'] = explode(' ', $details['end_date'])[2] . '-' . $months[explode(' ', $details['end_date'])[1]] . '-' . explode(' ', $details['end_date'])[0] . ' ';
    if (explode(' ', $details['end_time'])[1] == 'PM')
      $raw['end_date'] .= ((int)explode(':', $details['end_time'])[0] % 12) + 12;
    else
      $raw['end_date'] .= ((int)explode(':', $details['end_time'])[0] % 12);

    $raw['end_date'] .= ':' . preg_replace('/ (AM|am|PM|pm)/', '', explode(':', $details['end_time'])[1]) . ':00';


    $final = [];
    $final['worker_id'] = $details['worker_id'];
    $final['start_date'] = Carbon::createFromFormat('Y-m-d H:i:s', $raw['start_date']);
    $final['end_date'] = Carbon::createFromFormat('Y-m-d H:i:s', $raw['end_date']);
    $final['building_id'] = $details['building_id'];

    if ($final['start_date']->gte($final['end_date']))
      throw new RedirectException('You cannot work negative or zero hours!', 'desk/punchclock/create', 'error', true);

    // Insert into the database
    PunchClock::create($final);


    return true;
  }






  private function startDate()
  {
    return date("F d, Y, h:i A", strtotime($this->attributes['start_date']));
  }


  public function startDateTime()
  {
    return Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['start_date']);
  }

  private function endDate()
  {
    return date("F d, Y, h:i A", strtotime($this->attributes['end_date']));
  }

  public function endDateTime()
  {
    return Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['end_date']);
  }











  /**
   * Allows for $package->dateIn to actually call $package->dateIn()
   *
   * Allows other function calls to be made without (), does not work on scope functions or
   * functions with parameters
   *
   * @param string $key
   * @return the function, or calls parent __get
   */
  public function __get($key)
  {
    if (in_array($key, $this->functions))
      return $this->$key(); // Isn't PHP Awesome???

    // Go to the parent
    return parent::__get($key);
  }




  /**
   * Finds a package, returns an error if it failed
   *
   * @throws App\Exceptions\RedirectException
   * @param int $id The ID of the package to find
   * @param string $returnPath (Default: 'desk/packages') The path to return to if it fails
   * @return App\Package or redirect
   */
  public static function findOrError($id, $returnPath = 'desk/punchclock')
  {
    $entry = PunchClock::find($id);

    if (!is_null($entry))
      return $entry;

    // Generate the error
    $error = [];
    $error['type'] = 'error_dismisses';
    $error['msg'] = 'Punch Clock entry was not found.';

    throw new RedirectException('Punch Clock entry was not found.', $returnPath, 'error', true);
  }

}


/**
 *
 *
 * @param
 * @return
 */
