<?php

namespace App;

use Illuminate\Database\Eloquent\Model;



/**
 * Class to represent a Building
 *
 */
class BuildingGroups extends Model
{
  protected $primaryKey = 'id';
  protected $table = 'building_groups';

  public $timestamps = false;

  // Fillable stuff
  protected $fillable = ['name'];

  private $functions = ['name']; // List of functions to make accessible from the __get magic operator



  public function scopeName($query, $name)
  {
    return $query->where('name', '=', $name);
  }

  /* -------------------------------------------------
                   NON-SCOPE FUNCTIONS
     ------------------------------------------------- */
  private function name()
  {
    return $this->attributes['name'];
  }



  /**
   * Allows for $buidling->shortName to actually call $building->shortName()
   * And other function calls to be made without the parenthesis
   *
   * This will not work on scope functions though, or functions that have
   * parameters
   *
   * @param string $key the variable to lookup
   * @return yes
   */
  public function __get($key)
  {
    if (in_array($key, $this->functions))
      return $this->$key();

    // Try the parent __get function
    return parent::__get($key);
  }

}

/**
 *
 *
 * @param
 * @return
 */
