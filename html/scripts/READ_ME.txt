This folder is where all CSS and JS files need to GO!

When you do dfd-pull or dfd-pull-dev on the server, the minify script will run and automatically compress and move files from here into public/assets matching the folder structure in here.