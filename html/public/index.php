<?php

// Constants and Global Variables
define('DFLT', NULL);

function is_default($parameter)
{
  return (is_null($parameter));
}



/*
 *                            READ ME
 *--------------------------------------------------------------------
 *
 * It turns out that Laravel is pretty cool, I'm loving it.
 *
 * Here is a short explanation of things:
 *    This is the only page that is ever REALLY loaded, everything is handled by the
 *    routes.php file in /app/Routes/
 *
 *    So when someone loads up digitalfrontdesk.com/desk/packages  (digitalfrontdesk.com is not really our domain name)
 *    The routes file will be loaded, and it will use the path desk/packages (from the URL) to find out what to dom_import_simplexml
 *
 *    In the routes file, there is the line:
 *      Route::get('desk/packages', 'DeskController@packages');
 *      The file will match the path from the URL to this, since the first parameter is 'desk/packages' and it's a MATCH!
 *      After the match is found, it will load up the fuction called packages in the DeskController class.
 *      That function will return a view, specifically:
 *        view('desk.packages_index', compact('packages'))
 *        ^ Once that line is reached Laravel will go find the correct file (from /resources/views/) and get it, periods in the view path are actually
 *          saying that this is a folder. So desk.packages_index is actually the file: resources/views/desk/packages_index.blade.php
 *          The compact('packages') is saying that you want to pass the view the variable called $packages that exists inside the function you're calling the view from (more magic)
 *
 *     As for the .blade.php extension, there isn't anything special about it, it's just Laravels thing for making PHP easy.
 *
 *     In a view file you might see something like @extends('app') only at the very top! This tells laravel that this view needs the content from the app.blade.php file in the views folder
 *     The other stuff @section('x') say that the stuff between that line and @stop will be placed in the location in the app.blade.php file where @yield('x') is at.
 *
 * http://laravel.com/docs/5.1
 *
 * Laravel Videos:
 *    https://laracasts.com/series/laravel-from-scratch/
 *    https://laracasts.com/series/laravel-5-fundamentals
 *
 *
 * Programming Standards:
 *   - Just please keep comments nice and abundant, I'd rather have too many comments.
 *     Read the comment standard I use at: http://www.phpdoc.org/docs/latest/references/phpdoc/tags/index.html
 /*
  */

include_once('functions.php');



/**
 * Laravel - A PHP Framework For Web Artisans
 *
 * @package  Laravel
 * @author   Taylor Otwell <taylorotwell@gmail.com>
 */

/*
|--------------------------------------------------------------------------
| Register The Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader for
| our application. We just need to utilize it! We'll simply require it
| into the script here so that we don't have to worry about manual
| loading any of our classes later on. It feels nice to relax.
|
*/
date_default_timezone_set("America/Chicago");

 try {
require __DIR__.'/../bootstrap/autoload.php';

/*
|--------------------------------------------------------------------------
| Turn On The Lights
|--------------------------------------------------------------------------
|
| We need to illuminate PHP development, so let us turn on the lights.
| This bootstraps the framework and gets it ready for use, then it
| will load up this application so that we can run it and send
| the responses back to the browser and delight our users.
|
*/

$app = require_once __DIR__.'/../bootstrap/app.php';

/*
|--------------------------------------------------------------------------
| Run The Application
|--------------------------------------------------------------------------
|
| Once we have the application, we can handle the incoming request
| through the kernel, and send the associated response back to
| the client's browser allowing them to enjoy the creative
| and wonderful application we have prepared for them.
|
*/

$kernel = $app->make(Illuminate\Contracts\Http\Kernel::class);
$response = $kernel->handle(
    $request = Illuminate\Http\Request::capture()
);

$response->send();
$kernel->terminate($request, $response);


 }
// Exception handling
catch(Exception $e)
{
  abort(404);

}
