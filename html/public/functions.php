<?php

// Function to relocate on an error
function goHome()
{
  header("Location: /desk/packages");

  exit;
}

// Get a random greeting
function getGreeting()
{
  $greetings = ['Hello', 'Howdy', 'Hi', 'Greetings'];
  return $greetings[array_rand($greetings)];

}
